private _unit = "Exile_Unit_Player" createVehicleLocal [0,0,0];
_unit moveInAny ExileClientModelBoxVehicle;
_unit moveInDriver ExileClientModelBoxVehicle;
private _backpack = backpack player;
if (_backpack != "") then
{
	_unit addBackpack _backpack;
}
else
{
	removeBackpack _unit;
};
private _vest = vest player;
if (_vest != "") then
{
	_unit addVest _vest;
}
else
{
	removeVest _unit;
};

private _uniform = uniform player;
if (_uniform != "") then
{
	_unit forceAddUniform _uniform;
}
else
{
	removeUniform _unit;
};
private _goggles = goggles player;
if (_goggles != "") then
{
	_unit addGoggles _goggles;
}
else
{
	removeGoggles _unit;
};
private _headgear = headgear player;
if (_headgear != "") then
{
	_unit addHeadgear _headgear;
}
else
{
	removeHeadgear _unit;
};
true