	class CUPUnarmed
	{
		name = "CUP Vehicles Unarmed";
		icon = "a3\ui_f\data\gui\Rsc\RscDisplayArsenal\itemacc_ca.paa";
		items[] =
		{
			"CUP_O_Ural_RU",
			"CUP_O_Ural_Open_RU",
			"CUP_O_Ural_Empty_RU",
			"CUP_B_LR_Transport_CZ_W",
			"CUP_B_LR_Transport_GB_W",
			"CUP_O_UAZ_Unarmed_RU",
			"CUP_C_UAZ_Open_TK_CIV",
			"CUP_O_UAZ_Open_RU",
			"CUP_B_HMMWV_Unarmed_USA",
			"CUP_B_HMMWV_Unarmed_USMC",
			"CUP_B_HMMWV_Transport_USA",
			"CUP_I_BTR40_TKG",
			"CUP_C_Golf4_yellow_Civ",
			"CUP_C_Golf4_blue_Civ",
			"CUP_C_Golf4_white_Civ",
			"CUP_C_Golf4_green_Civ",
			"CUP_C_Golf4_whiteblood_Civ",
			"CUP_C_Golf4_camo_Civ",
			"CUP_C_Golf4_camodigital_Civ",
			"CUP_C_Golf4_camodark_Civ",
			"CUP_C_Golf4_reptile_Civ",
			"CUP_C_Golf4_kitty_Civ",
			"CUP_C_Golf4_crowe_Civ",
			"CUP_C_Skoda_White_CIV",
			"CUP_C_Skoda_Red_CIV",
			"CUP_C_Skoda_Blue_CIV",
			"CUP_C_Skoda_Green_CIV",
			"CUP_B_T810_Unarmed_CZ_WDL",
			"CUP_B_T810_Unarmed_CZ_DES",
			"CUP_C_S1203_CIV",
			"CUP_C_S1203_Ambulance_CIV",
			"CUP_B_S1203_Ambulance_CDF",
			"CUP_C_S1203_Militia_CIV",
			"CUP_B_M1151_USA",
			"CUP_B_M1151_NATO_T",
			"CUP_B_M1151_USMC",
			"CUP_B_HMMWV_Ambulance_ACR",
			"CUP_C_SUV_CIV",
			"CUP_I_FENNEK_ION",
			"CUP_I_MATV_ION",
			"CUP_I_Van_Transport_ION",
			"CUP_I_Van_Cargo_ION",
			"CUP_I_LSV_02_unarmed_ION"
		};
	};
	class CUPArmed
	{
		name = "CUP Vehicles Armed";
		icon = "a3\ui_f\data\gui\Rsc\RscDisplayArsenal\itemacc_ca.paa";
		items[] =
		{
			"CUP_O_LR_MG_TKM",
			"CUP_O_LR_MG_TKA",
			"CUP_I_Datsun_PK_Random",
			"CUP_O_Datsun_PK_Random",
			"CUP_O_UAZ_MG_RU",
			"CUP_I_UAZ_MG_UN",
			"CUP_O_UAZ_AGS30_RU",
			"CUP_BAF_Jackal2_L2A1_W",
			"CUP_BAF_Jackal2_GMG_W",
			"CUP_B_LR_Special_CZ_W",
			"CUP_B_LR_MG_CZ_W",
			"CUP_B_LR_MG_GB_W",
			"CUP_B_HMMWV_M1114_USMC",
			"CUP_B_HMMWV_M2_USMC",
			"CUP_B_HMMWV_Crows_M2_USA",
			"CUP_B_HMMWV_M2_GPK_USA",
			"CUP_B_HMMWV_M2_USA",
			"CUP_B_HMMWV_DSHKM_GPK_ACR",
			"CUP_B_HMMWV_AGS_GPK_ACR",
			"CUP_B_HMMWV_MK19_USMC",
			"CUP_B_HMMWV_Crows_MK19_USA",
			"CUP_B_HMMWV_SOV_USA",
			"CUP_O_BRDM2_HQ_SLA",
			"CUP_O_BRDM2_HQ_CHDKZ",
			"CUP_O_BRDM2_CHDKZ",
			"CUP_O_BRDM2_SLA",
			"CUP_B_Dingo_GER_Wdl",
			"CUP_B_Mastiff_HMG_GB_W",
			"CUP_B_Ridgback_HMG_GB_W",
			"CUP_B_LAV25_HQ_USMC",
			"CUP_O_GAZ_Vodnik_PK_RU",
			"CUP_B_BAF_Coyote_L2A1_W",
			"CUP_I_BTR60_UN",
			"CUP_B_Wolfhound_HMG_GB_D",
			"CUP_B_Wolfhound_HMG_GB_W",
			"CUP_O_BTR90_HQ_RU",
			"CUP_M1030",
			"CUP_B_T810_Armed_CZ_WDL",
			"CUP_B_T810_Armed_CZ_DES",
			"CUP_B_M113_USA",
			"CUP_RG31_M2",
			"CUP_RG31_M2_OD",
			"CUP_B_RG31_M2_USMC",
			"CUP_B_RG31_M2_OD_USMC",
			"CUP_I_FENNEK_HMG_ION",
			"CUP_I_MATV_HMG_ION",
			"CUP_I_LSV_02_Minigun_ION"
		};
	};
	class CUPChoppers
	{
		name = "CUP Helicopters";
		icon = "a3\ui_f\data\gui\Rsc\RscDisplayArsenal\itemacc_ca.paa";
		items[] =
		{
			"CUP_B_CH53E_USMC",
			"CUP_B_CH53E_GER",
			"CUP_B_CH53E_VIV_GER",
			"CUP_B_UH1Y_UNA_F",
			"CUP_B_UH1Y_MEV_F",
			"CUP_C_Mi17_Civilian_RU",			
			"CUP_B_Mi17_medevac_CDF",
			"CUP_B_Mi171Sh_Unarmed_ACR",
			"CUP_O_Mi8_medevac_CHDKZ",
			"CUP_O_Mi8_medevac_RU",			
			"CUP_B_UH60M_Unarmed_US",
			"CUP_B_UH60L_Unarmed_US",
			"CUP_B_UH60M_Unarmed_FFV_US",
			"CUP_B_UH60L_Unarmed_FFV_US",
			"CUP_B_UH60M_Unarmed_FFV_MEV_US",
			"CUP_B_UH60L_Unarmed_FFV_MEV_US",			
			"CUP_I_UH60L_Unarmed_RACS",
			"CUP_I_UH60L_Unarmed_FFV_Racs",
			"CUP_I_UH60L_Unarmed_FFV_MEV_Racs",
			"CUP_B_MI6A_CDF",			
			"CUP_B_MH6J_USA",
			"CUP_B_Merlin_HC3_GB",
			"CUP_B_SA330_Puma_HC1_BAF",
			"CUP_B_SA330_Puma_HC2_BAF"
		};
	};
	class CUPChoppersArmed
	{
		name = "CUP Helicopters";
		icon = "a3\ui_f\data\gui\Rsc\RscDisplayArsenal\itemacc_ca.paa";
		items[] =
		{
			"CUP_B_Mi17_CDF",
			"CUP_O_Mi17_TK",
			"CUP_B_MH60S_USMC",
			"CUP_B_MH60S_FFV_USMC",
			"CUP_B_AW159_Unarmed_BAF",
			"CUP_B_CH47F_USA",
			"CUP_B_CH47F_GB",
			"CUP_B_UH60M_US",
			"CUP_B_UH60L_US",
			"CUP_B_UH60M_FFV_US",
			"CUP_B_UH60L_FFV_US",
			"CUP_I_UH60L_RACS",
			"CUP_I_UH60L_FFV_RACS",
			"CUP_B_AH1Z_NoWeapons",
			"CUP_B_AH64D_NO_USA",			
			"CUP_B_AW159_Unarmed_RN_Blackcat",
			"CUP_B_AH6M_Cannons_USA",
			"CUP_B_UH1Y_Gunship_Dynamic_USMC"
		};
	};
	class CUPPlanes
	{
		name = "CUP Planes";
		icon = "a3\ui_f\data\gui\Rsc\RscDisplayArsenal\itemacc_ca.paa";
		items[] =
		{ 
		  "CUP_O_AN2_TK",
          "CUP_B_C130J_USMC",
          "CUP_B_C130J_Cargo_USMC",
          "CUP_B_C130J_GB",
          "CUP_B_C130J_Cargo_GB",
          "CUP_I_C130J_AAF",
          "CUP_I_C130J_Cargo_AAF",
          "CUP_I_C130J_RACS",
          "CUP_I_C130J_Cargo_RACS",
          "CUP_O_C130J_TKA",
          "CUP_O_C130J_Cargo_TKA",
          "CUP_B_MV22_USMC",
          "CUP_B_F35B_Stealth_USMC",
          "CUP_C_DC3_CIV",
          "CUP_C_DC3_TanoAir_CIV"
		};
	};
	class CUPBoats
	{
		name = "CUP Boats";
		icon = "a3\ui_f\data\gui\Rsc\RscDisplayArsenal\itemacc_ca.paa";
		items[] =
		{
			 "CUP_C_Fishing_Boat_Chernarus",
	         "CUP_B_RHIB2Turret_USMC",
	         "CUP_B_Zodiac_USMC"
		};
	};