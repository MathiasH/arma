-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Oct 23, 2018 at 10:11 PM
-- Server version: 5.7.17
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exile`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `uid` varchar(32) NOT NULL,
  `clan_id` int(11) UNSIGNED DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  `kills` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `deaths` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `locker` int(11) NOT NULL DEFAULT '0',
  `first_connect_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_connect_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_disconnect_at` datetime DEFAULT NULL,
  `total_connections` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `zedkills` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`uid`, `clan_id`, `name`, `score`, `kills`, `deaths`, `locker`, `first_connect_at`, `last_connect_at`, `last_disconnect_at`, `total_connections`, `zedkills`) VALUES
('76561197973996366', NULL, 'Barrazal', 2029, 71, 3, 2314, '2018-10-22 20:26:08', '2018-10-23 21:48:55', '2018-10-23 21:50:22', 6, 71),
('76561197988232530', NULL, 'Techno Viking', 1152, 66, 1, 0, '2018-10-22 20:16:44', '2018-10-22 22:54:53', '2018-10-22 23:53:15', 3, 66),
('76561197990557284', NULL, 'Phoenix', 8081, 122, 1, 23300, '2018-10-23 10:47:08', '2018-10-23 21:09:02', '2018-10-23 21:19:36', 5, 122),
('76561198001767153', NULL, 'Portion Zombiefutter', 1243, 46, 0, 835, '2018-10-22 20:20:57', '2018-10-23 21:25:48', '2018-10-23 21:32:12', 2, 46),
('76561198008624689', NULL, 'Der Türke', 0, 0, 0, 0, '2018-10-23 21:31:56', '2018-10-23 21:31:56', '2018-10-23 21:32:27', 1, 0),
('76561198026891445', NULL, 'Elchsocke', 1425, 81, 7, 3945, '2018-10-22 18:37:51', '2018-10-23 21:52:20', '2018-10-23 21:53:37', 11, 81),
('76561198086210494', NULL, 'DrSnuggles', 2746, 49, 0, 6652, '2018-10-23 13:50:24', '2018-10-23 13:55:44', '2018-10-23 16:01:21', 2, 49),
('76561198153523968', NULL, 'YoSwagi', 2237, 80, 2, 485, '2018-10-22 18:58:00', '2018-10-23 21:24:32', '2018-10-23 21:32:22', 11, 80),
('DMS_PersistentVehicle', NULL, 'DMS_PersistentVehicle', 0, 0, 0, 0, '2018-10-21 20:36:43', '2018-10-21 20:36:43', NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `clan`
--

CREATE TABLE `clan` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `leader_uid` varchar(32) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `clan_map_marker`
--

CREATE TABLE `clan_map_marker` (
  `id` int(11) UNSIGNED NOT NULL,
  `clan_id` int(11) UNSIGNED NOT NULL,
  `markerType` tinyint(4) NOT NULL DEFAULT '-1',
  `positionArr` text NOT NULL,
  `color` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `iconSize` float UNSIGNED NOT NULL,
  `label` varchar(255) NOT NULL,
  `labelSize` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `construction`
--

CREATE TABLE `construction` (
  `id` int(11) UNSIGNED NOT NULL,
  `class` varchar(64) DEFAULT NULL,
  `account_uid` varchar(32) DEFAULT NULL,
  `spawned_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `position_x` double NOT NULL DEFAULT '0',
  `position_y` double NOT NULL DEFAULT '0',
  `position_z` double NOT NULL DEFAULT '0',
  `direction_x` double NOT NULL DEFAULT '0',
  `direction_y` double NOT NULL DEFAULT '0',
  `direction_z` double NOT NULL DEFAULT '0',
  `up_x` double NOT NULL DEFAULT '0',
  `up_y` double NOT NULL DEFAULT '0',
  `up_z` double NOT NULL DEFAULT '0',
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `pin_code` varchar(6) NOT NULL DEFAULT '000000',
  `damage` tinyint(1) UNSIGNED DEFAULT '0',
  `territory_id` int(11) UNSIGNED DEFAULT NULL,
  `last_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `container`
--

CREATE TABLE `container` (
  `id` int(11) UNSIGNED NOT NULL,
  `class` varchar(64) DEFAULT NULL,
  `spawned_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `account_uid` varchar(32) DEFAULT NULL,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `position_x` double NOT NULL DEFAULT '0',
  `position_y` double NOT NULL DEFAULT '0',
  `position_z` double NOT NULL DEFAULT '0',
  `direction_x` double NOT NULL DEFAULT '0',
  `direction_y` double NOT NULL DEFAULT '0',
  `direction_z` double NOT NULL DEFAULT '0',
  `up_x` double NOT NULL DEFAULT '0',
  `up_y` double NOT NULL DEFAULT '0',
  `up_z` double NOT NULL DEFAULT '1',
  `cargo_items` text,
  `cargo_magazines` text,
  `cargo_weapons` text,
  `cargo_container` text,
  `last_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pin_code` varchar(6) NOT NULL DEFAULT '000000',
  `territory_id` int(11) UNSIGNED DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `money` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `abandoned` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `player`
--

CREATE TABLE `player` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `account_uid` varchar(32) NOT NULL,
  `money` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `damage` double UNSIGNED NOT NULL DEFAULT '0',
  `hunger` double UNSIGNED NOT NULL DEFAULT '100',
  `thirst` double UNSIGNED NOT NULL DEFAULT '100',
  `alcohol` double UNSIGNED NOT NULL DEFAULT '0',
  `temperature` double NOT NULL DEFAULT '37',
  `wetness` double UNSIGNED NOT NULL DEFAULT '0',
  `oxygen_remaining` double UNSIGNED NOT NULL DEFAULT '1',
  `bleeding_remaining` double UNSIGNED NOT NULL DEFAULT '0',
  `hitpoints` varchar(1024) NOT NULL DEFAULT '[]',
  `direction` double NOT NULL DEFAULT '0',
  `position_x` double NOT NULL DEFAULT '0',
  `position_y` double NOT NULL DEFAULT '0',
  `position_z` double NOT NULL DEFAULT '0',
  `spawned_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `assigned_items` text,
  `backpack` varchar(64) DEFAULT NULL,
  `backpack_items` text,
  `backpack_magazines` text,
  `backpack_weapons` text,
  `current_weapon` varchar(64) DEFAULT NULL,
  `goggles` varchar(64) DEFAULT NULL,
  `handgun_items` text,
  `handgun_weapon` varchar(64) DEFAULT NULL,
  `headgear` varchar(64) DEFAULT NULL,
  `binocular` varchar(64) DEFAULT NULL,
  `loaded_magazines` text,
  `primary_weapon` varchar(64) DEFAULT NULL,
  `primary_weapon_items` text,
  `secondary_weapon` varchar(64) DEFAULT NULL,
  `secondary_weapon_items` text,
  `uniform` varchar(64) DEFAULT NULL,
  `uniform_items` text,
  `uniform_magazines` text,
  `uniform_weapons` text,
  `vest` varchar(64) DEFAULT NULL,
  `vest_items` text,
  `vest_magazines` text,
  `vest_weapons` text,
  `last_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `player`
--

INSERT INTO `player` (`id`, `name`, `account_uid`, `money`, `damage`, `hunger`, `thirst`, `alcohol`, `temperature`, `wetness`, `oxygen_remaining`, `bleeding_remaining`, `hitpoints`, `direction`, `position_x`, `position_y`, `position_z`, `spawned_at`, `assigned_items`, `backpack`, `backpack_items`, `backpack_magazines`, `backpack_weapons`, `current_weapon`, `goggles`, `handgun_items`, `handgun_weapon`, `headgear`, `binocular`, `loaded_magazines`, `primary_weapon`, `primary_weapon_items`, `secondary_weapon`, `secondary_weapon_items`, `uniform`, `uniform_items`, `uniform_magazines`, `uniform_weapons`, `vest`, `vest_items`, `vest_magazines`, `vest_weapons`, `last_updated_at`) VALUES
(12, 'Portion Zombiefutter', '76561198001767153', 95, 0.0433071, 65.237885, 70.749718, 0, 37, 0.983103, 1, 0, '[[\"face_hub\",0.0433071],[\"neck\",0.0433071],[\"head\",0.0433071],[\"pelvis\",0.0433071],[\"spine1\",0.0433071],[\"spine2\",0.0433071],[\"spine3\",0.0433071],[\"body\",0.0433071],[\"arms\",0.0433071],[\"head_hit\",0.0433071],[\"hand_r\",0.0433071],[\"body\",0.0433071]]', 346.872894, 8127.019043, 10918.782227, 0.00116264, '2018-10-22 20:21:34', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\"]', 'B_Kitbag_sgg', '[[\"V_Press_F\",1],[\"V_Rangemaster_belt\",1],[\"V_TacVest_blk_POLICE\",1]]', '[[\"Exile_Item_Beer\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_DuctTape\",1],[\"Exile_Item_BeefParts\",1],[\"Exile_Item_BBQSandwich\",1],[\"9Rnd_45ACP_Mag\",9],[\"Exile_Item_CatFood\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_ToiletPaper\",1],[\"Exile_Item_ZipTie\",1],[\"Exile_Item_MetalScrews\",1],[\"Exile_Item_ToiletPaper\",1],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_CanOpener\",1]]', '[]', '', 'G_Sport_Blackred', '[\"\",\"\",\"\",\"\"]', 'Exile_Weapon_SA61', 'H_HelmetB', '', '[]', '', '[\"\",\"\",\"\",\"\"]', '', '[\"\",\"\",\"\",\"\"]', 'U_B_T_Sniper_F', '[]', '[[\"Exile_Item_DuctTape\",1],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_CanOpener\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_DuctTape\",1],[\"Exile_Item_ToiletPaper\",1]]', '[]', 'V_PlateCarrierSpec_mtp', '[]', '[[\"CUP_200Rnd_TE4_Green_Tracer_556x45_L110A1\",200]]', '[]', '2018-10-23 21:32:12'),
(14, 'Techno Viking', '76561197988232530', 3853, 0.106299, 81.521492, 55.139717, 0, 37, 0, 1, 0, '[[\"face_hub\",0.106299],[\"neck\",0.106299],[\"head\",0.106299],[\"pelvis\",0.106299],[\"spine1\",0.106299],[\"spine2\",0.106299],[\"spine3\",0.106299],[\"body\",0.106299],[\"arms\",0.106299],[\"hands\",0.106299],[\"legs\",0.106299],[\"body\",0.106299]]', 1.283527, 12191.484375, 8171.40918, 0.00145542, '2018-10-22 20:53:39', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\",\"ItemGPS\",\"Rangefinder\"]', 'B_Carryall_oli', '[]', '[[\"Exile_Item_CanOpener\",1],[\"Exile_Item_Vishpirin\",1],[\"Exile_Item_EnergyDrink\",1],[\"Exile_Item_Vishpirin\",1],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_InstaDoc\",1],[\"16Rnd_9x21_Mag\",16],[\"16Rnd_9x21_Mag\",16],[\"16Rnd_9x21_Mag\",16],[\"16Rnd_9x21_Mag\",16],[\"16Rnd_9x21_Mag\",16],[\"10Rnd_338_Mag\",10],[\"HandGrenade\",1]]', '[[\"arifle_SPAR_01_khk_F\",1]]', 'srifle_DMR_02_F', 'G_Shades_Blue', '[\"muzzle_snds_L\",\"\",\"\",\"\"]', 'hgun_P07_F', 'H_HelmetB', 'Rangefinder', '[[\"srifle_DMR_02_F\",\"10Rnd_338_Mag\",8,\"srifle_DMR_02_F\"],[\"Exile_Melee_Axe\",\"Exile_Magazine_Swing\",975,\"Exile_Melee_Axe\"],[\"hgun_P07_F\",\"16Rnd_9x21_Mag\",11,\"hgun_P07_F\"]]', 'srifle_DMR_02_F', '[\"\",\"\",\"optic_LRPS_tna_F\",\"bipod_02_F_tan\"]', 'Exile_Melee_Axe', '[\"\",\"\",\"\",\"\"]', 'U_B_T_Soldier_F', '[]', '[[\"Exile_Item_PowerDrink\",1],[\"Exile_Item_PlasticBottleFreshWater\",1]]', '[]', 'V_PlateCarrier1_rgr', '[[\"optic_Holosight\",1]]', '[[\"Exile_Item_PlasticBottleFreshWater\",1],[\"hlc_8Rnd_357SIG_JHP_P239\",8],[\"16Rnd_9x21_Mag\",16],[\"16Rnd_9x21_Mag\",16],[\"10Rnd_338_Mag\",10],[\"HandGrenade\",1],[\"30Rnd_556x45_Stanag\",30],[\"30Rnd_556x45_Stanag\",30],[\"10Rnd_338_Mag\",9],[\"10Rnd_338_Mag\",10],[\"30Rnd_556x45_Stanag\",30],[\"30Rnd_556x45_Stanag\",30],[\"30Rnd_556x45_Stanag\",30],[\"30Rnd_556x45_Stanag\",30]]', '[]', '2018-10-22 23:52:41'),
(15, 'YoSwagi', '76561198153523968', 0, 0.0984252, 69.818153, 52.830917, 0, 37, 0, 1, 0, '[[\"face_hub\",0.0984252],[\"neck\",0.0984252],[\"head\",0.0984252],[\"pelvis\",0.0984252],[\"spine1\",0.0984252],[\"spine2\",0.0984252],[\"spine3\",0.0984252],[\"body\",0.0984252],[\"arms\",0.0984252],[\"head_hit\",0.0984252],[\"hand_r\",0.0984252],[\"body\",0.0984252]]', 220.235825, 8006.065918, 8155.864746, 0.120944, '2018-10-22 21:06:47', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\",\"ItemGPS\",\"Rangefinder\"]', 'B_Bergen_hex_F', '[[\"H_HelmetSpecO_ocamo\",1]]', '[[\"Exile_Magazine_5Rnd_127x108_APDS_Bullet_Cam_Mag\",5],[\"20Rnd_762x51_Mag\",20],[\"Exile_Magazine_5Rnd_127x108_APDS_Bullet_Cam_Mag\",5],[\"Exile_Magazine_5Rnd_127x108_APDS_Bullet_Cam_Mag\",5],[\"Exile_Magazine_5Rnd_127x108_APDS_Bullet_Cam_Mag\",5],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",20],[\"Exile_Magazine_5Rnd_127x108_APDS_Bullet_Cam_Mag\",5],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_650x39_Cased_Mag_F\",20],[\"20Rnd_650x39_Cased_Mag_F\",20],[\"20Rnd_650x39_Cased_Mag_F\",20],[\"Exile_Magazine_10Rnd_338_Bullet_Cam_Mag\",10],[\"Exile_Magazine_10Rnd_338_Bullet_Cam_Mag\",10],[\"Exile_Magazine_10Rnd_338_Bullet_Cam_Mag\",10],[\"7Rnd_408_Mag\",7],[\"MiniGrenade\",1],[\"Exile_Item_Heatpack\",1],[\"20Rnd_650x39_Cased_Mag_F\",20],[\"20Rnd_650x39_Cased_Mag_F\",20],[\"20Rnd_650x39_Cased_Mag_F\",20],[\"HandGrenade\",1],[\"Exile_Item_Heatpack\",1],[\"10Rnd_338_Mag\",10],[\"10Rnd_338_Mag\",10],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",20],[\"Exile_Item_Heatpack\",1],[\"Exile_Magazine_10Rnd_93x64_DMR_05_Bullet_Cam_Mag\",10],[\"Exile_Magazine_10Rnd_93x64_DMR_05_Bullet_Cam_Mag\",10],[\"Exile_Magazine_10Rnd_338_Bullet_Cam_Mag\",10]]', '[]', 'srifle_DMR_06_olive_F', '', '[\"muzzle_snds_L\",\"\",\"\",\"\"]', 'hgun_P07_khk_F', 'H_HelmetLeaderO_oucamo', 'Rangefinder', '[[\"srifle_DMR_06_olive_F\",\"20Rnd_762x51_Mag\",6,\"srifle_DMR_06_olive_F\"],[\"hgun_P07_khk_F\",\"16Rnd_9x21_Mag\",16,\"hgun_P07_khk_F\"]]', 'srifle_DMR_06_olive_F', '[\"\",\"\",\"optic_KHS_hex\",\"bipod_01_F_snd\"]', '', '[\"\",\"\",\"\",\"\"]', 'U_B_FullGhillie_sard', '[]', '[[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_DuctTape\",1],[\"Exile_Item_DuctTape\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_CatFood_Cooked\",1],[\"Exile_Item_CatFood_Cooked\",1],[\"Exile_Item_SausageGravy_Cooked\",1],[\"Exile_Item_GloriousKnakworst_Cooked\",1]]', '[]', 'V_PlateCarrierSpec_tna_F', '[[\"optic_KHS_tan\",1]]', '[[\"Exile_Item_DuctTape\",1],[\"Exile_Item_CanOpener\",1],[\"Exile_Item_DuctTape\",1],[\"Exile_Item_PowerDrink\",1],[\"Exile_Item_Codelock\",1],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_Heatpack\",1],[\"20Rnd_650x39_Cased_Mag_F\",20],[\"20Rnd_650x39_Cased_Mag_F\",20],[\"20Rnd_650x39_Cased_Mag_F\",20],[\"Exile_Item_Heatpack\",1]]', '[]', '2018-10-23 21:32:22'),
(18, 'Barrazal', '76561197973996366', 0, 0, 92.151642, 76.0776596, 0, 36.984272, 0.280303, 1, 0, '[[\"face_hub\",0],[\"neck\",0],[\"head\",0],[\"pelvis\",0],[\"spine1\",0],[\"spine2\",0],[\"spine3\",0],[\"body\",0],[\"arms\",0],[\"head_hit\",0],[\"hand_r\",0],[\"body\",0]]', 128.106476, 12807.519531, 7426.626953, 13.404076, '2018-10-22 22:56:21', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\"]', 'B_Carryall_oli', '[]', '[[\"Exile_Item_CatFood_Cooked\",1]]', '[]', 'srifle_DMR_05_tan_f', '', '[\"muzzle_snds_L\",\"\",\"\",\"\"]', 'hgun_P07_F', 'H_HelmetLeaderO_ocamo', '', '[[\"srifle_DMR_05_tan_f\",\"10Rnd_93x64_DMR_05_Mag\",4,\"srifle_DMR_05_tan_f\"],[\"hgun_P07_F\",\"16Rnd_9x21_Mag\",14,\"hgun_P07_F\"]]', 'srifle_DMR_05_tan_f', '[\"muzzle_snds_93mmg_tan\",\"\",\"optic_KHS_old\",\"\"]', '', '[\"\",\"\",\"\",\"\"]', 'U_I_FullGhillie_ard', '[]', '[[\"Exile_Item_CatFood_Cooked\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_DuctTape\",1],[\"16Rnd_9x21_Mag\",16],[\"16Rnd_9x21_Mag\",16],[\"Exile_Item_CanOpener\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1]]', '[]', 'V_TacVest_blk_POLICE', '[]', '[[\"10Rnd_93x64_DMR_05_Mag\",10],[\"10Rnd_93x64_DMR_05_Mag\",10],[\"10Rnd_93x64_DMR_05_Mag\",10],[\"10Rnd_93x64_DMR_05_Mag\",10],[\"10Rnd_93x64_DMR_05_Mag\",10],[\"Exile_Item_Matches\",10]]', '[]', '2018-10-23 21:50:22'),
(21, 'DrSnuggles', '76561198086210494', 0, 0, 99.605583, 99.411766, 0, 37, 0, 1, 0, '[[\"face_hub\",0],[\"neck\",0],[\"head\",0],[\"pelvis\",0],[\"spine1\",0],[\"spine2\",0],[\"spine3\",0],[\"body\",0],[\"arms\",0],[\"head_hit\",0],[\"hand_r\",0],[\"body\",0]]', 266.148682, 8012.183105, 12408.496094, 0.2508, '2018-10-23 13:51:13', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\",\"ItemGPS\"]', 'B_Bergen_tna_F', '[]', '[[\"Exile_Item_PortableGeneratorKit\",1],[\"hlc_50rnd_556x45_EPR\",50],[\"hlc_50rnd_556x45_EPR\",50],[\"hlc_50rnd_556x45_EPR\",50],[\"hlc_50rnd_556x45_EPR\",50],[\"hlc_50rnd_556x45_EPR\",50]]', '[]', 'arifle_SPAR_01_blk_F', '', '[\"\",\"\",\"\",\"\"]', '', 'H_HelmetB_light_grass', '', '[[\"arifle_SPAR_01_blk_F\",\"hlc_50rnd_556x45_EPR\",50,\"arifle_SPAR_01_blk_F\"]]', 'arifle_SPAR_01_blk_F', '[\"muzzle_snds_M\",\"\",\"optic_Arco_blk_F\",\"\"]', '', '[\"\",\"\",\"\",\"\"]', 'U_B_GhillieSuit', '[]', '[[\"Exile_Item_CanOpener\",1],[\"30Rnd_65x39_caseless_mag\",6],[\"Exile_Item_Pliers\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleEmpty\",1],[\"Exile_Item_PlasticBottleEmpty\",1]]', '[[\"hgun_Pistol_Signal_F\",1]]', 'V_Rangemaster_belt', '[[\"optic_AMS\",1]]', '[[\"Exile_Item_GloriousKnakworst\",1]]', '[]', '2018-10-23 16:00:37'),
(22, 'Phoenix', '76561197990557284', 6655, 0.0314961, 82.126274, 73.66185, 0, 37, 0, 1, 0, '[[\"face_hub\",0.0314961],[\"neck\",0.0314961],[\"head\",0.0314961],[\"pelvis\",0.0314961],[\"spine1\",0.0314961],[\"spine2\",0.0314961],[\"spine3\",0.0314961],[\"body\",0.0314961],[\"arms\",0.0314961],[\"head_hit\",0.0314961],[\"hand_r\",0.0314961],[\"body\",0.0314961]]', 227.125793, 2663.0673828, 8874.521484, 0.00151439, '2018-10-23 18:32:15', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\",\"ItemGPS\",\"Rangefinder\"]', 'B_Bergen_hex_F', '[]', '[]', '[]', 'srifle_GM6_F', 'G_Tactical_Black', '[\"\",\"\",\"\",\"\"]', '', 'H_HelmetLeaderO_oucamo', 'Rangefinder', '[[\"srifle_GM6_F\",\"5Rnd_127x108_Mag\",5,\"srifle_GM6_F\"]]', 'srifle_GM6_F', '[\"\",\"\",\"optic_LRPS_tna_F\",\"\"]', '', '[\"\",\"\",\"\",\"\"]', 'U_B_T_FullGhillie_tna_F', '[]', '[]', '[]', 'V_PlateCarrierSpec_blk', '[]', '[]', '[]', '2018-10-23 21:19:36'),
(23, 'Elchsocke', '76561198026891445', 0, 0, 92.285156, 88.358818, 0, 37, 0, 1, 0, '[[\"face_hub\",0],[\"neck\",0],[\"head\",0],[\"pelvis\",0],[\"spine1\",0],[\"spine2\",0],[\"spine3\",0],[\"body\",0],[\"arms\",0],[\"hands\",0],[\"legs\",0],[\"body\",0]]', 251.796173, 7918.930664, 7986.712402, 0.00169743, '2018-10-23 21:12:30', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\",\"ItemGPS\",\"Rangefinder\"]', 'B_Carryall_mcamo', '[]', '[]', '[]', 'hgun_Rook40_F', '', '[\"\",\"\",\"\",\"\"]', 'hgun_Rook40_F', 'H_HelmetLeaderO_ocamo', 'Rangefinder', '[[\"arifle_ARX_ghex_F\",\"30Rnd_65x39_caseless_green\",23,\"arifle_ARX_ghex_F\"],[\"hgun_Rook40_F\",\"16Rnd_9x21_Mag\",9,\"hgun_Rook40_F\"]]', 'arifle_ARX_ghex_F', '[\"\",\"\",\"optic_DMS_ghex_F\",\"\"]', '', '[\"\",\"\",\"\",\"\"]', 'U_B_T_Soldier_F', '[]', '[[\"30Rnd_65x39_caseless_green\",30],[\"HandGrenade\",1],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_PlasticBottleFreshWater\",1]]', '[]', 'V_PlateCarrierGL_rgr', '[]', '[[\"Exile_Item_CatFood_Cooked\",1],[\"30Rnd_65x39_caseless_green\",30],[\"Exile_Item_CanOpener\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_EnergyDrink\",1],[\"30Rnd_65x39_caseless_green\",30],[\"30Rnd_65x39_caseless_green\",30],[\"30Rnd_65x39_caseless_green\",30],[\"30Rnd_65x39_caseless_green\",30],[\"Exile_Item_Beer\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_DuctTape\",1],[\"HandGrenade\",1],[\"HandGrenade\",1],[\"Exile_Item_InstaDoc\",1],[\"16Rnd_9x21_Mag\",16]]', '[]', '2018-10-23 21:53:37'),
(24, 'Der Türke', '76561198008624689', 0, 0, 100, 100, 0, 37, 0.109187, 1, 0, '[[\"face_hub\",0],[\"neck\",0],[\"head\",0],[\"pelvis\",0],[\"spine1\",0],[\"spine2\",0],[\"spine3\",0],[\"body\",0],[\"arms\",0],[\"hands\",0],[\"legs\",0],[\"body\",0]]', 251.640106, 10431.853516, 6514.679688, 0.00139336, '2018-10-23 21:32:12', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\"]', '', '[]', '[]', '[]', 'Exile_Weapon_Colt1911', 'G_Sport_Blackred', '[\"\",\"\",\"\",\"\"]', 'Exile_Weapon_Colt1911', '', '', '[[\"Exile_Weapon_Colt1911\",\"Exile_Magazine_7Rnd_45ACP\",7,\"Exile_Weapon_Colt1911\"]]', '', '[\"\",\"\",\"\",\"\"]', '', '[\"\",\"\",\"\",\"\"]', 'Exile_Uniform_BambiOverall', '[]', '[[\"Exile_Item_Beer\",1],[\"Exile_Item_DuctTape\",1]]', '[]', 'V_Rangemaster_belt', '[]', '[]', '[]', '2018-10-23 21:32:27');

-- --------------------------------------------------------

--
-- Table structure for table `player_history`
--

CREATE TABLE `player_history` (
  `id` int(11) UNSIGNED NOT NULL,
  `account_uid` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `died_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `position_x` double NOT NULL,
  `position_y` double NOT NULL,
  `position_z` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `player_history`
--

INSERT INTO `player_history` (`id`, `account_uid`, `name`, `died_at`, `position_x`, `position_y`, `position_z`) VALUES
(2, '76561198153523968', 'YoSwagi', '2018-10-22 18:59:58', 12419.1, 12685.4, 8.4557),
(3, '76561198026891445', 'Elchsocke', '2018-10-22 19:30:06', 6464.05, 13030.8, 0.00206757),
(4, '76561198026891445', 'Elchsocke', '2018-10-22 19:30:35', 6417.04, 12835.3, 0.735114),
(5, '76561198026891445', 'Elchsocke', '2018-10-22 19:42:10', 6941.36, 13171.1, 0.00270081),
(6, '76561198026891445', 'Elchsocke', '2018-10-22 19:52:17', 6948.11, 13203.2, 0.0026741),
(7, '76561198026891445', 'Elchsocke', '2018-10-22 20:14:46', 6762.87, 13190, 0.00201797),
(8, '76561197988232530', 'Techno Viking', '2018-10-22 20:53:00', 7759.72, 11126.4, 0.000610352),
(9, '76561198153523968', 'YoSwagi', '2018-10-22 21:05:09', 8252.78, 9177.6, 0.00156021),
(10, '76561197973996366', 'Barrazal', '2018-10-22 22:22:43', 13781.5, 8419.46, 0.00125408),
(11, '76561197973996366', 'Barrazal', '2018-10-22 22:49:43', 13716.4, 9356.32, 0.00157166),
(12, '76561197973996366', 'Barrazal', '2018-10-22 22:55:45', 14136.2, 8719.18, 0.00120926),
(13, '76561198026891445', 'Elchsocke', '2018-10-22 23:24:05', 7529.56, 7616.01, 0.00200701),
(14, '76561197990557284', 'Phoenix', '2018-10-23 18:31:50', 11113.1, 8396.55, -0.000350952),
(15, '76561198026891445', 'Elchsocke', '2018-10-23 21:12:19', 10791.5, 6517.23, 0.00143886);

-- --------------------------------------------------------

--
-- Table structure for table `territory`
--

CREATE TABLE `territory` (
  `id` int(11) UNSIGNED NOT NULL,
  `owner_uid` varchar(32) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `position_x` double DEFAULT NULL,
  `position_y` double DEFAULT NULL,
  `position_z` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `flag_texture` varchar(255) DEFAULT NULL,
  `flag_stolen` tinyint(1) NOT NULL DEFAULT '0',
  `flag_stolen_by_uid` varchar(32) DEFAULT NULL,
  `flag_stolen_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_paid_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `xm8_protectionmoney_notified` tinyint(1) NOT NULL DEFAULT '0',
  `build_rights` varchar(640) NOT NULL DEFAULT '0',
  `moderators` varchar(320) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE `vehicle` (
  `id` int(11) UNSIGNED NOT NULL,
  `class` varchar(64) DEFAULT NULL,
  `spawned_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `account_uid` varchar(32) DEFAULT NULL,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `fuel` double UNSIGNED NOT NULL DEFAULT '0',
  `damage` double UNSIGNED NOT NULL DEFAULT '0',
  `hitpoints` text,
  `position_x` double NOT NULL DEFAULT '0',
  `position_y` double NOT NULL DEFAULT '0',
  `position_z` double NOT NULL DEFAULT '0',
  `direction_x` double NOT NULL DEFAULT '0',
  `direction_y` double NOT NULL DEFAULT '0',
  `direction_z` double NOT NULL DEFAULT '0',
  `up_x` double NOT NULL DEFAULT '0',
  `up_y` double NOT NULL DEFAULT '0',
  `up_z` double NOT NULL DEFAULT '1',
  `cargo_items` text,
  `cargo_magazines` text,
  `cargo_weapons` text,
  `cargo_container` text,
  `last_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pin_code` varchar(6) NOT NULL DEFAULT '000000',
  `deleted_at` datetime DEFAULT NULL,
  `money` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `vehicle_texture` text,
  `territory_id` int(11) UNSIGNED DEFAULT NULL,
  `nickname` varchar(64) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`id`, `class`, `spawned_at`, `account_uid`, `is_locked`, `fuel`, `damage`, `hitpoints`, `position_x`, `position_y`, `position_z`, `direction_x`, `direction_y`, `direction_z`, `up_x`, `up_y`, `up_z`, `cargo_items`, `cargo_magazines`, `cargo_weapons`, `cargo_container`, `last_updated_at`, `pin_code`, `deleted_at`, `money`, `vehicle_texture`, `territory_id`, `nickname`) VALUES
(7, 'CUP_C_Fishing_Boat_Chernarus', '2018-10-23 18:55:12', '76561197990557284', -1, 0.825445, 0, '[[\"hitengine\",0],[\"hithull\",0],[\"#svetlo\",0],[\"#deck_light_pos\",0]]', 8969.0517578, 4819.555176, 0.39432, -0.0122681, -0.99548, 0.0941772, -0.112484, 0.0949604, 0.989106, '[[],[]]', '[]', '[]', '[]', '2018-10-23 19:19:38', '8032', NULL, 0, NULL, NULL, ''),
(9, 'CUP_I_FENNEK_ION', '2018-10-23 20:46:04', '76561197990557284', -1, 0.912353, 0, '[[\"hitlfwheel\",0],[\"hitlf2wheel\",0],[\"hitrfwheel\",0],[\"hitrf2wheel\",0],[\"hitfuel\",0],[\"hithull\",0],[\"hitengine\",0],[\"hitbody\",0],[\"hitglass1\",0],[\"hitglass2\",0],[\"hitglass3\",0],[\"hitglass4\",0],[\"hitglass5\",0],[\"hitglass6\",0],[\"hitlbwheel\",0],[\"hitrbwheel\",0],[\"hitlmwheel\",0],[\"hitrmwheel\",0],[\"hitrglass\",0],[\"hitlglass\",0],[\"hitglass7\",0],[\"hitglass8\",0],[\"hitturret\",0],[\"hitgun\",0],[\"#light_l\",0],[\"#light_l\",0],[\"#light_r\",0],[\"#light_r\",0]]', 2656.731445, 8870.777344, -0.253377, -0.986755, -0.156337, -0.0432739, -0.0259636, -0.111116, 0.993468, '[[\"Exile_Headgear_GasMask\"],[1]]', '[[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_EMRE\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_EMRE\",1],[\"Exile_Item_Bandage\",1],[\"Exile_Item_Vishpirin\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_Vishpirin\",1],[\"Exile_Item_EMRE\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_Surstromming_Cooked\",1],[\"Exile_Item_PlasticBottleEmpty\",1],[\"Exile_Item_CanOpener\",1],[\"Exile_Item_PlasticBottleEmpty\",1],[\"Exile_Item_Surstromming_Cooked\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_Surstromming_Cooked\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleCoffee\",1],[\"Exile_Item_Surstromming_Cooked\",1],[\"Exile_Item_Vishpirin\",1],[\"Exile_Item_CanOpener\",1],[\"Exile_Item_PlasticBottleEmpty\",1],[\"5Rnd_127x108_Mag\",5],[\"5Rnd_127x108_Mag\",5],[\"5Rnd_127x108_Mag\",5],[\"5Rnd_127x108_Mag\",5],[\"5Rnd_127x108_Mag\",5],[\"5Rnd_127x108_Mag\",5],[\"Exile_Item_EMRE\",1],[\"Exile_Item_PlasticBottleCoffee\",1],[\"Exile_Item_Surstromming_Cooked\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_CanOpener\",1],[\"Exile_Item_CanOpener\",1],[\"Exile_Item_Surstromming_Cooked\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_Surstromming_Cooked\",1],[\"Exile_Item_EMRE\",1],[\"Exile_Item_CatFood_Cooked\",1],[\"Exile_Item_PlasticBottleFreshWater\",1]]', '[]', '[]', '2018-10-23 21:19:21', '8032', NULL, 0, NULL, NULL, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`uid`),
  ADD KEY `clan_id` (`clan_id`);

--
-- Indexes for table `clan`
--
ALTER TABLE `clan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leader_uid` (`leader_uid`);

--
-- Indexes for table `clan_map_marker`
--
ALTER TABLE `clan_map_marker`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clan_id` (`clan_id`);

--
-- Indexes for table `construction`
--
ALTER TABLE `construction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_uid` (`account_uid`),
  ADD KEY `territory_id` (`territory_id`);

--
-- Indexes for table `container`
--
ALTER TABLE `container`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_uid` (`account_uid`),
  ADD KEY `territory_id` (`territory_id`);

--
-- Indexes for table `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`id`),
  ADD KEY `player_uid` (`account_uid`);

--
-- Indexes for table `player_history`
--
ALTER TABLE `player_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `territory`
--
ALTER TABLE `territory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `owner_uid` (`owner_uid`),
  ADD KEY `flag_stolen_by_uid` (`flag_stolen_by_uid`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_uid` (`account_uid`),
  ADD KEY `vehicle_ibfk_2_idx` (`territory_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clan`
--
ALTER TABLE `clan`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `clan_map_marker`
--
ALTER TABLE `clan_map_marker`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `construction`
--
ALTER TABLE `construction`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `container`
--
ALTER TABLE `container`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `player`
--
ALTER TABLE `player`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `player_history`
--
ALTER TABLE `player_history`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `territory`
--
ALTER TABLE `territory`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account`
--
ALTER TABLE `account`
  ADD CONSTRAINT `account_ibfk_1` FOREIGN KEY (`clan_id`) REFERENCES `clan` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `clan`
--
ALTER TABLE `clan`
  ADD CONSTRAINT `clan_ibfk_1` FOREIGN KEY (`leader_uid`) REFERENCES `account` (`uid`) ON DELETE CASCADE;

--
-- Constraints for table `clan_map_marker`
--
ALTER TABLE `clan_map_marker`
  ADD CONSTRAINT `clan_map_marker_ibfk_1` FOREIGN KEY (`clan_id`) REFERENCES `clan` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `construction`
--
ALTER TABLE `construction`
  ADD CONSTRAINT `construction_ibfk_1` FOREIGN KEY (`account_uid`) REFERENCES `account` (`uid`) ON DELETE CASCADE,
  ADD CONSTRAINT `construction_ibfk_2` FOREIGN KEY (`territory_id`) REFERENCES `territory` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `container`
--
ALTER TABLE `container`
  ADD CONSTRAINT `container_ibfk_1` FOREIGN KEY (`account_uid`) REFERENCES `account` (`uid`) ON DELETE CASCADE,
  ADD CONSTRAINT `container_ibfk_2` FOREIGN KEY (`territory_id`) REFERENCES `territory` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `player`
--
ALTER TABLE `player`
  ADD CONSTRAINT `player_ibfk_1` FOREIGN KEY (`account_uid`) REFERENCES `account` (`uid`) ON DELETE CASCADE;

--
-- Constraints for table `territory`
--
ALTER TABLE `territory`
  ADD CONSTRAINT `territory_ibfk_1` FOREIGN KEY (`owner_uid`) REFERENCES `account` (`uid`) ON DELETE CASCADE,
  ADD CONSTRAINT `territory_ibfk_2` FOREIGN KEY (`flag_stolen_by_uid`) REFERENCES `account` (`uid`) ON DELETE SET NULL;

--
-- Constraints for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD CONSTRAINT `vehicle_ibfk_1` FOREIGN KEY (`account_uid`) REFERENCES `account` (`uid`) ON DELETE CASCADE,
  ADD CONSTRAINT `vehicle_ibfk_2` FOREIGN KEY (`territory_id`) REFERENCES `territory` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
