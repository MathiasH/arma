-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Oct 24, 2018 at 11:33 PM
-- Server version: 5.7.17
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exile`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `uid` varchar(32) NOT NULL,
  `clan_id` int(11) UNSIGNED DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  `kills` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `deaths` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `locker` int(11) NOT NULL DEFAULT '0',
  `first_connect_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_connect_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_disconnect_at` datetime DEFAULT NULL,
  `total_connections` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `zedkills` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`uid`, `clan_id`, `name`, `score`, `kills`, `deaths`, `locker`, `first_connect_at`, `last_connect_at`, `last_disconnect_at`, `total_connections`, `zedkills`) VALUES
('76561197973996366', NULL, 'Barrazal', 5703, 146, 3, 817, '2018-10-22 20:26:08', '2018-10-24 21:51:18', '2018-10-24 00:42:08', 10, 146),
('76561197988232530', NULL, 'Techno Viking', 1667, 87, 4, 0, '2018-10-22 20:16:44', '2018-10-24 21:45:37', '2018-10-22 23:53:15', 4, 87),
('76561197990557284', NULL, 'Phoenix', 8081, 122, 1, 23300, '2018-10-23 10:47:08', '2018-10-24 22:33:03', '2018-10-23 21:19:36', 6, 122),
('76561198001767153', NULL, 'Portion Zombiefutter', 1688, 65, 0, 12416, '2018-10-22 20:20:57', '2018-10-24 21:53:28', '2018-10-24 22:19:09', 4, 65),
('76561198008624689', NULL, 'Der Türke', 0, 0, 0, 0, '2018-10-23 21:31:56', '2018-10-23 21:31:56', '2018-10-23 21:32:27', 1, 0),
('76561198026007736', NULL, 'KillCrackerX', 20, 2, 0, 0, '2018-10-24 20:33:07', '2018-10-24 20:33:07', NULL, 1, 2),
('76561198026891445', NULL, 'Elchsocke', 14152, 213, 10, 50000, '2018-10-22 18:37:51', '2018-10-24 21:44:11', '2018-10-24 00:41:28', 18, 213),
('76561198041043249', NULL, 'XlrT | Mathias', 1172, 52, 4, 45, '2018-10-24 18:55:54', '2018-10-24 18:55:54', '2018-10-24 20:28:50', 1, 52),
('76561198086210494', NULL, 'DrSnuggles', 10381, 80, 2, 44087, '2018-10-23 13:50:24', '2018-10-24 17:03:46', '2018-10-23 16:01:21', 5, 80),
('76561198153523968', NULL, 'YoSwagi', 3451, 115, 5, 8753, '2018-10-22 18:58:00', '2018-10-24 21:45:20', '2018-10-24 21:54:47', 15, 115),
('76561198400608195', NULL, 'Dion', 30, 3, 0, 0, '2018-10-24 20:16:57', '2018-10-24 20:16:57', '2018-10-24 20:23:01', 1, 3),
('DMS_PersistentVehicle', NULL, 'DMS_PersistentVehicle', 0, 0, 0, 0, '2018-10-21 20:36:43', '2018-10-21 20:36:43', NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `clan`
--

CREATE TABLE `clan` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `leader_uid` varchar(32) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `clan_map_marker`
--

CREATE TABLE `clan_map_marker` (
  `id` int(11) UNSIGNED NOT NULL,
  `clan_id` int(11) UNSIGNED NOT NULL,
  `markerType` tinyint(4) NOT NULL DEFAULT '-1',
  `positionArr` text NOT NULL,
  `color` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `iconSize` float UNSIGNED NOT NULL,
  `label` varchar(255) NOT NULL,
  `labelSize` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `construction`
--

CREATE TABLE `construction` (
  `id` int(11) UNSIGNED NOT NULL,
  `class` varchar(64) DEFAULT NULL,
  `account_uid` varchar(32) DEFAULT NULL,
  `spawned_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `position_x` double NOT NULL DEFAULT '0',
  `position_y` double NOT NULL DEFAULT '0',
  `position_z` double NOT NULL DEFAULT '0',
  `direction_x` double NOT NULL DEFAULT '0',
  `direction_y` double NOT NULL DEFAULT '0',
  `direction_z` double NOT NULL DEFAULT '0',
  `up_x` double NOT NULL DEFAULT '0',
  `up_y` double NOT NULL DEFAULT '0',
  `up_z` double NOT NULL DEFAULT '0',
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `pin_code` varchar(6) NOT NULL DEFAULT '000000',
  `damage` tinyint(1) UNSIGNED DEFAULT '0',
  `territory_id` int(11) UNSIGNED DEFAULT NULL,
  `last_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `container`
--

CREATE TABLE `container` (
  `id` int(11) UNSIGNED NOT NULL,
  `class` varchar(64) DEFAULT NULL,
  `spawned_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `account_uid` varchar(32) DEFAULT NULL,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `position_x` double NOT NULL DEFAULT '0',
  `position_y` double NOT NULL DEFAULT '0',
  `position_z` double NOT NULL DEFAULT '0',
  `direction_x` double NOT NULL DEFAULT '0',
  `direction_y` double NOT NULL DEFAULT '0',
  `direction_z` double NOT NULL DEFAULT '0',
  `up_x` double NOT NULL DEFAULT '0',
  `up_y` double NOT NULL DEFAULT '0',
  `up_z` double NOT NULL DEFAULT '1',
  `cargo_items` text,
  `cargo_magazines` text,
  `cargo_weapons` text,
  `cargo_container` text,
  `last_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pin_code` varchar(6) NOT NULL DEFAULT '000000',
  `territory_id` int(11) UNSIGNED DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `money` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `abandoned` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `player`
--

CREATE TABLE `player` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `account_uid` varchar(32) NOT NULL,
  `money` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `damage` double UNSIGNED NOT NULL DEFAULT '0',
  `hunger` double UNSIGNED NOT NULL DEFAULT '100',
  `thirst` double UNSIGNED NOT NULL DEFAULT '100',
  `alcohol` double UNSIGNED NOT NULL DEFAULT '0',
  `temperature` double NOT NULL DEFAULT '37',
  `wetness` double UNSIGNED NOT NULL DEFAULT '0',
  `oxygen_remaining` double UNSIGNED NOT NULL DEFAULT '1',
  `bleeding_remaining` double UNSIGNED NOT NULL DEFAULT '0',
  `hitpoints` varchar(1024) NOT NULL DEFAULT '[]',
  `direction` double NOT NULL DEFAULT '0',
  `position_x` double NOT NULL DEFAULT '0',
  `position_y` double NOT NULL DEFAULT '0',
  `position_z` double NOT NULL DEFAULT '0',
  `spawned_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `assigned_items` text,
  `backpack` varchar(64) DEFAULT NULL,
  `backpack_items` text,
  `backpack_magazines` text,
  `backpack_weapons` text,
  `current_weapon` varchar(64) DEFAULT NULL,
  `goggles` varchar(64) DEFAULT NULL,
  `handgun_items` text,
  `handgun_weapon` varchar(64) DEFAULT NULL,
  `headgear` varchar(64) DEFAULT NULL,
  `binocular` varchar(64) DEFAULT NULL,
  `loaded_magazines` text,
  `primary_weapon` varchar(64) DEFAULT NULL,
  `primary_weapon_items` text,
  `secondary_weapon` varchar(64) DEFAULT NULL,
  `secondary_weapon_items` text,
  `uniform` varchar(64) DEFAULT NULL,
  `uniform_items` text,
  `uniform_magazines` text,
  `uniform_weapons` text,
  `vest` varchar(64) DEFAULT NULL,
  `vest_items` text,
  `vest_magazines` text,
  `vest_weapons` text,
  `last_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `player`
--

INSERT INTO `player` (`id`, `name`, `account_uid`, `money`, `damage`, `hunger`, `thirst`, `alcohol`, `temperature`, `wetness`, `oxygen_remaining`, `bleeding_remaining`, `hitpoints`, `direction`, `position_x`, `position_y`, `position_z`, `spawned_at`, `assigned_items`, `backpack`, `backpack_items`, `backpack_magazines`, `backpack_weapons`, `current_weapon`, `goggles`, `handgun_items`, `handgun_weapon`, `headgear`, `binocular`, `loaded_magazines`, `primary_weapon`, `primary_weapon_items`, `secondary_weapon`, `secondary_weapon_items`, `uniform`, `uniform_items`, `uniform_magazines`, `uniform_weapons`, `vest`, `vest_items`, `vest_magazines`, `vest_weapons`, `last_updated_at`) VALUES
(12, 'Portion Zombiefutter', '76561198001767153', 0, 0.011811, 57.974667, 44.221287, 0, 37, 0, 1, 0, '[[\"face_hub\",0.011811],[\"neck\",0.011811],[\"head\",0.011811],[\"pelvis\",0.011811],[\"spine1\",0.011811],[\"spine2\",0.011811],[\"spine3\",0.011811],[\"body\",0.011811],[\"arms\",0.011811],[\"head_hit\",0.011811],[\"hand_r\",0.011811],[\"body\",0.011811]]', 123.893227, 7188.0170898, 6987.751953, 0.00143886, '2018-10-22 20:21:34', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\"]', 'B_Kitbag_sgg', '[]', '[[\"150Rnd_762x54_Box\",150],[\"150Rnd_762x54_Box\",150],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_BBQSandwich\",1],[\"Exile_Item_Surstromming_Cooked\",1],[\"Exile_Item_EnergyDrink\",1]]', '[]', 'LMG_Zafir_F', 'G_Sport_Blackred', '[\"muzzle_snds_L\",\"\",\"\",\"\"]', 'hgun_Rook40_F', 'H_HelmetB_Enh_tna_F', '', '[[\"LMG_Zafir_F\",\"150Rnd_762x54_Box\",105,\"LMG_Zafir_F\"],[\"hgun_Rook40_F\",\"16Rnd_9x21_Mag\",15,\"hgun_Rook40_F\"]]', 'LMG_Zafir_F', '[\"\",\"\",\"optic_Nightstalker\",\"\"]', '', '[\"\",\"\",\"\",\"\"]', 'U_B_T_Sniper_F', '[]', '[[\"Exile_Item_CanOpener\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_DuctTape\",1],[\"Exile_Item_InstaDoc\",1]]', '[]', 'V_PlateCarrierSpec_mtp', '[]', '[[\"16Rnd_9x21_Mag\",16],[\"150Rnd_762x54_Box\",150]]', '[]', '2018-10-24 22:19:09'),
(18, 'Barrazal', '76561197973996366', 250, 0.125984, 54.956078, 21.217054, 0, 37, 0, 1, 0, '[[\"face_hub\",0.125984],[\"neck\",0.125984],[\"head\",0.125984],[\"pelvis\",0.125984],[\"spine1\",0.125984],[\"spine2\",0.125984],[\"spine3\",0.125984],[\"body\",0.125984],[\"arms\",0.125984],[\"head_hit\",0.125984],[\"hand_r\",0.125984],[\"body\",0.125984]]', 97.531967, 7702.0141602, 8560.203125, 0.825299, '2018-10-22 22:56:21', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\",\"ItemGPS\",\"Rangefinder\"]', 'B_Carryall_oli', '[]', '[[\"16Rnd_9x21_Mag\",16],[\"16Rnd_9x21_Mag\",15]]', '[]', 'srifle_EBR_F', '', '[\"muzzle_snds_L\",\"\",\"\",\"\"]', 'hgun_P07_F', 'H_HelmetB_Enh_tna_F', 'Rangefinder', '[[\"srifle_EBR_F\",\"20Rnd_762x51_Mag\",20,\"srifle_EBR_F\"],[\"hgun_P07_F\",\"16Rnd_9x21_Mag\",3,\"hgun_P07_F\"]]', 'srifle_EBR_F', '[\"\",\"\",\"optic_SOS\",\"bipod_03_F_oli\"]', '', '[\"\",\"\",\"\",\"\"]', 'U_I_FullGhillie_ard', '[]', '[[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_CanOpener\",1],[\"Exile_Item_EMRE\",1],[\"Exile_Item_DuctTape\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_Matches\",10]]', '[]', 'V_PlateCarrierGL_tna_F', '[[\"optic_KHS_old\",1]]', '[[\"9Rnd_45ACP_Mag\",9],[\"9Rnd_45ACP_Mag\",9],[\"Exile_Item_EMRE\",1],[\"Exile_Item_InstaDoc\",1],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",20]]', '[]', '2018-10-24 23:06:35'),
(22, 'Phoenix', '76561197990557284', 0, 0.0944882, 43.0355148, 14.922086, 0, 37, 0, 1, 0, '[[\"face_hub\",0.0944882],[\"neck\",0.0944882],[\"head\",0.0944882],[\"pelvis\",0.0944882],[\"spine1\",0.0944882],[\"spine2\",0.0944882],[\"spine3\",0.0944882],[\"body\",0.0944882],[\"arms\",0.0944882],[\"head_hit\",0.0944882],[\"hand_r\",0.0944882],[\"body\",0.0944882]]', 119.91568, 7425.634766, 8682.302734, 0.00192466, '2018-10-23 18:32:15', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\",\"ItemGPS\",\"Rangefinder\"]', 'B_Bergen_hex_F', '[]', '[[\"5Rnd_127x108_Mag\",5],[\"5Rnd_127x108_Mag\",5],[\"5Rnd_127x108_Mag\",5],[\"5Rnd_127x108_Mag\",5],[\"5Rnd_127x108_Mag\",5],[\"5Rnd_127x108_Mag\",5]]', '[]', 'srifle_GM6_F', 'G_Tactical_Black', '[\"\",\"\",\"\",\"\"]', '', 'H_HelmetLeaderO_oucamo', 'Rangefinder', '[[\"srifle_GM6_F\",\"5Rnd_127x108_Mag\",3,\"srifle_GM6_F\"]]', 'srifle_GM6_F', '[\"\",\"\",\"optic_LRPS_tna_F\",\"\"]', '', '[\"\",\"\",\"\",\"\"]', 'U_B_T_FullGhillie_tna_F', '[]', '[[\"Exile_Item_Surstromming_Cooked\",1],[\"Exile_Item_Surstromming_Cooked\",1],[\"Exile_Item_CanOpener\",1],[\"Exile_Item_PlasticBottleCoffee\",1],[\"Exile_Item_PlasticBottleCoffee\",1],[\"Exile_Item_Vishpirin\",1],[\"Exile_Item_Vishpirin\",1]]', '[]', 'V_PlateCarrierSpec_blk', '[]', '[]', '[]', '2018-10-24 23:08:41'),
(24, 'Der Türke', '76561198008624689', 0, 0, 100, 100, 0, 37, 0.109187, 1, 0, '[[\"face_hub\",0],[\"neck\",0],[\"head\",0],[\"pelvis\",0],[\"spine1\",0],[\"spine2\",0],[\"spine3\",0],[\"body\",0],[\"arms\",0],[\"hands\",0],[\"legs\",0],[\"body\",0]]', 251.640106, 10431.853516, 6514.679688, 0.00139336, '2018-10-23 21:32:12', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\"]', '', '[]', '[]', '[]', 'Exile_Weapon_Colt1911', 'G_Sport_Blackred', '[\"\",\"\",\"\",\"\"]', 'Exile_Weapon_Colt1911', '', '', '[[\"Exile_Weapon_Colt1911\",\"Exile_Magazine_7Rnd_45ACP\",7,\"Exile_Weapon_Colt1911\"]]', '', '[\"\",\"\",\"\",\"\"]', '', '[\"\",\"\",\"\",\"\"]', 'Exile_Uniform_BambiOverall', '[]', '[[\"Exile_Item_Beer\",1],[\"Exile_Item_DuctTape\",1]]', '[]', 'V_Rangemaster_belt', '[]', '[]', '[]', '2018-10-23 21:32:27'),
(27, 'DrSnuggles', '76561198086210494', 925, 0, 69.499855, 94.690826, 0, 37, 0, 1, 0, '[[\"face_hub\",0],[\"neck\",0],[\"head\",0],[\"pelvis\",0],[\"spine1\",0],[\"spine2\",0],[\"spine3\",0],[\"body\",0],[\"arms\",0],[\"head_hit\",0],[\"hand_r\",0],[\"body\",0]]', 85.128792, 11964.978516, 8108.340332, 0.388223, '2018-10-24 16:22:56', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\",\"ItemGPS\",\"Rangefinder\"]', 'B_Bergen_tna_F', '[]', '[[\"Exile_Item_EMRE\",1],[\"Exile_Item_InstaDoc\",1],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",2]]', '[[\"arifle_Mk20_plain_F\",1],[\"Exile_Weapon_Taurus\",2],[\"Exile_Weapon_AKS_Gold\",1],[\"hgun_Pistol_heavy_01_F\",1]]', 'srifle_EBR_F', 'G_Sport_Blackred', '[\"\",\"\",\"\",\"\"]', 'hgun_Rook40_F', '', 'Rangefinder', '[[\"srifle_EBR_F\",\"20Rnd_762x51_Mag\",15,\"srifle_EBR_F\"],[\"hgun_Rook40_F\",\"16Rnd_9x21_Mag\",6,\"hgun_Rook40_F\"]]', 'srifle_EBR_F', '[\"muzzle_snds_B\",\"\",\"optic_AMS\",\"bipod_02_F_tan\"]', '', '[\"\",\"\",\"\",\"\"]', 'U_B_T_FullGhillie_tna_F', '[]', '[[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_PlasticBottleFreshWater\",1]]', '[]', 'V_PlateCarrierSpec_blk', '[[\"optic_Arco_blk_F\",1]]', '[[\"Exile_Item_CanOpener\",1],[\"Exile_Item_DuctTape\",1],[\"Exile_Item_PlasticBottleCoffee\",1],[\"Exile_Item_EMRE\",1],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",20],[\"20Rnd_762x51_Mag\",20]]', '[]', '2018-10-24 17:48:22'),
(35, 'Dion', '76561198400608195', 0, 0, 95.187111, 92.722321, 0, 37, 0, 1, 0, '[[\"face_hub\",0],[\"neck\",0],[\"head\",0],[\"pelvis\",0],[\"spine1\",0],[\"spine2\",0],[\"spine3\",0],[\"body\",0],[\"arms\",0],[\"hands\",0],[\"legs\",0],[\"body\",0]]', 271.538635, 6500.915039, 12967.458008, 1.368353, '2018-10-24 20:17:53', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\"]', '', '[]', '[]', '[]', 'Exile_Weapon_Colt1911', '', '[\"\",\"\",\"\",\"\"]', 'Exile_Weapon_Colt1911', '', '', '[]', '', '[\"\",\"\",\"\",\"\"]', '', '[\"\",\"\",\"\",\"\"]', 'Exile_Uniform_BambiOverall', '[[\"muzzle_snds_acp\",1]]', '[[\"Exile_Item_Beer\",1],[\"Exile_Item_DuctTape\",1]]', '[]', 'V_Rangemaster_belt', '[]', '[]', '[]', '2018-10-24 20:23:01'),
(36, 'YoSwagi', '76561198153523968', 158, 0.212598, 71.696655, 57.211662, 0, 36.990936, 1, 1, 0, '[[\"face_hub\",0.212598],[\"neck\",0.212598],[\"head\",0.212598],[\"pelvis\",0.212598],[\"spine1\",0.212598],[\"spine2\",0.212598],[\"spine3\",0.212598],[\"body\",0.212598],[\"arms\",0.212598],[\"hands\",0.212598],[\"legs\",0.212598],[\"body\",0.212598]]', 338.668121, 4511.421387, 5054.912598, 0.00125574, '2018-10-24 20:27:57', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\"]', '', '[]', '[]', '[]', '', '', '[\"\",\"\",\"\",\"\"]', 'hgun_Rook40_F', 'H_HelmetB', '', '[[\"hgun_Rook40_F\",\"30Rnd_9x21_Red_Mag\",21,\"hgun_Rook40_F\"]]', '', '[\"\",\"\",\"\",\"\"]', '', '[\"\",\"\",\"\",\"\"]', 'U_C_Scientist', '[]', '[[\"30Rnd_9x21_Red_Mag\",30],[\"30Rnd_9x21_Red_Mag\",30]]', '[]', 'V_PlateCarrier1_rgr', '[]', '[[\"30Rnd_9x21_Green_Mag\",30],[\"30Rnd_9x21_Red_Mag\",30]]', '[]', '2018-10-24 21:54:47'),
(37, 'KillCrackerX', '76561198026007736', 0, 0, 88.0867996, 81.712326, 0, 37, 0, 1, 0, '[[\"face_hub\",0],[\"neck\",0],[\"head\",0],[\"pelvis\",0],[\"spine1\",0],[\"spine2\",0],[\"spine3\",0],[\"body\",0],[\"arms\",0],[\"hands\",0],[\"legs\",0],[\"body\",0]]', 127.650818, 13345.463867, 12072.525391, 0.00143886, '2018-10-24 20:35:30', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\"]', '', '[]', '[]', '[]', 'Exile_Weapon_Colt1911', '', '[\"\",\"\",\"\",\"\"]', 'Exile_Weapon_Colt1911', 'H_MilCap_mcamo', '', '[]', '', '[\"\",\"\",\"\",\"\"]', '', '[\"\",\"\",\"\",\"\"]', 'Exile_Uniform_BambiOverall', '[]', '[[\"Exile_Item_Beer\",1],[\"Exile_Item_DuctTape\",1],[\"Exile_Item_Heatpack\",1],[\"Exile_Item_CatFood\",1],[\"Chemlight_blue\",1]]', '[]', 'V_HarnessO_gry', '[]', '[[\"Exile_Item_CatFood\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_Magazine03\",1],[\"Exile_Item_CookingPot\",1]]', '[]', '2018-10-24 20:45:54'),
(40, 'Elchsocke', '76561198026891445', 100, 0, 81.703575, 72.134125, 0, 37, 0, 1, 0, '[[\"face_hub\",0],[\"neck\",0],[\"head\",0],[\"pelvis\",0],[\"spine1\",0],[\"spine2\",0],[\"spine3\",0],[\"body\",0],[\"arms\",0],[\"head_hit\",0],[\"hand_r\",0],[\"body\",0]]', 96.0746994, 7312.807617, 8567.121094, 5.300305, '2018-10-24 22:51:55', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\",\"ItemGPS\",\"Rangefinder\"]', 'B_Bergen_tna_F', '[]', '[[\"30Rnd_65x39_caseless_green\",30],[\"30Rnd_65x39_caseless_green\",30],[\"30Rnd_65x39_caseless_green\",30],[\"30Rnd_65x39_caseless_green\",30],[\"30Rnd_65x39_caseless_green\",30],[\"30Rnd_65x39_caseless_green\",30],[\"30Rnd_65x39_caseless_green\",30],[\"30Rnd_65x39_caseless_green\",30],[\"30Rnd_65x39_caseless_green\",30],[\"Exile_Item_SausageGravy_Cooked\",1],[\"30Rnd_65x39_caseless_green_mag_Tracer\",30],[\"30Rnd_65x39_caseless_green_mag_Tracer\",30],[\"16Rnd_9x21_Mag\",16],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_Vishpirin\",1],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_BBQSandwich\",1],[\"Exile_Item_EnergyDrink\",1],[\"Exile_Item_EnergyDrink\",1],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_Surstromming_Cooked\",1],[\"Exile_Item_EnergyDrink\",1],[\"Exile_Item_BBQSandwich\",1],[\"Exile_Item_EnergyDrink\",1]]', '[[\"arifle_AK12_F\",1]]', 'Rangefinder', 'G_Sport_Blackred', '[\"muzzle_snds_L\",\"\",\"\",\"\"]', 'hgun_Rook40_F', 'H_HelmetLeaderO_oucamo', 'Rangefinder', '[[\"arifle_ARX_ghex_F\",\"30Rnd_65x39_caseless_green\",26,\"arifle_ARX_ghex_F\"],[\"hgun_Rook40_F\",\"16Rnd_9x21_Mag\",7,\"hgun_Rook40_F\"]]', 'arifle_ARX_ghex_F', '[\"muzzle_snds_65_TI_hex_F\",\"\",\"optic_KHS_tan\",\"bipod_02_F_hex\"]', '', '[\"\",\"\",\"\",\"\"]', 'U_B_FullGhillie_sard', '[]', '[[\"Exile_Item_DuctTape\",1],[\"Exile_Item_CatFood_Cooked\",1],[\"Exile_Item_SausageGravy_Cooked\",1],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_CatFood_Cooked\",1],[\"Exile_Item_CatFood_Cooked\",1],[\"Exile_Item_CanOpener\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1]]', '[]', 'V_PlateCarrierSpec_tna_F', '[]', '[[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_Vishpirin\",1],[\"Exile_Item_EnergyDrink\",1],[\"Exile_Item_Vishpirin\",1],[\"Exile_Item_Vishpirin\",1],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_EnergyDrink\",1],[\"Exile_Item_Vishpirin\",1],[\"Exile_Item_EnergyDrink\",1],[\"30Rnd_65x39_caseless_green_mag_Tracer\",30],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_Bandage\",1],[\"Exile_Item_Bandage\",1]]', '[]', '2018-10-24 23:08:05'),
(41, 'Techno Viking', '76561197988232530', 0, 0, 100, 100, 0, 37, 0, 1, 0, '[[\"face_hub\",0],[\"neck\",0],[\"head\",0],[\"pelvis\",0],[\"spine1\",0],[\"spine2\",0],[\"spine3\",0],[\"body\",0],[\"arms\",0],[\"hands\",0],[\"legs\",0],[\"body\",0]]', 273.254242, 5230.828125, 8440.516602, 0.000000086965, '2018-10-24 23:10:40', '[\"ItemMap\",\"ItemCompass\",\"Exile_Item_XM8\",\"ItemRadio\"]', '', '[]', '[]', '[]', '', '', '[\"\",\"\",\"\",\"\"]', 'hgun_Rook40_F', '', '', '[[\"hgun_Rook40_F\",\"16Rnd_9x21_Mag\",16,\"hgun_Rook40_F\"]]', '', '[\"\",\"\",\"\",\"\"]', '', '[\"\",\"\",\"\",\"\"]', 'Exile_Uniform_BambiOverall', '[]', '[[\"Exile_Item_PlasticBottleFreshWater\",1]]', '[]', '', '[]', '[]', '[]', '2018-10-24 23:10:40');

-- --------------------------------------------------------

--
-- Table structure for table `player_history`
--

CREATE TABLE `player_history` (
  `id` int(11) UNSIGNED NOT NULL,
  `account_uid` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `died_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `position_x` double NOT NULL,
  `position_y` double NOT NULL,
  `position_z` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `player_history`
--

INSERT INTO `player_history` (`id`, `account_uid`, `name`, `died_at`, `position_x`, `position_y`, `position_z`) VALUES
(2, '76561198153523968', 'YoSwagi', '2018-10-22 18:59:58', 12419.1, 12685.4, 8.4557),
(3, '76561198026891445', 'Elchsocke', '2018-10-22 19:30:06', 6464.05, 13030.8, 0.00206757),
(4, '76561198026891445', 'Elchsocke', '2018-10-22 19:30:35', 6417.04, 12835.3, 0.735114),
(5, '76561198026891445', 'Elchsocke', '2018-10-22 19:42:10', 6941.36, 13171.1, 0.00270081),
(6, '76561198026891445', 'Elchsocke', '2018-10-22 19:52:17', 6948.11, 13203.2, 0.0026741),
(7, '76561198026891445', 'Elchsocke', '2018-10-22 20:14:46', 6762.87, 13190, 0.00201797),
(8, '76561197988232530', 'Techno Viking', '2018-10-22 20:53:00', 7759.72, 11126.4, 0.000610352),
(9, '76561198153523968', 'YoSwagi', '2018-10-22 21:05:09', 8252.78, 9177.6, 0.00156021),
(10, '76561197973996366', 'Barrazal', '2018-10-22 22:22:43', 13781.5, 8419.46, 0.00125408),
(11, '76561197973996366', 'Barrazal', '2018-10-22 22:49:43', 13716.4, 9356.32, 0.00157166),
(12, '76561197973996366', 'Barrazal', '2018-10-22 22:55:45', 14136.2, 8719.18, 0.00120926),
(13, '76561198026891445', 'Elchsocke', '2018-10-22 23:24:05', 7529.56, 7616.01, 0.00200701),
(14, '76561197990557284', 'Phoenix', '2018-10-23 18:31:50', 11113.1, 8396.55, -0.000350952),
(15, '76561198026891445', 'Elchsocke', '2018-10-23 21:12:19', 10791.5, 6517.23, 0.00143886),
(16, '76561198026891445', 'Elchsocke', '2018-10-24 00:21:20', 7949.85, 8177.73, 0.00212097),
(17, '76561198086210494', 'DrSnuggles', '2018-10-24 13:36:15', 10009.1, 9924.03, 0.00125122),
(18, '76561198086210494', 'DrSnuggles', '2018-10-24 16:22:08', 9559.24, 9132.76, 0.0256042),
(19, '76561198153523968', 'YoSwagi', '2018-10-24 16:31:34', 9025.75, 9294.58, 0.00100708),
(20, '76561198153523968', 'YoSwagi', '2018-10-24 16:35:51', 5949.88, 9088.3, 0.8629),
(21, '76561198041043249', 'XlrT | Mathias', '2018-10-24 18:56:51', 10826.6, 6314.65, 0.00145864),
(22, '76561198041043249', 'XlrT | Mathias', '2018-10-24 19:04:45', 10858.6, 6309.2, 0.00141096),
(23, '76561198026891445', 'Elchsocke', '2018-10-24 19:33:48', 4952.95, 4682.67, 0.00162816),
(24, '76561198041043249', 'XlrT | Mathias', '2018-10-24 19:40:49', 5096.31, 4458.34, 0.0014987),
(25, '76561198041043249', 'XlrT | Mathias', '2018-10-24 20:26:37', 5120.54, 4294.85, 0.00144005),
(26, '76561198153523968', 'YoSwagi', '2018-10-24 20:27:25', 5149.21, 4282.36, 0.000267029),
(27, '76561197988232530', 'Techno Viking', '2018-10-24 21:51:34', 11045.4, 8082.19, 0.0018692),
(28, '76561197988232530', 'Techno Viking', '2018-10-24 22:49:59', 7307.82, 8510.01, 1.9426),
(29, '76561198026891445', 'Elchsocke', '2018-10-24 22:51:31', 7300.39, 8510.84, 0.00177383),
(30, '76561197988232530', 'Techno Viking', '2018-10-24 23:10:07', 7453.58, 8685.7, 0.001719);

-- --------------------------------------------------------

--
-- Table structure for table `territory`
--

CREATE TABLE `territory` (
  `id` int(11) UNSIGNED NOT NULL,
  `owner_uid` varchar(32) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `position_x` double DEFAULT NULL,
  `position_y` double DEFAULT NULL,
  `position_z` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `flag_texture` varchar(255) DEFAULT NULL,
  `flag_stolen` tinyint(1) NOT NULL DEFAULT '0',
  `flag_stolen_by_uid` varchar(32) DEFAULT NULL,
  `flag_stolen_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_paid_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `xm8_protectionmoney_notified` tinyint(1) NOT NULL DEFAULT '0',
  `build_rights` varchar(640) NOT NULL DEFAULT '0',
  `moderators` varchar(320) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE `vehicle` (
  `id` int(11) UNSIGNED NOT NULL,
  `class` varchar(64) DEFAULT NULL,
  `spawned_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `account_uid` varchar(32) DEFAULT NULL,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `fuel` double UNSIGNED NOT NULL DEFAULT '0',
  `damage` double UNSIGNED NOT NULL DEFAULT '0',
  `hitpoints` text,
  `position_x` double NOT NULL DEFAULT '0',
  `position_y` double NOT NULL DEFAULT '0',
  `position_z` double NOT NULL DEFAULT '0',
  `direction_x` double NOT NULL DEFAULT '0',
  `direction_y` double NOT NULL DEFAULT '0',
  `direction_z` double NOT NULL DEFAULT '0',
  `up_x` double NOT NULL DEFAULT '0',
  `up_y` double NOT NULL DEFAULT '0',
  `up_z` double NOT NULL DEFAULT '1',
  `cargo_items` text,
  `cargo_magazines` text,
  `cargo_weapons` text,
  `cargo_container` text,
  `last_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pin_code` varchar(6) NOT NULL DEFAULT '000000',
  `deleted_at` datetime DEFAULT NULL,
  `money` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `vehicle_texture` text,
  `territory_id` int(11) UNSIGNED DEFAULT NULL,
  `nickname` varchar(64) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`id`, `class`, `spawned_at`, `account_uid`, `is_locked`, `fuel`, `damage`, `hitpoints`, `position_x`, `position_y`, `position_z`, `direction_x`, `direction_y`, `direction_z`, `up_x`, `up_y`, `up_z`, `cargo_items`, `cargo_magazines`, `cargo_weapons`, `cargo_container`, `last_updated_at`, `pin_code`, `deleted_at`, `money`, `vehicle_texture`, `territory_id`, `nickname`) VALUES
(7, 'CUP_C_Fishing_Boat_Chernarus', '2018-10-23 18:55:12', '76561197990557284', -1, 0.825445, 0, '[[\"hitengine\",0],[\"hithull\",0],[\"#svetlo\",0],[\"#deck_light_pos\",0]]', 8969.0517578, 4819.555176, 0.39432, -0.0122681, -0.99548, 0.0941772, -0.112484, 0.0949604, 0.989106, '[[],[]]', '[]', '[]', '[]', '2018-10-23 19:19:38', '8032', NULL, 0, NULL, NULL, ''),
(9, 'CUP_I_FENNEK_ION', '2018-10-23 20:46:04', '76561197990557284', -1, 0.952098, 0, '[[\"hitlfwheel\",0],[\"hitlf2wheel\",0],[\"hitrfwheel\",0],[\"hitrf2wheel\",0],[\"hitfuel\",0],[\"hithull\",0],[\"hitengine\",0],[\"hitbody\",0],[\"hitglass1\",0],[\"hitglass2\",0],[\"hitglass3\",0],[\"hitglass4\",0],[\"hitglass5\",0],[\"hitglass6\",0],[\"hitlbwheel\",0],[\"hitrbwheel\",0],[\"hitlmwheel\",0],[\"hitrmwheel\",0],[\"hitrglass\",0],[\"hitlglass\",0],[\"hitglass7\",0],[\"hitglass8\",0],[\"hitturret\",0],[\"hitgun\",0],[\"#light_l\",0],[\"#light_l\",0],[\"#light_r\",0],[\"#light_r\",0]]', 7432.543945, 8653.170898, -0.115496, 0.849426, 0.527688, 0.0045166, -0.00480531, -0.000823951, 0.999988, '[[\"Exile_Headgear_GasMask\"],[1]]', '[[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_EMRE\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_EMRE\",1],[\"Exile_Item_Bandage\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_EMRE\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleEmpty\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_Surstromming_Cooked\",1],[\"Exile_Item_Vishpirin\",1],[\"Exile_Item_PlasticBottleEmpty\",1],[\"Exile_Item_EMRE\",1],[\"Exile_Item_Surstromming_Cooked\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_CanOpener\",1],[\"Exile_Item_CanOpener\",1],[\"Exile_Item_Surstromming_Cooked\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_Surstromming_Cooked\",1],[\"Exile_Item_EMRE\",1],[\"Exile_Item_CatFood_Cooked\",1],[\"Exile_Item_PlasticBottleFreshWater\",1],[\"Exile_Item_PlasticBottleEmpty\",1],[\"Exile_Item_CanOpener\",1],[\"Exile_Item_Surstromming_Cooked\",1]]', '[]', '[]', '2018-10-24 23:01:17', '8032', NULL, 6655, NULL, NULL, ''),
(12, 'CUP_I_FENNEK_ION', '2018-10-24 15:18:12', '76561198086210494', -1, 0.726459, 0, '[[\"hitlfwheel\",0.00393701],[\"hitlf2wheel\",0.0275591],[\"hitrfwheel\",0.015748],[\"hitrf2wheel\",0.019685],[\"hitfuel\",0],[\"hithull\",0.019685],[\"hitengine\",0.023622],[\"hitbody\",0.00787402],[\"hitglass1\",0.019685],[\"hitglass2\",0.00393701],[\"hitglass3\",0.019685],[\"hitglass4\",0.0472441],[\"hitglass5\",0.015748],[\"hitglass6\",0.00787402],[\"hitlbwheel\",0.023622],[\"hitrbwheel\",0.0275591],[\"hitlmwheel\",0.0393701],[\"hitrmwheel\",0.0472441],[\"hitrglass\",0.023622],[\"hitlglass\",0.0314961],[\"hitglass7\",0.0314961],[\"hitglass8\",0.0472441],[\"hitturret\",0.00787402],[\"hitgun\",0.0354331],[\"#light_l\",0.019685],[\"#light_l\",0.019685],[\"#light_r\",0.0275591],[\"#light_r\",0.0275591]]', 7197.0678711, 6970.274414, -0.0824597, -0.967649, -0.252242, 0.00546112, 0.00541946, 0.000859903, 0.999985, '[[],[]]', '[[\"Exile_Item_ConcreteGateKit\",1],[\"Exile_Item_ConcreteGateKit\",1],[\"Exile_Item_ConcreteGateKit\",1],[\"Exile_Item_EMRE\",1],[\"Exile_Item_ConcreteWindowKit\",1],[\"Exile_Item_ConcreteWindowKit\",1],[\"Exile_Item_EMRE\",1],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_InstaDoc\",1],[\"Exile_Item_DuctTape\",1],[\"Exile_Item_DuctTape\",1]]', '[]', '[]', '2018-10-24 22:19:25', '8032', NULL, 0, NULL, NULL, ''),
(13, 'Exile_Car_Strider', '2018-10-24 16:41:06', 'DMS_PersistentVehicle', 0, 0.889336, 0, '[[\"hitlfwheel\",0.0472441],[\"hitlf2wheel\",0.0708661],[\"hitrfwheel\",0.0708661],[\"hitrf2wheel\",0.00393701],[\"hitfuel\",0],[\"hithull\",0.0472441],[\"hitengine\",0.015748],[\"hitbody\",0.106299],[\"hitglass1\",0.110236],[\"hitglass2\",0],[\"hitglass3\",0.0708661],[\"hitglass4\",0.0314961],[\"hitglass5\",0.023622],[\"hitglass6\",0.023622],[\"hitlbwheel\",0.0393701],[\"hitrbwheel\",0.023622],[\"hitlmwheel\",0.023622],[\"hitrmwheel\",0.0393701],[\"hitrglass\",0.00393701],[\"hitlglass\",0.0275591],[\"hitglass7\",0.0472441],[\"hitglass8\",0.011811],[\"hitturret\",0.0433071],[\"hitgun\",0.023622],[\"#light_l\",0.212598],[\"#light_l\",0.212598],[\"#light_r\",0.011811],[\"#light_r\",0.011811]]', 7311.352539, 8513.793945, -0.0955291, 0.587097, 0.809516, 0.000732422, -0.0130741, 0.00857729, 0.999878, '[[],[]]', '[[\"30Rnd_65x39_caseless_green\",30]]', '[]', '[]', '2018-10-24 22:50:48', '8032', NULL, 7961, NULL, NULL, ''),
(15, 'Exile_Chopper_Hummingbird_Civillian_Wasp', '2018-10-24 19:27:23', '76561198153523968', -1, 0.721546, 0, '[[\"hitfuel\",0],[\"hithull\",0],[\"hitengine\",0],[\"hitavionics\",0],[\"hithrotor\",0],[\"hitvrotor\",0],[\"hitglass1\",0],[\"hitglass2\",0],[\"hitglass3\",0],[\"hitglass4\",0],[\"hitglass5\",0],[\"hitglass6\",0],[\"hitengine1\",0],[\"hitengine2\",0],[\"hitmissiles\",0],[\"hitrglass\",0],[\"hitlglass\",0],[\"hitengine3\",0],[\"hitwinch\",0],[\"hittransmission\",0],[\"hitlight\",0],[\"hithydraulics\",0],[\"hitgear\",0],[\"hithstabilizerl1\",0],[\"hithstabilizerr1\",0],[\"hitvstabilizer1\",0],[\"hittail\",0],[\"hitpitottube\",0],[\"hitstaticport\",0],[\"hitstarter1\",0],[\"hitstarter2\",0],[\"hitstarter3\",0],[\"hitturret\",0],[\"hitgun\",0],[\"#light_hitpoint\",0]]', 7223.439941, 6991.400391, 0.00954509, 0.345764, -0.93832, 0.00152588, -0.00101124, 0.00125355, 0.999999, '[[],[]]', '[]', '[[\"Exile_Weapon_SA61\",\"\",\"\",\"\",[],\"\"]]', '[]', '2018-10-24 22:51:03', '8032', NULL, 0, NULL, NULL, ''),
(16, 'Exile_Boat_RubberDuck_Black', '2018-10-24 20:02:22', '76561198041043249', 0, 0.95207, 0, '[[\"hitbody\",0],[\"hitengine\",0]]', 4913.00634766, 5408.768555, 0.00211386, -0.554565, -0.82776, 0.0852661, 0.0374193, 0.0775562, 0.996285, '[[],[]]', '[]', '[]', '[]', '2018-10-24 20:07:49', '1337', NULL, 0, NULL, NULL, ''),
(18, 'Exile_Boat_MotorBoat_Police', '2018-10-24 21:49:00', '76561198153523968', 0, 0.967443, 0, '[[\"hithull\",0],[\"hitengine1\",0],[\"hitengine2\",0],[\"hitengine\",0],[\"hitglass1\",0],[\"hitglass3\",0],[\"hitglass2\",0]]', 4509.223145, 5051.566895, -0.0219241, 0.599182, -0.800137, -0.0275879, -0.0452326, -0.0682356, 0.996643, '[[],[]]', '[]', '[]', '[]', '2018-10-24 21:54:45', '8032', NULL, 0, NULL, NULL, ''),
(19, 'CUP_C_S1203_Militia_CIV', '2018-10-24 21:49:31', '76561197988232530', 0, 0.977044, 0, '[[\"hitlfwheel\",0],[\"hitlf2wheel\",0],[\"hitrfwheel\",0],[\"hitrf2wheel\",0],[\"hitfuel\",0],[\"hitengine\",0],[\"hitbody\",0],[\"hitglass1\",0],[\"hitglass2\",0],[\"hitglass3\",0],[\"hitglass4\",0],[\"hitglass5\",0],[\"hitglass6\",0],[\"hitlbwheel\",0],[\"hitrbwheel\",0],[\"hitlmwheel\",0],[\"hitrmwheel\",0],[\"hitrglass\",0],[\"hitlglass\",0],[\"hithull\",0],[\"hitglass7\",0],[\"hitglass8\",0],[\"#l svetlo\",0],[\"#p svetlo\",0]]', 12198.410156, 8161.40332, -0.0175231, -0.0391846, 0.997728, 0.0548096, -0.0134442, -0.0553732, 0.998375, '[[],[]]', '[]', '[]', '[]', '2018-10-24 22:39:44', '1991', NULL, 0, NULL, NULL, ''),
(20, 'CUP_I_LSV_02_unarmed_ION', '2018-10-24 22:44:20', '76561197973996366', 0, 0.935081, 0, '[[\"hitlfwheel\",0],[\"hitlf2wheel\",0],[\"hitrfwheel\",0.015748],[\"hitrf2wheel\",0],[\"hitfuel\",0],[\"hithull\",0],[\"hitengine\",0.0708661],[\"hitbody\",0.122047],[\"hitglass1\",0],[\"hitglass2\",0],[\"hitglass3\",0],[\"hitglass4\",0],[\"hitglass5\",0],[\"hitglass6\",0],[\"hitlbwheel\",0],[\"hitrbwheel\",0],[\"hitlmwheel\",0],[\"hitrmwheel\",0],[\"hitrglass\",0],[\"hitlglass\",0],[\"hitglass7\",0],[\"hitglass8\",0],[\"#light_1\",0.023622],[\"#light_2\",0.440945]]', 7973.266602, 8500.216797, 0.311387, 0.452271, 0.872119, 0.186707, -0.416797, 0.0215988, 0.908743, '[[],[]]', '[]', '[]', '[]', '2018-10-24 23:08:20', '2172', NULL, 0, NULL, NULL, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`uid`),
  ADD KEY `clan_id` (`clan_id`);

--
-- Indexes for table `clan`
--
ALTER TABLE `clan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leader_uid` (`leader_uid`);

--
-- Indexes for table `clan_map_marker`
--
ALTER TABLE `clan_map_marker`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clan_id` (`clan_id`);

--
-- Indexes for table `construction`
--
ALTER TABLE `construction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_uid` (`account_uid`),
  ADD KEY `territory_id` (`territory_id`);

--
-- Indexes for table `container`
--
ALTER TABLE `container`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_uid` (`account_uid`),
  ADD KEY `territory_id` (`territory_id`);

--
-- Indexes for table `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`id`),
  ADD KEY `player_uid` (`account_uid`);

--
-- Indexes for table `player_history`
--
ALTER TABLE `player_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `territory`
--
ALTER TABLE `territory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `owner_uid` (`owner_uid`),
  ADD KEY `flag_stolen_by_uid` (`flag_stolen_by_uid`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_uid` (`account_uid`),
  ADD KEY `vehicle_ibfk_2_idx` (`territory_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clan`
--
ALTER TABLE `clan`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `clan_map_marker`
--
ALTER TABLE `clan_map_marker`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `construction`
--
ALTER TABLE `construction`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `container`
--
ALTER TABLE `container`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `player`
--
ALTER TABLE `player`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `player_history`
--
ALTER TABLE `player_history`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `territory`
--
ALTER TABLE `territory`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account`
--
ALTER TABLE `account`
  ADD CONSTRAINT `account_ibfk_1` FOREIGN KEY (`clan_id`) REFERENCES `clan` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `clan`
--
ALTER TABLE `clan`
  ADD CONSTRAINT `clan_ibfk_1` FOREIGN KEY (`leader_uid`) REFERENCES `account` (`uid`) ON DELETE CASCADE;

--
-- Constraints for table `clan_map_marker`
--
ALTER TABLE `clan_map_marker`
  ADD CONSTRAINT `clan_map_marker_ibfk_1` FOREIGN KEY (`clan_id`) REFERENCES `clan` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `construction`
--
ALTER TABLE `construction`
  ADD CONSTRAINT `construction_ibfk_1` FOREIGN KEY (`account_uid`) REFERENCES `account` (`uid`) ON DELETE CASCADE,
  ADD CONSTRAINT `construction_ibfk_2` FOREIGN KEY (`territory_id`) REFERENCES `territory` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `container`
--
ALTER TABLE `container`
  ADD CONSTRAINT `container_ibfk_1` FOREIGN KEY (`account_uid`) REFERENCES `account` (`uid`) ON DELETE CASCADE,
  ADD CONSTRAINT `container_ibfk_2` FOREIGN KEY (`territory_id`) REFERENCES `territory` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `player`
--
ALTER TABLE `player`
  ADD CONSTRAINT `player_ibfk_1` FOREIGN KEY (`account_uid`) REFERENCES `account` (`uid`) ON DELETE CASCADE;

--
-- Constraints for table `territory`
--
ALTER TABLE `territory`
  ADD CONSTRAINT `territory_ibfk_1` FOREIGN KEY (`owner_uid`) REFERENCES `account` (`uid`) ON DELETE CASCADE,
  ADD CONSTRAINT `territory_ibfk_2` FOREIGN KEY (`flag_stolen_by_uid`) REFERENCES `account` (`uid`) ON DELETE SET NULL;

--
-- Constraints for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD CONSTRAINT `vehicle_ibfk_1` FOREIGN KEY (`account_uid`) REFERENCES `account` (`uid`) ON DELETE CASCADE,
  ADD CONSTRAINT `vehicle_ibfk_2` FOREIGN KEY (`territory_id`) REFERENCES `territory` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
