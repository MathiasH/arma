_obelisk = createVehicle ["Land_Grave_obelisk_F", [4472.8481, 8752.3516, 5.7112436], [], 0, "CAN_COLLIDE"];
_obelisk allowDamage false;
_obelisk enableDynamicSimulation false;
_obelisk enableSimulationGlobal false;
_obelisk setPosWorld [4472.8481, 8752.3516, 5.7112436];
_obelisk setDir 180;
_sign = createVehicle ["Exile_Sign_Vehicles_Small", [4472.8462, 8751.7041, 3.3470936], [], 0, "CAN_COLLIDE"];
_sign setObjectTextureGlobal [0, "Exile_Sign_ReginaMemorial_co.paa"];
_sign allowDamage false;
_sign enableDynamicSimulation false;
_sign enableSimulationGlobal false;
_sign setPosWorld [4472.8462, 8751.7041, 3.3470936];
_sign setDir 0; 
_objects =
[
    ["Land_WoodenTable_large_F", [11033.2, 9606.35, 101.423], 0, false]
];
{
    private ["_object"];

    _object = (_x select 0) createVehicle [0,0,0];
    _object setDir (deg (_x select 2));
    _object setPosWorld (_x select 1);
    _object allowDamage false;
    _object enableDynamicSimulation (_x select 3);
}
forEach _objects;
ExileRouletteChairs = [];
ExileRouletteChairPositions = 
[
    [[11033.3, 9604.83, 101.002], 3.13798],
	[[11034.2, 9605.91, 101.025], 1.94934],
	[[11034.2, 9607.01, 100.999], 1.44236],
	[[11032.2, 9606.94, 100.957], 4.16024],
	[[11032.1, 9605.92, 100.962], 4.18024]	
];
{
    private ["_chair"];
    _chair = "Exile_RussianRouletteChair" createVehicle [0, 0, 0];
    _chair setDir (deg (_x select 1));
    _chair setPosWorld (_x select 0);
    _chair allowDamage false;
    _chair enableDynamicSimulation true;
    ExileRouletteChairs pushBack _chair;
}
forEach ExileRouletteChairPositions;
_concreteMixers = 
[
	[[8817.02,6612.5,5.33936],  35],
	[[8815.86,6617.9,5.33961],  90],
	[[13274.8,12571.3,2.55], -40],
	[[13269.8,12571.8,2.55], -40]
];
{
	_concreteMixer = "Exile_ConcreteMixer" createVehicle (_x select 0);
	_concreteMixer setDir (_x select 1);
	_concreteMixer setPosASL (_x select 0);
    _concreteMixer allowDamage false;
    _concreteMixer enableDynamicSimulation true;
}
forEach _concreteMixers;