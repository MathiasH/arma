if (!hasInterface || isServer) exitWith {};
profileNamespace setVariable ["connectedtime",diag_tickTime];
enableEnvironment [false, true];
"ColorCorrections" ppEffectEnable true;
"ColorCorrections" ppEffectAdjust [0.88, 0.88, 0, [0.2, 0.29, 0.4, -0.22], [1, 1, 1, 1.3], [0.15, 0.09, 0.09, 0.0]];
"ColorCorrections" ppEffectCommit 0;
_objs1 = nearestTerrainObjects [[11834,11886.1,0.00135803], ["Tree","Bush"], 20];  
{ _x hideObject true; } forEach _objs1;
_objs2 = nearestTerrainObjects [[8171.16,8412.29,0], ["Tree","Bush"], 14];
{ _x hideObject true; } forEach _objs2;
_objs3 = nearestTerrainObjects [[11012.3,9611.46,0], ["RAILWAY"], 150]; 
{ _x hideObject true; } forEach _objs3;
_objs4 = nearestTerrainObjects [[11012.3,9611.46,0], ["Tree","Bush"], 50]; 
{ _x hideObject true; } forEach _objs4;
/*_objs5 = nearestTerrainObjects [[7181.8,4242.22,0], ["BUILDING","HOUSE"],3];
{ _x hideObject true; } forEach _objs5;
_objs6 = nearestTerrainObjects [[7170.82,4253.47,0], ["BUILDING","HOUSE"],5];
{ _x hideObject true; } forEach _objs6;
_objs7 = nearestTerrainObjects [[8796.71,4007.46,0], ["Tree","Bush"], 5];  
{ _x hideObject true; } forEach _objs7;*/
_trader = 
[
    "Exile_Trader_Vehicle",
    "CONLAND",
    "WhiteHead_11",
    ["HubStandingUA_move1", "HubStandingUA_move2", "HubStandingUA_idle1", "HubStandingUA_idle2", "HubStandingUA_idle3"],
    [6143.14,11156.3,0.292451],
    299.747
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_RussianRoulette",
	"Exile_Trader_RussianRoulette",
    "GreekHead_A3_07",
    ["HubStandingUC_idle1"],
    [11031.9,9609.09,-0.00144958],
    134.326
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_Aircraft",
	"Exile_Trader_Aircraft",
    "WhiteHead_21",
    ["LHD_krajPaluby"],
    [11449.6,13232.2,0.00143886],
    118.769
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_AircraftCustoms",
	"Exile_Trader_AircraftCustoms",
    "WhiteHead_21",
    ["HubStandingUC_idle1", "HubStandingUC_idle2", "HubStandingUC_idle3", "HubStandingUC_move1", "HubStandingUC_move2"],
    [11446.3,13223.3,0.027277],
    87.3676
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_WasteDump",
	"Exile_Trader_WasteDump",
    "WhiteHead_04",
    ["HubStandingUA_move1", "HubStandingUA_move2", "HubStandingUA_idle1", "HubStandingUA_idle2", "HubStandingUA_idle3"],
    [4561.66,8225.45,0.00117517],
    250.303
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_WasteDump",
	"Exile_Trader_WasteDump",
    "WhiteHead_04",
    ["HubStandingUA_move1", "HubStandingUA_move2", "HubStandingUA_idle1", "HubStandingUA_idle2", "HubStandingUA_idle3"],
    [7047.76,4112.16,0.644354],
    210.461
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_WasteDump",
	"Exile_Trader_WasteDump",
    "WhiteHead_03",
    ["HubStandingUA_move1", "HubStandingUA_move2", "HubStandingUA_idle1", "HubStandingUA_idle2", "HubStandingUA_idle3"],
    [11452.5,13244.6,0.0273952],
    86.9522
]
call ExileClient_object_trader_create;
_trader =  
[ 
    "Exile_Trader_Aircraft",
    "CONAIR",
    "WhiteHead_19",
    ["HubStandingUA_move1", "HubStandingUA_move2", "HubStandingUA_idle1", "HubStandingUA_idle2", "HubStandingUA_idle3"],
    [12172.1,12960.6,0.080934],
    141.767
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_Aircraft",
	"Exile_Trader_Aircraft",
    "WhiteHead_21",
    ["LHD_krajPaluby"],
    [1871.37,3466.01,0.00143886],
    176.84
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_AircraftCustoms",
	"Exile_Trader_AircraftCustoms",
    "WhiteHead_21",
    ["HubStandingUC_idle1", "HubStandingUC_idle2", "HubStandingUC_idle3", "HubStandingUC_move1", "HubStandingUC_move2"],
    [1862.1,3464.07,0.00143909],
    145.439
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_WasteDump",
	"Exile_Trader_WasteDump",
   "WhiteHead_03",
    ["HubStandingUA_move1", "HubStandingUA_move2", "HubStandingUA_idle1", "HubStandingUA_idle2", "HubStandingUA_idle3"],
    [1883.46,3470.12,0.00143909],
    145.023
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_Boat",
	"Exile_Trader_Boat",
    "WhiteHead_09",
    ["HubBriefing_scratch", "HubBriefing_stretch", "HubBriefing_think", "HubBriefing_lookAround1", "HubBriefing_lookAround2"],
    [4566.77,8212.85,1.04836],
    288.259
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_Boat",
	"Exile_Trader_Boat",
    "AfricanHead_03",
    ["HubBriefing_scratch", "HubBriefing_stretch", "HubBriefing_think", "HubBriefing_lookAround1", "HubBriefing_lookAround2"],
    [11437.4,13288.4,0.0122495],
    279.405
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_Boat",
	"Exile_Trader_Boat",
    "WhiteHead_19",
    ["HubBriefing_scratch", "HubBriefing_stretch", "HubBriefing_think", "HubBriefing_lookAround1", "HubBriefing_lookAround2"],
    [3104.76,10942.1,9.87938],
    320.889
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_BoatCustoms",
	"Exile_Trader_BoatCustoms",
    "WhiteHead_03",
    ["HubStanding_idle1", "HubStanding_idle2", "HubStanding_idle3"],
    [11433.6,13285.4,0.00667554],
    0
]
call ExileClient_object_trader_create;
_trader = 
[
	"Exile_Trader_CommunityCustoms",
	"Exile_Trader_CommunityCustoms",
	"WhiteHead_16",
	["HubStanding_idle1", "HubStanding_idle2", "HubStanding_idle3"],
	[6138.55,11159,0.109539],
	122.788
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_Hardware",
	"Exile_Trader_Hardware",
    "WhiteHead_17",
    ["HubStanding_idle1", "HubStanding_idle2", "HubStanding_idle3"],
    [11041.1,9615.17,-0.0027771],
    223.217
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_Food",
	"Exile_Trader_Food",
    "GreekHead_A3_01",
    ["HubBriefing_scratch", "HubBriefing_stretch", "HubBriefing_think", "HubBriefing_lookAround1", "HubBriefing_lookAround2"],
    [11031.7,9618.13,-0.00106812],
    178.528
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_Armory",
	"Exile_Trader_Armory",
    "PersianHead_A3_02",
    ["HubStanding_idle1", "HubStanding_idle2", "HubStanding_idle3"],
    [11042.7,9600.44,0.00196838],
    271.421
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_Equipment",
	"Exile_Trader_Equipment",
    "WhiteHead_19",
    ["HubBriefing_scratch", "HubBriefing_stretch", "HubBriefing_think", "HubBriefing_lookAround1", "HubBriefing_lookAround2"],
    [11034.6,9595.24,0.00289154],
    356.797
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_SpecialOperations",
	"Exile_Trader_SpecialOperations",
    "AfricanHead_02",
    ["HubStanding_idle1", "HubStanding_idle2", "HubStanding_idle3"],
    [11023.7,9614.18,0.015],
    112.388
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_Office",
	"Exile_Trader_Office",
    "GreekHead_A3_04",
    ["HubBriefing_scratch", "HubBriefing_stretch", "HubBriefing_think", "HubBriefing_lookAround1", "HubBriefing_lookAround2"],
    [11025.4,9597.97,0.009758],
    38.1943
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_WasteDump",
	"Exile_Trader_WasteDump",
    "GreekHead_A3_05",
    ["HubStandingUA_move1", "HubStandingUA_move2", "HubStandingUA_idle1", "HubStandingUA_idle2", "HubStandingUA_idle3"],
    [11048.8,9615.2,-0.00462341],
    51.3646
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_WasteDump",
	"Exile_Trader_WasteDump",
    "GreekHead_A3_01",
    ["HubStandingUA_move1", "HubStandingUA_move2", "HubStandingUA_idle1", "HubStandingUA_idle2", "HubStandingUA_idle3"],
    [3100.49,10941.8,9.26747],
    8.61019
]
call ExileClient_object_trader_create;
_trader =
[
    "Exile_Trader_Vehicle",
	"Exile_Trader_Vehicle",
    "WhiteHead_11",
    ["HubBriefing_scratch", "HubBriefing_stretch", "HubBriefing_think", "HubBriefing_lookAround1", "HubBriefing_lookAround2"],
    [11019.5,9617.65,-0.00325775],
    321.354
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_VehicleCustoms",
	"Exile_Trader_VehicleCustoms",
    "WhiteHead_11",
    ["AidlPercMstpSnonWnonDnon_G01", "AidlPercMstpSnonWnonDnon_G02", "AidlPercMstpSnonWnonDnon_G03", "AidlPercMstpSnonWnonDnon_G04", "AidlPercMstpSnonWnonDnon_G05", "AidlPercMstpSnonWnonDnon_G06"],
    [11022.8,9620.76,0.00012207],
    315.859
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_Hardware",
	"Exile_Trader_Hardware",
    "WhiteHead_17",
    ["HubBriefing_scratch", "HubBriefing_stretch", "HubBriefing_think", "HubBriefing_lookAround1", "HubBriefing_lookAround2"],
    [4534.52,8749.4,0.0155504],
    223.217
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_Food",
	"Exile_Trader_Food",
    "GreekHead_A3_01",
    ["HubBriefing_scratch", "HubBriefing_stretch", "HubBriefing_think", "HubBriefing_lookAround1", "HubBriefing_lookAround2"],
    [4525.11,8752.36,0.012965],
    178.528
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_Armory",
	"Exile_Trader_Armory",
    "PersianHead_A3_02",
    ["HubBriefing_scratch", "HubBriefing_stretch", "HubBriefing_think", "HubBriefing_lookAround1", "HubBriefing_lookAround2"],
    [4536.12,8734.67,-0.00884438],
    271.421
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_Equipment",
	"Exile_Trader_Equipment",
    "WhiteHead_19",
    ["HubStanding_idle1", "HubStanding_idle2", "HubStanding_idle3"],
    [4528.03,8729.46,0.0125353],
    356.797
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_SpecialOperations",
	"Exile_Trader_SpecialOperations",
    "AfricanHead_02",
    ["HubStanding_idle1", "HubStanding_idle2", "HubStanding_idle3"],
    [4517.58,8748.51,0.00198698],
    112.388
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_Office",
	"Exile_Trader_Office",
    "GreekHead_A3_04",
    ["HubBriefing_scratch", "HubBriefing_stretch", "HubBriefing_think", "HubBriefing_lookAround1", "HubBriefing_lookAround2"],
    [4518.81,8732.2,0.020154],
    38.1943
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_WasteDump",
	"Exile_Trader_WasteDump",
    "GreekHead_A3_01",
    ["HubStandingUA_move1", "HubStandingUA_move2", "HubStandingUA_idle1", "HubStandingUA_idle2", "HubStandingUA_idle3"],
    [4542.24,8749.43,0.00758386],
    51.3646
]
call ExileClient_object_trader_create;
_trader =
[
    "Exile_Trader_Vehicle",
	"Exile_Trader_Vehicle",
    "WhiteHead_11",
    ["HubStanding_idle1", "HubStanding_idle2", "HubStanding_idle3"],
    [4512.93,8751.88,-0.00256729],
    321.354
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_VehicleCustoms",
	"Exile_Trader_VehicleCustoms",
    "WhiteHead_11",
    ["AidlPercMstpSnonWnonDnon_G01", "AidlPercMstpSnonWnonDnon_G02", "AidlPercMstpSnonWnonDnon_G03", "AidlPercMstpSnonWnonDnon_G04", "AidlPercMstpSnonWnonDnon_G05", "AidlPercMstpSnonWnonDnon_G06"],
    [4516.23,8754.99,0.00869727],
    315.859
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_Boat",
	"Exile_Trader_Boat",
    "PersianHead_A3_02",
    ["HubBriefing_scratch", "HubBriefing_stretch", "HubBriefing_think", "HubBriefing_lookAround1", "HubBriefing_lookAround2"],
    [7043.31,4072.36,1.87691],
    63.6064
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_BoatCustoms",
	"Exile_Trader_BoatCustoms",
    "GreekHead_A3_01",
    ["HubStanding_idle1", "HubStanding_idle2", "HubStanding_idle3"],
    [7024.94,4073.84,1.63222],
    279.254
]
call ExileClient_object_trader_create;
_trader = 
[
    "Exile_Trader_Vehicle",
    "CONLAND",
    "WhiteHead_11",
    ["HubStandingUA_move1", "HubStandingUA_move2", "HubStandingUA_idle1", "HubStandingUA_idle2", "HubStandingUA_idle3"],
    [13382.8,7505.73,0.585738],
    227.594
]
call ExileClient_object_trader_create;
_trader = 
[
	"Exile_Trader_CommunityCustoms",
	"Exile_Trader_CommunityCustoms",
	"WhiteHead_16",
	["HubStanding_idle1", "HubStanding_idle2", "HubStanding_idle3"],
	[13383.1,7502.15,0.657474],
	288.172
]
call ExileClient_object_trader_create;
ExileClientXM8IsPowerOn = true;
private["_code","_function","_file"];
{
    _code = '';
    _function = _x select 0;
    _file = _x select 1;
    _code = compileFinal (preprocessFileLineNumbers _file);
    missionNamespace setVariable [_function, _code];
}
forEach
[
    ["SM_Paint","Custom\BasePainting\Paint\SM_Paint.sqf"],
    ["SM_Paint_Finish","Custom\BasePainting\Paint\SM_Paint_Finish.sqf"],
    ["SM_Paint_Config","Custom\BasePainting\Paint\SM_Paint_Config.sqf"],
    ["SM_Paint_OnLBChange","Custom\BasePainting\Paint\SM_Paint_OnLBChange.sqf"],
    ["SM_Paint_ChangeMode","Custom\BasePainting\Paint\SM_Paint_ChangeMode.sqf"],
    ["ExileClient_MGT_network_incomingMissile","Custom\IncomingMissile\ExileClient_MGT_network_incomingMissile.sqf"]
];
[] call SM_Paint_Config;
[] execVM "paintshop\paintshop.sqf";
[] execVM "VirtualGarage\VirtualGarage_Client_Init.sqf";
[] execVM "MostWanted_Client\MostWanted_Init.sqf";
[] execVM "MarXet\MarXet_Init.sqf";
TFS_gui_modelBoxPreview_update = compileFinal preprocessFileLineNumbers "TRADERS\TFS\3DPreview\code\TFS_gui_modelBoxPreview_update.sqf";
TFS_gui_onPreviewButtonClick = compileFinal preprocessFileLineNumbers "TRADERS\TFS\3DPreview\code\TFS_gui_onPreviewButtonClick.sqf";
TFS_gui_modelBoxPreview_createUnit = compileFinal preprocessFileLineNumbers "TRADERS\TFS\3DPreview\code\TFS_gui_modelBoxPreview_createUnit.sqf";
if ((getPlayerUID player) in sideblocked) then
{
	1 enableChannel false;
	["ErrorTitleAndText", ["Side Chat", "You have temporarily been blocked from using side chat"]] call ExileClient_gui_toaster_addTemplateToast;
};
_paycheque = compileFinal preprocessFileLineNumbers "addons\custom\EXILE\painting.sqf";
[300, _paycheque, [], true] call ExileClient_system_thread_addtask;
[] call compileFinal preprocessFileLineNumbers "Exile_TFS_logistics\Exile_TFS_Logistics_Init.sqf";
[] call compileFinal preprocessFileLineNumbers "addons\restart\restart.sqf";
[] call compileFinal preprocessFileLineNumbers "Custom\MGTVectorBuilding\MGTVectorBuilding_Init.sqf";
[] call compileFinal preprocessFileLineNumbers "addons\welcome\welcome.sqf";
[] call compileFinal preprocessFileLineNumbers "Custom\TerritoryAlert\territory_alert.sqf";
[] call compileFinal preprocessFileLineNumbers "addons\custom\EXILE\DisableThermal.sqf";