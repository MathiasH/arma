waitUntil {if(!(isNull(findDisplay 46))&&((typeOf player)isEqualTo "Exile_Unit_Player"))then{true}else{uiSleep 0.5; false}};
_skills = "true" configClasses (missionConfigFile >> "CfgSkills" >> "Skills");
{
    _skillName = configName _x;

    _skillInitializeFunction = missionNamespace getVariable [format ["ExileClient_Skills_%1_initialize", _skillName], ""];
	if !(_skillInitializeFunction isEqualTo "") then
	{
		call _skillInitializeFunction;
	};
} forEach _skills;