_blck_clientVersion = "6.72 Build 82";
blck_fnc_spawnMarker = compileFinal preprocessfilelinenumbers "debug\spawnMarker.sqf";
blck_fnc_deleteMarker = compileFinal preprocessfilelinenumbers "debug\deleteMarker.sqf";
blck_fnc_missionCompleteMarker = compileFinal preprocessfilelinenumbers "debug\missionCompleteMarker.sqf";
if !(isServer) then
{
	blck_MarkerPeristTime = 300;  
	blck_useHint = false;
	blck_useSystemChat = false;
	blck_useTitleText = false;
	blck_useDynamic = true;
	blck_useToast = false;
	blck_aiKilluseSystemChat = false;
	blck_aiKilluseDynamic = false;
	blck_aiKilluseTitleText = false;
	blck_processingMsg = -1;
	blck_processingKill = -1;
	blck_message = "";
	fn_killScoreNotification = {
		params["_bonus","_distanceBonus","_killStreak"];
		_msg2 = format["<t color ='#7CFC00' size = '1.4' align='right'>AI Killed</t><br/>"];
		playSound "FD_CP_Clear_F";
		if (typeName _bonus isEqualTo "SCALAR") then
		{
			if (_bonus > 0) then 
			{
				_msg2 = _msg2 + format["<t color = '#7CFC00' size ='1.4' align='right'>Bonus <t color = '#ffffff'>+%1<br/>",_bonus];
			};
		};
		if (typeName _distanceBonus isEqualTo "SCALAR") then
		{
			if (_distanceBonus > 0) then
			{
				_msg2 = _msg2 + format["<t color = '#7CFC00' size = '1.4' align = 'right'>Dist Bonus<t color = '#ffffff'> +%1<br/>",_distanceBonus];
			};
		};
		if (typeName _killStreak isEqualTo "SCALAR") then
		{
			if (_killStreak > 0) then
			{
				_msg2 = _msg2 + format["<t color = '#7CFC00' size = '1.4' align = 'right'>Killstreak <t color = '#ffffff'>%1X<br/>",_killStreak];
			};
		};
		[parseText _msg2,[0.0823437 * safezoneW + safezoneX,0.379 * safezoneH + safezoneY,0.0812109 * safezoneW,0.253 * safezoneH], nil, 7, 0.3, 0] spawn BIS_fnc_textTiles;	
	};
	fn_dynamicNotification = {
			private["_text","_screentime","_xcoord","_ycoord"];
			params["_mission","_message"];
			
			waitUntil {blck_processingMsg < 0};
			blck_processingMsg = 1;
			_screentime = 10;
			_text = format[
				"<t align='left' size='0.8' color='#3FD4FC'>%1</t><br/><br/>
				<t align='left' size='0.6' color='#F0F0F0'>%2</t><br/>",
				_mission,_message
				];
			_ycoord = [safezoneY + safezoneH - 0.8,0.7];
			_xcoord = [safezoneX + safezoneW - 0.5,0.35];
			[_text,_xcoord,_ycoord,_screentime,0.5] spawn BIS_fnc_dynamicText;
			uiSleep 3;
			blck_processingMsg = -1;
	};
	fn_missionNotification = {
		params["_event","_message","_mission"];
		if (blck_useSystemChat) then {systemChat format["%1",_message];};
		if (blck_useHint) then {
			hint parseText format[
			"<t align='center' size='2.0' color='#f29420'>%1</t><br/>
			<t size='1.5' color='#01DF01'></t><br/><br/>
			<t size='1.5' color='#ffff00'>%2</t><br/>
			<t size='1.5' color='#01DF01'></t><br/><br/>
			<t size='1.5' color='#FFFFFF'>Any loot you find is yours as payment for eliminating the threat</t>",_mission,_message
			];	
		};
		if (blck_useDynamic) then {
			[_mission,_message] call fn_dynamicNotification;
		};		
		if (blck_useTitleText) then {
			[_message] spawn {
				params["_msg"];
				titleText [_msg, "PLAIN DOWN",5];uiSleep 5; titleText ["", "PLAIN DOWN",5]
			};
		};
		if (blck_useToast) then
		{
			["InfoTitleAndText", [_mission, _message]] call ExileClient_gui_toaster_addTemplateToast;
		};
	};
	fn_AI_KilledNotification = {
		private["_message","_text","_screentime","_xcoord","_ycoord"];
		_message = _this select 0;
		if (blck_aiKilluseSystemChat) then {systemChat format["%1",_message];};
		if (blck_aiKilluseTitleText) then {titleText [_message, "PLAIN DOWN",5];uiSleep 5; titleText ["", "PLAIN DOWN",5]};
		if (blck_aiKilluseDynamic) then {
			waitUntil{blck_processingKill < 0};
			blck_processingKill = 1;
			_text = format["<t align='left' size='0.5' color='#3FD4FC'>%1</t>",_message];
			_xcoord = [safezoneX,0.8];
			_ycoord = [safezoneY + safezoneH - 0.5,0.2];
			_screentime = 5;
			["   "+ _text,_xcoord,_ycoord,_screentime] spawn BIS_fnc_dynamicText;	
			uiSleep 3;
			blck_processingKill = -1;
		};
	};
	fn_handleMessage = {
		diag_log format["fn_handleMessage ====]  Paremeters = _this = %1",_this];
		params["_event","_message",["_mission",""]];
		switch (_event) do 
		{
			case "start": 
			{
				playSound "UAV_05";
				[_event,_message,_mission] spawn fn_missionNotification;
			};
			case "end": 
			{
				playSound "UAV_03";
				[_event,_message,_mission] spawn fn_missionNotification;
			};
			case "aikilled": 
			{
				[_message] spawn fn_AI_KilledNotification;
			};
			case "DLS":
			{
				if ( (player distance _mission) < 1000) then {playsound "AddItemOK"; hint _message;systemChat _message};
			};	
			case "reinforcements":
			{
				if ( (player distance _mission) < 1000) then {playsound "AddItemOK"; ["Alert",_message] call fn_missionNotification;};
			};
			case "IED":
			{
				playSound "Alarm";
				["IED","Bandit Grenades Detonated Under Your Vehicle","Nearby Explosion"] call fn_missionNotification;
				[1] call BIS_fnc_Earthquake;
				for "_i" from 1 to 3 do {playSound "BattlefieldExplosions3_3D";uiSleep 0.3;};
			};
			case "showScore":
			{
				[_message select 0, _message select 1, _message select 2] call fn_killScoreNotification;
			};
			case "abort":
			{
				playSound "Alarm";
				[_event,_message,"Warning"] spawn fn_missionNotification;					
			};
		};
	};
	[] spawn
	{
		while {true} do
		{
			waitUntil {!(blck_message isEqualTo "")};
			private["_message"];
			_message = blck_message;
			_message spawn fn_handleMessage;
			blck_Message = "";	
		};
	};
};