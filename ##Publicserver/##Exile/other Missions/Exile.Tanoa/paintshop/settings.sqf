_paintshopbuildings = ["Exile_Construction_Flag_Static","Land_FuelStation_feed_F","Land_FuelStation_build_F","Land_FuelStation_01_pump_F","Land_FuelStation_02_pump_F","Land_FS_feed_F","Land_FuelStation_build","Land_FuelStation_Shed","Land_FuelStation_feed","Land_A_FuelStation_Feed"];
_textures = [	
	["Red","#(argb,8,8,3)color(1,0,0,1)"],
	["Green","#(argb,8,8,3)color(0,1,0,1)"],
	["Blue","#(argb,8,8,3)color(0,0,1,1)"],
	["Black","#(argb,8,8,3)color(0,0,0,0.6)"],
	["Pink","#(argb,8,8,3)color(1,0.75,0.84,0.2)"],
	["Tan","#(argb,8,8,3)color(0.8,0.7,0.5,0.2)"],
	["DMD","\exile_assets\texture\flag\flag_mate_21dmd_co.paa"],
	["BIS","\exile_assets\texture\flag\flag_mate_bis_co.paa"],
	["Jankon","\exile_assets\texture\flag\flag_mate_jankon_co.paa"],
	["Legion","\exile_assets\texture\flag\flag_mate_legion_ca.paa"],
	["Secret","\exile_assets\texture\flag\flag_mate_secretone_co.paa"],
	["Sp4wny","\exile_assets\texture\flag\flag_mate_spawny_co.paa"],
	["Vish","\exile_assets\texture\flag\flag_mate_vish_co.paa"],
	["Zanders","\exile_assets\texture\flag\flag_mate_zanders_streched_co.paa"],
	["Battleye","\exile_assets\texture\flag\flag_misc_battleye_co.paa"],
	["Brunswik","\exile_assets\texture\flag\flag_misc_brunswik_co.paa"],
	["BSD","\exile_assets\texture\flag\flag_misc_bss_co.paa"],
	["Dickbutt","\exile_assets\texture\flag\flag_misc_dickbutt_co.paa"],
	["Dorset","\exile_assets\texture\flag\flag_misc_dorset_co.paa"],
	["Cat","\exile_assets\texture\flag\flag_misc_dream_cat_co.paa"],
	["Exile","\exile_assets\texture\flag\flag_misc_exile_co.paa"],
	["Kiwifern","\exile_assets\texture\flag\flag_misc_kiwifern_co.paa"],
	["Lemon party","\exile_assets\texture\flag\flag_misc_lemonparty_co.paa"],
	["Pedobear","\exile_assets\texture\flag\flag_misc_pedobear_co.paa"],
	["Petoria","\exile_assets\texture\flag\flag_misc_petoria_co.paa"],
	["Pirate","\exile_assets\texture\flag\flag_misc_pirate_co.paa"],
	["Rainbow","\exile_assets\texture\flag\flag_misc_rainbow_co.paa"],
	["RMA","\exile_assets\texture\flag\flag_misc_rma_co.paa"],
	["Skippy","\exile_assets\texture\flag\flag_misc_skippy_co.paa"],
	["Smashing","\exile_assets\texture\flag\flag_misc_smashing_co.paa"],
	["Svarog","\exile_assets\texture\flag\flag_misc_svarog_co.paa"],
	["Trololol","\exile_assets\texture\flag\flag_misc_trololol_co.paa"],
	["Utcity","\exile_assets\texture\flag\flag_misc_utcity_co.paa"],
	["Trouble2","\exile_assets\texture\flag\flag_trouble2_co.paa"],
	["Kayno MGT","addons\texture\flag\kayno.paa"],
	["Maca","addons\texture\flag\maca.paa"],
	["Goat","addons\texture\flag\goat.paa"],
	["Portugal","addons\texture\flag\portugal.paa"],
	["Banhammer","addons\texture\flag\banhammer.paa"],
	["Warning","addons\texture\flag\warning.paa"],
	["Claud","addons\texture\flag\claud.paa"],
	["Heaven","addons\texture\flag\outer_heaven.paa"],
	["BLG","addons\texture\flag\BLGbaseFlag.paa"],
	["Beardageddon","addons\texture\flag\Beardageddon.paa"],
	["Zarbon","addons\texture\flag\zarbon.paa"],
	["Luxembourg","addons\texture\flag\luxembourg.paa"],
	["Luxembourgensign","addons\texture\flag\EnsignLuxembourg.paa"],
	["PJSalt","addons\texture\flag\pjsalt.paa"],
	["Yorkshire","addons\texture\flag\yorkshire.paa"],
	["AU","\exile_assets\texture\flag\flag_country_au_co.paa"],
	["BE","\exile_assets\texture\flag\flag_country_be_co.paa"],
	["BY","\exile_assets\texture\flag\flag_country_by_co.paa"],
	["CN","\exile_assets\texture\flag\flag_country_cn_co.paa"],
	["CZ","\exile_assets\texture\flag\flag_country_cz_co.paa"],
	["DE","\exile_assets\texture\flag\flag_country_de_co.paa"],
	["FI","\exile_assets\texture\flag\flag_country_fi_co.paa"],
	["FR","\exile_assets\texture\flag\flag_country_fr_co.paa"],
	["HR","\exile_assets\texture\flag\flag_country_hr_co.paa"],
	["IR","\exile_assets\texture\flag\flag_country_ir_co.paa"],
	["IT","addons\texture\flag\italy.paa"],
	["NL","\exile_assets\texture\flag\flag_country_nl_co.paa"],
	["PL","\exile_assets\texture\flag\flag_country_pl_co.paa"],
	["RU","\exile_assets\texture\flag\flag_country_ru_co.paa"],
	["SE","\exile_assets\texture\flag\flag_country_se_co.paa"],
	["CP","\exile_assets\texture\flag\flag_cp_co.paa"],
	["UK","\a3\data_f\Flags\flag_uk_co.paa"],
	["USA","\a3\data_f\Flags\flag_us_co.paa"],
	["POW","\a3\data_f\Flags\flag_pow_co.paa"],
	["Altis1","\a3\data_f\Flags\flag_altis_co.paa"],
	["Altis2","\a3\data_f\Flags\flag_altiscolonial_co.paa"],
	["CSAT","\a3\data_f\Flags\flag_csat_co.paa"],
	["ION","\a3\data_f\Flags\flag_ion_co.paa"],
	["AAF","\a3\data_f\Flags\flag_aaf_co.paa"],
	["Crystal","\a3\data_f\Flags\flag_rcrystal_co.paa"],
	["NATO","\a3\data_f\Flags\flag_nato_co.paa"],
	["Linus","\exile_assets\texture\item\Exile_Item_RubberDuck.paa"],
	["Black and White camo","addons\texture\flag\blackandwhite_camo.paa"],
	["Blue camo","addons\texture\flag\blue_camo.paa"],
	["Blue lava","addons\texture\flag\bluelava.paa"],
	["Aqua","addons\texture\flag\aqua.paa"],
	["Cats","addons\texture\flag\cats.paa"],
	["Death","addons\texture\flag\death.paa"],
	["Digital camo","addons\texture\flag\digital_camo.paa"],
	["Flecked fall","addons\texture\flag\fleckedfall_camo.paa"],
	["Flecked green","addons\texture\flag\fleckedgreen_camo.paa"],
	["Green and red camo","addons\texture\flag\greenandred_camo.paa"],
	["Green camo","addons\texture\flag\GreenCamo_co.paa"],
	["Green stripes","addons\texture\flag\greenstripes_camo.paa"],
	["hex camo","addons\texture\flag\hex_camo.paa"],
	["Interference","addons\texture\flag\interference.paa"],
	["Rusty","addons\texture\flag\rusty.paa"],
	["Sexy","addons\texture\flag\sexy_camo.paa"],
	["Skull","addons\texture\flag\skull_camo.paa"],
	["Splat","addons\texture\flag\splat_camo.paa"],
	["Urban","addons\texture\flag\urban.paa"],
	["Urban digital","addons\texture\flag\urbandigital_camo.paa"],
	["Dark Metal","addons\texture\flag\darkMetal.paa"],
	["Stripes Multi","addons\texture\flag\stripes.paa"],
	["Black Triangles","addons\texture\flag\blackTri.paa"],
	["Modern Hex","addons\texture\flag\modernHex.paa"],
	["Blue Pattern","addons\texture\flag\bluePattern.paa"]
];