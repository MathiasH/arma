	class MASCars
	{
		name = "MAS Cars";
		icon = "a3\ui_f\data\gui\Rsc\RscDisplayArsenal\itemacc_ca.paa";
		items[] =
		{
			"I_mas_cars_UAZ_Unarmed",
			"I_mas_cars_UAZ_Med",
			"O_mas_cars_UAZ_Unarmed",
			"O_mas_cars_UAZ_Med",
			"B_mas_cars_Hilux_Unarmed",
			"B_mas_cars_Hilux_Med",
			"B_mas_cars_LR_Unarmed",
			"B_mas_cars_LR_Med",
			"I_mas_cars_LR_soft_Unarmed",
			"I_mas_cars_LR_soft_Med",
			"B_mas_HMMWV_UNA",
			"B_mas_HMMWV_MEV",
			"B_mas_HMMWV_UNA_des",
			"B_mas_HMMWV_MEV_des"
		};
	};
	class MASArmed
	{
		name = "MAS Armed";
		icon = "a3\ui_f\data\gui\Rsc\RscDisplayArsenal\itemacc_ca.paa";
		items[] =
		{
			"B_mas_cars_Hilux_MG",
			"B_mas_cars_Hilux_AGS30",
			"I_mas_cars_LR_soft_M2",
			"I_mas_cars_LR_soft_Mk19",
			"B_mas_cars_LR_M2",
			"B_mas_cars_LR_Mk19",
			"B_mas_HMMWV_M2",
			"B_mas_HMMWV_M2_des",
			"B_mas_HMMWV_SOV_M134",
			"B_mas_HMMWV_M134",
			"B_mas_cars_Hilux_M2",
			"I_mas_BRDM2",
			"O_mas_BRDM2",
			"I_mas_BTR60",
			"O_mas_BTR60",
            "I_mas_cars_Ural_ZU23",
            "O_mas_cars_Ural_ZU23"
		};
	};
	class MASTrucks
	{
		name = "MAS Trucks";
		icon = "a3\ui_f\data\gui\Rsc\RscDisplayArsenal\itemacc_ca.paa";
		items[] =
		{
			"I_mas_cars_Ural",
			"I_mas_cars_Ural_open",
			"O_mas_cars_Ural",
			"O_mas_cars_Ural_open"
		};
	};
	class MASChoppersArmed
	{
		name = "MAS Helicopters";
		icon = "a3\ui_f\data\gui\Rsc\RscDisplayArsenal\itemacc_ca.paa";
		items[]=
		{
			"B_mas_CH_47F",
			"B_mas_UH60M",
			"B_mas_UH60M_SF",
			"B_mas_UH60M_MEV",
			"I_mas_MI8",
			"O_mas_MI8"
		};
	};
	class MASChoppers
	{
		name = "MAS Helicopters";
		icon = "a3\ui_f\data\gui\Rsc\RscDisplayArsenal\itemacc_ca.paa";
		items[]=
		{			
			"B_mas_UH60M_MEV"
		};
	};