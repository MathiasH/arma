ExileClientModelBoxCamera cameraEffect ["terminate", "back"];
1 fadeSound 1;
terminate ExileClientModelThreadHandle;
deleteVehicle ExileClientModelBoxVehicle;
deleteVehicle ExileClientModelBoxCameraFocusObject;
deleteVehicle ExileClientModelBoxBackgroundObject;
camDestroy ExileClientModelBoxCamera;
if !(isNil "ExileClientModelBoxVehicleUnit") then
{
	if !(isNull ExileClientModelBoxVehicleUnit) then
	{
		deleteVehicle ExileClientModelBoxVehicleUnit;
	};
};
true