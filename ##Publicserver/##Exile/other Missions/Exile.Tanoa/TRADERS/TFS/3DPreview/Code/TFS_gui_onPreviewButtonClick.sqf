disableSerialization;
private _display = uiNameSpace getVariable ["RscExileTraderDialog", displayNull];
if !(TFS_isUsing3DPreview) then
{ 
	[] call ExileClient_gui_modelBox_create;
	1 fadeSound 0;
	TFS_isUsing3DPreview = true;
	false call ExileClient_gui_postProcessing_toggleDialogBackgroundBlur;
	{
		private _ctrl = _display displayCtrl _x;
		private _pos = ctrlPosition _ctrl;
		_ctrl ctrlSetPosition [(_pos select 0) - 2, (_pos select 1), (_pos select 2), (_pos select 3)];
		_ctrl ctrlCommit 1;
	} forEach [4000,2000,4014,4013,4012,4043,4011,4007,4005,4004,4002];
	{
		private _ctrl = _display displayCtrl _x;
		private _pos = ctrlPosition _ctrl;
		_ctrl ctrlSetPosition [(_pos select 0) + 2, (_pos select 1), (_pos select 2), (_pos select 3)];
		_ctrl ctrlCommit 1;
	} forEach [2002,2003,4042,4041,4040,4039,4038,4037,4036,4035,4034,4033,4032,4031,4030,4029,4028,4027,4026,4025,4024,4023,4022,4021,4020,4019,4018,4017,4016,4015,4006,4003,1032];
	{
		private _ctrl = _display displayCtrl _x;
		private _pos = ctrlPosition _ctrl;
		_ctrl ctrlSetPosition [(_pos select 0) - 0.7, (_pos select 1), (_pos select 2), (_pos select 3)];
		_ctrl ctrlCommit 0.5;
	} forEach [4010,4009,4008,4001,2001,4890,4044,4045];
}
else
{
	[] call ExileClient_gui_modelBox_destroy;
	true call ExileClient_gui_postProcessing_toggleDialogBackgroundBlur;
	TFS_isUsing3DPreview = false;
	{
		private _ctrl = _display displayCtrl _x;
		private _pos = ctrlPosition _ctrl;
		_ctrl ctrlSetPosition [(_pos select 0) + 2, (_pos select 1), (_pos select 2), (_pos select 3)];
		_ctrl ctrlCommit 1;
	} forEach [4000,2000,4014,4013,4012,4043,4011,4007,4005,4004,4002];
	{
		private _ctrl = _display displayCtrl _x;
		private _pos = ctrlPosition _ctrl;
		_ctrl ctrlSetPosition [(_pos select 0) - 2, (_pos select 1), (_pos select 2), (_pos select 3)];
		_ctrl ctrlCommit 1;
	} forEach [2002,2003,4042,4041,4040,4039,4038,4037,4036,4035,4034,4033,4032,4031,4030,4029,4028,4027,4026,4025,4024,4023,4022,4021,4020,4019,4018,4017,4016,4015,4006,4003,1032];
	{
		private _ctrl = _display displayCtrl _x;
		private _pos = ctrlPosition _ctrl;
		_ctrl ctrlSetPosition [(_pos select 0) + 0.7, (_pos select 1), (_pos select 2), (_pos select 3)];
		_ctrl ctrlCommit 1;
	} forEach [4010,4009,4008,4001,2001,4890,4044,4045];
};
true