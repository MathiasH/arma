private _itemClassName = _this;
private _vehicleClassName = "LootWeaponHolder";
private _itemTypeBIS = [_itemClassName] call BIS_fnc_itemType;
private _itemType = _itemClassName call ExileClient_util_cargo_getType;
switch (true) do
{
	case (((toLower (_itemTypeBIS select 1)) in ["backpack","uniform","vest","glasses","headgear"]) || (toLower (_itemTypeBIS select 0)) in ["weapon"]):
	{
		_vehicleClassName = "Exile_Unit_Player";
	};
};
if !(isNull ExileClientModelBoxVehicle) then
{
	deleteVehicle ExileClientModelBoxVehicle;
};
ExileClientModelBoxVehicle = _vehicleClassName createVehicleLocal ExileClientModelBoxPosition;
ExileClientModelBoxVehicle setPosATL ExileClientModelBoxPosition;
ExileClientModelBoxVehicle setDir 0;
ExileClientModelBoxVehicle enableSimulation false;
_modelBoundingDimensions = ExileClientModelBoxVehicle call BIS_fnc_boundingBoxDimensions;
_modelBoundingRadius = ExileClientModelBoxVehicle call BIS_fnc_boundingCircle;
if (ExileClientModelBoxVehicle isKindOf "Exile_Unit_Player") then
{
	ExileClientModelBoxVehicle switchMove "amovpercmstpsnonwnondnon";
	private _backpack = backpack player;
	if (_backpack != "") then
	{
		ExileClientModelBoxVehicle addBackpack _backpack;
	}
	else
	{
		removeBackpack ExileClientModelBoxVehicle;
	};
	private _vest = vest player;
	if (_vest != "") then
	{
		ExileClientModelBoxVehicle addVest _vest;
	}
	else
	{
		removeVest ExileClientModelBoxVehicle;
	};
	private _uniform = uniform player;
	if (_uniform != "") then
	{
		ExileClientModelBoxVehicle forceAddUniform _uniform;
	}
	else
	{
		removeUniform ExileClientModelBoxVehicle;
	};
	private _goggles = goggles player;
	if (_goggles != "") then
	{
		ExileClientModelBoxVehicle addGoggles _goggles;
	}
	else
	{
		removeGoggles ExileClientModelBoxVehicle;
	};
	private _headgear = headgear player;
	if (_headgear != "") then
	{
		ExileClientModelBoxVehicle addHeadgear _headgear;
	}
	else
	{
		removeHeadgear ExileClientModelBoxVehicle;
	};
	removeAllWeapons ExileClientModelBoxVehicle;
	private _pw = primaryWeapon player;
	if (_pw	!= "") then
	{
		ExileClientModelBoxVehicle addWeapon _pw;
	};
	private _sw = secondaryWeapon player;
	if (_sw != "") then
	{
		ExileClientModelBoxVehicle addWeapon _sw;
	};
	private _hw = handgunWeapon player;
	if (_hw	!= "") then
	{
		ExileClientModelBoxVehicle addWeapon _hw;
	};
	ExileClientModelBoxVehicle selectWeapon (currentWeapon player);
	switch (true) do
	{
		case ((_itemTypeBIS select 1) == "backpack"):
		{
			removeBackpack ExileClientModelBoxVehicle;
			ExileClientModelBoxVehicle addBackpack _itemClassName;
		};
		case ((_itemTypeBIS select 1) == "uniform"):
		{
			removeUniform ExileClientModelBoxVehicle;
			ExileClientModelBoxVehicle forceAddUniform _itemClassName;
		};
		case ((_itemTypeBIS select 1) == "vest"):
		{
			removeVest ExileClientModelBoxVehicle;
			ExileClientModelBoxVehicle addVest _itemClassName;
		};
		case ((_itemTypeBIS select 1) == "glasses"):
		{
			removeGoggles ExileClientModelBoxVehicle;
			ExileClientModelBoxVehicle addGoggles _itemClassName;
		};
		case ((_itemTypeBIS select 1) == "headgear"):
		{
			removeHeadgear ExileClientModelBoxVehicle;
			ExileClientModelBoxVehicle addHeadgear _itemClassName;
		};
		case ((_itemTypeBIS select 0) == "Weapon"):
		{
			removeAllWeapons ExileClientModelBoxVehicle;
			ExileClientModelBoxVehicle addWeapon _itemClassName;
			switch (true) do
			{
				case (((_itemTypeBIS select 1) == "assaultrifle") || ((_itemTypeBIS select 1) == "sniperrifle") || ((_itemTypeBIS select 1) == "machinegun") || ((_itemTypeBIS select 1) == "submachinegun")):
				{
					ExileClientModelBoxVehicle switchMove "amovpercmstpsraswrfldnon";
				};
				case ((_itemTypeBIS select 1) == "handgun"):
				{
					ExileClientModelBoxVehicle switchMove "amovpercmstpsraswpstdnon";		
				};
				default
				{
					ExileClientModelBoxVehicle switchMove "amovpercmstpsraswlnrdnon";
				};
			};
		};
		default {};
	};
}
else
{
	_modelBoundingRadius = 1.5;
	_modelBoundingDimensions set[2, (_modelBoundingDimensions select 2) - 1];
	switch (_itemType) do
	{
		case 1: 	
		{ 
			ExileClientModelBoxVehicle addMagazineCargo [_itemClassName, 1]; 
		};
		case 2: 	
		{ 
			ExileClientModelBoxVehicle addWeaponCargo [_itemClassName, 1]; 
		};
		default
		{ 
			ExileClientModelBoxVehicle addItemCargo [_itemClassName, 1]; 
		};
	};
};
ExileClientModelBoxCameraFocusPosition = 
[
	ExileClientModelBoxPosition select 0, 
	ExileClientModelBoxPosition select 1, 
	(ExileClientModelBoxPosition select 2) + ((_modelBoundingDimensions select 2) * 0.5)
];
ExileClientModelBoxCameraFocusObject setPosATL ExileClientModelBoxCameraFocusPosition;
ExileClientModelBoxCameraPosition = 
[
	(ExileClientModelBoxCameraFocusPosition select 0) - _modelBoundingRadius, 
	ExileClientModelBoxCameraFocusPosition select 1, 
	(ExileClientModelBoxPosition select 2) + (_modelBoundingDimensions select 2) 
];
ExileClientModelBoxCamera camPrepareTarget ExileClientModelBoxCameraFocusObject;
ExileClientModelBoxCamera setPosATL ExileClientModelBoxCameraPosition;
ExileClientModelBoxCamera camCommitPrepared 0;
true