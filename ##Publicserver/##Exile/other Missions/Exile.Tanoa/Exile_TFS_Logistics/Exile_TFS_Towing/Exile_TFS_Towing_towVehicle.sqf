/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

params[["_vehicle", objNull],"_item","_neededCount","_playerCount","_displayNameMissing","_typeOfVeh","_canTow","_vehicleData"];

try
{
	if (isNull TFSTowingSelectedVehicle) throw "You do not have a vehicle selected!";
	if ((player distance TFSTowingSelectedVehicle) > 10) throw "The selected vehicle is too far away!";
	if (_vehicle getVariable ["TFSTowingIsBeingTowed", false]) throw "You cannot tow to a vehicle that's already being towed!";
	if !(isNull (_vehicle getVariable["TFSTowingTowedVehicle", objNull])) throw "This vehicle is already towing something!";
	if (ExilePlayerInSafezone && !TFSTowingTowInSafezone) throw "You cannot tow in a trader!";

	if (TFSTowingRequireItems) then
	{
		{
			_item = _x;
			_neededCount = { (_item == _x) } count TFSTowingRequiredItems;
			_playerCount = { (_item == _x) } count (magazines player);

			if (_playerCount < _neededCount) then
			{
				_displayNameMissing = getText(configFile >> "CfgMagazines" >> _item >> "displayName");
				throw format["You're missing %1 %2 to tow!", (_neededCount - _playerCount), _displayNameMissing];
			};
		} forEach TFSTowingRequiredItems;
	};

	_typeOfVeh = typeOf _vehicle;
	_canTow = false;
	if !(TFSTowingTowEverything) then
	{
		_vehicleData = missionNamespace getVariable format["Exile_TFS_Logistics_%1", _typeOfVeh];
		if (isNil "_vehicleData") exitWith {};
		_canTow = _vehicleData select 1;
	};

	if (!_canTow && !TFSTowingTowEverything) throw "This vehicle cannot tow!";

	["vehicleTowRequest", [(netId _vehicle), (netId TFSTowingSelectedVehicle)]] call ExileClient_system_network_send;
	TFSTowingSelectedVehicle = objNull;
	call ExileClient_gui_interactionMenu_unhook;
}
catch
{
	["ErrorTitleAndText", ["Exile_TFS_Towing:", _exception]] call ExileClient_gui_toaster_addTemplateToast;
};
true