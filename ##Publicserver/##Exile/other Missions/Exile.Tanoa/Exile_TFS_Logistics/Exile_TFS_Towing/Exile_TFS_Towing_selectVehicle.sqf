/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

params[["_vehicle", objNull],"_typeOfVeh","_isTowable","_vehicleData","_displayName"];

try
{
	if (_vehicle getVariable ["TFSTowingIsBeingTowed", false]) throw "You cannot tow a vehicle that's already being towed!";
	if !(isNull (_vehicle getVariable["TFSTowingTowedVehicle", objNull])) throw "This vehicle is already towing something!";
	if !(isNull TFSTowingSelectedVehicle) throw "You already have a vehicle selected!";
	if (ExilePlayerInSafezone && !TFSTowingTowInSafezone) throw "You cannot tow in a trader!";

	_typeOfVeh = typeOf _vehicle;
	_isTowable = false;
	if !(TFSTowingTowEverything) then
	{
		_vehicleData = missionNamespace getVariable format["Exile_TFS_Logistics_%1", _typeOfVeh];
		if (isNil "_vehicleData") exitWith {};
		_isTowable = _vehicleData select 0;
	};

	if (!_isTowable && !TFSTowingTowEverything) throw "This vehicle cannot be towed!";

	TFSTowingSelectedVehicle = _vehicle;

	_displayName = getText(configFile >> "CfgVehicles" >> _typeOfVeh >> "displayName");
	["SuccessTitleAndText", ["Exile_TFS_Logistics:", format["Vehicle %1 selected!", _displayName]]] call ExileClient_gui_toaster_addTemplateToast;
	call ExileClient_gui_interactionMenu_unhook;
}
catch
{
	["ErrorTitleAndText", ["Exile_TFS_Towing:", _exception]] call ExileClient_gui_toaster_addTemplateToast;
};
true