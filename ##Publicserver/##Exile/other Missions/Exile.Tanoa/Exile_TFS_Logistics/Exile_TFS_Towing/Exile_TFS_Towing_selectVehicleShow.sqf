/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

if !(TFSTowingEnabled) exitWith { false };
(
	!((locked ExileClientInteractionObject) isEqualTo 2)
	&&
	(isNull TFSTowingSelectedVehicle)
	&&
	(isNull (ExileClientInteractionObject getVariable ["TFSTowingTowedVehicle", objNull]))
	&&
	!(ExileClientInteractionObject getVariable ["TFSTowingIsBeingTowed", false])
)