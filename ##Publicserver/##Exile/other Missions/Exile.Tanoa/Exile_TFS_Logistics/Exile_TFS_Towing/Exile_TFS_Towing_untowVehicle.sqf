/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

params[["_vehicle", objNull],"_vehicleToUntow"];

try
{
	if (ExilePlayerInSafezone && !TFSTowingTowInSafezone) throw "You cannot untow in a trader - recycle towed vehicle first";

	_vehicleToUntow = _vehicle getVariable["TFSTowingTowedVehicle", objNull];
	if (isNull _vehicleToUntow) throw "This vehicle isn't towing something!";

	["vehicleUntowRequest", [(netId _vehicle)]] call ExileClient_system_network_send;
	call ExileClient_gui_interactionMenu_unhook;
}
catch
{
	["ErrorTitleAndText", ["Exile_TFS_Towing:", _exception]] call ExileClient_gui_toaster_addTemplateToast;
};
true