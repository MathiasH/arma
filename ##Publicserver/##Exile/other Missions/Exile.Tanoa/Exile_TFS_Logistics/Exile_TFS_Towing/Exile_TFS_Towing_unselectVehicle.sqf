/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

if (isNull TFSTowingSelectedVehicle) exitWith
{
	["ErrorTitleAndText", ["Exile_TFS_Towing:", "You do not have a vehicle selected!"]] call ExileClient_gui_toaster_addTemplateToast;
};
TFSTowingSelectedVehicle = objNull;
call ExileClient_gui_interactionMenu_unhook;
true