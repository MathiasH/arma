/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

params[["_netIdVehicle",""],["_cratesLoaded",[]]];

try
{
	disableSerialization;
	if !(TFSLoadingEnabled) then { throw "Loading is disabled!"; };
	_vehicle = objectFromNetId _netIdVehicle;
	if (isNull _vehicle) then { throw "The vehicle was null!"; };
	TFSLoadingCurrentVehicle = _vehicle;
	TFSLoadingCurrentLoaded = _cratesLoaded;

	createDialog "Exile_TFS_Loading";
	_display = (uiNamespace getVariable ["Exile_TFS_Loading", displayNull]);
	(_display displayCtrl 1200) ctrlShow false;
	call Exile_TFS_Loading_populateDisplayLists;
	[] spawn
	{
		disableSerialization;
		_display = (uiNamespace getVariable ["Exile_TFS_Loading", displayNull]);
		waitUntil { uiSleep 0.1; (isNull _display) };
		["vehicleCrateResetAccessRequest", [(netId TFSLoadingCurrentVehicle)]] call ExileClient_system_network_send;
		TFSLoadingCurrentVehicle = objNull;
		true
	};
}
catch
{
	["ErrorTitleAndText", ["Exile_TFS_Loading:", _exception]] call ExileClient_gui_toaster_addTemplateToast;
};
true