/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

params[["_message",""],["_cratesLoaded",[]]];
TFSLoadingCurrentLoaded = _cratesLoaded;
call Exile_TFS_Loading_populateDisplayLists;
if (_message != "") then
{
	["SuccessTitleAndText", ["Exile_TFS_Loading:", _message]] call ExileClient_gui_toaster_addTemplateToast;
};
true