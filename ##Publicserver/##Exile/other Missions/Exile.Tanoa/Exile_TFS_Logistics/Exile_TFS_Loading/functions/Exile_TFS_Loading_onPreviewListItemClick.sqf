/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

params[["_listBox", 0],["_listBoxIndex", 0],"_display"];
disableSerialization;
_display = (uiNamespace getVariable ["Exile_TFS_Loading", displayNull]);
_previewPicture = _display displayCtrl 1200;
_previewPicture ctrlShow false;

_getConfigName =
{
	params[["_itemClass", ""],"_configName"];
	_configName = "CfgMagazines";
	if (isClass(configFile >> "CfgMagazines" >> _itemClass)) exitWith { "CfgMagazines" };
	if (isClass(configFile >> "CfgWeapons" >> _itemClass)) exitWith { "CfgWeapons" };
	_configName
};

_listBoxData = lbData [_listBox, _listBoxIndex];
if (_listBoxData == "") exitWith {_previewPicture ctrlShow false;};
_config = [_listBoxData] call _getConfigName;

_picture = getText(configFile >> _config >> _listBoxData >> "picture");
if (_picture isEqualTo "") then { _picture = ""; };
_previewPicture ctrlSetText _picture;
_previewPicture ctrlShow true;
true