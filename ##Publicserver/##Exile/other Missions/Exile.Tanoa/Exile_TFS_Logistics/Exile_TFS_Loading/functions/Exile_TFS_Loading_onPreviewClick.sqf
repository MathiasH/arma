/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

params[["_listBox", 0],["_listBoxIndex", 0]];

disableSerialization;
_display = (uiNamespace getVariable ["Exile_TFS_Loading", displayNull]);
_previewPicture = _display displayCtrl 1200;
_previewPicture ctrlShow false;

lbClear 1502;
switch (_listBox) do
{
	case 1500:
	{
		private["_listBoxData","_crate","_gear","_previewData","_index"];
		_listBoxData = lbData [_listBox, _listBoxIndex];
		if (_listBoxData == "") exitWith {};
		_crate = objectFromNetId _listBoxData;

		_gear =
		[
			(_crate call Exile_TFS_Loading_getCrateItems),
			(magazinesAmmoCargo _crate),
			(weaponsItems _crate),
			(_crate call Exile_TFS_Loading_getCrateContainers)
		];

		_previewData = [_gear] call Exile_TFS_Loading_getPreviewData;
		{
			_index = lbAdd [1502, (_x select 0)];
			lbSetData [1502, _index, (_x select 1)];
		} forEach _previewData;
	};
	case 1501:
	{
		private["_listBoxData","_crateData","_gear","_previewData","_index"];
		_listBoxData = lbData [_listBox, _listBoxIndex];
		if (_listBoxData == "") exitWith {};
		_crateData = parseSimpleArray _listBoxData;

		_gear = _crateData select 1;
		_previewData = [_gear] call Exile_TFS_Loading_getPreviewData;
		{
			_index = lbAdd [1502, (_x select 0)];
			lbSetData [1502, _index, (_x select 1)];
		} forEach _previewData;
	};
};
true