/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

params[["_vehicle", objNull],"_typeOfVeh"];

try
{
	if !(TFSLoadingEnabled) throw "Loading is disabled!";
	if (ExilePlayerInSafezone && !TFSLoadingLoadInSafezone) throw "You cannot load crates in a safezone!";
	if (_vehicle getVariable ["TFSLoadingIsInUse", false]) throw "Another player is already using crate storage!";
	["vehicleCrateDialogRequest", [(netId _vehicle)]] call ExileClient_system_network_send;
}
catch
{
	["ErrorTitleAndText", ["Exile_TFS_Logistics:", _exception]] call ExileClient_gui_toaster_addTemplateToast;
};
true