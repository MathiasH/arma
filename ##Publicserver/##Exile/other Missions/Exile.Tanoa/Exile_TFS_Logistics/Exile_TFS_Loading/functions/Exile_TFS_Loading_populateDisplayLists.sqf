/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

private["_display","_nearCrates","_displayNameNear","_indexNear","_displayNameLoaded","_indexLoaded","_vehicleType","_vehicleData","_maxCrates"];
disableSerialization;
_display = (uiNamespace getVariable ["Exile_TFS_Loading", displayNull]);
lbClear 1500;
lbClear 1501;
lbClear 1502;

_nearCrates = nearestObjects [TFSLoadingCurrentVehicle, TFSLoadingCrateClasses, TFSLoadingCrateSearchRadius];
{
	if ((_x getvariable ['ExileOwnerUID',1]) isEqualTo 1) then
	{
		_displayNameNear = getText(configfile >> "CfgVehicles" >> (typeOf _x) >> "displayName");
		_indexNear = lbAdd [1500, _displayNameNear];
		lbSetData [1500, _indexNear, (netId _x)];
	};
} forEach _nearCrates;

{
	_displayNameLoaded = getText(configfile >> "CfgVehicles" >> (_x select 0) >> "displayName");
	_indexLoaded = lbAdd [1501, _displayNameLoaded];
	lbSetData [1501, _indexLoaded, str(_x)];
} forEach TFSLoadingCurrentLoaded;

_vehicleType = typeOf TFSLoadingCurrentVehicle;
_vehicleVar = format["Exile_TFS_Logistics_%1", _vehicleType];
_vehicleData = missionNamespace getVariable _vehicleVar;
_maxCrates = TFSLoadingDefaultMaxCrates;
if (!isNil "_vehicleData") then
{
	_maxCrates = _vehicleData select 3;
};
(_display displayCtrl 1003) ctrlSetText format["%1/%2", (count TFSLoadingCurrentLoaded), _maxCrates];
true