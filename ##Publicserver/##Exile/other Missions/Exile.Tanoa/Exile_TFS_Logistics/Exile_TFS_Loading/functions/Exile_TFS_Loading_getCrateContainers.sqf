/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

private["_object","_cargo","_allContainers","_containerType","_containerObject","_temp"];
_object = _this;
_cargo = [];
_allContainers = everyContainer _object;

if !(_allContainers isEqualTo []) then
{
	{
		_containerType = _x select 0;
		_containerObject = _x select 1;
		_temp = [];
		_temp pushBack _containerType;
		_temp pushBack (weaponsItemsCargo _containerObject);
		_temp pushBack (magazinesAmmoCargo _containerObject);
		_temp pushBack (getItemCargo _containerObject);
		_cargo pushBack _temp;
	} forEach _allContainers;
};
_cargo