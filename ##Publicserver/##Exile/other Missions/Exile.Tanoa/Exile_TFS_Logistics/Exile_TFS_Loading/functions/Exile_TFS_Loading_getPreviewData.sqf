/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

params[["_gear",[]],"_filteredData","_getDisplayName","_getItemNames","_getMagNames","_getWeaponNames","_getContainerNames"];
//_gear = _this select 0;
_filteredData = [];

_getDisplayName =
{
	params[["_item", [], ["", []]],"_displayName"];
	//_item = _this select 0;
	if ((typeName _item) isEqualTo "ARRAY") then { _item = (_item select 0); };
	_displayName = ["",""];
	_displayName =
	[
		(
			switch true do
			{
				case (isClass(configFile >> "CfgMagazines" >> _item)): { (getText(configFile >> "CfgMagazines" >> _item >> "displayName")) };
				case (isClass(configFile >> "CfgWeapons" >> _item)): { (getText(configFile >> "CfgWeapons" >> _item >> "displayName")) };
				case (isClass(configFile >> "CfgVehicles" >> _item)): { (getText(configFile >> "CfgVehicles" >> _item >> "displayName")) };
				default { _item };
			}
		),
		_item
	];
	_displayName
};

_getItemNames =
{
	params[["_items", []],"_filteredData","_name"];
	_items = _this select 0;
	_filteredData = [];
	if !(_items isEqualTo [[],[]])then
	{
		{
			_name = [_x] call _getDisplayName;
			_filteredData = _filteredData + [_name];
		} forEach (_items select 0);
	};
	_filteredData
};

_getMagNames =
{
	private["_magazines","_filteredData","_name"];
	_magazines = _this select 0;
	_filteredData = [];
	if !(_magazines isEqualTo []) then
	{
		{
			if ((typeName (_x select 0)) isEqualTo "ARRAY") then
			{
				_name = [((_x select 0) select 0)] call _getDisplayName;
			}
			else
			{
				_name = [(_x select 0)] call _getDisplayName;
			};
			_filteredData = _filteredData + [_name];
		} forEach _magazines;
	};
	_filteredData
};

_getWeaponNames =
{
	private["_weapons","_filteredData","_name","_i","_thing"];
	_weapons = _this select 0;
	_filteredData = [];
	if !(_weapons isEqualTo []) then
	{
		{
			_name = [(_x select 0)] call _getDisplayName;
			_filteredData = _filteredData + [_name];
			for "_i" from 1 to ((count _x) -1) do
			{
				_thing = _x select _i;
				if !(_thing isEqualTo "") then
				{
					if ((typeName _i) isEqualTo "ARRAY") then
					{
						_name = [(_thing select 0)] call _getDisplayName;
						_filteredData = _filteredData + [_name];
					}
					else
					{
						_name = [_thing] call _getDisplayName;
						_filteredData = _filteredData + [_name];
					};
				};
			};
		} forEach _weapons;
	};
	_filteredData
};

_getContainerNames =
{
	private["_containers","_filteredData","_current","_control","_container","_weapons","_magazines","_items","_type","_name"];
	_containers = _this select 0;
	_filteredData = [];
	if !(_containers isEqualTo []) then
	{
		_current_filled = [];
		_control = [];
		{
			_container = _x select 0;
			_weapons = _x select 1;
			_magazines = _x select 2;
			_items = _x select 3;
			_type = [_container] call BIS_fnc_itemType;
			if ((_type select 1) isEqualTo "Backpack") then
			{
				_name = [_container] call _getDisplayName;
				_filteredData = _filteredData + [_name];
			}
			else
			{
				_name = [_container] call _getDisplayName;
				_filteredData = _filteredData + [_name];
			};
			_control = (everyContainer _object);
			{
				if !((_x select 1) in _current_filled) exitWith
				{
					_current_filled pushBack (_x select 1);
				};
			} forEach _control;
			if !(_weapons isEqualTo []) then
			{
				_names = [(_current_filled select _forEachIndex), _weapons] call _getWeaponNames;
				_filteredData = _filteredData + _names;
			};
			if !(_magazines isEqualTo []) then
			{
				_names = [(_current_filled select _forEachIndex), _magazines] call _getMagNames;
				_filteredData = _filteredData + _names;
			};
			if !(_items isEqualTo [[],[]]) then
			{
				_names = [(_current_filled select _forEachIndex), _items] call _getItemNames;
				_filteredData = _filteredData + _names;
			};
		} forEach _containers;
	};
	_filteredData
};

_names = [(_gear select 0)] call _getItemNames;
_filteredData = _filteredData + _names;
_names = [(_gear select 1)] call _getMagNames;
_filteredData = _filteredData + _names;
_names = [(_gear select 2)] call _getWeaponNames;
_filteredData = _filteredData + _names;
if !((_gear select 3) isEqualTo []) then
{
	_names = [(_gear select 3)] call _getContainerNames;
	_filteredData = _filteredData + _names;
};
_filteredData