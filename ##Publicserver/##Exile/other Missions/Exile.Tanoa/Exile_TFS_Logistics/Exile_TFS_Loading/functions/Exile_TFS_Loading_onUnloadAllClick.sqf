/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

try
{
	if (ExilePlayerInSafezone && !TFSLoadingLoadInSafezone) throw "You cannot unload crates in a safezone!";
	if (isNull TFSLoadingCurrentVehicle) throw "Cannot find vehicle!";
	if (TFSLoadingCurrentVehicle getVariable ["TFSLoadingActionInProgress", false]) throw "Crates are already being loaded/unloaded!";
	if ((count TFSLoadingCurrentLoaded) isEqualTo 0) throw "There are no crates in this vehicle!";

	["vehicleCrateUnloadAllRequest", [(netId TFSLoadingCurrentVehicle), TFSLoadingCurrentLoaded]] call ExileClient_system_network_send;
}
catch
{
	["ErrorTitleAndText", ["Exile_TFS_Loading:", _exception]] call ExileClient_gui_toaster_addTemplateToast;
};
true