/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

try
{
	if (ExilePlayerInSafezone && !TFSLoadingLoadInSafezone) throw "You cannot unload crates in a safezone!";
	if (isNull TFSLoadingCurrentVehicle) throw "Cannot find vehicle!";
	if (TFSLoadingCurrentVehicle getVariable ["TFSLoadingActionInProgress", false]) throw "Crates are already being loaded/unloaded!";
	_vehicleType = typeOf TFSLoadingCurrentVehicle;
	_vehicleData = missionNamespace getVariable format["Exile_TFS_Logistics_%1", _vehicleType];
	_maxCrates = TFSLoadingDefaultMaxCrates;
	if (!isNil "_vehicleData") then
	{
		_maxCrates = _vehicleData select 3;
	};
	if !(_maxCrates isEqualTo -1) then
	{
		_nearCrates = nearestObjects [TFSLoadingCurrentVehicle, TFSLoadingCrateClasses, TFSLoadingCrateSearchRadius];
		if((count _nearCrates) < 1) then { throw "There are no crates nearby!"; };
		if (((count TFSLoadingCurrentLoaded) + (count _nearCrates)) > _maxCrates) throw "Maximum amount of loading crates would be exceeded";
	};
	["vehicleCrateLoadAllRequest", [(netId TFSLoadingCurrentVehicle), TFSLoadingCurrentLoaded]] call ExileClient_system_network_send;
}
catch
{
	["ErrorTitleAndText", ["Exile_TFS_Loading:", _exception]] call ExileClient_gui_toaster_addTemplateToast;
};
true