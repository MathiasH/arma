/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

params[["_listBox", 0],["_listBoxIndex", 0]];
_listBoxData = lbData [_listBox, _listBoxIndex];

try
{
	if (_listBoxData isEqualTo "") throw "Please select a crate";
	if (ExilePlayerInSafezone && !TFSLoadingLoadInSafezone) throw "You cannot unload crates in a safezone!";
	if (isNull TFSLoadingCurrentVehicle) then { throw "Cannot find vehicle!"; };
	if (TFSLoadingCurrentVehicle getVariable ["TFSLoadingActionInProgress", false]) throw "Crates are already being loaded/unloaded!";
	if ((count TFSLoadingCurrentLoaded) isEqualTo 0) throw "There are no crates in this vehicle!";
	_crateData = parseSimpleArray _listBoxData;

	_dataIndex = TFSLoadingCurrentLoaded find _crateData;
	if (_dataIndex isEqualTo -1) throw "That crate does no exist or has been unloaded!";

	["vehicleCrateUnloadRequest", [(netId TFSLoadingCurrentVehicle), TFSLoadingCurrentLoaded, _crateData]] call ExileClient_system_network_send;
}
catch
{
	["ErrorTitleAndText", ["Exile_TFS_Loading:", _exception]] call ExileClient_gui_toaster_addTemplateToast;
};
true