/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

["vehicleCrateResetAccessRequest", [(netId TFSLoadingCurrentVehicle)]] call ExileClient_system_network_send;
TFSLoadingCurrentVehicle = objNull;
closeDialog 0;
true