/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

params[["_listBox", 0],["_listBoxIndex", 0]];
_listBoxData = lbData [_listBox, _listBoxIndex];

try
{
	if (_listBoxData isEqualTo "") throw "Please select a crate!";
	_crate = objectFromNetId _listBoxData;
	if (isNull _crate) then { throw "Cannot find crate object!"; };
	if (ExilePlayerInSafezone && !TFSLoadingLoadInSafezone) throw "You cannot load crates in a safezone!";
	_vehicleType = typeOf TFSLoadingCurrentVehicle;
	_vehicleData = missionNamespace getVariable format["Exile_TFS_Logistics_%1", _vehicleType];
	_maxCrates = TFSLoadingDefaultMaxCrates;
	if (!isNil "_vehicleData") then
	{
		_maxCrates = _vehicleData select 3;
	};
	if !(_maxCrates isEqualTo -1) then
	{
		if ((count TFSLoadingCurrentLoaded) isEqualTo _maxCrates) throw "Maximum amount of loading crates reached";
	};
	if (TFSLoadingCurrentVehicle getVariable ["TFSLoadingActionInProgress", false]) throw "Crates are already being loaded!";
	["vehicleCrateLoadRequest", [(netId TFSLoadingCurrentVehicle), (netId _crate)]] call ExileClient_system_network_send;
}
catch
{
	["ErrorTitleAndText", ["Exile_TFS_Loading:", _exception]] call ExileClient_gui_toaster_addTemplateToast;
};
true