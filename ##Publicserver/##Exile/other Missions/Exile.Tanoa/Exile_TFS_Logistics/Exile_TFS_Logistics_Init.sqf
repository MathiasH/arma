/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

private["_code","_function","_file"];
{
	_x params ["_function","_file"];
	_code = compileFinal (preprocessFileLineNumbers _file);
	if (isNil "_code") then	{ _code = ""; };
	missionNamespace setVariable [_function, _code];
}
forEach
[
	//Towing
	["Exile_TFS_Towing_selectVehicle", "Exile_TFS_Logistics\Exile_TFS_Towing\Exile_TFS_Towing_selectVehicle.sqf"],
	["Exile_TFS_Towing_selectVehicleShow", "Exile_TFS_Logistics\Exile_TFS_Towing\Exile_TFS_Towing_selectVehicleShow.sqf"],
	["Exile_TFS_Towing_towVehicle", "Exile_TFS_Logistics\Exile_TFS_Towing\Exile_TFS_Towing_towVehicle.sqf"],
	["Exile_TFS_Towing_towVehicleShow", "Exile_TFS_Logistics\Exile_TFS_Towing\Exile_TFS_Towing_towVehicleShow.sqf"],
	["Exile_TFS_Towing_unselectVehicle", "Exile_TFS_Logistics\Exile_TFS_Towing\Exile_TFS_Towing_unselectVehicle.sqf"],
	["Exile_TFS_Towing_unselectVehicleShow", "Exile_TFS_Logistics\Exile_TFS_Towing\Exile_TFS_Towing_unselectVehicleShow.sqf"],
	["Exile_TFS_Towing_untowVehicle", "Exile_TFS_Logistics\Exile_TFS_Towing\Exile_TFS_Towing_untowVehicle.sqf"],
	["Exile_TFS_Towing_untowVehicleShow", "Exile_TFS_Logistics\Exile_TFS_Towing\Exile_TFS_Towing_untowVehicleShow.sqf"],

	//Loading
	["Exile_TFS_Loading_crateStorage", "Exile_TFS_Logistics\Exile_TFS_Loading\functions\Exile_TFS_Loading_crateStorage.sqf"],
	["Exile_TFS_Loading_crateStorageShow", "Exile_TFS_Logistics\Exile_TFS_Loading\functions\Exile_TFS_Loading_crateStorageShow.sqf"],
	["Exile_TFS_Loading_getCrateContainers", "Exile_TFS_Logistics\Exile_TFS_Loading\functions\Exile_TFS_Loading_getCrateContainers.sqf"],
	["Exile_TFS_Loading_getCrateItems", "Exile_TFS_Logistics\Exile_TFS_Loading\functions\Exile_TFS_Loading_getCrateItems.sqf"],
	["Exile_TFS_Loading_getPreviewData", "Exile_TFS_Logistics\Exile_TFS_Loading\functions\Exile_TFS_Loading_getPreviewData.sqf"],
	["Exile_TFS_Loading_onCloseClick", "Exile_TFS_Logistics\Exile_TFS_Loading\functions\Exile_TFS_Loading_onCloseClick.sqf"],
	["Exile_TFS_Loading_onLoadAllClick", "Exile_TFS_Logistics\Exile_TFS_Loading\functions\Exile_TFS_Loading_onLoadAllClick.sqf"],
	["Exile_TFS_Loading_onLoadClick", "Exile_TFS_Logistics\Exile_TFS_Loading\functions\Exile_TFS_Loading_onLoadClick.sqf"],
	["Exile_TFS_Loading_onPreviewClick", "Exile_TFS_Logistics\Exile_TFS_Loading\functions\Exile_TFS_Loading_onPreviewClick.sqf"],
	["Exile_TFS_Loading_onPreviewListItemClick", "Exile_TFS_Logistics\Exile_TFS_Loading\functions\Exile_TFS_Loading_onPreviewListItemClick.sqf"],
	["Exile_TFS_Loading_onUnloadAllClick", "Exile_TFS_Logistics\Exile_TFS_Loading\functions\Exile_TFS_Loading_onUnloadAllClick.sqf"],
	["Exile_TFS_Loading_onUnloadClick", "Exile_TFS_Logistics\Exile_TFS_Loading\functions\Exile_TFS_Loading_onUnloadClick.sqf"],
	["Exile_TFS_Loading_populateDisplayLists", "Exile_TFS_Logistics\Exile_TFS_Loading\functions\Exile_TFS_Loading_populateDisplayLists.sqf"],
	["Exile_TFS_Loading_onSellClick", "Exile_TFS_Logistics\Exile_TFS_Loading\functions\Exile_TFS_Loading_onSellClick.sqf"],
	["ExileClient_TFSLoading_network_vehicleCrateDialogResponse", "Exile_TFS_Logistics\Exile_TFS_Loading\network\ExileClient_TFSLoading_network_vehicleCrateDialogResponse.sqf"],
	["ExileClient_TFSLoading_network_vehicleCrateUpdateLoaded", "Exile_TFS_Logistics\Exile_TFS_Loading\network\ExileClient_TFSLoading_network_vehicleCrateUpdateLoaded.sqf"]/*,

	//Deployables
	["Exile_TFS_Deploy_deployVehicle", "Exile_TFS_Logistics\Exile_TFS_Deploy\Exile_TFS_Deploy_deployVehicle.sqf"],
	["Exile_TFS_Deploy_loadDialog", "Exile_TFS_Logistics\Exile_TFS_Deploy\Exile_TFS_Deploy_loadDialog.sqf"],
	["Exile_TFS_Deploy_onPreviewClick", "Exile_TFS_Logistics\Exile_TFS_Deploy\Exile_TFS_Deploy_onPreviewClick.sqf"]*/
];

TFSTowingSelectedVehicle = objNull;
TFSLoadingCurrentVehicle = objNull;
TFSLoadingCurrentLoaded = [];

true