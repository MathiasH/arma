/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

try
{
	if !(TFSDeployEnabled) throw "Deployables are not enabled!";
	if (isNil "TFSDeployClasses") throw "Vehicle config missing, contact the admin!";
	if (ExilePlayerInSafezone && !TFSDeployInSafezone) throw "You cannot load crates in a safezone!";
	disableSerialization;
	createDialog "Exile_TFS_Deploy";

	{
		_deployData = missionNamespace getVariable format["Exile_TFS_Deployable_%1", _x];
		_name = _deployData param[0, ""];
		_index = lbAdd [1500, _name];
		lbSetData [1500, _index, _x];
	} forEach TFSDeployClasses;
}
catch
{
	["ErrorTitleAndText", ["Exile_TFS_Deploy:", _exception]] call ExileClient_gui_toaster_addTemplateToast; 
};
true