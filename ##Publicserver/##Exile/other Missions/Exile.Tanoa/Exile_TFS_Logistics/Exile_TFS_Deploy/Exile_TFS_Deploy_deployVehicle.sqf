/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

params[["_listBox",0],["_listBoxIndex",0],"_listBoxData","_deployData","_currentMoney","_currentScore","_item","_neededCount","_playerCount","_displayName"];
_listBoxData = lbData [_listBox, _listBoxIndex];

try
{
	if !(TFSDeployEnabled) throw "Deployables are not enabled!";
	if (ExilePlayerInSafezone && !TFSDeployInSafezone) throw "You cannot load crates in a safezone!";
	
	_deployData = missionNamespace getVariable format["Exile_TFS_Deployable_%1", _listBoxData];
	if (isNil "_deployData") exitWith {};
	_deployData params[["_name",""],["_poptabSettings",[]],["_respectSettings",[]],["_enableItems", false],["_itemsArray",[]]];
	_poptabSettings params[["_poptabEnable",false],["_poptabCost",0]];
	_respectSettings params[["_respectEnable",false],["_respectCost",0]];
	if (_poptabEnable) then
	{
		_currentMoney = player getVariable ["ExileMoney",0];
		if (_currentMoney < _poptabCost) then
		{
			throw format["You are missing %1 poptabs!", (_poptabCost - _currentMoney)];
		};
	};
	if (_respectEnable) then
	{
		_currentScore = player getVariable ["ExileScore",0];
		if (_currentScore < _respectCost) then
		{
			throw format["You are missing %1 respect!", (_respectCost - _currentScore)];
		};
	};
	if (_enableItems) then
	{
		{
			_item = _x;
			_neededCount = { _item isEqualTo _x } count _itemsArray;
			_playerCount = { _item isEqualTo _x } count (magazines player);
			if (_playerCount < _neededCount) then
			{
				_displayName = getText(configFile >> "CfgMagazines" >> _item >> "displayName");
				throw format["You are missing %1 %2", (_neededCount - _playerCount), _displayName];
			};
		} forEach _itemsArray;
	};
	["vehicleDeployRequest", [_listBoxData]] call ExileClient_system_network_send;
}
catch
{
	["ErrorTitleAndText", ["Exile_TFS_Deploy:", _exception]] call ExileClient_gui_toaster_addTemplateToast;
};
true