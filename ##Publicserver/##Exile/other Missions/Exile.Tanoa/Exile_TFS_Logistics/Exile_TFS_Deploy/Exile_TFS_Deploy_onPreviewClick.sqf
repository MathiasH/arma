/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

params[["_listBox",0],["_listBoxIndex",0],"_listBoxData","_display","_deployData","_poptabText","_respectText"];
_listBoxData = lbData [_listBox, _listBoxIndex];

try
{
	disableSerialization;
	_display = (uiNamespace getVariable ["Exile_TFS_Deploy", displayNull]);
	if (isNull _display) throw "Cannot find deploy display!";
	_deployData = missionNamespace getVariable format["Exile_TFS_Deployable_%1", _listBoxData];
	if (isNil "_deployData") throw "Cannot find vehicle config!";
	_deployData params[["_name",""],["_poptabSettings",[]],["_respectSettings",[]],["_enableItems", false],["_itemsArray",[]]];

	_poptabSettings params[["_poptabEnable",false],["_poptabCost",0]];
	_respectSettings params[["_respectEnable",false],["_respectCost",0]];

	(_display displayCtrl 1002) ctrlSetText _name;
	_poptabText = "";
	_poptabText = if (_poptabEnable) then { format["Poptabs: %1", _poptabCost] } else { "Free!" };
	_respectText = "";
	_respectText = if (_respectEnable) then { format["Respect: %1", _respectCost] } else { "Free!" };
	(_display displayCtrl 1003) ctrlSetText _poptabText;
	(_display displayCtrl 1004) ctrlSetText _respectText;
	(_display displayCtrl 1501) ctrlSetText getText(configFile >> "CfgVehicles" >> _listBoxData >> "icon");
}
catch
{
	["ErrorTitleAndText", ["Exile_TFS_Deploy:", _exception]] call ExileClient_gui_toaster_addTemplateToast;
};
true