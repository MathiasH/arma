/**
 * TheForsakenSurvivors Community
 * www.theforsakensurvivors.co.uk
 * © 2017 TheForsakenSurvivors Community
 *
 * This work is property of TheForsakenSurvivors. You do not have permissions to use/edit/distribute any of this content
 * without written permission from the TheForsakenSurvivors community.
 */

class TFSRscButton
{
	access = 0;
	type = 1;
	style = 2;
	x = 0;
	y = 0;
	w = 0.055589;
	h = 0.039216;
	shadow = 0;
	font = "OrbitronLight";
	sizeEx = 0.03;
	colorText[] = {1,1,1,.9};
	colorDisabled[] = {0.4,0.4,0.4,0};
	colorBackground[] = {0,0,0,0.8};
	colorBackgroundDisabled[] = {0,0,0,0.4};
	colorBackgroundActive[] = {0,0,0,0.4};
	colorFocused[] = {0,0,0,0.4};
	colorShadow[] = {0.023529,0,0.0313725,0};
	colorBorder[] = {0.023529,0,0.0313725,1};
	offsetX = 0.003;
	offsetY = 0.003;
	offsetPressedX = 0.002;
	offsetPressedY = 0.002;
	borderSize = 0.0;
	soundEnter[] = {"\A3\ui_f\data\sound\RscButton\soundEnter",0.09,1};
	soundPush[] = {"\A3\ui_f\data\sound\RscButton\soundPush",0.09,1};
	soundClick[] = {"\A3\ui_f\data\sound\RscButton\soundClick",0.09,1};
	soundEscape[] = {"\A3\ui_f\data\sound\RscButton\soundEscape",0.09,1};
};
class TFSRscFrame
{
	access = 0;
	type = 0;
	idc = -1;
	style = 64;
	shadow = 0;
	colorBackground[] = {0.19,0.23,0.24,0.6};
	colorText[] = {1,1,1,1};
	font = "OrbitronLight";
	sizeEx = 0.03;
	text = "";
};
class TFSRscText
{
	access = 0;
	type = 0;
	idc = -1;
	style = 0x00;
	colorBackground[] = {0,0,0,0.8};
	colorText[] = {1,1,1,1};
	font = "OrbitronLight";
	sizeEx = 0.032;
	h = 0.05;
	text = "";
};
class TFSRscListBox
{
	access = 0;
	type = 5;
	w = 0.4;
	h = 0.4;
	text = "";
	rowHeight = 0;
	style = 16;
	font = "OrbitronLight";
	sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	shadow = 0;
	color[] = {1,1,1,1};
	colorActive[] = {1,0,0,1};
	colorShadow[] = {0,0,0,0.5};
	colorText[] = {1,1,1,1};
	colorDisabled[] = {1,1,1,0.25};
	colorScrollbar[] = {1,0,0,0};
	colorSelect[] = {0,0,0,1};
	colorSelect2[] = {0,0,0,1};
	colorSelectBackground[] = {0.95,0.95,0.95,1};
	colorSelectBackground2[] = {1,1,1,0.5};
	period = 1.2;
	colorBackground[] = {0.19,0.23,0.24,0.6};
	maxHistoryDelay = 1.0;
	colorPicture[] = {1,1,1,1};
	colorPictureSelected[] = {1,1,1,1};
	colorPictureDisabled[] = {1,1,1,0.25};
	colorPictureRight[] = {1,1,1,1};
	colorPictureRightSelected[] = {1,1,1,1};
	colorPictureRightDisabled[] = {1,1,1,0.25};
	colorTextRight[] = {1,1,1,1};
	colorSelectRight[] = {0,0,0,1};
	colorSelect2Right[] = {0,0,0,1};
	tooltipColorText[] = {1,1,1,1};
	tooltipColorBox[] = {1,1,1,1};
	tooltipColorShade[] = {0,0,0,0.65};
	soundSelect[] = {"\A3\ui_f\data\sound\RscListBox\soundSelect",0.09,1};
	class ListScrollBar
	{
		color[] = {1,1,1,0.6};
		colorActive[] = {1,1,1,1};
		colorDisabled[] = {1,1,1,0.3};
		thumb = "\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa";
		arrowEmpty = "\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa";
		arrowFull = "\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa";
		border = "\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa";
		shadow = 0;
		scrollSpeed = 0.06;
		autoScrollEnabled = 0;
		autoScrollSpeed = -1;
		autoScrollDelay = 5;
		autoScrollRewind = 0;
	};
};
class TFSRscPicture
{
	access = 0;
	type = 0;
	style = 48;
	shadow = 0;
	font = "OrbitronLight";
	sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	colorText[] = {1,1,1,1};
	colorBackground[] = {0,0,0,0};
	x = 0;
	y = 0;
	w = 0.2;
	h = 0.15;
	tooltipColorText[] = {1,1,1,1};
	tooltipColorBox[] = {1,1,1,1};
	tooltipColorShade[] = {0,0,0,0.65};
};
class TFSRscPictureKeepAspect: TFSRscPicture
{
	style = "0x30 + 0x800";
};
class Exile_TFS_Deploy
{
	idd=28698;
	duration = -1;
	onLoad = "uiNamespace setVariable ['Exile_TFS_Deploy', _this select 0];";
	onUnload = "uiNamespace setVariable ['Exile_TFS_Deploy', displayNull];";
	class controls
	{
		class RscButton_1600: TFSRscButton
		{
			idc = 1600;
			text = "Deploy"; //--- ToDo: Localize;
			x = 0.61 * safezoneW + safezoneX;
			y = 0.723 * safezoneH + safezoneY;
			w = 0.0590625 * safezoneW;
			h = 0.042 * safezoneH;
			onButtonClick = "[1500, (lbCurSel 1500)] call Exile_TFS_Deploy_deployVehicle";
		};
		class Exile_TFS_Deploy_RscText_1000: TFSRscText
		{
			idc = 1000;
			text = "DEPLOYABLES:";
			x = 0.36875 * safezoneW + safezoneX;
			y = 0.29 * safezoneH + safezoneY;
			w = 0.170625 * safezoneW;
			h = 0.056 * safezoneH;
		};
		class Exile_TFS_Deploy_TFSRscPicture_1502: TFSRscPicture
		{
			idc = 1502;
			text = "";
			style = 1;
			x = 0.539375 * safezoneW + safezoneX;
			y = 0.29 * safezoneH + safezoneY;
			w = 0.13125 * safezoneW;
			h = 0.21 * safezoneH;
			colorBackground[] = {0.19,0.23,0.24,0.6};
		};
		class Exile_TFS_Deploy_TFSRscPicture_1501: TFSRscPicture
		{
			idc = 1501;
			text = "";
			x = 0.539375 * safezoneW + safezoneX;
			y = 0.29 * safezoneH + safezoneY;
			w = 0.13125 * safezoneW;
			h = 0.21 * safezoneH;
		};
		class Exile_TFS_Deploy_TFSRscPicture_1503: TFSRscPicture
		{
			idc = 1503;
			text = "";
			style = 1;
			x = 0.539375 * safezoneW + safezoneX;
			y = 0.5 * safezoneH + safezoneY;
			w = 0.13125 * safezoneW;
			h = 0.224 * safezoneH;
			colorBackground[] = {0.19,0.23,0.24,0.6};
		};
		class Exile_TFS_Deploy_RscText_1001: TFSRscText
		{
			idc = 1001;
			text = "Deploy Information:";
			sizeEx = 0.023;
			x = 0.5525 * safezoneW + safezoneX;
			y = 0.528 * safezoneH + safezoneY;
			w = 0.105 * safezoneW;
			h = 0.042 * safezoneH;
		};
		class Exile_TFS_Deploy_RscText_1002: TFSRscText
		{
			idc = 1002;
			text = "";
			sizeEx = 0.023;
			x = 0.5525 * safezoneW + safezoneX;
			y = 0.5756 * safezoneH + safezoneY;
			w = 0.105 * safezoneW;
			h = 0.042 * safezoneH;
		};
		class Exile_TFS_Deploy_RscText_1003: TFSRscText
		{
			idc = 1003;
			text = "";
			sizeEx = 0.023;
			x = 0.5525 * safezoneW + safezoneX;
			y = 0.6204 * safezoneH + safezoneY;
			w = 0.105 * safezoneW;
			h = 0.042 * safezoneH;
		};
		class Exile_TFS_Deploy_RscText_1004: TFSRscText
		{
			idc = 1004;
			text = "";
			sizeEx = 0.023;
			x = 0.5525 * safezoneW + safezoneX;
			y = 0.668 * safezoneH + safezoneY;
			w = 0.105 * safezoneW;
			h = 0.042 * safezoneH;
		};
		class Exile_TFS_Deploy_RscListbox_1500: TFSRscListbox
		{
			idc = 1500;
			x = 0.36875 * safezoneW + safezoneX;
			y = 0.346 * safezoneH + safezoneY;
			w = 0.170625 * safezoneW;
			h = 0.378 * safezoneH;
			onMouseButtonClick = "[1500, (lbCurSel 1500)] call Exile_TFS_Deploy_onPreviewClick";
		};
		class Exile_TFS_Deploy_RscFrame_1802: TFSRscFrame
		{
			idc = 1802;
			x = 0.539375 * safezoneW + safezoneX;
			y = 0.5 * safezoneH + safezoneY;
			w = 0.13125 * safezoneW;
			h = 0.224 * safezoneH;
		};
		class Exile_TFS_Deploy_RscFrame_1803: TFSRscFrame
		{
			idc = 1803;
			x = 0.36875 * safezoneW + safezoneX;
			y = 0.346 * safezoneH + safezoneY;
			w = 0.170625 * safezoneW;
			h = 0.378 * safezoneH;
		};
		class Exile_TFS_Deploy_RscFrame_1801: TFSRscFrame
		{
			idc = 1801;
			x = 0.539375 * safezoneW + safezoneX;
			y = 0.29 * safezoneH + safezoneY;
			w = 0.13125 * safezoneW;
			h = 0.21 * safezoneH;
		};
		class Exile_TFS_Deploy_RscFrame_1800: TFSRscFrame
		{
			idc = 1800;
			x = 0.36875 * safezoneW + safezoneX;
			y = 0.29 * safezoneH + safezoneY;
			w = 0.170625 * safezoneW;
			h = 0.056 * safezoneH;
		};
	};
};
class Exile_TFS_Loading
{
	idd=28697; //-1
	duration = -1;
	onLoad = "uiNamespace setVariable ['Exile_TFS_Loading', _this select 0];";
	onUnload = "uiNamespace setVariable ['Exile_TFS_Loading', displayNull];";
	class controls
	{
		class Exile_TFS_Loading_NearbyObjectsTextFrame: TFSRscFrame
		{
			idc = 1800;
			x = 0.237533 * safezoneW + safezoneX;
			y = 0.206 * safezoneH + safezoneY;
			w = 0.164042 * safezoneW;
			h = 0.056 * safezoneH;
		};
		class Exile_TFS_Loading_NearbyObjectsFrame: TFSRscFrame
		{
			idc = 1801;
			x = 0.237533 * safezoneW + safezoneX;
			y = 0.262 * safezoneH + safezoneY;
			w = 0.164042 * safezoneW;
			h = 0.42 * safezoneH;
		};
		class Exile_TFS_Loading_LoadedObjectsFrame: TFSRscFrame
		{
			idc = 1802;
			x = 0.408137 * safezoneW + safezoneX;
			y = 0.262 * safezoneH + safezoneY;
			w = 0.164042 * safezoneW;
			h = 0.42 * safezoneH;
		};
		class Exile_TFS_Loading_LoadedObjectsTextFrame: TFSRscFrame
		{
			idc = 1803;
			x = 0.408137 * safezoneW + safezoneX;
			y = 0.206 * safezoneH + safezoneY;
			w = 0.164042 * safezoneW;
			h = 0.056 * safezoneH;
		};
		class Exile_TFS_Loading_LoadButton: TFSRscButton
		{
			idc = 1600;
			text = "LOAD";
			x = 0.237533 * safezoneW + safezoneX;
			y = 0.71 * safezoneH + safezoneY;
			w = 0.0787402 * safezoneW;
			h = 0.084 * safezoneH;
			onButtonClick = "[1500, (lbCurSel 1500)] call Exile_TFS_Loading_onLoadClick";
		};
		class Exile_TFS_Loading_LoadAllButton: TFSRscButton
		{
			idc = 1601;
			text = "LOAD ALL";
			x = 0.322835 * safezoneW + safezoneX;
			y = 0.71 * safezoneH + safezoneY;
			w = 0.0787402 * safezoneW;
			h = 0.084 * safezoneH;
			onButtonClick = "call Exile_TFS_Loading_onLoadAllClick";
		};
		class Exile_TFS_Loading_UnloadButton: TFSRscButton
		{
			idc = 1602;
			text = "UNLOAD";
			x = 0.408137 * safezoneW + safezoneX;
			y = 0.71 * safezoneH + safezoneY;
			w = 0.0787402 * safezoneW;
			h = 0.084 * safezoneH;
			onButtonClick = "[1501, (lbCurSel 1501)] call Exile_TFS_Loading_onUnloadClick";
		};
		class Exile_TFS_Loading_UnloadAllButton: TFSRscButton
		{
			idc = 1603;
			text = "UNLOAD ALL";
			x = 0.493438 * safezoneW + safezoneX;
			y = 0.71 * safezoneH + safezoneY;
			w = 0.0787402 * safezoneW;
			h = 0.084 * safezoneH;
			onButtonClick = "call Exile_TFS_Loading_onUnloadAllClick";
		};
		class Exile_TFS_Loading_SellButton: TFSRscButton
		{
			idc = 6583;
			text = "SELL";
			x = 0.578739 * safezoneW + safezoneX;
			y = 0.71 * safezoneH + safezoneY;
			w = 0.0787402 * safezoneW;
			h = 0.084 * safezoneH;
			onButtonClick = "[1501, (lbCurSel 1501)] spawn Exile_TFS_Loading_onSellClick";
		};
		class Exile_TFS_Loading_CancelButton: TFSRscButton
		{
			idc = 1604;
			text = "CANCEL";
			x = 0.683727 * safezoneW + safezoneX;
			y = 0.71 * safezoneH + safezoneY;
			w = 0.0787402 * safezoneW;
			h = 0.084 * safezoneH;
			onButtonClick = "closeDialog 0";//"call Exile_TFS_Loading_onCloseClick";
		};
		class Exile_TFS_Loading_NearbyObjectsListBox: TFSRscListBox
		{
			idc = 1500;
			x = 0.237533 * safezoneW + safezoneX;
			y = 0.262 * safezoneH + safezoneY;
			w = 0.164042 * safezoneW;
			h = 0.42 * safezoneH;
			onMouseButtonClick = "[1500, (lbCurSel 1500)] call Exile_TFS_Loading_onPreviewClick";
		};
		class Exile_TFS_Loading_LoadedObjectsListBox: TFSRscListBox
		{
			idc = 1501;
			x = 0.408137 * safezoneW + safezoneX;
			y = 0.262 * safezoneH + safezoneY;
			w = 0.164042 * safezoneW;
			h = 0.42 * safezoneH;
			onMouseButtonClick = "[1501, (lbCurSel 1501)] call Exile_TFS_Loading_onPreviewClick";
		};
		class Exile_TFS_Loading_NearbyObjectsText: TFSRscText
		{
			idc = 1000;
			text = "NEARBY OBJECTS:";
			x = 0.237533 * safezoneW + safezoneX;
			y = 0.206 * safezoneH + safezoneY;
			w = 0.164042 * safezoneW;
			h = 0.056 * safezoneH;
		};
		class Exile_TFS_Loading_LoadedObjectsText: TFSRscText
		{
			idc = 1001;
			text = "LOADED OBJECTS:";
			x = 0.408137 * safezoneW + safezoneX;
			y = 0.206 * safezoneH + safezoneY;
			w = 0.164042 * safezoneW;
			h = 0.056 * safezoneH;
		};
		class Exile_TFS_Loading_UnloadButtonFrame: TFSRscFrame
		{
			idc = 1806;
			x = 0.408137 * safezoneW + safezoneX;
			y = 0.71 * safezoneH + safezoneY;
			w = 0.0787402 * safezoneW;
			h = 0.084 * safezoneH;
		};
		class Exile_TFS_Loading_UnloadAllButtonFrame: TFSRscFrame
		{
			idc = 1807;
			x = 0.493438 * safezoneW + safezoneX;
			y = 0.71 * safezoneH + safezoneY;
			w = 0.0787402 * safezoneW;
			h = 0.084 * safezoneH;
		};
		class Exile_TFS_Loading_SellButtonFrame: TFSRscFrame
		{
			idc = 6582;
			x = 0.578739 * safezoneW + safezoneX;
			y = 0.71 * safezoneH + safezoneY;
			w = 0.0787402 * safezoneW;
			h = 0.084 * safezoneH;
		};
		class Exile_TFS_Loading_LoadButtonFrame: TFSRscFrame
		{
			idc = 1808;
			x = 0.237533 * safezoneW + safezoneX;
			y = 0.71 * safezoneH + safezoneY;
			w = 0.0787402 * safezoneW;
			h = 0.084 * safezoneH;
		};
		class Exile_TFS_Loading_LoadAllButtonFrame: TFSRscFrame
		{
			idc = 1809;
			x = 0.322835 * safezoneW + safezoneX;
			y = 0.71 * safezoneH + safezoneY;
			w = 0.0787402 * safezoneW;
			h = 0.084 * safezoneH;
		};
		class Exile_TFS_Loading_CancelButtonFrame: TFSRscFrame
		{
			idc = 1810;
			x = 0.683727 * safezoneW + safezoneX;
			y = 0.71 * safezoneH + safezoneY;
			w = 0.0787402 * safezoneW;
			h = 0.084 * safezoneH;
		};
		class Exile_TFS_Loading_ObjectPreviewFrame: TFSRscFrame
		{
			idc = 1805;
			x = 0.57874 * safezoneW + safezoneX;
			y = 0.5 * safezoneH + safezoneY;
			w = 0.183727 * safezoneW;
			h = 0.182 * safezoneH;
		};
		class Exile_TFS_Loading_ObjectsPreview: TFSRscListBox
		{
			idc = 1502;
			x = 0.57874 * safezoneW + safezoneX;
			y = 0.5 * safezoneH + safezoneY;
			w = 0.183727 * safezoneW;
			h = 0.182 * safezoneH;
			onMouseButtonClick = "[1502, (lbCurSel 1502)] call Exile_TFS_Loading_onPreviewListItemClick";
		};
		class Exile_TFS_Loading_PreviewTextFrame: TFSRscFrame
		{
			idc = 1811;
			x = 0.57874 * safezoneW + safezoneX;
			y = 0.444 * safezoneH + safezoneY;
			w = 0.183727 * safezoneW;
			h = 0.056 * safezoneH;
		};
		class Exile_TFS_Loading_PreviewText: TFSRscText
		{
			idc = 1002;
			text = "CRATE INFORMATION:";
			x = 0.57874 * safezoneW + safezoneX;
			y = 0.444 * safezoneH + safezoneY;
			w = 0.183727 * safezoneW;
			h = 0.056 * safezoneH;
		};
		class Exile_TFS_Loading_PreviewPictureBackGround: TFSRscPicture
		{
			idc = 1814;
			text = "";
			style = 1;
			x = 0.591864 * safezoneW + safezoneX;
			y = 0.206 * safezoneH + safezoneY;
			w = 0.144357 * safezoneW;
			h = 0.21 * safezoneH;
			colorBackground[] = {0.19,0.23,0.24,0.6};
		};
		class Exile_TFS_Loading_PreviewPictureBackGroundFrame: TFSRscFrame
		{
			idc = 1813;
			x = 0.591864 * safezoneW + safezoneX;
			y = 0.206 * safezoneH + safezoneY;
			w = 0.144357 * safezoneW;
			h = 0.21 * safezoneH;
		};
		class Exile_TFS_Loading_PreviewPicture: TFSRscPicture
		{
			idc = 1200;
			text = "";
			x = 0.591864 * safezoneW + safezoneX;
			y = 0.206 * safezoneH + safezoneY;
			w = 0.144357 * safezoneW;
			h = 0.21 * safezoneH;
		};
		class Exile_TFS_VehicleCapacityText: TFSRscText
		{
			idc = 1003;
			text = "0";
			sizeEx = 0.03921;
			x = 0.53937 * safezoneW + safezoneX;
			y = 0.206 * safezoneH + safezoneY;
			w = 0.0328084 * safezoneW;
			h = 0.056 * safezoneH;
		};
		class Exile_TFS_VehicleStorageCapacityFrame: TFSRscFrame
		{
			idc = 1812;
			x = 0.53937 * safezoneW + safezoneX;
			y = 0.206 * safezoneH + safezoneY;
			w = 0.0328084 * safezoneW;
			h = 0.056 * safezoneH;
		};
	};
};