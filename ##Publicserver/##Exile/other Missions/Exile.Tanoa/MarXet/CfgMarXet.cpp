class CfgMarXet
{
    class Database
    {
        restrictTime = 5;
        deleteTime = 10;
    };
    class Settings
    {
        staticVehicleSpawning = 0;
        disableSellerBuyback = 1;
        sellerBuybackPercentage = 0.5;
        maxNumberOfListings = -1;
        disableVehicleListing = 0;
    };
};