_traders = [];
switch (toLower worldName) do {
	case "tanoa":
    {
        _trader =
        [
            "Exile_Cutscene_Prisoner01",
			"",
            "GreekHead_A3_01",
            ["AidlPercMstpSnonWnonDnon_G01", "AidlPercMstpSnonWnonDnon_G02", "AidlPercMstpSnonWnonDnon_G03", "AidlPercMstpSnonWnonDnon_G04", "AidlPercMstpSnonWnonDnon_G05", "AidlPercMstpSnonWnonDnon_G06"],
            [11062.6,9570.53,0.00141907],
            350.795
        ]
        call ExileClient_object_trader_create;
		_traders pushBack _trader;
    };
};
{
	_x forceAddUniform "U_O_Protagonist_VR";
	_x addHeadgear "H_Cap_grn_BI";
	_x addWeapon "srifle_DMR_04_F";
	_x addPrimaryWeaponItem "optic_LRPS";
	_x addWeapon "hgun_ACPC2_F";
	_x addAction ["<img image='\a3\ui_f\data\IGUI\Cfg\Actions\reammo_ca.paa' size='1' shadow='false' />Access MarXet","createDialog 'RscMarXetDialog'","",1,false,true,"","((position player) distance _target) <= 4"];
} forEach _traders;
[format["MarXet Trader has been placed. Trader count: %1. Map: %2",(count(_traders)),worldName],"Client Traders"] call ExileClient_MarXet_util_log;