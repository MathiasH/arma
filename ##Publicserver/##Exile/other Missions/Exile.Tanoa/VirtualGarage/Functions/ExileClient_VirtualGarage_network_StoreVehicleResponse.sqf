private ["_response","_this"];
_response = _this select 0;
if(_response == "true")then
{
  ["SuccessTitleAndText", ["Vehicle", "Successfully Stored"]] call ExileClient_gui_toaster_addTemplateToast;
}
else
{
  ["ErrorTitleOnly", ["The Vehicle Could Not Be Stored"]] call ExileClient_gui_toaster_addTemplateToast;
};
(findDisplay 0720) closeDisplay 0;