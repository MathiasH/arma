private["_result"];
disableSerialization;
_result = ["Do you really want to retrieve this vehicle?", "Confirm", "Yes", "Nah"] call BIS_fnc_guiMessage;
waitUntil { !isNil "_result" };
if (_result) then
{
	call ExileClient_VirtualGarage_network_RetrieveVehicleRequest;
};
true