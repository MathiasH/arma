private["_position","_icon"];
_position = getPos VGVehicle;
if (VirtualGarage3DIconVisible) then
{
	_icon = "\exile_assets\texture\ui\snap_blue_ca.paa";
	drawIcon3D [_icon, [1, 1, 1, 1], _position, 1, 1, 0];
};