private ["_Flag","_BuildRights"];
_playerUID = getPlayerUID player;
_playerHasToBeOnFlag = getNumber (missionconfigfile >> "VirtualGarageSettings" >> "VirtualGarage_PlayerHasToBeOnFlag");
if(_playerHasToBeOnFlag == 1)then{
  _Flag = nearestObject [player, "Exile_Construction_Flag_Static"];
  _BuildRights = _Flag getVariable ["ExileTerritoryBuildRights", []];
  if(getPlayerUID player in _BuildRights) then
  {
    if (ExileClientPlayerIsInCombat) then
    {
		  ["ErrorTitleOnly", ["You're in combat!"]] call ExileClient_gui_toaster_addTemplateToast;
    } else {
          createDialog "VirtualGarageDialog";
    }
  } else {
	["ErrorTitleOnly", ["You Do Not have access to this Garage"]] call ExileClient_gui_toaster_addTemplateToast;
  };
}
else
{
  createDialog "VirtualGarageDialog";
};