class CfgRemoteExec
{
	class Functions
	{
		mode = 1;
		jip = 0;
		class fnc_AdminReq 										 { allowedTargets=2; };
		class ExileServer_system_network_dispatchIncomingMessage { allowedTargets=2; };
		class fnc_cls_request 									 { allowedTargets=2; };
		class chopTreeRequest									 { allowedTargets=2; };
		class fn_xm8apps_server 								 { allowedTargets=2; };
		class BIS_fnc_effectKilledSecondaries					 { allowedTargets=2; };
		class bis_fnc_effectkilledairdestruction				 { allowedTargets=2; };
		class AUR_Play_Rappelling_Sounds						 { allowedTargets=2; };
		class AUR_Hint											 { allowedTargets=2; };
		class AUR_Hide_Object_Global							 { allowedTargets=2; };
		class AUR_Play_Rappelling_Sounds_Global					 { allowedTargets=2; };
		class AUR_Enable_Rappelling_Animation					 { allowedTargets=2; };
		class AUR_Enable_Rappelling_Animation_Global			 { allowedTargets=2; };
	};
	class Commands
	{
		mode=0;
		jip=0;
	};
};