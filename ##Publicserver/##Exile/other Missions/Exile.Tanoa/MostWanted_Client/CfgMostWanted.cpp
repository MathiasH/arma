class CfgMostWanted
{
    class Database
    {
        class Cleanup
        {
            friendsLifetime = 14;
        };

        class Immunity
        {
            interval = 1;
        };
    };
    class BountyValues
    {
        Values[]=
        {
            { "10000",  "1"  },
            { "20000",  "2"  },
            { "30000",  "3"  },
            { "40000",  "4"  },
            { "50000",  "5"  },
            { "60000",  "6"  },
            { "70000",  "7"  },
            { "80000",  "8"  },
            { "90000",  "9"  },
            { "100000", "10" },
            { "150000", "15" },
            { "200000", "20" }
        };
    };
};