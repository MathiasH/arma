private["_display","_bountyLB"];
call ExileClient_MostWanted_Dialog_HideSetBountiesTab;
call ExileClient_MostWanted_Dialog_HideContractsTab;
call ExileClient_MostWanted_Dialog_ShowBountiesTab;
call ExileClient_MostWanted_Dialog_ListPlayers;
call ExileClient_MostWanted_Dialog_ListPrices;
call ExileClient_MostWanted_Dialog_showActiveBounties;
call ExileClient_MostWanted_Dialog_showActiveContracts;
call ExileClient_MostWanted_Dialog_UpdatePlayerStats;
disableSerialization;
_display = uiNameSpace getVariable ["MostWantedDialog", displayNull];
_bountyLB = (_display displayCtrl 1501);
_bountyLB ctrlRemoveAllEventHandlers "LBSelChanged";
_bountyLB ctrlSetEventHandler ["LBSelChanged", "call ExileClient_MostWanted_Dialog_onLBChange;"];
ctrlEnable [2402,false];