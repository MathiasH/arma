class RscExileStatusBar{
        idd = -1;
		duration = 10e10;
        onLoad = "uiNamespace setVariable ['RscExileStatusBar', _this select 0];";
		fadein = 0;
		fadeout = 0;
		movingEnable = 0;
		objects[] = {};
        class controlsBackground
        {
                class statusBarImage
                {
                        idc = 55557;
						type = 0;
						style = 48;
                        x = 0.185 * safezoneW + safezoneX;
						y = 0.940044 * safezoneH + safezoneY;
						w = 0.65 * safezoneW;
						h = 0.0330033 * safezoneH;
                        colorText[] = {1, 1, 1, 1};
						colorBackground[]={0,0,0,0};
						sizeEx = 0.4;
						font = "OrbitronLight";
                        text = "";
                };
        }; 
        class controls
        {
                class statusBarText
                {
                        idc = 55554;
                        x = 0.185 * safezoneW + safezoneX;
						y = 0.940044 * safezoneH + safezoneY;
						w = 0.65 * safezoneW;
						h = 0.0330033 * safezoneH;
                        shadow = 2;
                        font = "OrbitronLight";
                        size = 0.032;
                        type = 13;
						style = 2;
                        text = "";
 
                        class Attributes
                        {
                                align="center";
                                color = "#ffffff";
                                font = "OrbitronLight";
                        };
                };
        };
};