private "_uid","_pic","_restart","_rscLayer","_unit","_damage","_hunger","_thirst","_moneyP","_time","_hours","_minutes";
disableSerialization;
if(isStreamFriendlyUIEnabled) exitWith {};
_uid = getPlayerUID player;
_pic = "mgt.paa"; 
[ 
    '<img align=''left'' size=''1.0'' shadow=''1'' image='+(str(_pic))+' />', 
    safeZoneX+0.027, 
    safeZoneY+safeZoneH-0.1, 
    99999, 
    0, 
    0, 
    3090 
] spawn bis_fnc_dynamicText;
MGTSB = true;
_restart =
{
	if !(MGTSB) then
	{
		_rscLayer = "RscExileStatusBar" call BIS_fnc_rscLayer;
		_rscLayer cutText ["","PLAIN",1,false];
	}
	else
	{
		_rscLayer = "RscExileStatusBar" call BIS_fnc_rscLayer;
		_rscLayer cutRsc ["RscExileStatusBar","PLAIN",1,false];
		if(isNull ((uiNamespace getVariable "RscExileStatusBar")displayCtrl 55554)) then
		{
			disableSerialization;
			_rscLayer = "RscExileStatusBar" call BIS_fnc_rscLayer;
			_rscLayer cutRsc ["RscExileStatusBar","PLAIN",1,false];
		};
		_unit = _this select 0;
		_damage = round ((1 - (damage player)) * 100);
		_hunger = round (ExileClientPlayerAttributes select 2);
		_thirst = round (ExileClientPlayerAttributes select 3);
		_moneyP = (player getVariable ["ExileMoney", 0]);
		if(_moneyP > 999)then{_moneyP = format ["%1k", floor (_moneyP / 1000)];};
		_respectTAN = ExileClientPlayerScore;
		profileNamespace setVariable ["ExileClientPlayerScore",_respectTAN];
		_time = (round(239-(serverTime)/60));
		_hours = (floor(_time/60));
		_minutes = (_time - (_hours * 60));
		switch(_minutes) do
		{
			case 9: {_minutes = "09"};
			case 8: {_minutes = "08"};
			case 7: {_minutes = "07"};
			case 6: {_minutes = "06"};
			case 5: {_minutes = "05"};
			case 4: {_minutes = "04"};
			case 3: {_minutes = "03"};
			case 2: {_minutes = "02"};
			case 1: {_minutes = "01"};
			case 0: {_minutes = "00"};
		};	
		((uiNamespace getVariable "RscExileStatusBar")displayCtrl 55554) ctrlSetStructuredText
		parseText
		format
		["
			<t shadow='2' color='#3FD4FC'><img size='1.6'  shadowColor='#131718' image='addons\restart\icons\players.paa' color='#3FD4FC'/> %2</t>
			<t shadow='2' color='#3FD4FC'><img size='1.0'  shadowColor='#131718' image='addons\restart\icons\health.paa' color='#3FD4FC'/> %3%1</t>
			<t shadow='2' color='#3FD4FC'><img size='1.0'  shadowColor='#131718' image='addons\restart\icons\poptab_ca.paa' color='#3FD4FC'/> %4</t>
			<t shadow='2' color='#3FD4FC'><img size='1.0'  shadowColor='#131718' image='addons\restart\icons\exile.paa' color='#3FD4FC'/> %5</t>
			<t shadow='2' color='#3FD4FC'><img size='1.6'  shadowColor='#131718' image='addons\restart\icons\hunger.paa' color='#3FD4FC'/> %6%1</t>
			<t shadow='2' color='#3FD4FC'><img size='1.6'  shadowColor='#131718' image='addons\restart\icons\thirst.paa' color='#3FD4FC'/> %7%1</t>
			<t shadow='2' color='#3FD4FC'><img size='1.6'  shadowColor='#131718' image='addons\restart\icons\restart.paa' color='#3FD4FC'/>%8:%9</t>",
					"%",
					((playersNumber west)+(playersNumber east)+(playersNumber civilian)+(playersNumber resistance)),
					_damage,
					_moneyP,
					_respectTAN,
					_hunger,
					_thirst,
					_hours,
					_minutes
		];
	};
};
[5, _restart, [], true] call ExileClient_system_thread_addtask;