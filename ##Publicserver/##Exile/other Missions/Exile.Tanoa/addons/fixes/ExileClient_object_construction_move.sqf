private["_object", "_result", "_tabs"];
disableSerialization;
_object = _this select 0;
if ((_object getVariable ["ExileMoney", 0] > 0) || !(magazineCargo _object isEqualTo []) || !(weaponCargo _object isEqualTo [])|| !(itemCargo _object isEqualTo [])) exitWith
{
	["ErrorTitleOnly", ["You can only move empty storage"]] call ExileClient_gui_toaster_addTemplateToast;
};
setMousePosition [0.5,0.5];
_result = ["Do you really want to move this object?", "Move?", "Yes", "Nah"] call BIS_fnc_guiMessage;
_pos = getPos player;
waitUntil {uiSleep 0.05; !isNil "_result" };
if(_pos distance (getPos _object) > 50)exitWith
{
	player setHitPointDamage ["HitLegs",1];
	["ErrorTitleOnly", ["You can't use this glitch anymore..!"]] call ExileClient_gui_toaster_addTemplateToast;
};
_playerHeight = (getPosATL player) select 2;
if (_playerHeight >= 45) exitWith
{
	["ErrorTitleAndText", ["Construction aborted!", "You are too high! #nopenisbases"]] call ExileClient_gui_toaster_addTemplateToast;
};
if (_result) then
{
	if (ExileClientPlayerIsInCombat) then
	{
		["ErrorTitleAndText", ["Construction aborted!", "You cannot build during a combat."]] call ExileClient_gui_toaster_addTemplateToast;
	}
	else
	{
		if ([player, 20] call ExileClient_util_world_isClimbingPlayerNearby) then
		{
			["ErrorTitleAndText", ["Construction aborted!", "You cannot build while someone is on a ladder."]] call ExileClient_gui_toaster_addTemplateToast;
		}
		else
		{
			ExileClientConstructionPosition = getPosATL _object;
			["moveConstructionRequest", [netId _object]] call ExileClient_system_network_send;
		};
	};
};
true