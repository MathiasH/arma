if !(isNull ExileClientInteractionObject) then 
{
	if ("Exile_Item_ThermalScannerPro" in (magazines player)) then 
	{		
		_territory = ExileClientInteractionObject call ExileClient_util_world_getTerritoryAtPosition;	
		if ((_territory getvariable ["ExileFlagProtected", 0]) isEqualTo 1) exitWith
		{
			["ErrorTitleAndText", ["Thermal Scanner", "Base is protected for now, come back later"]] call ExileClient_gui_toaster_addTemplateToast;
		};
		["scanCodeLockRequest", [netId ExileClientInteractionObject]] call ExileClient_system_network_send;
	};
};