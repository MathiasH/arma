private["_dialog", "_lockerAmount", "_lockerLimit", "_depositInput", "_withdrawInput", "_lockerAmountLabel", "_inventoryAmount", "_inventoryAmountLabel"];
_dialog = uiNameSpace getVariable ["RscExileLockerDialog", displayNull];
_lockerAmount = player getVariable ["ExileLocker", 0];
_lockerLimit = (getNumber(missionConfigFile >> "CfgLocker" >> "maxDeposit"));
_levelAMT = 0;
if(ExileClientPlayerScore >10000 && ExileClientPlayerScore < 50000) then {_levelAMT = 10000;};
if(ExileClientPlayerScore >50000 && ExileClientPlayerScore < 100000) then {_levelAMT = 30000;};
if(ExileClientPlayerScore >100000 && ExileClientPlayerScore < 200000) then {_levelAMT = 50000;};
if(ExileClientPlayerScore >200000 && ExileClientPlayerScore < 300000) then {_levelAMT = 75000;};
if(ExileClientPlayerScore >300000 && ExileClientPlayerScore < 400000) then {_levelAMT = 100000;};
if(ExileClientPlayerScore >400000 && ExileClientPlayerScore < 500000) then {_levelAMT = 150000;};
if(ExileClientPlayerScore >500000) then {_levelAMT = 200000;};
_lockerLimit = _lockerLimit + _levelAMT;
_depositInput = _dialog displayCtrl 4006;
_depositInput ctrlSetText "";
_withdrawInput = _dialog displayCtrl 4005;
_withdrawInput ctrlSetText "";
_lockerAmountLabel = _dialog displayCtrl 4000;
_lockerAmountLabel ctrlSetStructuredText (parseText format["<t size='1.4'>%1 / %2 <img image='\exile_assets\texture\ui\poptab_inline_ca.paa' size='1' shadow='true' /></t>", _lockerAmount call ExileClient_util_string_exponentToString, _lockerLimit call ExileClient_util_string_exponentToString]);
_inventoryAmount = player getVariable ["ExileMoney", 0];
_inventoryAmountLabel = _dialog displayCtrl 4001;
_inventoryAmountLabel ctrlSetStructuredText (parseText format["<t size='1.4'>%1 <img image='\exile_assets\texture\ui\poptab_inline_ca.paa' size='1' shadow='true' /></t>", _inventoryAmount call ExileClient_util_string_exponentToString]);
true