private["_maximumNumberOfTerritoriesPerPlayer", "_numberOfTerritories", "_display", "_config", "_price", "_priceText", "_range", "_rangeText", "_purchaseButton"];
disableSerialization;
if !((getPlayerUID player) in forumsupporter) then
{
	if (ExileClientPlayerScore > 1000) then
	{
		["SuccessTitleAndText", ["Office", localize 'str_TradOff']] call ExileClient_gui_toaster_addTemplateToast;
	};
};
_maximumNumberOfTerritoriesPerPlayer = getNumber (missionConfigFile >> "CfgTerritories" >> "maximumNumberOfTerritoriesPerPlayer");
_numberOfTerritories = player call ExileClient_util_territory_getNumberOfTerritories;
if (_numberOfTerritories >= _maximumNumberOfTerritoriesPerPlayer) exitWith
{
	["ErrorTitleAndText", ["Limit reached!", format["You can only own %1 territories at a time!", _maximumNumberOfTerritoriesPerPlayer]]] call ExileClient_gui_toaster_addTemplateToast;
};
createDialog "RscExilePurchaseTerritoryDialog";
_display = uiNameSpace getVariable ["RscExilePurchaseTerritoryDialog", displayNull];
_config = (getArray(missionConfigFile >> "CfgTerritories" >> "prices")) select 0;
_price = _config select 0;
_priceText = _display displayCtrl 4000;
_priceText ctrlSetStructuredText parsetext format ["<t size='1.4'>%1<img image='\exile_assets\texture\ui\poptab_notification_ca.paa' size='1' shadow='true' /></t>",_price];
_range = _config select 1;
_rangeText = _display displayCtrl 4002;
_rangeText ctrlSetStructuredText parsetext format ["<t size='1.4'>%1m</t>", _range];
if(_price > (player getVariable ["ExileMoney", 0]))then
{
	_purchaseButton = _display displayCtrl 4001;
	_purchaseButton ctrlEnable false;
	_priceText ctrlSetTextColor [0.91, 0, 0, 1];
};
true call ExileClient_gui_postProcessing_toggleDialogBackgroundBlur;
true