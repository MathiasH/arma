private["_listBoxControl","_listBoxControlIndex","_display","_spawnButton"];
disableSerialization;
_listBoxControl = _this select 0;
_listBoxControlIndex = _this select 1;
_display = uiNamespace getVariable ["xstremeGroundorHaloDialog",displayNull];
ExileClientSelectedSpawnLocationMarkerName = _listBoxControl lbData _listBoxControlIndex;
_spawnButton = _display displayCtrl 1600;
_spawnButton ctrlEnable true;
if !((_listBoxControl lbText _listBoxControlIndex) == "Random") then 
{
	ExileClientSelectedSpawnLocationMarkerName call ExileClient_gui_selectSpawnLocation_zoomToMarker;
};
true