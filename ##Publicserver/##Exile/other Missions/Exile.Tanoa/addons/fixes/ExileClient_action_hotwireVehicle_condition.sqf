private["_vehicle", "_result"];
_vehicle = _this;
_result = "";
try 
{
	if (ExilePlayerInSafezone) then
	{
		throw "You are in a safe zone!";
	};
	if ((getPosASL player) select 2 <-3) then
	{
		throw "You cannot do that in water!";
	};
	if (ExileClientPlayerIsInCombat) then
	{
		throw "You are in combat!";
	};
	switch (locked _vehicle) do 
	{
		case 0:	{ throw "Vehicle is not locked!"; };
		case 1:	{ throw "Vehicle does not have a lock!"; };
	};
	if !("Exile_Item_Knife" in (magazines player)) then
	{
		throw "You need a knife!";
	};
	if ((_vehicle distance player) > 7) then 
	{
		throw "You are too far away!";
	};
}
catch 
{
	_result = _exception;
};
_result