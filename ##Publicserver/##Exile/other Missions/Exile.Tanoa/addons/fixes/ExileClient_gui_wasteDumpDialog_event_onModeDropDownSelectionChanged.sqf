private["_display","_dropdown","_index","_vehicleObject","_mode","_cargo","_revenue","_revenueControl","_sellButton"];
disableSerialization;
_display = uiNameSpace getVariable ["RscExileWasteDumpDialog", displayNull];
_dropdown = _this select 0;
_index = _this select 1;
_vehicleObject = objectFromNetId (_dropdown lbData _index);
if (_vehicleObject getVariable ["TFSTowingIsBeingTowed", false])then
{
	["vehicleUntowRequest", [(netId _vehicleObject)]] call ExileClient_system_network_send;
};
if (isNull _vehicleObject) then
{
	closeDialog 0;
}
else 
{
	_mode = _dropdown lbValue _index;
	_cargo = _vehicleObject call ExileClient_util_containerCargo_list;
	_revenue = _cargo call ExileClient_util_gear_calculateTotalSellPrice;
	if (_mode isEqualTo 2) then
	{
		_revenue = _revenue + ([(typeOf _vehicleObject)] call ExileClient_util_gear_calculateTotalSellPrice);
	};
	_revenueControl = _display displayCtrl 4001;
	_revenueControl ctrlSetStructuredText (parseText (format ["<t size='1.4'>%1<img image='\exile_assets\texture\ui\poptab_notification_ca.paa' size='1' shadow='true' /></t>", _revenue]));
	_sellButton = _display displayCtrl 4000;
	if(_revenue > 0)then
	{
		_sellButton ctrlEnable true;
	}
	else
	{
		_sellButton ctrlEnable false;
	};
};
true