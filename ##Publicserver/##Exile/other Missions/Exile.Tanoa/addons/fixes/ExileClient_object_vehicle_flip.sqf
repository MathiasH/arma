private["_vehicle", "_pos"];
_vehicle = _this select 0;
_nearestVehicles = count nearestObjects [player, ["LandVehicle","Tank","Air","Ship"], 10];
if((ExilePlayerInSafezone) && (_nearestVehicles > 1)) exitWith 
{
	["ErrorTitleOnly", ["Other vehicle in close proximity"]] call ExileClient_gui_toaster_addTemplateToast;
	false
};
if (!(typeOf _vehicle in ["Exile_Bike_MountainBike","Exile_Bike_OldBike"]) && (isEngineOn _vehicle)) exitWith {false};
if ((locked _vehicle) isEqualTo 2) exitWith {false};
if (local _vehicle) then
{
	_vehicle allowDamage false;
	_pos = getPosATL _vehicle;
	_pos set [2,(_pos select 2) + 2];
	_vehicle setPosATL _pos;
	_vehicle setVectorUp [0, 0, 1];
	uiSleep 5;
	_vehicle allowDamage true;
}
else
{
	["flipVehRequest",[netId _vehicle]] call ExileClient_system_network_send;
};
true