private["_flag","_buildRights","_territoryLevel","_flags"];
disableSerialization;
try 
{
	if ((getNumber(missionConfigFile >> "CfgVirtualGarage" >> "canAccessGarageInCombat") isEqualTo 1) && {ExileClientPlayerIsInCombat}) then 
	{
		throw "You cannot access Virtual Garage while in combat!";	
	};
	_flag = player call ExileClient_util_world_getTerritoryAtPosition;
	if (isNull _flag) then 
	{
		throw "You must be in your territory in order to access Virtual Garage";
	};
	_buildRights = _flag getVariable ["ExileTerritoryBuildRights", []];
	if !((getPlayerUID player) in _buildRights) then 
	{
		throw "You do not have permission to access this territory's Virtual Garage";
	};
	if ((_flag getvariable ["ExileFlagStolen", 0]) isEqualTo 1) then
	{
		throw "You cannot access Virtual Garage until flag is restored!";
	};
	_flags = player nearObjects ["Exile_Construction_Flag_Static", 5];
	if(_flags isEqualTo [])exitWith
	{
		throw "Move closer to flag!";
	};
	call ExileClient_VirtualGarage_AccessGarage;
}
catch 
{
	[_exception, 'Okay'] call ExileClient_gui_xm8_showWarning;
};
true