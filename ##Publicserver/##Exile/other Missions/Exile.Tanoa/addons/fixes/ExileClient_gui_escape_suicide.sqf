private["_result", "_display"];
if(ExilePlayerInSafezone) exitWith
{
	["ErrorTitleAndText", ["Suicide?", "Leave trader to end your life"]] call ExileClient_gui_toaster_addTemplateToast;
};
disableSerialization;
_result = ["Do you really want to end your life?", "Confirm", "Yes", "Nah"] call BIS_fnc_guiMessage;
waitUntil { !isNil "_result" };
if (_result) then
{
	_display = findDisplay 49;
	if !(isNull _display) then
	{
		_display closeDisplay 2; 
	};
	player allowDamage true;
	player setDamage 1;
	player setVariable ['EnigmaRevivePermitted', false, false];
};
true