private["_responseCode", "_vehicleID"];
_responseCode = _this select 0;
_vehicleID = _this select 1;
if (_responseCode isEqualTo "Reset Code") then
{
	_vehicleID spawn ExileClient_object_vehicle_resetCode;
}
else 
{
	["ErrorTitleAndText", ["Failed to change PIN!", "Sell vehicle is your only option"]] call ExileClient_gui_toaster_addTemplateToast;
};
true