private["_display", "_playerUID", "_territoryDropDown", "_flag", "_buildRights", "_index", "_payButton", "_connectedSince", "_secs", "_min"];
disableSerialization;
_connectedSince = profileNamespace getVariable ["connectedtime",0];
if(_connectedSince > diag_tickTime - 900)then
{
	if((239-(serverTime)/60) >20)then
	{
		_secs = _connectedSince - (diag_tickTime -900);
		_min = _secs /60;
		if (_secs <60)then
		{
			["ErrorTitleAndText", ["I'm a bit busy right now",format ["Come back in <t>%1</t> seconds",round _secs]]] call ExileClient_gui_toaster_addTemplateToast;
		}
		else
		{
			if (_secs >90)then
			{
				["ErrorTitleAndText", ["I'm a bit busy right now",format ["Come back in <t>%1</t> minutes",round _min]]] call ExileClient_gui_toaster_addTemplateToast;
			}
		    else
			{
				["ErrorTitleAndText", ["I'm a bit busy right now",format ["Come back in <t>%1</t> minute",round _min]]] call ExileClient_gui_toaster_addTemplateToast;
			};
		};
	}
	else
	{
		if !((getPlayerUID player) in forumsupporter) then
		{
			if (ExileClientPlayerScore > 1000) then
			{
				["SuccessTitleAndText", ["Office", localize 'str_TradOff']] call ExileClient_gui_toaster_addTemplateToast;
			};
		};
		createDialog "RscExilePayTerritoryProtectionMoneyDialog";
		_display = uiNameSpace getVariable ["RscExilePayTerritoryProtectionMoneyDialog", displayNull];
		_playerUID = getPlayerUID player;
		_territoryDropDown = _display displayCtrl 4001;
		lbClear _territoryDropDown;
		{
			_flag = _x;
			_buildRights = _flag getVariable ["ExileTerritoryBuildRights", []];
			if (_playerUID in _buildRights) then
			{
				_name = _flag getVariable ["ExileTerritoryName", ""];
				_index = _territoryDropDown lbAdd _name;
				_territoryDropDown lbSetData [_index, netId _flag];
			};
		}
		forEach (allMissionObjects "Exile_Construction_Flag_Static");
		_payButton = _display displayCtrl 4002;
		_payButton ctrlEnable false;
	};
}
else
{
	if !((getPlayerUID player) in forumsupporter) then
	{
		if (ExileClientPlayerScore > 1000) then
		{
			["SuccessTitleAndText", ["Office", localize 'str_TradOff']] call ExileClient_gui_toaster_addTemplateToast;
		};
	};
	createDialog "RscExilePayTerritoryProtectionMoneyDialog";
	_display = uiNameSpace getVariable ["RscExilePayTerritoryProtectionMoneyDialog", displayNull];
	_playerUID = getPlayerUID player;
	_territoryDropDown = _display displayCtrl 4001;
	lbClear _territoryDropDown;
	{
		_flag = _x;
		_buildRights = _flag getVariable ["ExileTerritoryBuildRights", []];
		if (_playerUID in _buildRights) then
		{
			_name = _flag getVariable ["ExileTerritoryName", ""];
			_index = _territoryDropDown lbAdd _name;
			_territoryDropDown lbSetData [_index, netId _flag];
		};
	}
	forEach (allMissionObjects "Exile_Construction_Flag_Static");
	_payButton = _display displayCtrl 4002;
	_payButton ctrlEnable false;
};