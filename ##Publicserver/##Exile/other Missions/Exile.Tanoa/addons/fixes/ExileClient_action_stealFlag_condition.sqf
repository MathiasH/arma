private["_flagObject", "_result", "_buildRights","_ExileBountyFriends","_numberOfConstructions","_owner"];
_flagObject = _this;
_result = "";
try 
{
	if (isNull _flagObject) then
	{
		throw "Invalid flag object";
	};
	if (player getVariable ["AUR_Is_Rappelling", false]) then
	{
		throw "You cant do that when rappelling"; 
	};
	if ((_flagObject getvariable ["ExileFlagStolen", 0]) isEqualTo 1) then
	{
		throw "Cannot steal a flag twice!";
	};
	if ((_flagObject distance2D player) > 4) then 
	{
		throw "You are too far away!";
	};
	if (((getPosASL player) select 2) > (((getPosASL _flagObject) select 2) + 3)) then 
	{
		throw "You need to be lower on the pole to steal the flag!";
	};
	_buildRights = _flagObject getVariable ["ExileTerritoryBuildRights",[]];
	_ExileBountyFriends = player getVariable ["ExileBountyFriends",[]];
	if ((_buildRights) in _ExileBountyFriends) then
	{
		throw "You were recently grouped - You cannot steal this flag!";
	};
	if ((getPlayerUID player) in _buildRights) then
	{
		throw "You cannot steal your own flag!";
	};
	_numberOfConstructions = count (getPosATL player nearObjects ["Exile_Construction_Abstract_Static", 10]);
	if (((_flagObject getvariable ["ExileFlagProtected", 0]) isEqualTo 1) && (_numberOfConstructions > 2)) then
	{
		throw "Base is protected for now, come back later";
	};
	["ErrorTitleAndText", ["Base Raiding", localize 'str_raid']] call ExileClient_gui_toaster_addTemplateToast;
}
catch 
{
	_result = _exception;
};
_result