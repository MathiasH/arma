private["_vehicle"];
_vehicle = vehicle player;
if (!ExilePlayerInSafezone) exitWith {false};
{
    ExileClientVehiclesCollision pushBackUnique _x;
    player disableCollisionWith _x;
} forEach (player nearEntities [["LandVehicle","Tank","Air","Ship"], 50]);
{player disableUAVConnectability [_x,true]} foreach allUnitsUAV;
if (_vehicle isEqualTo player) then
{
	if !(isNull ExileClientSafeZoneVehicle) then
	{
		ExileClientSafeZoneVehicle removeEventHandler ["Fired", ExileClientSafeZoneVehicleFiredEventHandler];	
		ExileClientSafeZoneVehicle = objNull;
		ExileClientSafeZoneVehicleFiredEventHandler = nil;
	};
}
else 
{
	if (local _vehicle) then 
	{
		_vehicle allowDamage false;
	};
	if !(_vehicle isEqualTo ExileClientSafeZoneVehicle) then 
	{
		if !(isNull ExileClientSafeZoneVehicle) then 
		{
			ExileClientSafeZoneVehicle removeEventHandler ["Fired", ExileClientSafeZoneVehicleFiredEventHandler];	
			ExileClientSafeZoneVehicle = objNull;
			ExileClientSafeZoneVehicleFiredEventHandler = nil;
		};
		ExileClientSafeZoneVehicle = _vehicle;
		ExileClientSafeZoneVehicleFiredEventHandler = _vehicle addEventHandler ["Fired", {_this call ExileClient_object_player_event_onFiredSafeZoneVehicle}];
	};
};
true