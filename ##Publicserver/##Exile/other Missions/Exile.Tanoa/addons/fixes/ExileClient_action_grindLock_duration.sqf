private["_door", "_startTime", "_fullTime"];
_door = _this;
_startTime = _door getVariable ["ExileGrindTime", 0];
_fullTime = getNumber(missionConfigFile >> "CfgGrinding" >> "grindDuration");
_currentSkillLevel = player getVariable ["Exile_Skills_Engineer", 0];
if !(_currentSkillLevel isEqualTo 0) then
{
    _skillAttributes = getArray (missionConfigFile >> "CfgSkills" >> "Skills" >> "Engineer" >> "Attributes");
    _levelAttributesValue = _skillAttributes select (_currentSkillLevel -1);
    _fullTime = _fullTime - _levelAttributesValue;
};
(_fullTime * 60) - _startTime