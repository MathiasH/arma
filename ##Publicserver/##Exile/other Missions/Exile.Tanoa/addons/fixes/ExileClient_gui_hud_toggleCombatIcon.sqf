private["_display", "_icon"];
disableSerialization;
_display = uiNamespace getVariable "RscExileHUD";
_icon = _display displayCtrl 1008;
if (_this && {!(player getVariable ["ExileIsInCombat", false])}) then
{
    player setVariable ["ExileIsInCombat", true, true];
}
else
{
    if (!(_this) && {player getVariable["ExileIsInCombat", false]}) then
    {
        player setVariable ["ExileIsInCombat", nil, true];
    };
};
_icon ctrlShow _this;
true