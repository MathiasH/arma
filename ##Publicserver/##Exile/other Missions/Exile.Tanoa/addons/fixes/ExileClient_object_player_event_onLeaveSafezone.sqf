private["_vehicle"];
if !(ExilePlayerInSafezone) exitWith { false };
ExilePlayerInSafezone = false;
{
	if !(isNull _x) then
	{
		player enableCollisionWith _x;
	};
} forEach ExileClientVehiclesCollision;
ExileClientVehiclesCollision = [];
if !(isNil "ExileClientSafeZoneUpdateThreadHandle") then 
{
	[ExileClientSafeZoneUpdateThreadHandle] call ExileClient_system_thread_removeTask;
	ExileClientSafeZoneUpdateThreadHandle = nil;
};
player allowDamage true;
player addEventHandler ["HandleDamage", {_this call ExileClient_object_player_event_onHandleDamage}];
if !(isNull ExileClientSafeZoneVehicle) then
{
	if (local ExileClientSafeZoneVehicle) then 
	{
		ExileClientSafeZoneVehicle allowDamage true;
	};
	ExileClientSafeZoneVehicle removeEventHandler ["Fired", ExileClientSafeZoneVehicleFiredEventHandler];	
	ExileClientSafeZoneVehicle = objNull;
	ExileClientSafeZoneVehicleFiredEventHandler = nil;
};
_vehicle = vehicle player; 
if !(_vehicle isEqualTo player) then 
{
	if (local _vehicle) then 
	{
		_vehicle allowDamage true;
	};
};
if !(isNil "ExileClientSafeZoneESPEventHandler") then 
{
	removeMissionEventHandler ["Draw3D", ExileClientSafeZoneESPEventHandler];
	ExileClientSafeZoneESPEventHandler = nil;
};
{player enableUAVConnectability [_x,true]} foreach allUnitsUAV;
if (alive player) then 
{
	["Goodbye! Left trader"] spawn ExileClient_gui_baguette_show;
};
true