private["_display", "_playerUID", "_territoryDropDown", "_flag", "_buildRights", "_index", "_upgradeButton"];
disableSerialization;
if !((getPlayerUID player) in forumsupporter) then
{
	if (ExileClientPlayerScore > 1000) then
	{
		["SuccessTitleAndText", ["Office", localize 'str_TradOff']] call ExileClient_gui_toaster_addTemplateToast;
	};
};
createDialog "RscExileUpgradeTerritoryDialog";
_display = uiNameSpace getVariable ["RscExileUpgradeTerritoryDialog", displayNull];
_playerUID = getPlayerUID player;
_territoryDropDown = _display displayCtrl 4000;
lbClear _territoryDropDown;
{
	_flag = _x;
	_buildRights = _flag getVariable ["ExileTerritoryBuildRights", []];
	if (_playerUID in _buildRights) then
	{
		_name = _flag getVariable ["ExileTerritoryName", ""];
		_index = _territoryDropDown lbAdd _name;
		_territoryDropDown lbSetData [_index, netId _flag]; 
	};
}
forEach (allMissionObjects "Exile_Construction_Flag_Static");
true call ExileClient_gui_postProcessing_toggleDialogBackgroundBlur;
_upgradeButton = _display displayCtrl 4001;
_upgradeButton ctrlEnable false;