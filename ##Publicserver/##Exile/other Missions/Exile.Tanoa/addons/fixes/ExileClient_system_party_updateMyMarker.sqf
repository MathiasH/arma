private["_intersectingObjectArray", "_position"];
if ((getNumber (missionConfigFile >> "CfgExileParty" >> "allow3DMarkers")) isEqualTo 1) then 
{
	_intersectingObjectArray = lineIntersectsSurfaces [AGLToASL positionCameraToWorld [0, 0, 0], AGLToASL positionCameraToWorld [0, 0, 3000], vehicle player, objNull, true, 1, "VIEW", "FIRE"];
	if !(_intersectingObjectArray isEqualTo []) then 
	{
		_position = (_intersectingObjectArray select 0) select 0;
		["updateMyPartyMarkerRequest", [true, _position]] call ExileClient_system_network_send;
		player setVariable ["ExilePartyMarker", _position];
	}
	else 
	{
		["updateMyPartyMarkerRequest", [false, []]] call ExileClient_system_network_send;
		player setVariable ["ExilePartyMarker", -1];
	};
};