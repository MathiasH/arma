closeDialog 0;
ExileClientLastDiedPlayerObject = player;
ExileClientIsAutoRunning = false;
private _deadPosition = getPos ExileClientLastDiedPlayerObject;
if !((vehicle player) isEqualTo player) then
{
	unassignVehicle player;
	player action ["GetOut", vehicle player];
	player action ["Eject", vehicle player];
	private _relPos = (vehicle player) worldToModel (getPos player);
	private _position = call
	{
		if ((_relPos select 0) < 0.02) exitWith { _relPos set [0, -2]; _relPos };
		if ((_relPos select 0) > 0.02) exitWith { _relPos set [0, 2]; _relPos };
		[2,2,0]
	};
	_position = ((vehicle player) modelToWorld _position);
	player setPosATL _position;
};
setGroupIconsVisible [false, false];
false call ExileClient_gui_hud_toggle;
[] call ExileClient_object_player_event_unhook;
if !(ExileClientLastDeathMarker isEqualTo "") then 
{
	deleteMarkerLocal ExileClientLastDeathMarker;
};
ExileClientLastDeathMarker = createMarkerLocal [format ["Death%1", time], _deadPosition];
ExileClientLastDeathMarker setMarkerShapeLocal "ICON";
ExileClientLastDeathMarker setMarkerTypeLocal "KIA";
ExileClientLastDeathMarker setMarkerColorLocal "ColorRed";
ExileClientLastDeathMarker setMarkerAlphaLocal 0.5;
if (ExileClientIsInBush) then 
{
	call ExileClient_object_bush_detach;
};
if !(ExileClientBreakFreeActionHandle isEqualTo -1) then 
{
	player removeAction ExileClientBreakFreeActionHandle;
	ExileClientBreakFreeActionHandle = -1;
};
ExileClientIsHandcuffed = false;
ExileClientHostageTaker = objNull;
ExileClientNotificationQueue = [];
ExileIsPlayingRussianRoulette = false;
ExileRussianRouletteChair = false;
[] call ExileClient_gui_russianRoulette_hide;
[] call ExileClient_system_breathing_event_onPlayerDied;
[] call ExileClient_system_snow_event_onPlayerDied;
[] call ExileClient_system_radiation_event_onPlayerDied;