private ["_curSel","_data","_num"]; 
disableSerialization;
_display = uiNamespace getVariable ["xstremeGroundorHaloDialog",displayNull];
_lB = _display displayCtrl 1500;
_curSel = lbCurSel _lB;
_data = _lB lbData _curSel;
_num = parseNumber _data;
_flag = playerFlags select _num;
_baseName = lbText [1500, _curSel];
spawnRegistry pushBack [_baseName, time];
ExileClientSpawnLocationSelectionDone = true;
closeDialog 1;
true