private["_door","_result","_grinderUID","_numberOfConstructions","_territory"];
_door = _this;
_result = "";
try 
{
	switch (locked _door) do 
	{
		case 0:	{ throw "The door is not locked!"; };
		case 1:	{ throw "The door does not have a lock!"; };
	};
	_territory = player call ExileClient_util_world_getTerritoryAtPosition;
	if ((_territory getvariable ["ExileFlagProtected", 0]) isEqualTo 1) exitWith
	{
		throw "Base is protected for now, come back later";
	};
	if ((_door animationPhase 'DoorRotation') > 0.5) then 
	{
		throw "Please close the door first.";
	};
	if !("Exile_Item_Grinder" in (magazines player)) then
	{
		throw "You need a grinder!";
	};
	if !("Exile_Magazine_Battery" in (magazines player)) then
	{
		throw "You need a battery!";
	};
	if ((_door distance player) > 5.5) then 
	{
		throw "You are too far away!";
	};
	if (player getVariable ["AUR_Is_Rappelling", false]) then
	{
		throw "You cant do that when rappelling"; 
	};
	_grinderUID = _door getVariable ["ExileGrinderUID", ""];
	if (!(_grinderUID isEqualTo "") && {!(_grinderUID isEqualTo (getPlayerUID player))}) then
	{
		throw "Another grind is in progress.";
	};
	_door setVariable ["ExileGrindStartTime", diag_tickTime];
	if (_grinderUID isEqualTo "") then 
	{
		["grindNotificationRequest", [netId _this]] call ExileClient_system_network_send;
	};
	["ErrorTitleAndText", ["Base Raiding", localize 'str_raid']] call ExileClient_gui_toaster_addTemplateToast;
}
catch 
{
	_result = _exception;
};
_result