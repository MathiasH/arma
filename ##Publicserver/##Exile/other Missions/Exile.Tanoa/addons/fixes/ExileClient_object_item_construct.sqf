private["_itemClassName","_minimumDistanceToTraderZones","_minimumDistanceToSpawnZones","_maximumNumberOfTerritoriesPerPlayer","_numberOfTerritories","_cantBuildNear"];
_itemClassName = _this select 0;
if !(_itemClassName in (magazines player)) exitWith {false};
if( isClass(configFile >> "CfgMagazines" >> _itemClassName >> "Interactions" >> "Constructing") ) then
{
	if (findDisplay 602 != displayNull) then
	{
		(findDisplay 602) closeDisplay 2; 
	};
	try 
	{
		if !((vehicle player) isEqualTo player) then
		{
			throw "You cannot build while in a vehicle.";  
		};
		_minimumDistanceToTraderZones = getNumber (missionConfigFile >> "CfgTerritories" >> "minimumDistanceToTraderZones");
		if ([player, _minimumDistanceToTraderZones] call ExileClient_util_world_isTraderZoneInRange) then
		{
			throw "You are too close to a safe zone.";
		};
		if (player call ExileClient_util_world_isInNonConstructionZone) then
		{
			throw "Building is disallowed here on this server.";
		};
		_cantBuildNear = [
			"Land_i_Barracks_V1_F","Land_i_Barracks_V1_dam_F","Land_i_Barracks_V2_F","Land_Cargo_House_V1_F","Land_Cargo_HQ_V1_F","Land_Cargo_HQ_V2_F","Land_Cargo_HQ_V3_F",
			"Land_Cargo_Patrol_V2_F","Land_Cargo_Patrol_V3_F","Land_Cargo_Tower_V1_F","Land_Cargo_Tower_V1_No1_F","Land_Cargo_Tower_V1_No2_F","Land_Cargo_Tower_V1_No3_F",
			"Land_Cargo_Tower_V1_No4_F","Land_Cargo_Tower_V1_No5_F","Land_Cargo_Tower_V1_No6_F","Land_Cargo_Tower_V1_No7_F","Land_Cargo_Tower_V2_F","Land_Cargo_Tower_V3_F",
			"Land_MilOffices_V1_F","Land_Dome_Big_F","Land_Dome_Small_F","Land_u_Barracks_V2_F","Land_Hangar_F","Land_Mil_ControlTower_EP1","Land_Mil_hangar_EP1",
			"Land_Mil_Guardhouse_EP1","Land_Mil_Barracks_i_EP1","Land_Mil_House_EP1","land_ibr_hangar","Land_Mil_Barracks","Land_Ss_hangar","Land_a_stationhouse",
			"Land_Mil_ControlTower","Land_Mil_House","Land_Barrack2","Land_Hospital_main_F","Land_Hospital_side1_F","Land_Hospital_side2_F","Land_A_Hospital",
			"land_st_vez","Land_budova4_winter","Land_ns_Jbad_A_Stationhouse","land_pozorovatelna","Land_ns_Jbad_Mil_Barracks","Land_Mil_Barracks_i","Land_Barracks_01_grey_F",
			"Land_Airport_01_controlTower_F","Land_Airport_02_controlTower_F","Land_Airport_02_terminal_F","Land_Barracks_01_camo_F","Land_Airport_01_hangar_F",
			"Land_Airport_02_hangar_right_F","Land_Airport_02_hangar_left_F","Land_Radar_F","Land_Cargo_Patrol_V1_F","C130J_wreck_EP1","Land_AirstripPlatform_01_F"
		];
		if ({typeOf _x in _cantBuildNear} count nearestObjects[player, _cantBuildNear, 200] > 0) then 
		{ 
			throw "Building is disallowed here on this server."; 
		};
		if (player call ExileClient_util_world_isInConcreteMixerZone) then 
		{
			throw "You are too close to a concrete mixer zone.";
		};
		_minimumDistanceToSpawnZones = getNumber (missionConfigFile >> "CfgTerritories" >> "minimumDistanceToSpawnZones");
		if ([player, _minimumDistanceToSpawnZones] call ExileClient_util_world_isSpawnZoneInRange) then
		{
			throw "You are too close to a spawn zone.";
		};
		if(_itemClassName isEqualTo "Exile_Item_Flag") then 
		{ 
			_maximumNumberOfTerritoriesPerPlayer = getNumber (missionConfigFile >> "CfgTerritories" >> "maximumNumberOfTerritoriesPerPlayer");
			_numberOfTerritories = player call ExileClient_util_territory_getNumberOfTerritories;
			if (_numberOfTerritories >= _maximumNumberOfTerritoriesPerPlayer) then
			{
				throw "You have reached the maximum number of territories you can own.";
			};
			call ExileClient_gui_setupTerritoryDialog_show;
		}
		else 
		{
			[_itemClassName] call ExileClient_construction_beginNewObject;
		};
	}
	catch 
	{
		["ErrorTitleAndText", ["Construction aborted!", _exception]] call ExileClient_gui_toaster_addTemplateToast;
	};
};
true