private["_playersNearby","_object"];
_object = _this;
if !(isNull _object) then 
{
	try 
	{
		_playersNearby = [player, 10] call ExileClient_util_world_getAlivePlayerInfantryInRange;
		{
			if !(_x isEqualTo player) then
			{
				throw "Another player is nearby.";
			};
		}
		forEach _playersNearby;
		if !(ExileClientIsWaitingForInventoryMoneyTransaction) then 
		{
			ExileClientIsWaitingForInventoryMoneyTransaction = true;
			["takeMoneyRequest", [netId _object, 0]] call ExileClient_system_network_send;
		};
	}
	catch
	{
		["ErrorTitleAndText", ["Take Pop Tabs aborted!", _exception]] call ExileClient_gui_toaster_addTemplateToast;
	};
	true
};