private["_vehicle","_attachedObjects","_position"];
if (ExilePlayerInSafezone) exitWith { false };
if !(alive player) exitWith { false };
ExilePlayerInSafezone = true;
if (ExileClientPlayerIsInCombat)then
{
	["ErrorTitleOnly", ["Do not enter trader when in combat!"]] call ExileClient_gui_toaster_addTemplateToast;
	playSound3D ["a3\sounds_f\environment\animals\hen1.wss", player];
};
ExileClientVehiclesCollision = [];
{
    ExileClientVehiclesCollision pushBackUnique _x;
    player disableCollisionWith _x;
} forEach (player nearEntities [["LandVehicle","Tank","Air","Ship"], 50]);
{player disableUAVConnectability [_x,true]} foreach allUnitsUAV;
player allowDamage false;
player removeAllEventHandlers "HandleDamage";
_vehicle = vehicle player;
if (_vehicle isKindOf "Exile_Bike_MountainBike") then 
{
	deleteVehicle _vehicle;
	["ErrorTitleOnly", ["Bikes not permitted within traders"]] call ExileClient_gui_toaster_addTemplateToast;
};
if !(_vehicle isEqualTo player) then 
{
	if (local _vehicle) then 
	{
		_vehicle allowDamage false;
	};
	_attachedObjects = attachedObjects _vehicle;
	if !(_attachedObjects isEqualTo []) then 
	{
		_position = getPosATL _vehicle;
		{
			if ((typeOf _x) in ["DemoCharge_Remote_Mag", "SatchelCharge_Remote_Mag"]) then 
			{
				detach _x;
				_x setPosATL [(_position select 0) + random 2, (_position select 1) + random 2, 0.05];
				_x setDir (random 360);
			};
		}
		forEach _attachedObjects;
	};
	ExileClientSafeZoneVehicle = _vehicle;
	ExileClientSafeZoneVehicleFiredEventHandler = _vehicle addEventHandler ["Fired", {_this call ExileClient_object_player_event_onFiredSafeZoneVehicle}];
};
ExileClientSafeZoneESPEventHandler = addMissionEventHandler ["Draw3D", {20 call ExileClient_gui_safezone_safeESP}];
["Welcome! Entered trader"] spawn ExileClient_gui_baguette_show;
["Rules! No ramming of vehicles"] spawn ExileClient_gui_baguette_show;
["Warning! Thieves active in this area"] spawn ExileClient_gui_baguette_show;
ExileClientSafeZoneUpdateThreadHandle = [1, ExileClient_object_player_thread_safeZone, [], true] call ExileClient_system_thread_addtask;
true