private["_duration"];
disableSerialization;
if !(ExileClientPlayerIsBambi) then 
{
	_duration = _this;
	ExileClientPlayerIsBambi = true;
	if !((getPlayerUID player) in forumsupporter) then 
	{
		_deployed = false;
		player setVariable ["deployVehicle",_deployed];
	};
	ExileClientPlayerBambiStateExpiresAt = time + _duration; 
	true call ExileClient_gui_hud_toggleBambiIcon;
	ExileClientEndBambiStateThread = [_duration, ExileClient_object_player_bambiStateEnd, [], true] call ExileClient_system_thread_addTask;
};
true