if(player getVariable ["deployVehicle", true])exitWith 
{ 
	["ErrorTitleAndText", ["Deploy vehicle", "You already did this"]] spawn ExileClient_gui_toaster_addTemplateToast; 
};
offroad = false;
if !((getPlayerUID player) in forumsupporter) exitWith
{
	execVM "addons\deploy\quad.sqf";
};
_overWater = ((getPosASLVisual player) select 2 <0);
if(!_overWater)then
{
	private["_result"];
	disableSerialization;
	_result = ["What type of vehicle?", "Confirm", "Offroad", "Quadbike"] call BIS_fnc_guiMessage;
	waitUntil { !isNil "_result" };
	if (_result) then
	{
		offroad = true;
	};
};
execVM "addons\deploy\quad.sqf";