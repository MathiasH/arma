private "_startPosition","_endPosition","_intersections","_isBelowRoof","_vehicles";
_startPosition = getPosASL player;
_endPosition = [_startPosition select 0, _startPosition select 1, (_startPosition select 2 ) + 20];
_intersections = lineIntersectsSurfaces [_startPosition, _endPosition, player, objNull, false, 1, "GEOM", "VIEW"];
_isBelowRoof = !(_intersections isEqualTo []);
if(ExileClientPlayerIsInCombat)exitWith
{
	["WarningTitleAndText", ["Craft Vehicle", "You are in combat"]] call ExileClient_gui_toaster_addTemplateToast;
};
if(_isBelowRoof)exitWith
{
	["ErrorTitleAndText", ["Craft Vehicle", "Move into the open to do that"]] call ExileClient_gui_toaster_addTemplateToast;
};
_flags = player nearObjects ["Exile_Construction_Flag_Static", 60];
if!(_flags isEqualTo [])exitWith
{
	["ErrorTitleAndText", ["Craft Vehicle", "You cannot do that near bases"]] call ExileClient_gui_toaster_addTemplateToast;
};
if (player getVariable ["AUR_Is_Rappelling", false])exitWith
{
	["ErrorTitleOnly", ["You cant do that when rappelling"]] call ExileClient_gui_toaster_addTemplateToast; 
};
if(ExilePlayerInSafezone)exitWith
{
	["ErrorTitleAndText", ["Craft Vehicle", "You cannot do that in traders"]] call ExileClient_gui_toaster_addTemplateToast;
};
if!(vehicle player isEqualTo player)exitWith
{
	["ErrorTitleAndText", ["Craft Vehicle", "You cannot do that from vehicles"]] call ExileClient_gui_toaster_addTemplateToast;
};
_vehicles = player nearEntities [["LandVehicle","Air","Ship"],10];
if!(_vehicles isEqualTo [])exitWith
{
	["ErrorTitleAndText", ["Craft Vehicle", "You cannot do that near vehicles"]] call ExileClient_gui_toaster_addTemplateToast;
};
_overWater = ((getPosASLVisual player) select 2 <0);
if(!_overWater)then
{
	player playActionNow "Medic";
};
_deployed = true;
player setVariable ["deployVehicle",_deployed];
_spawnPos = player modelToWorld [0,3,0];
_spawnDir = (getDir player) -90;
SPAZ = [player, _spawnPos, _spawnDir, _overWater, offroad];
uiSleep 3;
publicVariableServer "SPAZ";
uiSleep 1;
if ((getPlayerUID player) in forumsupporter) then
{
	["SuccessTitleAndText", ["Craft Vehicle", "You crafted a temporary vehicle"]] call ExileClient_gui_toaster_addTemplateToast;
}
else
{
if (ExileClientPlayerScore < 1000) then
	{
		["SuccessTitleAndText", ["Craft Vehicle", "You crafted a temporary vehicle"]] call ExileClient_gui_toaster_addTemplateToast;
	}
	else
	{
		["SuccessTitleAndText", ["Craft Vehicle", "You crafted a temporary vehicle"]] call ExileClient_gui_toaster_addTemplateToast;
		["SuccessTitleAndText", ["Craft Vehicle", localize 'str_points']] call ExileClient_gui_toaster_addTemplateToast;
	};
};