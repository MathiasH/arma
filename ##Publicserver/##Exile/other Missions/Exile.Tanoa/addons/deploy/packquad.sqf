if !((vehicle player) isEqualTo player) exitWith 
{
	["ErrorTitleAndText", ["Pack Quadbike", "You cannot do that from vehicles"]] call ExileClient_gui_toaster_addTemplateToast;
};
if(ExilePlayerInSafezone)exitWith
{
	["ErrorTitleAndText", ["Pack Quadbike", "You cannot do that in traders"]] call ExileClient_gui_toaster_addTemplateToast;
};
if ((typeOf cursorTarget == "Exile_Bike_QuadBike_Black") && (getDammage cursorTarget < 0.25)) then {
    player playMoveNow "AinvPknlMstpSlayWrflDnon_medicOther";
    private["_vehls"];
    _vehls = nearestObject [player, "Exile_Bike_QuadBike_Black"];
    deletevehicle _vehls;
    uiSleep 1;
    ['SuccessTitleAndText', ['Pack Quadbike', 'You packed your Quadbike']] call ExileClient_gui_toaster_addTemplateToast;
    _deployed = false;
    player setVariable ["deployVehicle",_deployed];
}
else
{
	["ErrorTitleAndText", ["Pack Quadbike", "Quadbike is too damaged - repair first"]] call ExileClient_gui_toaster_addTemplateToast;
};