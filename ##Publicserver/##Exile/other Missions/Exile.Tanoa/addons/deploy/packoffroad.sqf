if !((vehicle player) isEqualTo player) exitWith 
{
	["ErrorTitleAndText", ["Pack Offroad", "You cannot do that from vehicles"]] call ExileClient_gui_toaster_addTemplateToast;
};
if(ExilePlayerInSafezone)exitWith
{
	["ErrorTitleAndText", ["Pack Offroad", "You cannot do that in traders"]] call ExileClient_gui_toaster_addTemplateToast;
};
if ((typeOf cursorTarget == "C_Offroad_01_F") && (getDammage cursorTarget < 0.25)) then {
    player playMoveNow "AinvPknlMstpSlayWrflDnon_medicOther";
    private["_vehls"];
    _vehls = nearestObject [player, "C_Offroad_01_F"];
    deletevehicle _vehls;
    uiSleep 1;
    ['SuccessTitleAndText', ['Pack Offroad', 'You packed your Offroad']] call ExileClient_gui_toaster_addTemplateToast;
    _deployed = false;
    player setVariable ["deployVehicle",_deployed];
}
else
{
	["ErrorTitleAndText", ["Pack Offroad", "Offroad is too damaged - repair first"]] call ExileClient_gui_toaster_addTemplateToast;
};