if !((vehicle player) isEqualTo player) exitWith 
{
	["ErrorTitleAndText", ["Pack Bike", "You cannot do that from vehicles"]] call ExileClient_gui_toaster_addTemplateToast;
};
if(ExilePlayerInSafezone)exitWith
{
	["ErrorTitleAndText", ["Pack Bike", "You cannot do that in traders"]] call ExileClient_gui_toaster_addTemplateToast;
};
player playMoveNow "AinvPknlMstpSlayWrflDnon_medicOther";
private["_vehls"];
_vehls = nearestObject [player, "Exile_Bike_MountainBike"];
deletevehicle _vehls;
uiSleep 1;
['SuccessTitleAndText', ['Pack Bike', 'You packed your Bike']] call ExileClient_gui_toaster_addTemplateToast;
_deployed = false;
player setVariable ["deployVehicle",_deployed];