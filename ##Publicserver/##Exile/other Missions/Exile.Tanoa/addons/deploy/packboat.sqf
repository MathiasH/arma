if !((vehicle player) isEqualTo player) exitWith 
{
	["ErrorTitleAndText", ["Pack Vehicle", "You cannot do that from vehicles"]] call ExileClient_gui_toaster_addTemplateToast;
};
if ((typeOf cursorTarget == "C_Scooter_Transport_01_F") && (getDammage cursorTarget < 0.25)) then {
    private["_vehls"];
    _vehls = nearestObject [player, "C_Scooter_Transport_01_F"];
    deletevehicle _vehls;
    uiSleep 1;
    if(getOxygenRemaining player <0.5)then
	{
		player setOxygenRemaining 1;
	};
    ['SuccessTitleAndText', ['Pack Vehicle', 'You packed your jetski']] call ExileClient_gui_toaster_addTemplateToast;
    _deployed = false;
    player setVariable ["deployVehicle",_deployed];
}
else
{
	["ErrorTitleAndText", ["Pack Vehicle", "Jetski is too damaged - Repair first"]] call ExileClient_gui_toaster_addTemplateToast;
};