private "_sizeTitle","_sizeSubText","_alignTitle","_alignSubText","_fontTitle","_fontSubText","_Wait","_FadeIn","_posDefault","_title","_shorttext","_posText","_title2","_shorttext2","_posText2","_title3","_shorttext3","_posText3","_title4","_shorttext4","_posText4","_t","_s","_p","_at","_as","_ap","_ms","_pX","_pY","_pW","_m","_tm";
if(isDedicated) exitWith {};
if(isStreamFriendlyUIEnabled) exitWith {};
_sizeTitle     = 0.55;
_sizeSubText   = 0.45;
_alignTitle    = "right";
_alignSubText  = "right";
_fontTitle     = "OrbitronMedium";
_fontSubText   = "OrbitronLight";
_Wait          = 10;
_FadeIn        = 3;
_posDefault    = [0.3,0.55,0.5];
_title         = format ["Welcome %1",name player];
_shorttext     = "MGT Arma 3 Exile Tanoa";
_posText       = [0.8,0.5,0.4];
_title2        = "Contact";
_shorttext2    = "mgtrolls.eu<br />ts3.mgtrolls.eu";
_posText2      = [0.8,0.5,0.4];
_title3        = "Donate";
_shorttext3    = "mgtrolls.eu/donate";
_posText3      = [0.8,0.5,0.4];
_title4        = "Rules";
_shorttext4    = "mgtrolls.eu/rules";
_posText4      = [0.8,0.5,0.4];
_ms = [];
for "_i" from 1 to 50 do
{
    _t = if (_i!=1) then { format["_title%1",_i] } else { "_title" };
    _s = if (_i!=1) then { format["_shorttext%1",_i] } else { "_shorttext" };
    _p = if (_i!=1) then { format["_postext%1",_i] } else { "_postext" };
    if (!isNil _t or !isNil _s) then
    {
        _at = if (!isNil _t) then { call compile _t } else { "" };
        _as = if (!isNil _s) then { call compile _s } else { "" };
        _ap = if (!isNil _p) then { call compile _p } else { _posDefault };
        _ms = _ms + [[_at,_as,_ap]];
    }
};
waitUntil{uiSleep 1; ExileClientPlayerIsSpawned}; 
waitUntil{player == player};
uiSleep _Wait;
{
    _t = _x select 0;
    _s = _x select 1;
    _pX = _x select 2 select 0;
    _pY = _x select 2 select 1;
    _pW = _x select 2 select 2;
    _m = format ["<t color='#3FD4FC' shadowColor='#131718' size='%1' align='%2' font='%3'>%4<br /></t>", _sizeTitle, _alignTitle, _fontTitle, _t];
    _m = _m + format ["<t color='#3FD4FC' shadowColor='#131718' size='%1' align='%2' font='%3'>%4<br /></t>", _sizeSubText, _alignSubText, _fontSubText, _s];
    _tm = round (count toArray (_t+_s) / 6 / 2) + 3;
    [ _m, [_pX * safeZoneW + safeZoneX, _pW], [_pY * safezoneH + safezoneY, 1 * safezoneH + safezoneY], _tm, _FadeIn ] spawn BIS_fnc_dynamicText;
    uiSleep (_tm+_FadeIn+4);
} forEach _ms;
true