private "_paint","_painting";
if((player call ExileClient_util_world_isInOwnTerritory) or (ExilePlayerInSafezone)) then
{
	_paint = 0;
	[parseText format["<t size='0.3' shadow='2' color='#3FD4FC' shadowColor='#131718' font='OrbitronLight'>You do not earn a prisoners paycheck whilst here</t>"],0,-0.35,5,1] spawn bis_fnc_dynamictext;
	playSound "addItemFailed";
}
else
{
	_paint = 100;
	_painting = (player getVariable ["ExileMoney", 0]);
	_painting = _painting + _paint;
	playSound "readoutClick";
	[parseText format["<t size='0.3' shadow='2' color='#3FD4FC' shadowColor='#131718' font='OrbitronLight'>You received a prisoner paycheck of %1 poptabs, you now have %2 poptabs</t>",_paint,_painting],0,-0.35,5,1] spawn bis_fnc_dynamictext;
	player setVariable ["ExileMoney",_painting,true];
	call ExileClient_object_player_stats_update;
};