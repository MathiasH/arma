private["_result"];
disableSerialization;
_result = ["Do you really want to abandon your territory?                     Read the information carefully", "Abandon Territory Info", "continue"] call BIS_fnc_guiMessage;
_result1 = ["Have you removed any constructions you want to keep?    All constructions will be deleted after the next server restart", "Abandon Territory Info", "continue"] call BIS_fnc_guiMessage;
_result2 = ["Abandon territory", "Abandon Territory confirmation", "Yes", "No"] call BIS_fnc_guiMessage;
waitUntil { !isNil "_result2"};
if (_result2) then
{
	_tFlag = nearestObject [player, "Exile_Construction_Flag_Static"];
	_owner = _tFlag getVariable ["ExileOwnerUID", ""];
	if !(_owner isEqualTo getPlayerUID player) exitWith 
	{
		["ErrorTitleOnly", ["Only the territory owner can do this"]] call ExileClient_gui_toaster_addTemplateToast;
	};
	_pos = getPosATL _tFlag;
	_territoryName = _tFlag getVariable ["ExileTerritoryName", 0];
	_stolen = false;
	if !((_tFlag getvariable ['ExileFlagStolen',1]) isEqualTo 0) then
	{
		_stolen = true;
	};
	abandon = [_tFlag,_pos,player,getPlayerUID player,_territoryName,_stolen];
	uiSleep 0.2;
	publicVariableServer "abandon";
	uiSleep 0.2;
	deletevehicle _tFlag;
};
true