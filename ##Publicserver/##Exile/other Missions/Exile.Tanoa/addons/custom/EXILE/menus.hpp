class EBM_Metalwall_window
{
	targetType = 2;
	target = "EBM_Metalwall_window";
	class Actions 
	{
		class SetPinCode : ExileAbstractAction
		{
			title = "Set PIN";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 0)";
			action = "_this spawn ExileClient_object_lock_setPin";
		};
		class AddALock : ExileAbstractAction
		{
			title = "Add a Lock";
			condition = "call ExileClient_object_construction_lockAddShow";
			action = "_this spawn ExileClient_object_construction_lockAdd";
		};
		class GrindLock : ExileAbstractAction
		{
			title = "Grind Lock";
			condition = "(getNumber(missionConfigFile >> 'CfgGrinding' >> 'enableGrinding') isEqualTo 1) && ('Exile_Item_Grinder' in (magazines player)) && ('Exile_Magazine_Battery' in (magazines player)) && ((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo -1) && ((ExileClientInteractionObject animationPhase 'DoorRotation') < 0.5)";
			action = "['GrindLock', _this select 0] call ExileClient_action_execute";
		};
	};
};
class EBM_door_frame
{
	targetType = 2;
	target = "EBM_door_frame";
	class Actions 
	{
		class SetPinCode : ExileAbstractAction
		{
			title = "Set PIN";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 0)";
			action = "_this spawn ExileClient_object_lock_setPin";
		};
		class AddALock : ExileAbstractAction
		{
			title = "Add a Lock";
			condition = "call ExileClient_object_construction_lockAddShow";
			action = "_this spawn ExileClient_object_construction_lockAdd";
		};
		class GrindLock : ExileAbstractAction
		{
			title = "Grind Lock";
			condition = "(getNumber(missionConfigFile >> 'CfgGrinding' >> 'enableGrinding') isEqualTo 1) && ('Exile_Item_Grinder' in (magazines player)) && ('Exile_Magazine_Battery' in (magazines player)) && ((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo -1) && ((ExileClientInteractionObject animationPhase 'DoorRotation') < 0.5)";
			action = "['GrindLock', _this select 0] call ExileClient_action_execute";
		};
	};
};
class EBM_Metalwall_floorport_door
{
	targetType = 2;
	target = "EBM_Metalwall_floorport_door";
	class Actions 
	{
		class SetPinCode : ExileAbstractAction
		{
			title = "Set PIN";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 0)";
			action = "_this spawn ExileClient_object_lock_setPin";
		};
		class AddALock : ExileAbstractAction
		{
			title = "Add a Lock";
			condition = "call ExileClient_object_construction_lockAddShow";
			action = "_this spawn ExileClient_object_construction_lockAdd";
		};
		class GrindLock : ExileAbstractAction
		{
			title = "Grind Lock";
			condition = "(getNumber(missionConfigFile >> 'CfgGrinding' >> 'enableGrinding') isEqualTo 1) && ('Exile_Item_Grinder' in (magazines player)) && ('Exile_Magazine_Battery' in (magazines player)) && ((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo -1) && ((ExileClientInteractionObject animationPhase 'DoorRotation') < 0.5)";
			action = "['GrindLock', _this select 0] call ExileClient_action_execute";
		};
	};
};
class EBM_Metalwall_Door
{
	targetType = 2;
	target = "EBM_Metalwall_Door";
	class Actions 
	{
		class SetPinCode : ExileAbstractAction
		{
			title = "Set PIN";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 0)";
			action = "_this spawn ExileClient_object_lock_setPin";
		};
		class AddALock : ExileAbstractAction
		{
			title = "Add a Lock";
			condition = "call ExileClient_object_construction_lockAddShow";
			action = "_this spawn ExileClient_object_construction_lockAdd";
		};
		class GrindLock : ExileAbstractAction
		{
			title = "Grind Lock";
			condition = "(getNumber(missionConfigFile >> 'CfgGrinding' >> 'enableGrinding') isEqualTo 1) && ('Exile_Item_Grinder' in (magazines player)) && ('Exile_Magazine_Battery' in (magazines player)) && ((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo -1) && ((ExileClientInteractionObject animationPhase 'DoorRotation') < 0.5)";
			action = "['GrindLock', _this select 0] call ExileClient_action_execute";
		};
	};
};
class EBM_Brickwall_window
{
	targetType = 2;
	target = "EBM_Brickwall_window";
	
	class Actions 
	{
		class SetPinCode : ExileAbstractAction
		{
			title = "Set PIN";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 0)";
			action = "_this spawn ExileClient_object_lock_setPin";
		};
		
		class AddALock : ExileAbstractAction
		{
			title = "Add a Lock";
			condition = "call ExileClient_object_construction_lockAddShow";
			action = "_this spawn ExileClient_object_construction_lockAdd";
		};
		class GrindLock : ExileAbstractAction
		{
			title = "Grind Lock";
			condition = "(getNumber(missionConfigFile >> 'CfgGrinding' >> 'enableGrinding') isEqualTo 1) && ('Exile_Item_Grinder' in (magazines player)) && ('Exile_Magazine_Battery' in (magazines player)) && ((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo -1) && ((ExileClientInteractionObject animationPhase 'DoorRotation') < 0.5)";
			action = "['GrindLock', _this select 0] call ExileClient_action_execute";
		};
	};
};
class EBM_Brickwall_floorport_door
{
	targetType = 2;
	target = "EBM_Brickwall_floorport_door";
	
	class Actions 
	{
		class SetPinCode : ExileAbstractAction
		{
			title = "Set PIN";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 0)";
			action = "_this spawn ExileClient_object_lock_setPin";
		};
		
		class AddALock : ExileAbstractAction
		{
			title = "Add a Lock";
			condition = "call ExileClient_object_construction_lockAddShow";
			action = "_this spawn ExileClient_object_construction_lockAdd";
		};
		class GrindLock : ExileAbstractAction
		{
			title = "Grind Lock";
			condition = "(getNumber(missionConfigFile >> 'CfgGrinding' >> 'enableGrinding') isEqualTo 1) && ('Exile_Item_Grinder' in (magazines player)) && ('Exile_Magazine_Battery' in (magazines player)) && ((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo -1) && ((ExileClientInteractionObject animationPhase 'DoorRotation') < 0.5)";
			action = "['GrindLock', _this select 0] call ExileClient_action_execute";
		};
	};
};
class EBM_Brickwall_door
{
	targetType = 2;
	target = "EBM_Brickwall_Door";
	class Actions 
	{
		class SetPinCode : ExileAbstractAction
		{
			title = "Set PIN";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 0)";
			action = "_this spawn ExileClient_object_lock_setPin";
		};
		class AddALock : ExileAbstractAction
		{
			title = "Add a Lock";
			condition = "call ExileClient_object_construction_lockAddShow";
			action = "_this spawn ExileClient_object_construction_lockAdd";
		};
		class GrindLock : ExileAbstractAction
		{
			title = "Grind Lock";
			condition = "(getNumber(missionConfigFile >> 'CfgGrinding' >> 'enableGrinding') isEqualTo 1) && ('Exile_Item_Grinder' in (magazines player)) && ('Exile_Magazine_Battery' in (magazines player)) && ((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo -1) && ((ExileClientInteractionObject animationPhase 'DoorRotation') < 0.5)";
			action = "['GrindLock', _this select 0] call ExileClient_action_execute";
		};
	};
};
class CargoSmall
{
	targetType = 2;
	target = "Land_CargoBox_V1_F";
	
	class Actions 
	{
		class Lock : ExileAbstractAction
		{
			title = "Lock";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 0)";
			action = "true spawn ExileClient_object_lock_toggle";
		};
		class Unlock : ExileAbstractAction
		{
			title = "Unlock";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo -1)";
			action = "false spawn ExileClient_object_lock_toggle";
		};
		class SetPinCode : ExileAbstractAction
		{
			title = "Set PIN";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 0)";
			action = "_this spawn ExileClient_object_lock_setPin";
		};
		class Install: ExileAbstractAction
		{
			title = "Install";
			condition = "isNull (attachedTo ExileClientInteractionObject) && ((ExileClientInteractionObject getvariable ['ExileOwnerUID',1]) isEqualTo 1)";
			action = "_this call ExileClient_object_supplyBox_install";
		};
		class GrindLock : ExileAbstractAction
		{
			title = "Grind Lock";
			condition = "(getNumber(missionConfigFile >> 'CfgGrinding' >> 'enableGrinding') isEqualTo 1) && ('Exile_Item_Grinder' in (magazines player)) && ('Exile_Magazine_Battery' in (magazines player)) && ((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo -1)";
			action = "['GrindLock', _this select 0] call ExileClient_action_execute";
		};
	};
};
class Cargo20Military
{
	targetType = 2;
	target = "Land_Cargo20_military_green_F";
	
	class Actions 
	{
		class ScanLock: ExileAbstractAction
		{
			title = "Scan Lock";
			condition = "('Exile_Item_ThermalScannerPro' in (magazines player)) && !((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 1) && !ExilePlayerInSafezone";
			action = "_this call ExileClient_object_lock_scan";
		};
		class Lock : ExileAbstractAction
		{
			title = "Lock";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 0)";
			action = "true spawn ExileClient_object_lock_toggle";
		};

		class Unlock : ExileAbstractAction
		{
			title = "Unlock";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo -1)";
			action = "false spawn ExileClient_object_lock_toggle";
		};

		class SetPinCode : ExileAbstractAction
		{
			title = "Set PIN";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 0)";
			action = "_this spawn ExileClient_object_lock_setPin";
		};
		class GrindLock : ExileAbstractAction
		{
			title = "Grind Lock";
			condition = "(getNumber(missionConfigFile >> 'CfgGrinding' >> 'enableGrinding') isEqualTo 1) && ('Exile_Item_Grinder' in (magazines player)) && ('Exile_Magazine_Battery' in (magazines player)) && ((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo -1)";
			action = "['GrindLock', _this select 0] call ExileClient_action_execute";
		};
	};
};
class Cargo40
{
	targetType = 2;
	target = "Land_Cargo40_light_green_F";

	class Actions 
	{
		class ScanLock: ExileAbstractAction
		{
			title = "Scan Lock";
			condition = "('Exile_Item_ThermalScannerPro' in (magazines player)) && !((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 1) && !ExilePlayerInSafezone";
			action = "_this call ExileClient_object_lock_scan";
		};
		class Lock : ExileAbstractAction
		{
			title = "Lock";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 0)";
			action = "true spawn ExileClient_object_lock_toggle";
		};
		
		class Unlock : ExileAbstractAction
		{
			title = "Unlock";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo -1)";
			action = "false spawn ExileClient_object_lock_toggle";
		};

		class SetPinCode : ExileAbstractAction
		{
			title = "Set PIN";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 0)";
			action = "_this spawn ExileClient_object_lock_setPin";
		};
		class GrindLock : ExileAbstractAction
		{
			title = "Grind Lock";
			condition = "(getNumber(missionConfigFile >> 'CfgGrinding' >> 'enableGrinding') isEqualTo 1) && ('Exile_Item_Grinder' in (magazines player)) && ('Exile_Magazine_Battery' in (magazines player)) && ((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo -1)";
			action = "['GrindLock', _this select 0] call ExileClient_action_execute";
		};
	};
};
class Cargo20
{
	targetType = 2;
	target = "Land_Cargo20_sand_F";

	class Actions 
	{
		class ScanLock: ExileAbstractAction
		{
			title = "Scan Lock";
			condition = "('Exile_Item_ThermalScannerPro' in (magazines player)) && !((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 1) && !ExilePlayerInSafezone";
			action = "_this call ExileClient_object_lock_scan";
		};
		class Lock : ExileAbstractAction
		{
			title = "Lock";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 0)";
			action = "true spawn ExileClient_object_lock_toggle";
		};

		class Unlock : ExileAbstractAction
		{
			title = "Unlock";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo -1)";
			action = "false spawn ExileClient_object_lock_toggle";
		};

		class SetPinCode : ExileAbstractAction
		{
			title = "Set PIN";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 0)";
			action = "_this spawn ExileClient_object_lock_setPin";
		};
		class GrindLock : ExileAbstractAction
		{
			title = "Grind Lock";
			condition = "(getNumber(missionConfigFile >> 'CfgGrinding' >> 'enableGrinding') isEqualTo 1) && ('Exile_Item_Grinder' in (magazines player)) && ('Exile_Magazine_Battery' in (magazines player)) && ((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo -1)";
			action = "['GrindLock', _this select 0] call ExileClient_action_execute";
		};
	};
};
class Bunker
{
	targetType = 2;
	target = "Land_Bunker_F";

	class Actions 
	{
		class ScanLock: ExileAbstractAction
		{
			title = "Scan Lock";
			condition = "('Exile_Item_ThermalScannerPro' in (magazines player)) && !((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 1) && !ExilePlayerInSafezone";
			action = "_this call ExileClient_object_lock_scan";
		};
		class Lock : ExileAbstractAction
		{
			title = "Lock";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 0)";
			action = "true spawn ExileClient_object_lock_toggle";
		};

		class Unlock : ExileAbstractAction
		{
			title = "Unlock";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo -1)";
			action = "false spawn ExileClient_object_lock_toggle";
		};

		class SetPinCode : ExileAbstractAction
		{
			title = "Set PIN";
			condition = "((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo 0)";
			action = "_this spawn ExileClient_object_lock_setPin";
		};
		class GrindLock : ExileAbstractAction
		{
			title = "Grind Lock";
			condition = "(getNumber(missionConfigFile >> 'CfgGrinding' >> 'enableGrinding') isEqualTo 1) && ('Exile_Item_Grinder' in (magazines player)) && ('Exile_Magazine_Battery' in (magazines player)) && ((ExileClientInteractionObject getvariable ['ExileIsLocked',1]) isEqualTo -1)";
			action = "['GrindLock', _this select 0] call ExileClient_action_execute";
		};
	};
};
class Barrel
{
	targetType = 2;
	target = "MetalBarrel_burning_F";

	class Actions 
	{
		class Move: ExileAbstractAction
		{
			title = "Move";
			condition = "call ExileClient_util_world_isInOwnTerritory";
			action = "_this spawn ExileClient_object_construction_move";
		};
		class Deconstruct: ExileAbstractAction
		{
			title = "Remove";
			condition = "call ExileClient_util_world_isInOwnTerritory";
			action = "_this spawn ExileClient_object_construction_deconstruct";
		};
	};
};