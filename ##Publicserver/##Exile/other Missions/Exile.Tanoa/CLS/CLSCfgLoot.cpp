class CLSCfgLootTable
{
	class Items
	{
		itemType = "item";
		items[] = {
		{ "Exile_Item_SandBagsKit_Long" },{ "Exile_Item_SandBagsKit_Corner" },{ "Exile_Item_MetalHedgehogKit" },{ "Exile_Item_MetalScrews" },{ "NVGoggles" },{ "Binocular" },{ "Exile_Item_CanOpener" },{ "Exile_Item_Magazine01" },{ "Exile_Item_Magazine02" },{ "Exile_Item_Magazine03" },{ "Exile_Item_Magazine04" },{ "Exile_Item_Grinder" }
		};
	};
	class Equipment
	{
		itemType = "item";
		items[] = {
		{ "NVGoggles" },{ "Binocular" },{ "acc_flashlight" },{ "acc_pointer_IR" },{ "optic_MRCO" },{ "optic_DMS" },{ "optic_NVS" }
		};
	};
	class Medical
	{
		itemType = "magazine";
		items[] = {
		{ "Exile_Item_Vishpirin" },{ "Exile_Item_Bandage" },{ "Exile_Item_InstaDoc" },{ "Exile_Item_Heatpack" },{ "Exile_Item_Defibrillator" }
		};
	};
	class MedicalLOW
	{
		itemType = "magazine";
		items[] = {
		{ "Exile_Item_Vishpirin" },{ "Exile_Item_Bandage" }
		};
	};
	class Generic
	{
		itemType = "magazine";
		items[] = {
		{ "Exile_Item_FloodLightKit" },{ "Exile_Item_PortableGeneratorKit" },{ "Exile_Item_Rope" },{ "Exile_Item_LightBulb" },{ "Exile_Item_CamoTentKit" },{ "Exile_Item_Matches" },{ "Exile_Item_PlasticBottleDirtyWater" },{ "Exile_Item_MetalWire" },{ "Exile_Item_MetalScrews" },{ "exile_item_waterCanisterDirtyWater" },{ "Exile_Item_DuctTape" }
		};
	};
	class GenericInd
	{
		itemType = "magazine";
		items[] = {
		{ "Exile_Magazine_Battery" },{ "Exile_Item_ZipTie" },{ "Exile_Item_FuelCanisterFull" },{ "Exile_Item_FuelCanisterEmpty" },{ "Exile_Item_DuctTape" },{ "Exile_Item_Matches" },{ "Exile_Item_PlasticBottleDirtyWater" },{ "Exile_Item_PortableGeneratorKit" },{ "Exile_Item_ExtensionCord" },{ "Exile_Item_Rope" },{ "Exile_Item_CordlessScrewdriver" },{ "Exile_Item_FireExtinguisher" },{ "Exile_Item_Hammer" },{ "Exile_Item_OilCanister" },{ "Exile_Item_Screwdriver" },{ "Exile_Item_Wrench" },{ "Exile_Item_Cement" },{ "Exile_Item_Sand" },{ "Exile_Item_BurlapSack" },{ "Exile_Item_MetalWire" },{ "Exile_Item_MetalScrews" },{ "exile_item_waterCanisterDirtyWater" },{ "Exile_Item_MobilePhone" }
		};
	};
	class GenericAuto
	{
		itemType = "magazine";
		items[] = {
		{ "Exile_Item_MetalScrews" },{ "Exile_Magazine_Battery" },{ "Exile_Item_FuelCanisterFull" },{ "Exile_Item_FuelCanisterEmpty" },{ "Exile_Item_DuctTape" },{ "FlareGreen_F" },{ "FlareRed_F" },{ "FlareWhite_F" },{ "FlareYellow_F" },{ "Exile_Item_BeefParts" },{ "Exile_Item_Cheathas" },{ "Exile_Item_Noodles" },{ "Exile_Item_SeedAstics" },{ "Exile_Item_Raisins" },{ "Exile_Item_Moobar" },{ "Exile_Item_InstantCoffee" },{ "Exile_Item_PowerDrink" },{ "Exile_Item_PlasticBottleFreshWater" },{ "Exile_Item_Beer" },{ "Exile_Item_EnergyDrink" },{ "Exile_Item_MountainDupe" }
		};
	};
	class Special
	{
		itemType = "magazine";
		items[] = {
		{ "Exile_Item_Laptop" },{ "Exile_Item_BaseCameraKit" },{ "Exile_Item_ThermalScannerPro" },{ "Exile_Item_MobilePhone" },{ "Exile_Item_Knife" },{ "Exile_Item_Foolbox" }
		};
	};
	class SniperRifle
	{
		itemType = "weapon";
		items[] = {
		{ "Exile_Weapon_ksvk" },{ "hlc_rifle_SG550Sniper" },{ "hlc_rifle_SG550Sniper_RIS" },{ "hlc_rifle_M14dmr_Rail" }, { "hlc_rifle_M14_Rail" },{ "hlc_rifle_M21_Rail" },{ "CUP_srifle_G22_des" },{ "CUP_srifle_G22_wdl" },{ "srifle_DMR_01_F" },{ "srifle_EBR_F" },{ "srifle_GM6_F" },{ "srifle_LRR_F" },{ "srifle_DMR_02_camo_F" },{ "srifle_DMR_02_F" },{ "srifle_DMR_02_sniper_F" },{ "srifle_DMR_03_F" },{ "srifle_DMR_03_khaki_F" },{ "srifle_DMR_03_tan_F" },{ "srifle_DMR_03_woodland_F" },{ "srifle_DMR_04_F" },{ "srifle_DMR_04_Tan_F" },{ "srifle_DMR_05_blk_F" },{ "srifle_DMR_05_hex_F" },{ "srifle_DMR_05_tan_F" },{ "srifle_DMR_06_camo_F" },{ "srifle_DMR_06_olive_F" },{ "Exile_Weapon_DMR" },{ "Exile_Weapon_LeeEnfield" },{ "Exile_Weapon_CZ550" },{ "Exile_Weapon_SVD" },{ "Exile_Weapon_SVDCamo" },{ "Exile_Weapon_VSSVintorez" },{ "Exile_Weapon_m107" },{ "CUP_srifle_CZ550_rail" },{ "CUP_srifle_L129A1" },{ "hlc_rifle_M1903A1_unertl" },{ "hlc_rifle_M1903A1" },{ "hlc_rifle_M1903A1OMR" },{ "hlc_rifle_psg1" },{ "hlc_rifle_psg1A1" },{ "hlc_rifle_PSG1A1_RIS" },{ "hlc_rifle_awcovert" },{ "hlc_rifle_awcovert_BL" },{ "hlc_rifle_awcovert_FDE" },{ "hlc_rifle_awmagnum" },{ "hlc_rifle_awmagnum_BL" },{ "hlc_rifle_awmagnum_FDE" },{ "hlc_rifle_awMagnum_FDE_ghillie" },{ "hlc_rifle_awMagnum_BL_ghillie" },{ "hlc_rifle_awMagnum_OD_ghillie" },{ "hlc_rifle_FN3011" },{ "hlc_rifle_FN3011_WDL" },{ "hlc_rifle_FN3011Modern" },{ "hlc_rifle_FN3011Modern_camo" },{ "hlc_rifle_FN3011Lynx" },{ "hlc_rifle_FN3011Tactical" },{ "hlc_rifle_FN3011Tactical_grey" },{ "hlc_rifle_FN3011Tactical_green" }
		};
	};
	class SniperRifleDLC
	{
		itemType = "weapon";
		items[] = {
		{ "srifle_DMR_07_blk_F" },{ "srifle_DMR_07_ghex_F" },{ "srifle_DMR_07_hex_F" },{ "srifle_GM6_ghex_F" },{ "srifle_LRR_tna_F" }
		};
	};
	class Rifle
	{
		itemType = "weapon";
		items[] = {
		{ "hlc_rifle_416D145" },{ "hlc_rifle_416D145_tan" },{ "hlc_rifle_416D145_wdl" },{ "hlc_rifle_416D145_CAG" },{ "hlc_rifle_416D145C" },{ "hlc_rifle_416D145_gl" },{ "hlc_rifle_416D10" },{ "hlc_rifle_416D10_tan" },{ "hlc_rifle_416D10_wdl" },{ "hlc_rifle_416D10_gl" },{ "hlc_rifle_416D10C" },{ "hlc_rifle_416D10C_PTC" },{ "hlc_rifle_416D10_RAHG" },{ "hlc_rifle_416D10_geissele" },{ "hlc_rifle_416D10_ptato" },{ "hlc_rifle_416D10_st6" },{ "hlc_rifle_416C" },{ "hlc_rifle_BAB" },{ "hlc_rifle_416D165" },{ "hlc_rifle_416D165_tan" },{ "hlc_rifle_416D165_wdl" },{ "hlc_rifle_416D165_gl" },{ "hlc_rifle_416N" },{ "hlc_rifle_416N_gl" },{ "hlc_rifle_416D20" },{ "hlc_rifle_416D20_tan" },{ "hlc_rifle_416D20_wdl" },{ "hlc_rifle_sg553RSB_TAC_grip" },{ "hlc_rifle_SG553SB" },{ "hlc_rifle_SG553SB_RIS" },{ "hlc_rifle_SG553SB_TAC" },{ "hlc_rifle_SG553SB_TAC_grip" },{ "hlc_rifle_SG553SB_TAC_grip2" },{ "hlc_rifle_SG553SB_TAC_grip3" },{ "hlc_rifle_SG553LB" },{ "hlc_rifle_SG553LB_RIS" },{ "hlc_rifle_SG553LB_TAC" },{ "hlc_rifle_SG553LB_TAC_grip" },{ "hlc_rifle_SG553LB_TAC_grip2" },{ "hlc_rifle_SG553LB_TAC_grip3" },{ "hlc_rifle_sg553RSB" },{ "hlc_rifle_sg553RSB_TAC" },{ "hlc_rifle_sg553RSB_TAC_grip2" },{ "hlc_rifle_sg553RSB_TAC_grip3" },{ "hlc_rifle_sg553RLB" },{ "hlc_rifle_sg553RLB_TAC" },{ "hlc_rifle_sg553RLB_TAC_grip" },{ "hlc_rifle_sg553RLB_TAC_grip2" },{ "hlc_rifle_sg553RLB_TAC_grip3" },{ "hlc_rifle_SG550" },{ "hlc_rifle_SG550_RIS" },{ "hlc_rifle_SG550_TAC" },{ "hlc_rifle_SG550_TAC_grip" },{ "hlc_rifle_SG550_TAC_grip2" },{ "hlc_rifle_SG550_TAC_grip3" },{ "hlc_rifle_SG550_GL" },{ "hlc_rifle_SG550_TAC_GL" },{ "hlc_rifle_M4a1carryhandle" },{ "hlc_rifle_mk18mod0" },{ "CUP_srifle_LeeEnfield_rail" },{ "arifle_Katiba_F" },{ "arifle_Katiba_GL_F" },{ "arifle_MXC_F" },{ "arifle_MX_F" },{ "arifle_MX_GL_F" },{ "arifle_MXM_F" },{ "arifle_MXM_Black_F" },{ "arifle_SDAR_F" },{ "arifle_TRG21_F" },{ "arifle_TRG20_F" },{ "arifle_TRG21_GL_F" },{ "arifle_Mk20_F" },{ "arifle_Mk20C_F" },{ "arifle_Mk20_GL_F" },{ "arifle_MXC_Black_F" },{ "arifle_MX_Black_F" },{ "arifle_MX_GL_Black_F" },{ "Exile_Weapon_AK107" },{ "Exile_Weapon_AK107_GL" },{ "Exile_Weapon_AK74" },{ "Exile_Weapon_AK74_GL" },{ "Exile_Weapon_AK47" },{ "Exile_Weapon_AKS_Gold" },{ "Exile_Weapon_M1014" },{ "Exile_Weapon_M4" },{ "Exile_Weapon_M16A4" },{ "Exile_Weapon_M16A2" },{ "srifle_CZ550_shit_1" },{ "srifle_CZ550_shit_2" },{ "hlc_rifle_ak74" },{ "hlc_rifle_ak74_MTK" },{ "hlc_rifle_ak74_dirty" },{ "hlc_rifle_ak74_dirty2" },{ "hlc_rifle_aks74" },{ "hlc_rifle_aks74_MTK" },{ "hlc_rifle_aks74u" },{ "hlc_rifle_aks74u_MTK" },{ "hlc_rifle_ak47" },{ "hlc_rifle_akm" },{ "hlc_rifle_akm_MTK" },{ "hlc_rifle_ak12" },{ "hlc_rifle_aku12" },{ "hlc_rifle_rpk74n" },{ "hlc_rifle_ak12gl" },{ "hlc_rifle_akmgl" },{ "hlc_rifle_aks74_GL" },{ "hlc_rifle_aek971" },{ "hlc_rifle_aek971_mtk" },{ "hlc_rifle_RK62" },{ "hlc_rifle_slr107u" },{ "hlc_rifle_slr107u_MTK" },{ "hlc_rifle_ak74m" },{ "hlc_rifle_ak74m_gl" },{ "hlc_rifle_ak74m_MTK" },{ "hlc_rifle_RU556" },{ "hlc_rifle_RU5562" },{ "hlc_rifle_Colt727" },{ "hlc_rifle_Colt727_GL" },{ "hlc_rifle_bcmjack" },{ "hlc_rifle_Bushmaster300" },{ "hlc_rifle_vendimus" },{ "hlc_rifle_SAMR" },{ "hlc_barrel_standard" },{ "hlc_barrel_carbine" },{ "hlc_barrel_hbar" },{ "hlc_rifle_aug" },{ "hlc_rifle_auga1_t" },{ "hlc_rifle_auga1_B" },{ "hlc_rifle_auga1carb" },{ "hlc_rifle_auga1carb_t" },{ "hlc_rifle_auga1carb_b" },{ "hlc_rifle_aughbar" },{ "hlc_rifle_aughbar_b" },{ "hlc_rifle_aughbar_t" },{ "hlc_rifle_auga2" },{ "hlc_rifle_auga2_b" },{ "hlc_rifle_auga2_t" },{ "hlc_rifle_auga2carb" },{ "hlc_rifle_auga2carb_t" },{ "hlc_rifle_auga2carb_b" },{ "hlc_rifle_auga2lsw" },{ "hlc_rifle_auga2lsw_t" },{ "hlc_rifle_auga2lsw_b" },{ "hlc_rifle_augsr" },{ "hlc_rifle_augsr_b" },{ "hlc_rifle_augsr_t" },{ "hlc_rifle_augsrcarb" },{ "hlc_rifle_augsrcarb_t" },{ "hlc_rifle_augsrcarb_b" },{ "hlc_rifle_augsrhbar" },{ "hlc_rifle_augsrhbar_b" },{ "hlc_rifle_augsrhbar_t" },{ "hlc_rifle_auga3" },{ "hlc_rifle_auga3_bl" },{ "hlc_rifle_auga3_b" },{ "hlc_rifle_auga3_GL" },{ "hlc_rifle_auga3_GL_BL" },{ "hlc_rifle_auga3_GL_B" },{ "hlc_rifle_l1a1slr" },{ "hlc_rifle_SLR" },{ "hlc_rifle_STG58F" },{ "hlc_rifle_FAL5061" },{ "hlc_rifle_c1A1" },{ "hlc_rifle_LAR" },{ "hlc_rifle_SLRchopmod" },{ "hlc_rifle_falosw" },{ "hlc_rifle_osw_GL" },{ "hlc_rifle_g3sg1" },{ "hlc_rifle_g3a3" },{ "hlc_rifle_g3a3ris" },{ "hlc_rifle_g3ka4" },{ "HLC_Rifle_g3ka4_GL" },{ "hlc_rifle_hk51" },{ "hlc_rifle_hk53" },{ "hlc_rifle_hk53RAS" },{ "hlc_rifle_hk33a2" },{ "hlc_rifle_hk33a2RIS" },{ "hlc_rifle_g3a3v" },{ "hlc_rifle_G36A1" },{ "hlc_rifle_G36A1AG36" },{ "hlc_rifle_G36KA1" },{ "hlc_rifle_G36C" },{ "hlc_rifle_G36E1" },{ "hlc_rifle_G36E1AG36" },{ "hlc_rifle_G36KE1" },{ "hlc_rifle_G36V" },{ "hlc_rifle_G36KV" },{ "hlc_rifle_G36CV" },{ "hlc_rifle_G36VAG36" },{ "hlc_rifle_G36TAC" },{ "hlc_rifle_G36KTAC" },{ "hlc_rifle_G36CTAC" },{ "hlc_rifle_G36MLIC" },{ "hlc_rifle_G36KMLIC" },{ "hlc_rifle_G36CMLIC" },{ "hlc_rifle_G36MLIAG36" },{ "hlc_rifle_MG36" },{ "hlc_rifle_M14" },{ "hlc_rifle_M21" },{ "hlc_rifle_M14DMR" },{ "hlc_rifle_m14sopmod" },{ "hlc_rifle_STGW57" },{ "hlc_rifle_stgw57_RIS" },{ "hlc_rifle_stgw57_commando" },{ "hlc_rifle_sig5104" },{ "hlc_rifle_amt" },{ "hlc_rifle_bcmblackjack" },{ "hlc_rifle_FAL5000_RH" },{ "hlc_rifle_FAL5061Rail" },{ "hlc_rifle_honeybadger" },{ "hlc_rifle_ACR_SBR_tan" },{ "hlc_rifle_ACR_SBR_black" },{ "hlc_rifle_ACR_SBR_green" },{ "hlc_rifle_ACR_Carb_black" },{ "hlc_rifle_ACR_Carb_tan" },{ "hlc_rifle_ACR_Carb_green" },{ "hlc_rifle_ACR_MID_black" },{ "hlc_rifle_ACR_MID_tan" },{ "hlc_rifle_ACR_MID_green" },{ "hlc_rifle_ACR_full_black" },{ "hlc_rifle_ACR_full_tan" },{ "hlc_rifle_ACR_full_green" },{ "hlc_rifleACR_SBR_cliffhanger" },{ "hlc_rifle_ACR68_SBR_tan" },{ "hlc_rifle_ACR68_SBR_black" },{ "hlc_rifle_ACR68_SBR_green" },{ "hlc_rifle_ACR68_Carb_black" },{ "hlc_rifle_ACR68_Carb_tan" },{ "hlc_rifle_ACR68_Carb_green" },{ "hlc_rifle_ACR68_MID_black" },{ "hlc_rifle_ACR68_MID_tan" },{ "hlc_rifle_ACR68_MID_green" },{ "hlc_rifle_ACR68_full_black" },{ "hlc_rifle_ACR68_full_tan" },{ "hlc_rifle_ACR68_full_green" },{ "hlc_rifle_saiga12k" }
		};
	};
	class RifleDLC
	{
		itemType = "weapon";
		items[] = {
		{ "arifle_AK12_F" },{ "arifle_AK12_GL_F" },{ "arifle_AKM_F" },{ "arifle_AKM_FL_F" },{ "arifle_AKS_F" },{ "arifle_ARX_blk_F" },{ "arifle_ARX_ghex_F" },{ "arifle_ARX_hex_F" },{ "arifle_CTAR_blk_F" },{ "arifle_CTAR_GL_blk_F" },{ "arifle_CTARS_blk_F" },{ "arifle_MX_GL_khk_F" },{ "arifle_MX_khk_F" },{ "arifle_MXC_khk_F" },{ "arifle_MXM_khk_F" },{ "arifle_SPAR_01_blk_F" },{ "arifle_SPAR_01_khk_F" },{ "arifle_SPAR_01_snd_F" },{ "arifle_SPAR_01_GL_blk_F" },{ "arifle_SPAR_01_GL_khk_F" },{ "arifle_SPAR_01_GL_snd_F" },{ "arifle_SPAR_03_blk_F" },{ "arifle_SPAR_03_khk_F" },{ "arifle_SPAR_03_snd_F" },{ "arifle_CTAR_hex_F" },{ "arifle_CTAR_ghex_F" },{ "arifle_CTAR_GL_hex_F" },{ "arifle_CTAR_GL_ghex_F" },{ "arifle_CTARS_hex_F" },{ "arifle_CTARS_ghex_F" }
		};
	};
	class Grenades
	{
		itemType = "magazine";
		items[] = {
		{ "HandGrenade" },{ "MiniGrenade" }
		};
	};
	class Machinegun
	{
		itemType = "weapon";
		items[] = {
		{ "CUP_lmg_M60E4" },{ "CUP_BAF_L7A2_GPMG" },{ "CUP_lmg_minimi_railed" },{ "arifle_MX_SW_Black_F" },{ "arifle_MX_SW_F" },{ "LMG_Mk200_F" },{ "LMG_Zafir_F" },{ "Exile_Weapon_RPK" },{ "Exile_Weapon_PK" },{ "Exile_Weapon_PKP" },{ "LMG_03_F" },{ "lmg_UK59" },{ "hlc_lmg_minimipara" },{ "hlc_lmg_minimi" },{ "hlc_lmg_minimi_railed" },{ "hlc_lmg_m249para" },{ "hlc_lmg_M249E2" },{ "hlc_lmg_m249para" },{ "hlc_lmg_M249E2" },{ "hlc_lmg_M60E4" },{ "hlc_lmg_m60" },{ "hlc_lmg_mk48" },{ "hlc_m249_pip1" },{ "hlc_m249_pip2" },{ "hlc_m249_pip3" },{ "hlc_m249_pip4" },{ "hlc_rifle_rpk" },{ "hlc_rifle_rpk12" },{ "hlc_rifle_rpk74n" },{ "hlc_lmg_MG42" },{ "hlc_lmg_mg42kws_b" },{ "hlc_lmg_mg42kws_g" },{ "hlc_lmg_MG42KWS_t" },{ "hlc_lmg_MG3KWS_b" },{ "hlc_lmg_MG3KWS_g" }
		};
	};
	class Tools
	{
		itemType = "item";
		items[] = {
		{ "Exile_Item_Handsaw" },{ "Exile_Item_Pliers" },{ "Exile_Item_Knife" },{ "Exile_Item_Shovel" }
		};
	};
	class Pistols
	{
		itemType = "weapon";
		items[] = {
		{ "hgun_ACPC2_F" },{ "hgun_P07_F" },{ "hgun_Rook40_F" },{ "hgun_Pistol_heavy_01_F" },{ "hgun_Pistol_heavy_02_F" },{ "hgun_Pistol_Signal_F" },{ "Exile_Weapon_Colt1911" },{ "Exile_Weapon_Makarov" },{ "Exile_Weapon_Taurus" },{ "Exile_Weapon_TaurusGold" },{ "hgun_P07_khk_F" },{ "hgun_Pistol_01_F" },{ "SMG_05_F" },{ "SMG_01_F" },{ "SMG_02_F" },{ "Exile_Weapon_SA61" },{ "hlc_smg_mp5k_PDW" },{ "hlc_smg_mp5k" },{ "hlc_smg_mp5a2" },{ "hlc_smg_MP5N" },{ "hlc_smg_9mmar" },{ "hlc_smg_mp5a4" },{ "hlc_smg_mp510" },{ "hlc_smg_mp5sd5" },{ "hlc_smg_mp5a3" },{ "hlc_smg_mp5sd6" },{ "hlc_pistol_P226WestGerman" },{ "hlc_pistol_P226US" },{ "hlc_pistol_P226R" },{ "hlc_pistol_P226R_Elite" },{ "hlc_pistol_P226R_Combat" },{ "hlc_pistol_Mk25" },{ "hlc_pistol_Mk25D" },{ "hlc_pistol_P226R_40" },{ "hlc_pistol_P226R_40Elite" },{ "hlc_pistol_P226R_40Combat" },{ "hlc_pistol_P226R_40Enox" },{ "hlc_Pistol_P228" },{ "hlc_Pistol_M11" },{ "hlc_pistol_P229R" },{ "hlc_pistol_P229R_Combat" },{ "hlc_Pistol_M11A1" },{ "hlc_Pistol_M11A1D" },{ "hlc_pistol_P229R_Elite" },{ "hlc_pistol_P229R_40" },{ "hlc_pistol_P229R_40Combat" },{ "hlc_pistol_P229R_40Elite" },{ "hlc_pistol_P229R_40Enox" },{ "hlc_pistol_P229R_357" },{ "hlc_pistol_P229R_357Combat" },{ "hlc_pistol_P229R_357Elite" },{ "hlc_pistol_P229R_357Stainless" },{ "hlc_pistol_P226R_357" },{ "hlc_pistol_P226R_357Combat" },{ "hlc_pistol_P226R_357Elite" },{ "hlc_pistol_P239" },{ "hlc_pistol_P239_40" },{ "hlc_pistol_P239_357" }
		};
	};
	class Scopes
	{
		itemType = "item";
		items[] = {
		{ "hlc_optic_ZF95" },{ "hlc_optic_FNSTANAG4X_550" },{ "hlc_optic_FNSTANAG4X2d_550" },{ "hlc_optic_Kern_550" },{ "hlc_optic_Kern2d_550" },{ "optic_Arco" },{ "optic_Hamr" },{ "optic_Holosight" },{ "acc_flashlight" },{ "acc_pointer_IR" },{ "optic_MRCO" },{ "optic_DMS" },{ "optic_NVS" },{ "optic_AMS" },{ "optic_AMS_khk" },{ "optic_AMS_snd" },{ "optic_KHS_blk" },{ "optic_KHS_hex" },{ "optic_KHS_old" },{ "optic_KHS_tan" },{ "optic_Holosight_smg" },{ "optic_ACO_grn_smg" },{ "optic_Aco_smg" },{ "optic_ACO_grn" },{ "optic_Aco" },{ "optic_Arco_blk_F" },{ "optic_Arco_ghex_F" },{ "optic_DMS_ghex_F" },{ "optic_Hamr_khk_F" },{ "optic_ERCO_blk_F" },{ "optic_ERCO_khk_F" },{ "optic_ERCO_snd_F" },{ "optic_SOS_khk_F" },{ "optic_LRPS_tna_F" },{ "optic_LRPS_ghex_F" },{ "optic_Holosight_blk_F" },{ "optic_Holosight_khk_F" },{ "optic_Holosight_smg_blk_F" },{ "HLC_Optic_PSO1" },{ "HLC_Optic_1p29" },{ "hlc_optic_kobra" },{ "hlc_optic_suit" },{ "hlc_optic_PVS4FAL" },{ "HLC_Optic_G36dualoptic35x" },{ "HLC_Optic_G36dualoptic35x2d" },{ "HLC_Optic_G36Export35x" },{ "HLC_Optic_G36Export35x2d" },{ "HLC_Optic_G36Dualoptic15x" },{ "HLC_Optic_G36Dualoptic15x2d" },{ "HLC_Optic_G36Export15x" },{ "HLC_Optic_G36Export15x2d" },{ "hlc_optic_Kern" },{ "hlc_optic_Kern2d" },{ "hlc_optic_artel_m14" },{ "hlc_optic_PVS4M14" },{ "hlc_optic_LRT_m14" },{ "HLC_Optic_ZFSG1" },{ "hlc_optic_accupoint_g3" },{ "hlc_optic_PVS4G3" },{ "hlc_optic_DocterV" },{ "hlc_optic_RomeoV" },{ "hlc_optic_stavenhagen" },{ "hlc_optic_VTAC" },{ "hlc_optic_Siglite" },{ "hlc_optic_HP" },{ "hlc_optic_ATT" },{ "hlc_optic_XS" },{ "hlc_Optic_Romeo1_RX" },{ "hlc_Optic_Docter_CADEX" },{ "hlc_optic228_stavenhagen" },{ "hlc_optic228_VTAC" },{ "hlc_optic228_Siglite" },{ "hlc_optic228_HP" },{ "hlc_optic228_ATT" },{ "hlc_optic228_XS" },{ "hlc_Optic228_Romeo1_RX" },{ "hlc_Optic228_Docter_CADEX" },{ "hlc_optic_FNSTANAG4X" },{ "hlc_optic_FNSTANAG2D" },{ "hlc_optic_STANAGZF2D_G3" },{ "hlc_optic_STANAGZF_G3" },{ "hlc_optic_ZF95_3011" },{ "hlc_optic_LeupoldM3A_3011" },{ "hlc_optic_VOMZ" },{ "hlc_optic_VOMZ3d" },{ "hlc_optic_LeupoldM3A" },{ "hlc_optic_Kern_3011" },{ "hlc_optic_Kern2d_3011" },{ "hlc_optic_ANGSCHUTZ" }
		};
	};
	class Muzzles
	{
		itemType = "item";
		items[] = {
		{ "hlc_muzzle_apology" },{ "muzzle_snds_M" },{ "muzzle_snds_H" },{ "muzzle_snds_B" },{ "muzzle_snds_338_black" },{ "muzzle_snds_338_green" },{ "muzzle_snds_338_sand" },{ "muzzle_snds_93mmg" },{ "muzzle_snds_93mmg_tan" },{ "muzzle_snds_65_TI_blk_F" },{ "muzzle_snds_58_wdm_F" },{ "muzzle_snds_B_snd_F" },{ "muzzle_snds_B_khk_F" },{ "muzzle_snds_H_MG_khk_F" },{ "muzzle_snds_H_MG_blk_F" },{ "muzzle_snds_65_TI_ghex_F" },{ "muzzle_snds_65_TI_hex_F" },{ "muzzle_snds_acp" },{ "muzzle_snds_H_MG" },{ "muzzle_snds_H_SW" },{ "muzzle_snds_L" },{ "hlc_muzzle_545SUP_AK" },{ "hlc_muzzle_762SUP_AK" },{ "hlc_muzzle_Agendasix" },{ "hlc_muzzle_Tundra" },{ "hlc_muzzle_Agendasix10mm" },{ "hlc_muzzle_300blk_KAC" },{ "hlc_muzzle_556NATO_KAC" },{ "hlc_muzzle_snds_a6AUG" },{ "hlc_muzzle_snds_M14" },{ "hlc_muzzle_snds_g3" },{ "hlc_muzzle_snds_HK33" },{ "hlc_muzzle_Octane9" },{ "hlc_muzzle_Evo9" },{ "hlc_muzzle_TiRant9" },{ "hlc_muzzle_TiRant9S" },{ "hlc_muzzle_Octane45" },{ "hlc_muzzle_Evo40" }
		};
	};
	class Bipods
	{
		itemType = "item";
		items[] = {
		{ "bipod_03_F_oli" },{ "bipod_03_F_blk" },{ "bipod_02_F_tan" },{ "bipod_02_F_hex" },{ "bipod_02_F_blk" },{ "bipod_01_F_snd" },{ "bipod_01_F_mtp" },{ "bipod_01_F_blk" },{ "bipod_01_F_khk" },{ "hlc_Bipod_G36" }
		};
	};
	class Food
	{
		itemType = "magazine";
		items[] = {
		{ "Exile_Item_CookingPot" },{ "Exile_Item_CanOpener" },{ "Exile_Item_Matches" },{ "Exile_Item_EMRE" },{ "Exile_Item_GloriousKnakworst" },{ "Exile_Item_Surstromming" },{ "Exile_Item_SausageGravy" },{ "Exile_Item_ChristmasTinner" },{ "Exile_Item_BBQSandwich" },{ "Exile_Item_CatFood" },{ "Exile_Item_Dogfood" },{ "Exile_Item_BeefParts" },{ "Exile_Item_Cheathas" },{ "Exile_Item_Noodles" },{ "Exile_Item_SeedAstics" },{ "Exile_Item_Raisins" },{ "Exile_Item_Moobar" },{ "Exile_Item_InstantCoffee" },{ "Exile_Item_CockONut" },{ "Exile_Item_DsNuts" }, { "Exile_Item_MacasCheese_Cooked" }
		};
	};
	class Drink
	{
		itemType = "magazine";
		items[] = {
		{ "Exile_Item_PowerDrink" },{ "Exile_Item_PlasticBottleFreshWater" },{ "Exile_Item_Beer" },{ "Exile_Item_EnergyDrink" },{ "Exile_Item_MountainDupe" },{ "Exile_Item_PlasticBottleDirtyWater" },{ "Exile_Item_ChocolateMilk" }
		};
	};
	class Hand
	{
		itemType = "magazine";
		items[] = {
		{ "HandGrenade" },{ "MiniGrenade" },{ "Chemlight_blue" },{ "Chemlight_green" },{ "Chemlight_red" },{ "Chemlight_yellow" },{ "FlareGreen_F" },{ "FlareRed_F" },{ "FlareWhite_F" },{ "FlareYellow_F" },{ "SmokeShell" },{ "SmokeShellRed" },{ "SmokeShellGreen" },{ "SmokeShellYellow" },{ "SmokeShellPurple" },{ "SmokeShellBlue" },{ "SmokeShellOrange" },{ "Exile_Item_ZipTie" }
		};
	};
	class Explosives
	{
		itemType = "magazine";
		items[] = {
		{ "DemoCharge_Remote_Mag" },{ "IEDUrbanSmall_Remote_Mag" },{ "IEDLandSmall_Remote_Mag" },{ "APERSMine_Range_Mag" },{ "APERSBoundingMine_Range_Mag" },{ "APERSTripMine_Wire_Mag" },{ "SatchelCharge_Remote_Mag" },{ "HandGrenade" },{ "MiniGrenade" },{ "ATMine_Range_Mag" }
		};
	};
	class BackpackCIV
	{
		itemType = "backpack";
		items[] = {
		{ "B_OutdoorPack_blk" },{ "B_OutdoorPack_tan" },{ "B_OutdoorPack_blu" },{ "B_HuntingBackpack" },{ "B_AssaultPack_khk" },{ "B_AssaultPack_dgtl" },{ "B_AssaultPack_rgr" },{ "B_AssaultPack_sgg" },{ "B_AssaultPack_blk" },{ "B_AssaultPack_cbr" },{ "B_AssaultPack_mcamo" },{ "B_Kitbag_mcamo" },{ "B_Kitbag_sgg" },{ "B_Kitbag_cbr" },{ "B_FieldPack_ghex_F" },{ "B_AssaultPack_tna_F" },{ "B_Carryall_ghex_F" }
		};
	};
	class BackpackGOR
	{
		itemType = "backpack";
		items[] = {
		{ "B_FieldPack_blk" },{ "B_FieldPack_ocamo" },{ "B_FieldPack_oucamo" },{ "B_FieldPack_cbr" },{ "B_Bergen_sgg" },{ "B_Bergen_mcamo" },{ "B_Bergen_rgr" },{ "B_Bergen_blk" },{ "B_ViperLightHarness_khk_F" },{ "B_ViperLightHarness_blk_F" },{ "B_ViperLightHarness_hex_F" },{ "B_ViperLightHarness_oli_F" },{ "B_ViperLightHarness_ghex_F" },{ "B_ViperHarness_oli_F" },{ "B_ViperHarness_khk_F" },{ "B_ViperHarness_ghex_F" },{ "B_ViperHarness_blk_F" },{ "B_ViperHarness_hex_F" }
		};
	};
	class BackpackMIL
	{
		itemType = "backpack";
		items[] = {
		{ "CUP_B_ACRScout_m95" },{ "B_Carryall_ocamo" },{ "B_Carryall_oucamo" },{ "B_Carryall_mcamo" },{ "B_Carryall_oli" },{ "B_Carryall_khk" },{ "B_Carryall_cbr" },{ "B_Bergen_tna_F" },{ "B_Bergen_hex_F" },{ "B_Bergen_dgtl_F" },{ "B_Bergen_mcamo_F" },{ "B_Kitbag_rgr" }
		};
	};
	class UniformsCIV
	{
		itemType = "item";
		items[] = {
		{ "U_Marshal" },{ "U_C_Paramedic_01_F" },{ "U_C_Mechanic_01_F" },{ "U_C_Poor_1" },{ "U_C_Poor_2" },{ "U_C_Poor_shorts_1" },{ "U_C_HunterBody_grn" },{ "U_C_Poloshirt_salmon" },{ "U_C_Poloshirt_tricolour" },{ "U_C_Poloshirt_stripped" },{ "U_C_Poloshirt_burgundy" },{ "U_C_Poloshirt_blue" },{ "U_C_Journalist" },{ "U_C_Scientist" },{ "U_Rangemaster" },{ "U_OrestesBody" },{ "U_NikosBody" },{ "U_NikosAgedBody" },{ "U_C_man_sport_1_F" },{ "U_C_man_sport_2_F" },{ "U_C_man_sport_3_F" },{ "U_C_Man_casual_1_F" },{ "U_C_Man_casual_2_F" },{ "U_C_Man_casual_3_F" },{ "U_C_Man_casual_4_F" },{ "U_C_Man_casual_5_F" },{ "U_C_Man_casual_6_F" },{ "U_C_Driver_1" },{ "U_C_Driver_2" },{ "U_C_Driver_4" },{ "U_C_Driver_1_black" },{ "U_C_Driver_1_blue" },{ "U_C_Driver_1_green" },{ "U_C_Driver_1_red" },{ "U_C_Driver_1_yellow" },{ "U_C_Driver_3" },{ "U_C_Driver_1_white" },{ "U_C_Driver_1_orange" },{ "CUP_U_B_CZ_WDL_TShirt" },{ "CUP_U_B_CZ_DST_TShirt" }
		};
	};
	class UniformsGOR
	{
		itemType = "item";
		items[] = {
		{ "U_IG_Guerilla1_1" },{ "U_IG_Guerilla2_1" },{ "U_IG_Guerilla2_2" },{ "U_IG_Guerilla2_3" },{ "U_IG_Guerilla3_1" },{ "U_IG_Guerilla3_2" },{ "U_IG_leader" },{ "U_I_G_resistanceLeader_F" },{ "U_B_GhillieSuit" },{ "U_O_GhillieSuit" },{ "U_I_GhillieSuit" },{ "U_B_T_Soldier_F" },{ "U_B_T_Soldier_AR_F" },{ "U_B_T_Soldier_SL_F" },{ "U_B_T_Sniper_F" },{ "U_B_T_FullGhillie_tna_F" },{ "U_B_CTRG_Soldier_F" },{ "U_B_CTRG_Soldier_2_F" },{ "U_B_CTRG_Soldier_3_F" },{ "U_B_GEN_Soldier_F" },{ "U_B_GEN_Commander_F" },{ "U_O_T_Soldier_F" },{ "U_O_T_Officer_F" },{ "U_O_T_Sniper_F" },{ "U_O_T_FullGhillie_tna_F" },{ "U_O_V_Soldier_Viper_F" },{ "U_O_V_Soldier_Viper_hex_F" },{ "U_I_C_Soldier_Para_1_F" },{ "U_I_C_Soldier_Para_2_F" },{ "U_I_C_Soldier_Para_3_F" },{ "U_I_C_Soldier_Para_4_F" },{ "U_I_C_Soldier_Para_5_F" },{ "U_I_C_Soldier_Bandit_1_F" },{ "U_I_C_Soldier_Bandit_2_F" },{ "U_I_C_Soldier_Bandit_3_F" },{ "U_I_C_Soldier_Bandit_4_F" },{ "U_I_C_Soldier_Bandit_5_F" },{ "U_I_C_Soldier_Camo_F" },{ "CUP_U_B_CZ_WDL_NoKneepads" },{ "CUP_U_B_CZ_DST_NoKneepads" },{ "CUP_U_B_CZ_WDL_Kneepads" },{ "CUP_U_B_CZ_DST_Kneepads" },{ "CUP_U_B_CZ_DST_Kneepads_Sleeve" },{ "CUP_U_B_CZ_DST_Kneepads_Gloves" },{ "CUP_U_B_CZ_WDL_Kneepads_Gloves" }
		};
	};
	class UniformsMIL
	{
		itemType = "item";
		items[] = {
		{ "U_B_CTRG_1" },{ "U_B_CTRG_2" },{ "U_B_CTRG_3" },{ "U_B_CombatUniform_mcam" },{ "U_B_CombatUniform_mcam_tshirt" },{ "U_B_CombatUniform_mcam_vest" },{ "U_B_CombatUniform_mcam_worn" },{ "U_B_SpecopsUniform_sgg" },{ "U_O_OfficerUniform_ocamo" },{ "U_I_OfficerUniform" },{ "U_I_CombatUniform" },{ "U_I_CombatUniform_tshirt" },{ "U_I_CombatUniform_shortsleeve" },{ "U_B_HeliPilotCoveralls" },{ "U_O_PilotCoveralls" },{ "U_B_PilotCoveralls" },{ "U_I_pilotCoveralls" },{ "U_I_HeliPilotCoveralls" },{ "U_B_Wetsuit" },{ "U_O_Wetsuit" },{ "U_I_Wetsuit" },{ "U_O_CombatUniform_ocamo" },{ "U_O_CombatUniform_oucamo" },{ "U_O_SpecopsUniform_ocamo" },{ "U_O_SpecopsUniform_blk" },{ "U_B_FullGhillie_ard" },{ "U_B_FullGhillie_lsh" },{ "U_B_FullGhillie_sard" },{ "U_O_FullGhillie_ard" },{ "U_O_FullGhillie_lsh" },{ "U_O_FullGhillie_sard" },{ "U_I_FullGhillie_ard" },{ "U_I_FullGhillie_lsh" },{ "U_I_FullGhillie_sard" },{ "U_B_CTRG_Soldier_urb_1_F" },{ "U_B_CTRG_Soldier_urb_2_F" },{ "U_B_CTRG_Soldier_urb_3_F" },{ "CUP_U_B_CZ_Pilot_DST" },{ "CUP_U_B_CZ_Pilot_WDL" },{ "CUP_U_B_CZ_WDL_Ghillie" },{ "CUP_U_B_CZ_DST_Ghillie" }
		};
	};
	class VestsCIV
	{
		itemType = "item";
		items[] = {
		{ "V_Rangemaster_belt" },{ "V_Press_F" },{ "V_TacVest_blk_POLICE" },{ "V_HarnessO_ghex_F" },{ "V_HarnessOGL_ghex_F" },{ "V_BandollierB_ghex_F" }
		};
	};
	class VestsGOR
	{
		itemType = "item";
		items[] = {
		{ "V_BandollierB_khk" },{ "V_BandollierB_cbr" },{ "V_BandollierB_rgr" },{ "V_BandollierB_blk" },{ "V_BandollierB_oli" },{ "V_Chestrig_khk" },{ "V_Chestrig_rgr" },{ "V_Chestrig_blk" },{ "V_Chestrig_oli" },{ "V_HarnessO_brn" },{ "V_HarnessOGL_brn" },{ "V_HarnessO_gry" },{ "V_HarnessOGL_gry" },{ "V_HarnessOSpec_brn" },{ "V_HarnessOSpec_gry" },{ "V_I_G_resistanceLeader_F" },{ "V_TacVest_gen_F" },{ "V_PlateCarrier1_rgr_noflag_F" },{ "V_PlateCarrier2_rgr_noflag_F" }
		};
	};
	class VestsMIL
	{
		itemType = "item";
		items[] = {
		{ "V_PlateCarrier1_blk" },{ "V_PlateCarrier1_rgr" },{ "V_PlateCarrier2_rgr" },{ "V_PlateCarrier3_rgr" },{ "V_PlateCarrierGL_rgr" },{ "V_PlateCarrierIA1_dgtl" },{ "V_PlateCarrierIA2_dgtl" },{ "V_PlateCarrierIAGL_dgtl" },{ "V_PlateCarrierSpec_rgr" },{ "V_PlateCarrierL_CTRG" },{ "V_PlateCarrierH_CTRG" },{ "V_PlateCarrierGL_blk" },{ "V_PlateCarrierGL_mtp" },{ "V_PlateCarrierGL_rgr" },{ "V_PlateCarrierIAGL_dgtl" },{ "V_PlateCarrierIAGL_oli" },{ "V_PlateCarrierSpec_blk" },{ "V_PlateCarrierSpec_mtp" },{ "V_PlateCarrierSpec_rgr" },{ "V_RebreatherB" },{ "V_RebreatherIR" },{ "V_RebreatherIA" },{ "V_PlateCarrier1_tna_F" },{ "V_PlateCarrier2_tna_F" },{ "V_PlateCarrierSpec_tna_F" },{ "V_PlateCarrierGL_tna_F" },{"CUP_V_CZ_vest02"},{"CUP_V_CZ_vest03"},{"CUP_V_CZ_vest04"},{"CUP_V_CZ_vest01"},{"CUP_V_CZ_vest05"},{"CUP_V_CZ_vest06"},{"CUP_V_CZ_vest07"},{"CUP_V_CZ_vest08"},{"CUP_V_CZ_vest09"},{"CUP_V_CZ_vest10"},{"CUP_V_CZ_vest11"},{"CUP_V_CZ_vest16"},{"CUP_V_CZ_vest14"},{"CUP_V_CZ_vest12"},{"CUP_V_CZ_vest13"},{"CUP_V_CZ_vest15"},{"CUP_V_CZ_vest17"},{"CUP_V_CZ_vest18"},{"CUP_V_CZ_vest19"},{"CUP_V_CZ_vest20"}
		};
	};
	class HeadgearCIV
	{
		itemType = "item";
		items[] = {
		{ "H_Bandanna_surfer" },{ "H_Beret_blk_POLICE" },{ "H_Cap_blk" },{ "H_Cap_blk_Raven" },{ "H_Cap_blu" },{ "H_Cap_grn" },{ "H_Cap_headphones" },{ "H_Cap_oli" },{ "H_Cap_press" },{ "H_Cap_red" },{ "H_Cap_tan" },{ "H_Hat_blue" },{ "H_Hat_brown" },{ "H_Hat_checker" },{ "H_Hat_grey" },{ "H_Hat_tan" },{ "H_StrawHat" },{ "H_StrawHat_dark" },{ "H_Helmet_Skate" },{ "H_MilCap_tna_F" },{ "H_MilCap_ghex_F" },{ "H_Booniehat_tna_F" },{ "H_Beret_gen_F" },{ "H_MilCap_gen_F" },{ "Exile_Headgear_SafetyHelmet" },{ "H_RacingHelmet_2_F" },{ "H_RacingHelmet_3_F" },{ "H_RacingHelmet_1_F" },{ "H_RacingHelmet_4_F" },{ "H_RacingHelmet_1_black_F" },{ "H_RacingHelmet_1_blue_F" },{ "H_RacingHelmet_1_green_F" },{ "H_RacingHelmet_1_red_F" },{ "H_RacingHelmet_1_white_F" },{ "H_RacingHelmet_1_yellow_F" },{ "H_RacingHelmet_1_orange_F" },{"CUP_H_CZ_Hat01"},{"CUP_H_CZ_Hat02"},{"CUP_H_CZ_Hat03"},{"CUP_H_CZ_Hat04"}
		};
	};
	class HeadgearGOR
	{
		itemType = "item";
		items[] = {
		{ "H_Booniehat_khk" },{ "H_Booniehat_indp" },{ "H_Booniehat_mcamo" },{ "H_Booniehat_grn" },{ "H_Booniehat_tan" },{ "H_Booniehat_dirty" },{ "H_Booniehat_dgtl" },{ "H_Booniehat_khk_hs" },{ "H_Bandanna_khk" },{ "H_Bandanna_khk_hs" },{ "H_Bandanna_cbr" },{ "H_Bandanna_sgg" },{ "H_Bandanna_gry" },{ "H_Bandanna_camo" },{ "H_Bandanna_mcamo" },{ "H_BandMask_blk" },{ "H_Beret_blk" },{ "H_Beret_red" },{ "H_Beret_grn" },{ "H_Beret_grn_SF" },{ "H_Beret_brn_SF" },{ "H_Beret_ocamo" },{ "H_Beret_02" },{ "H_Beret_Colonel" },{ "H_Hat_camo" },{ "H_Cap_brn_SPECOPS" },{ "H_Cap_tan_specops_US" },{ "H_Cap_khaki_specops_UK" },{ "H_Watchcap_blk" },{ "H_Watchcap_khk" },{ "H_Watchcap_camo" },{ "H_Watchcap_sgg" },{ "H_TurbanO_blk" },{ "H_Shemag_khk" },{ "H_Shemag_tan" },{ "H_Shemag_olive" },{ "H_Shemag_olive_hs" },{ "H_ShemagOpen_khk" },{ "H_ShemagOpen_tan" },{ "H_HelmetB_TI_tna_F" },{ "H_HelmetB_tna_F" },{ "H_HelmetB_Enh_tna_F" },{ "H_HelmetB_Light_tna_F" },{ "H_Helmet_Kerry" }
		};
	};
	class HeadgearMIL
	{
		itemType = "item";
		items[] = {
		{ "H_MilCap_ocamo" },{ "H_MilCap_mcamo" },{ "H_MilCap_oucamo" },{ "H_MilCap_blue" },{ "H_MilCap_rucamo" },{ "H_MilCap_dgtl" },{ "H_HelmetB" },{ "H_HelmetB_paint" },{ "H_HelmetB_light" },{ "H_HelmetB_plain_blk" },{ "H_HelmetSpecB" },{ "H_HelmetSpecB_paint1" },{ "H_HelmetSpecB_paint2" },{ "H_HelmetSpecB_blk" },{ "H_HelmetIA" },{ "H_HelmetIA_net" },{ "H_HelmetIA_camo" },{ "H_HelmetB_grass" },{ "H_HelmetB_snakeskin" },{ "H_HelmetB_desert" },{ "H_HelmetB_black" },{ "H_HelmetB_sand" },{ "H_HelmetB_light_grass" },{ "H_HelmetB_light_snakeskin" },{ "H_HelmetB_light_desert" },{ "H_HelmetB_light_black" },{ "H_HelmetB_light_sand" },{ "H_HelmetCrew_B" },{ "H_HelmetCrew_O" },{ "H_HelmetCrew_I" },{ "H_PilotHelmetFighter_B" },{ "H_PilotHelmetFighter_O" },{ "H_PilotHelmetFighter_I" },{ "H_PilotHelmetHeli_B" },{ "H_PilotHelmetHeli_O" },{ "H_PilotHelmetHeli_I" },{ "H_HelmetB_camo" },{ "H_CrewHelmetHeli_B" },{ "H_CrewHelmetHeli_O" },{ "H_CrewHelmetHeli_I" },{ "H_BandMask_khk" },{ "H_BandMask_reaper" },{ "H_BandMask_demon" },{ "H_HelmetO_oucamo" },{ "H_HelmetLeaderO_oucamo" },{ "H_HelmetSpecO_ocamo" },{ "H_HelmetSpecO_blk" },{ "H_HelmetO_ocamo" },{ "H_HelmetLeaderO_ocamo" },{ "Exile_Headgear_GasMask" },{ "H_HelmetSpecO_ghex_F" },{ "H_HelmetLeaderO_ghex_F" },{ "H_HelmetO_ghex_F" },{ "H_HelmetCrew_O_ghex_F" },{"CUP_H_CZ_Helmet01"},{"CUP_H_CZ_Helmet02"},{"CUP_H_CZ_Helmet03"},{"CUP_H_CZ_Helmet04"},{"CUP_H_CZ_Helmet05"},{"CUP_H_CZ_Helmet06"},{"CUP_H_CZ_Helmet07"},{"CUP_H_CZ_Helmet08"},{"CUP_H_CZ_Helmet09"},{"CUP_H_CZ_Helmet10"}
		};
	};
	class SniperRifleCUP
	{
		itemType = "weapon";
		items[] = {
		{ "CUP_srifle_AWM_des" },{ "CUP_srifle_AWM_wdl" },{ "CUP_srifle_CZ750" },{ "CUP_srifle_M14" },{ "CUP_srifle_Mk12SPR" },{ "CUP_srifle_M24_des" },{ "CUP_srifle_M24_wdl" },{ "CUP_srifle_M40A3" },{ "CUP_srifle_M110" },{ "CUP_srifle_AS50" }
		};
	};
	class RifleCUP
	{
		itemType = "weapon";
		items[] = {
		{ "CUP_arifle_AKS74" },{ "CUP_arifle_AKS74U" },{ "CUP_arifle_CZ805_A2" },{ "CUP_arifle_FNFAL_railed" },{ "CUP_arifle_G36A" },{ "CUP_arifle_G36A_camo" },{ "CUP_arifle_G36K" },{ "CUP_arifle_G36K_camo" },{ "CUP_arifle_G36C" },{ "CUP_arifle_G36C_camo" },{ "CUP_arifle_MG36" },{ "CUP_arifle_MG36_camo" },{ "CUP_arifle_L85A2" },{ "CUP_arifle_L85A2_GL" },{ "CUP_arifle_L86A2" },{ "CUP_arifle_M16A2_GL" },{ "CUP_arifle_M16A4_GL" },{ "CUP_arifle_M4A1" },{ "CUP_arifle_M4A1_camo" },{ "CUP_arifle_M4A1_black" },{ "CUP_arifle_M4A1_desert" },{ "CUP_arifle_Sa58P" },{ "CUP_arifle_Sa58RIS1" },{ "CUP_arifle_Sa58V" },{ "CUP_arifle_Mk16_CQC" },{ "CUP_arifle_XM8_Compact_Rail" },{ "CUP_arifle_XM8_Railed" },{ "CUP_arifle_XM8_Carbine" },{ "CUP_arifle_XM8_Carbine_FG" },{ "CUP_arifle_XM8_Carbine_GL" },{ "CUP_arifle_XM8_Compact" },{ "CUP_arifle_xm8_SAW" },{ "CUP_arifle_xm8_sharpshooter" },{ "CUP_arifle_CZ805_A1" },{ "CUP_arifle_CZ805_GL" },{ "CUP_arifle_CZ805_B_GL" },{ "CUP_arifle_CZ805_B" },{ "CUP_arifle_Sa58P_des" },{ "CUP_arifle_Sa58V_camo" },{ "CUP_arifle_Mk16_CQC_FG" },{ "CUP_arifle_Mk16_CQC_SFG" },{ "CUP_arifle_Mk16_CQC_EGLM" },{ "CUP_arifle_Mk16_STD" },{ "CUP_arifle_Mk16_STD_FG" },{ "CUP_arifle_Mk16_STD_SFG" },{ "CUP_arifle_Mk16_STD_EGLM" },{ "CUP_arifle_Mk16_SV" },{ "CUP_arifle_Mk17_CQC" },{ "CUP_arifle_Mk17_CQC_FG" },{ "CUP_arifle_Mk17_CQC_SFG" },{ "CUP_arifle_Mk17_CQC_EGLM" },{ "CUP_arifle_Mk17_STD" },{ "CUP_arifle_Mk17_STD_FG" },{ "CUP_arifle_Mk17_STD_SFG" },{ "CUP_arifle_Mk17_STD_EGLM" },{ "CUP_arifle_Mk20" },{ "CUP_sgun_AA12" },{ "CUP_sgun_Saiga12K" },{ "CUP_arifle_M4A1_BUIS_GL" },{ "CUP_arifle_M4A1_BUIS_camo_GL" },{ "CUP_arifle_M4A1_BUIS_desert_GL" },{ "CUP_arifle_Mk16_CQC_woodland" },{ "CUP_arifle_Mk16_CQC_FG_woodland" },{ "CUP_arifle_Mk16_CQC_SFG_woodland" },{ "CUP_arifle_Mk16_CQC_EGLM_woodland" },{ "CUP_arifle_Mk16_STD_woodland" },{ "CUP_arifle_Mk16_STD_FG_woodland" },{ "CUP_arifle_Mk16_STD_SFG_woodland" },{ "CUP_arifle_Mk16_STD_EGLM_woodland" },{ "CUP_arifle_Mk16_SV_woodland" },{ "CUP_arifle_Mk17_CQC_woodland" },{ "CUP_arifle_Mk17_CQC_FG_woodland" },{ "CUP_arifle_Mk17_CQC_SFG_woodland" },{ "CUP_arifle_Mk17_CQC_EGLM_woodland" },{ "CUP_arifle_Mk17_STD_woodland" },{ "CUP_arifle_Mk17_STD_FG_woodland" },{ "CUP_arifle_Mk17_STD_SFG_woodland" },{ "CUP_arifle_Mk17_STD_EGLM_woodland" },{ "CUP_arifle_Mk16_CQC_black" },{ "CUP_arifle_Mk16_CQC_FG_black" },{ "CUP_arifle_Mk16_CQC_SFG_black" },{ "CUP_arifle_Mk16_CQC_EGLM_black" },{ "CUP_arifle_Mk16_STD_black" },{ "CUP_arifle_Mk16_STD_FG_black" },{ "CUP_arifle_Mk16_STD_SFG_black" },{ "CUP_arifle_Mk16_STD_EGLM_black" },{ "CUP_arifle_Mk16_SV_black" },{ "CUP_arifle_Mk17_CQC_black" },{ "CUP_arifle_Mk17_CQC_FG_black" },{ "CUP_arifle_Mk17_CQC_SFG_black" },{ "CUP_arifle_Mk17_CQC_EGLM_black" },{ "CUP_arifle_Mk17_STD_black" },{ "CUP_arifle_Mk17_STD_FG_black" },{ "CUP_arifle_Mk17_STD_SFG_black" },{ "CUP_arifle_Mk17_STD_EGLM_black" },{ "CUP_arifle_M4A3_desert" }
		};
	};
	class MachinegunCUP
	{
		itemType = "weapon";
		items[] = {
		{ "CUP_lmg_L7A2" },{ "CUP_lmg_L110A1" },{ "CUP_lmg_M240" },{ "CUP_lmg_M249" },{ "CUP_lmg_PKM" },{ "CUP_lmg_Pecheneg" }
		};
	};
	class PistolsCUP
	{
		itemType = "weapon";
		items[] = {
		{ "CUP_hgun_Compact" },{ "CUP_hgun_Glock17" },{ "CUP_hgun_M9" },{ "CUP_hgun_PB6P9" },{ "CUP_hgun_MicroUzi" },{ "CUP_hgun_TaurusTracker455" },{ "CUP_hgun_TaurusTracker455_gold" },{ "CUP_hgun_SA61" },{ "CUP_hgun_Duty" },{ "CUP_hgun_Phantom" },{ "CUP_smg_bizon" },{ "CUP_smg_EVO" },{ "CUP_smg_MP5SD6" },{ "CUP_smg_MP5A5" },{ "CUP_hgun_BallisticShield_Armed" },{ "CUP_MP5SD" }
		};
	};
	class ScopesCUP
	{
		itemType = "item";
		items[] = {
		{ "CUP_optic_PSO_1" },{ "CUP_optic_PSO_3" },{ "CUP_optic_Kobra" },{ "CUP_optic_NSPU" },{ "CUP_optic_PechenegScope" },{ "CUP_optic_LeupoldMk4" },{ "CUP_optic_HoloBlack" },{ "CUP_optic_HoloWdl" },{ "CUP_optic_HoloDesert" },{ "CUP_optic_Eotech533" },{ "CUP_optic_CompM4" },{ "CUP_optic_SUSAT" },{ "CUP_optic_ACOG" },{ "CUP_optic_Leupold_VX3" },{ "CUP_optic_AN_PVS_10" },{ "CUP_optic_CompM2_Black" },{ "CUP_optic_CompM2_Woodland" },{ "CUP_optic_CompM2_Woodland2" },{ "CUP_optic_CompM2_Desert" },{ "CUP_optic_RCO" },{ "CUP_optic_RCO_desert" },{ "CUP_optic_LeupoldM3LR" },{ "CUP_optic_LeupoldMk4_10x40_LRT_Desert" },{ "CUP_optic_LeupoldMk4_10x40_LRT_Woodland" },{ "CUP_optic_ElcanM145" },{ "CUP_optic_LeupoldMk4_CQ_T" },{ "CUP_optic_ELCAN_SpecterDR" },{ "CUP_optic_LeupoldMk4_MRT_tan" },{ "CUP_optic_SB_11_4x20_PM" },{ "CUP_optic_ZDDot" },{ "CUP_optic_MRad" },{ "CUP_optic_TrijiconRx01_desert" },{ "CUP_optic_TrijiconRx01_black" },{ "CUP_optic_AN_PVS_4" }
		};
	};
	class MuzzlesCUP
	{
		itemType = "item";
		items[] = {
		{ "CUP_muzzle_PBS4" },{ "CUP_muzzle_PB6P9" },{ "CUP_muzzle_Bizon" },{ "CUP_muzzle_snds_M110" },{ "CUP_muzzle_snds_M14" },{ "CUP_muzzle_snds_M9" },{ "CUP_muzzle_snds_MicroUzi" },{ "CUP_muzzle_snds_AWM" },{ "CUP_muzzle_snds_G36_black" },{ "CUP_muzzle_snds_G36_desert" },{ "CUP_muzzle_snds_L85" },{ "CUP_muzzle_snds_M16_camo" },{ "CUP_muzzle_snds_M16" },{ "CUP_muzzle_snds_SCAR_L" },{ "CUP_muzzle_mfsup_SCAR_L" },{ "CUP_muzzle_snds_SCAR_H" },{ "CUP_muzzle_mfsup_SCAR_H" },{ "CUP_muzzle_snds_XM8" }
		};
	};
	class BackpackCUP
	{
		itemType = "backpack";
		items[] = {
		{ "CUP_B_Bergen_BAF" },{ "CUP_B_GER_Pack_Tropentarn" },{ "CUP_B_GER_Pack_Flecktarn" },{ "CUP_B_HikingPack_Civ" },{ "CUP_B_RUS_Backpack" },{ "CUP_B_CivPack_WDL" },{ "CUP_B_USPack_Coyote" },{ "CUP_B_AssaultPack_ACU" },{ "CUP_B_AssaultPack_Coyote" },{ "CUP_B_USMC_AssaultPack" },{ "CUP_B_USMC_MOLLE" },{ "CUP_B_USPack_Black" },{ "CUP_B_ACRPara_m95" },{ "CUP_B_AssaultPack_Black" }
		};
	};
	class BackpackTRYK
	{
		itemType = "backpack";
		items[] = {
		{ "TRYK_B_AssaultPack_UCP" },{ "TRYK_B_AssaultPack_Type2camo" },{ "TRYK_B_AssaultPack_MARPAT_Desert" },{ "TRYK_B_AssaultPack_MARPAT_Wood" },{ "TRYK_B_Kitbag_Base" },{ "TRYK_B_Kitbag_blk" },{ "TRYK_B_Kitbag_aaf" },{ "TRYK_B_Carryall_blk" },{ "TRYK_B_Carryall_wh" },{ "TRYK_B_Carryall_wood" },{ "TRYK_B_Carryall_JSDF" },{ "TRYK_B_Kitbag_Base_JSDF" },{ "TRYK_B_Coyotebackpack" },{ "TRYK_B_Coyotebackpack_OD" },{ "TRYK_B_Coyotebackpack_BLK" },{ "TRYK_B_Coyotebackpack_WH" },{ "TRYK_B_Alicepack" },{ "TRYK_B_Medbag" },{ "TRYK_B_Medbag_OD" },{ "TRYK_B_Medbag_BK" },{ "TRYK_B_Medbag_ucp" },{ "TRYK_B_Belt" },{ "TRYK_B_Belt_BLK" },{ "TRYK_B_Belt_CYT" },{ "TRYK_B_Belt_tan" },{ "TRYK_B_Belt_br" },{ "TRYK_B_Belt_GR" },{ "TRYK_B_Belt_AOR1" },{ "TRYK_B_Belt_AOR2" },{ "TRYK_B_BAF_BAG_BLK" },{ "TRYK_B_BAF_BAG_CYT" },{ "TRYK_B_BAF_BAG_OD" },{ "TRYK_B_BAF_BAG_rgr" },{ "TRYK_B_BAF_BAG_mcamo" },{ "TRYK_B_tube_cyt" },{ "TRYK_B_tube_od" },{ "TRYK_B_tube_blk" },{ "TRYK_B_FieldPack_Wood" },{ "TRYK_Winter_pack" }
		};
	};
	class UniformsCUP
	{
		itemType = "item";
		items[] = {
		{ "CUP_O_TKI_Khet_Partug_01" },{ "CUP_O_TKI_Khet_Partug_02" },{ "CUP_O_TKI_Khet_Partug_03" },{ "CUP_O_TKI_Khet_Partug_04" },{ "CUP_O_TKI_Khet_Partug_05" },{ "CUP_O_TKI_Khet_Partug_06" },{ "CUP_O_TKI_Khet_Partug_07" },{ "CUP_O_TKI_Khet_Partug_08" },{ "CUP_O_TKI_Khet_Jeans_01" },{ "CUP_O_TKI_Khet_Jeans_02" },{ "CUP_O_TKI_Khet_Jeans_03" },{ "CUP_O_TKI_Khet_Jeans_04" },{ "CUP_U_C_Pilot_01" },{ "CUP_U_C_Citizen_01" },{ "CUP_U_C_Citizen_02" },{ "CUP_U_C_Citizen_03" },{ "CUP_U_C_Citizen_04" },{ "CUP_U_C_Worker_01" },{ "CUP_U_C_Worker_02" },{ "CUP_U_C_Worker_03" },{ "CUP_U_C_Worker_04" },{ "CUP_U_C_Profiteer_01" },{ "CUP_U_C_Profiteer_02" },{ "CUP_U_C_Profiteer_03" },{ "CUP_U_C_Profiteer_04" },{ "CUP_U_C_Woodlander_01" },{ "CUP_U_C_Woodlander_02" },{ "CUP_U_C_Woodlander_03" },{ "CUP_U_C_Woodlander_04" },{ "CUP_U_C_Villager_01" },{ "CUP_U_C_Villager_02" },{ "CUP_U_C_Villager_03" },{ "CUP_U_C_Villager_04" },{ "CUP_U_B_CZ_WDL_TShirt" },{ "CUP_U_B_GER_Tropentarn_1" },{ "CUP_U_B_GER_Tropentarn_2" },{ "CUP_U_B_GER_Ghillie" },{ "CUP_U_B_GER_Flecktarn_1" },{ "CUP_U_B_GER_Flecktarn_2" },{ "CUP_U_B_GER_Fleck_Ghillie" },{ "CUP_U_B_USMC_Officer" },{ "CUP_U_B_USMC_PilotOverall" },{ "CUP_U_B_USMC_Ghillie_WDL" },{ "CUP_U_B_USMC_MARPAT_WDL_Sleeves" },{ "CUP_U_B_USMC_MARPAT_WDL_RolledUp" },{ "CUP_U_B_USMC_MARPAT_WDL_Kneepad" },{ "CUP_U_B_USMC_MARPAT_WDL_TwoKneepads" },{ "CUP_U_B_USMC_MARPAT_WDL_RollUpKneepad" },{ "CUP_U_B_FR_SpecOps" },{ "CUP_U_B_FR_Scout" },{ "CUP_U_B_FR_Scout1" },{ "CUP_U_B_FR_Scout2" },{ "CUP_U_B_FR_Scout3" },{ "CUP_U_B_FR_Officer" },{ "CUP_U_B_FR_Corpsman" },{ "CUP_U_B_FR_DirAction" },{ "CUP_U_B_FR_DirAction2" },{ "CUP_U_B_FR_Light" },{ "CUP_U_I_GUE_Flecktarn" },{ "CUP_U_I_GUE_Flecktarn2" },{ "CUP_U_I_GUE_Flecktarn3" },{ "CUP_U_I_GUE_Woodland1" },{ "CUP_U_I_Ghillie_Top" },{ "CUP_U_I_RACS_PilotOverall" },{ "CUP_U_I_RACS_Desert_1" },{ "CUP_U_I_RACS_Desert_2" },{ "CUP_U_I_RACS_Urban_1" },{ "CUP_U_I_RACS_Urban_2" },{ "CUP_U_O_SLA_Officer_Suit" },{ "CUP_U_O_SLA_MixedCamo" },{ "CUP_U_O_SLA_Green" },{ "CUP_U_O_SLA_Urban" },{ "CUP_U_O_SLA_Desert" },{ "CUP_U_O_SLA_Overalls_Pilot" },{ "CUP_U_O_SLA_Overalls_Tank" },{ "CUP_U_O_Partisan_TTsKO" },{ "CUP_U_O_Partisan_TTsKO_Mixed" },{ "CUP_U_O_Partisan_VSR_Mixed1" },{ "CUP_U_O_Partisan_VSR_Mixed2" },{ "CUP_U_O_TK_Officer" },{ "CUP_U_O_TK_MixedCamo" },{ "CUP_U_O_TK_Green" },{ "CUP_U_O_TK_Ghillie" },{ "CUP_U_O_TK_Ghillie_Top" },{ "CUP_U_B_BAF_DDPM_S1_RolledUp" },{ "CUP_U_B_BAF_DDPM_Tshirt" },{ "CUP_U_B_BAF_DPM_S1_RolledUp" },{ "CUP_U_B_BAF_DPM_S2_UnRolled" },{ "CUP_U_B_BAF_DPM_Tshirt" },{ "CUP_U_B_BAF_MTP_S1_RolledUp" },{ "CUP_U_B_BAF_MTP_S2_UnRolled" },{ "CUP_U_B_BAF_MTP_Tshirt" },{ "CUP_U_B_BAF_MTP_S3_RolledUp" },{ "CUP_U_B_BAF_MTP_S4_UnRolled" },{ "CUP_U_B_BAF_MTP_S5_UnRolled" },{ "CUP_U_B_BAF_MTP_S6_UnRolled" }
		};
	};
	class UniformsTRYK
	{
		itemType = "item";
		items[] = {
		{ "TRYK_U_B_OD_OD_CombatUniform" },{ "TRYK_U_B_OD_OD_R_CombatUniform" },{ "TRYK_U_B_TANTAN_CombatUniform" },{ "TRYK_U_B_TANTAN_R_CombatUniform" },{ "TRYK_U_B_BLKBLK_CombatUniform" },{ "TRYK_U_B_BLKBLK_R_CombatUniform" },{ "TRYK_U_B_GRYOCP_CombatUniform" },{ "TRYK_U_B_GRYOCP_R_CombatUniformTshirt" },{ "TRYK_U_B_TANOCP_CombatUniform" },{ "TRYK_U_B_TANOCP_R_CombatUniformTshirt" },{ "TRYK_U_B_BLKOCP_CombatUniform" },{ "TRYK_U_B_BLKOCP_R_CombatUniformTshirt" },{ "TRYK_U_B_BLKTAN_CombatUniform" },{ "TRYK_U_B_BLKTANR_CombatUniformTshirt" },{ "TRYK_U_B_ODTAN_CombatUniform" },{ "TRYK_U_B_ODTANR_CombatUniformTshirt" },{ "TRYK_U_B_GRTAN_CombatUniform" },{ "TRYK_U_B_GRTANR_CombatUniformTshirt" },{ "TRYK_U_B_wood_CombatUniform" },{ "TRYK_U_B_woodR_CombatUniformTshirt" },{ "TRYK_U_B_wood3c_CombatUniform" },{ "TRYK_U_B_wood3c_CombatUniformTshirt" },{ "TRYK_U_B_MARPAT_WOOD_CombatUniform" },{ "TRYK_U_B_MARPAT_WOOD_CombatUniformTshirt" },{ "TRYK_U_B_WOOD_MARPAT_CombatUniform" },{ "TRYK_U_B_WOOD_MARPAT_CombatUniformTshirt" },{ "TRYK_U_B_woodtan_CombatUniform" },{ "TRYK_U_B_woodtanR_CombatUniformTshirt" },{ "TRYK_U_B_JSDF_CombatUniform" },{ "TRYK_U_B_JSDF_CombatUniformTshirt" },{ "TRYK_U_B_3CD_Delta_BDU" },{ "TRYK_U_B_3CD_Delta_BDUTshirt" },{ "TRYK_U_B_3CD_Ranger_BDU" },{ "TRYK_U_B_3CD_Ranger_BDUTshirt" },{ "TRYK_U_B_3CD_BLK_BDUTshirt" },{ "TRYK_U_B_3CD_BLK_BDUTshirt2" },{ "TRYK_U_B_ACU" },{ "TRYK_U_B_ACUTshirt" },{ "TRYK_U_B_MARPAT_Wood" },{ "TRYK_U_B_MARPAT_Wood_Tshirt" },{ "TRYK_U_B_MARPAT_Desert" },{ "TRYK_U_B_MARPAT_Desert_Tshirt" },{ "TRYK_U_B_MARPAT_Desert2" },{ "TRYK_U_B_MARPAT_Desert2_Tshirt" },{ "TRYK_U_B_3c" },{ "TRYK_U_B_3cr" },{ "TRYK_U_B_Sage_Tshirt" },{ "TRYK_U_B_BLK3CD" },{ "TRYK_U_B_BLK3CD_Tshirt" },{ "TRYK_U_B_BLK" },{ "TRYK_U_B_BLK_Tshirt" },{ "TRYK_U_B_BLKTAN" },{ "TRYK_U_B_BLKTAN_Tshirt" },{ "TRYK_U_B_ODTAN" },{ "TRYK_U_B_ODTAN_Tshirt" },{ "TRYK_U_B_BLK_OD" },{ "TRYK_U_B_BLK_OD_Tshirt" },{ "TRYK_U_B_C01_Tsirt" },{ "TRYK_U_B_C02_Tsirt" },{ "TRYK_U_B_OD_BLK" },{ "TRYK_U_B_OD_BLK_2" },{ "TRYK_U_B_BLK_TAN_1" },{ "TRYK_U_B_BLK_TAN_2" },{ "TRYK_U_B_wh_tan_Rollup_CombatUniform" },{ "TRYK_U_B_wh_OD_Rollup_CombatUniform" },{ "TRYK_U_B_wh_blk_Rollup_CombatUniform" },{ "TRYK_U_B_BLK_tan_Rollup_CombatUniform" },{ "TRYK_U_B_BLK_OD_Rollup_CombatUniform" },{ "TRYK_U_B_NATO_UCP_GRY_CombatUniform" },{ "TRYK_U_B_NATO_UCP_GRY_R_CombatUniform" },{ "TRYK_U_B_NATO_UCP_CombatUniform" },{ "TRYK_U_B_NATO_UCP_R_CombatUniform" },{ "TRYK_U_B_NATO_OCP_c_BLK_CombatUniform" },{ "TRYK_U_B_NATO_OCP_BLK_c_R_CombatUniform" },{ "TRYK_U_B_NATO_OCP_BLK_CombatUniform" },{ "TRYK_U_B_NATO_OCP_BLK_R_CombatUniform" },{ "TRYK_U_B_NATO_OCPD_CombatUniform" },{ "TRYK_U_B_NATO_OCPD_R_CombatUniform" },{ "TRYK_U_B_NATO_OCP_CombatUniform" },{ "TRYK_U_B_NATO_OCP_R_CombatUniform" },{ "TRYK_U_B_AOR1_Rollup_CombatUniform" },{ "TRYK_U_B_AOR2_Rollup_CombatUniform" },{ "TRYK_U_B_MTP_CombatUniform" },{ "TRYK_U_B_MTP_R_CombatUniform" },{ "TRYK_U_B_MTP_BLK_CombatUniform" },{ "TRYK_U_B_MTP_BLK_R_CombatUniform" },{ "TRYK_U_B_Woodland" },{ "TRYK_U_B_Woodland_Tshirt" },{ "TRYK_U_B_WDL_GRY_CombatUniform" },{ "TRYK_U_B_WDL_GRY_R_CombatUniform" },{ "TRYK_U_B_ARO1_GR_CombatUniform" },{ "TRYK_U_B_ARO1_GR_R_CombatUniform" },{ "TRYK_U_B_ARO1_GRY_CombatUniform" },{ "TRYK_U_B_ARO1_GRY_R_CombatUniform" },{ "TRYK_U_B_ARO1_CombatUniform" },{ "TRYK_U_B_ARO1R_CombatUniform" },{ "TRYK_U_B_ARO1_BLK_CombatUniform" },{ "TRYK_U_B_ARO1_BLK_R_CombatUniform" },{ "TRYK_U_B_ARO1_CBR_CombatUniform" },{ "TRYK_U_B_ARO1_CBR_R_CombatUniform" },{ "TRYK_U_B_ARO2_CombatUniform" },{ "TRYK_U_B_ARO2R_CombatUniform" },{ "TRYK_U_B_AOR2_BLK_CombatUniform" },{ "TRYK_U_B_AOR2_BLK_R_CombatUniform" },{ "TRYK_U_B_AOR2_OD_CombatUniform" },{ "TRYK_U_B_AOR2_OD_R_CombatUniform" },{ "TRYK_U_B_AOR2_GRY_CombatUniform" },{ "TRYK_U_B_AOR2_GRY_R_CombatUniform" },{ "TRYK_U_B_Snow_CombatUniform" },{ "TRYK_U_B_Snowt" },{ "TRYK_U_B_Denim_T_WH" },{ "TRYK_U_B_Denim_T_BK" },{ "TRYK_U_B_BLK_T_WH" },{ "TRYK_U_B_BLK_T_BK" },{ "TRYK_U_B_RED_T_BR" },{ "TRYK_U_B_Denim_T_BG_WH" },{ "TRYK_U_B_Denim_T_BG_BK" },{ "TRYK_U_B_BLK_T_BG_WH" },{ "TRYK_U_B_BLK_T_BG_BK" },{ "TRYK_U_B_RED_T_BG_BR" },{ "TRYK_U_B_fleece" },{ "TRYK_U_B_fleece_UCP" },{ "TRYK_U_B_UCP_PCUs" },{ "TRYK_U_B_GRY_PCUs" },{ "TRYK_U_B_Wood_PCUs" },{ "TRYK_U_B_PCUs" },{ "TRYK_U_B_UCP_PCUs_R" },{ "TRYK_U_B_GRY_PCUs_R" },{ "TRYK_U_B_Wood_PCUs_R" },{ "TRYK_U_B_PCUs_R" },{ "TRYK_U_B_PCUGs" },{ "TRYK_U_B_PCUODs" },{ "TRYK_U_B_PCUGs_gry" },{ "TRYK_U_B_PCUGs_BLK" },{ "TRYK_U_B_PCUGs_OD" },{ "TRYK_U_B_PCUGs_gry_R" },{ "TRYK_U_B_PCUGs_BLK_R" },{ "TRYK_U_B_PCUGs_OD_R" },{ "TRYK_U_Bts_GRYGRY_PCUs" },{ "TRYK_U_Bts_UCP_PCUs" },{ "TRYK_U_Bts_Wood_PCUs" },{ "TRYK_U_Bts_PCUs" },{ "TRYK_U_pad_j" },{ "TRYK_U_pad_j_blk" },{ "TRYK_U_pad_hood_Cl" },{ "TRYK_U_pad_hood_Cl_blk" },{ "TRYK_U_pad_hood_tan" },{ "TRYK_U_pad_hood_Blk" },{ "TRYK_U_pad_hood_CSATBlk" },{ "TRYK_U_pad_hood_Blod" },{ "TRYK_U_pad_hood_odBK" },{ "TRYK_U_pad_hood_BKT2" },{ "TRYK_hoodie_Blk" },{ "TRYK_hoodie_FR" },{ "TRYK_hoodie_Wood" },{ "TRYK_hoodie_3c" },{ "TRYK_T_camo_tan" },{ "TRYK_T_camo_3c" },{ "TRYK_T_camo_Wood" },{ "TRYK_T_camo_wood_marpat" },{ "TRYK_T_camo_Desert_marpat" },{ "TRYK_T_camo_3c_BG" },{ "TRYK_T_camo_Wood_BG" },{ "TRYK_T_camo_wood_marpat_BG" },{ "TRYK_T_camo_desert_marpat_BG" },{ "TRYK_T_PAD" },{ "TRYK_T_OD_PAD" },{ "TRYK_T_TAN_PAD" },{ "TRYK_T_BLK_PAD" },{ "TRYK_T_T2_PAD" },{ "TRYK_T_CSAT_PAD" },{ "TRYK_U_nohoodPcu_gry" },{ "TRYK_U_hood_nc" },{ "TRYK_U_hood_mc" },{ "TRYK_U_denim_hood_blk" },{ "TRYK_U_denim_hood_mc" },{ "TRYK_U_denim_hood_3c" },{ "TRYK_U_denim_hood_nc" },{ "TRYK_U_denim_jersey_blu" },{ "TRYK_U_denim_jersey_blk" },{ "TRYK_H_ghillie_over" },{ "TRYK_H_ghillie_top" },{ "TRYK_H_ghillie_top_headless" },{ "TRYK_H_ghillie_over_green" },{ "TRYK_H_ghillie_top_green" },{ "TRYK_H_ghillie_top_headless_green" },{ "TRYK_shirts_PAD" },{ "TRYK_shirts_OD_PAD" },{ "TRYK_shirts_TAN_PAD" },{ "TRYK_shirts_BLK_PAD" },{ "TRYK_shirts_PAD_BK" },{ "TRYK_shirts_OD_PAD_BK" },{ "TRYK_shirts_TAN_PAD_BK" },{ "TRYK_shirts_BLK_PAD_BK" },{ "TRYK_shirts_PAD_BLW" },{ "TRYK_shirts_OD_PAD_BLW" },{ "TRYK_shirts_TAN_PAD_BLW" },{ "TRYK_shirts_BLK_PAD_BLW" },{ "TRYK_shirts_PAD_YEL" },{ "TRYK_shirts_OD_PAD_YEL" },{ "TRYK_shirts_TAN_PAD_YEL" },{ "TRYK_shirts_BLK_PAD_YEL" },{ "TRYK_shirts_PAD_RED2" },{ "TRYK_shirts_OD_PAD_RED2" },{ "TRYK_shirts_TAN_PAD_RED2" },{ "TRYK_shirts_BLK_PAD_RED2" },{ "TRYK_shirts_PAD_BLU3" },{ "TRYK_shirts_OD_PAD_BLU3" },{ "TRYK_shirts_TAN_PAD_BLU3" },{ "TRYK_shirts_BLK_PAD_BLU3" },{ "TRYK_shirts_DENIM_R" },{ "TRYK_shirts_DENIM_BL" },{ "TRYK_shirts_DENIM_BK" },{ "TRYK_shirts_DENIM_WH" },{ "TRYK_shirts_DENIM_BWH" },{ "TRYK_shirts_DENIM_RED2" },{ "TRYK_shirts_DENIM_WHB" },{ "TRYK_shirts_DENIM_ylb" },{ "TRYK_shirts_DENIM_od" },{ "TRYK_shirts_DENIM_R_Sleeve" },{ "TRYK_shirts_DENIM_BL_Sleeve" },{ "TRYK_shirts_DENIM_BK_Sleeve" },{ "TRYK_shirts_DENIM_WH_Sleeve" },{ "TRYK_shirts_DENIM_BWH_Sleeve" },{ "TRYK_shirts_DENIM_RED2_Sleeve" },{ "TRYK_shirts_DENIM_WHB_Sleeve" },{ "TRYK_shirts_DENIM_ylb_Sleeve" },{ "TRYK_shirts_DENIM_od_Sleeve" },{ "TRYK_shirts_PAD_BL" },{ "TRYK_shirts_OD_PAD_BL" },{ "TRYK_shirts_TAN_PAD_BL" },{ "TRYK_shirts_BLK_PAD_BL" },{ "TRYK_U_taki_wh" },{ "TRYK_U_taki_COY" },{ "TRYK_U_taki_BL" },{ "TRYK_U_taki_BLK" },{ "TRYK_U_Bts_PCUGs" },{ "TRYK_U_Bts_PCUODs" },{ "TRYK_U_taki_G_WH" },{ "TRYK_U_taki_G_COY" },{ "TRYK_U_taki_G_BL" },{ "TRYK_U_taki_G_BLK" },{ "TRYK_U_B_PCUHs" },{ "TRYK_U_B_PCUGHs" },{ "TRYK_U_B_PCUODHs" },{ "TRYK_B_USMC_R" },{ "TRYK_B_USMC_R_ROLL" },{ "TRYK_ZARATAKI" },{ "TRYK_ZARATAKI2" },{ "TRYK_ZARATAKI3" },{ "TRYK_B_TRYK_UCP_T" },{ "TRYK_B_TRYK_3C_T" },{ "TRYK_B_TRYK_MTP_T" },{ "TRYK_B_TRYK_OCP_T" },{ "TRYK_B_TRYK_OCP_D_T" },{ "TRYK_DMARPAT_T" },{ "TRYK_C_AOR2_T" },{ "TRYK_U_B_Sage_T" },{ "TRYK_U_B_Wood_T" },{ "TRYK_U_B_BLTAN_T" },{ "TRYK_U_B_BLOD_T" },{ "TRYK_OVERALL_flesh" },{ "TRYK_OVERALL_nok_flesh" },{ "TRYK_OVERALL_SAGE_BLKboots" },{ "TRYK_OVERALL_SAGE_BLKboots_nk_blk" },{ "TRYK_OVERALL_SAGE_BLKboots_nk" },{ "TRYK_OVERALL_SAGE_BLKboots_nk_blk2" },{ "TRYK_OVERALL_SAGE" },{ "TRYK_SUITS_BLK_F" },{ "TRYK_SUITS_BR_F" },{ "TRYK_U_B_PCUHsW" },{ "TRYK_U_B_PCUHsW2" },{ "TRYK_U_B_PCUHsW3" },{ "TRYK_U_B_PCUHsW4" },{ "TRYK_U_B_PCUHsW5" },{ "TRYK_U_B_PCUHsW6" },{ "TRYK_U_B_PCUHsW7" },{ "TRYK_U_B_PCUHsW8" },{ "TRYK_U_B_PCUHsW9" }
		};
	};
	class VestsCUP
	{
		itemType = "item";
		items[] = {
		{ "CUP_V_B_GER_Carrier_Rig" },{ "CUP_V_B_GER_Carrier_Rig_2" },{ "CUP_V_B_GER_Carrier_Vest" },{ "CUP_V_B_GER_Carrier_Vest_2" },{ "CUP_V_B_GER_Carrier_Vest_3" },{ "CUP_V_B_GER_Vest_1" },{ "CUP_V_B_GER_Vest_2" },{ "CUP_V_B_MTV" },{ "CUP_V_B_MTV_Patrol" },{ "CUP_V_B_MTV_Pouches" },{ "CUP_V_B_MTV_noCB" },{ "CUP_V_B_MTV_Marksman" },{ "CUP_V_B_MTV_PistolBlack" },{ "CUP_V_B_MTV_LegPouch" },{ "CUP_V_B_MTV_MG" },{ "CUP_V_B_MTV_Mine" },{ "CUP_V_B_MTV_TL" },{ "CUP_V_B_PilotVest" },{ "CUP_V_B_RRV_TL" },{ "CUP_V_B_RRV_Officer" },{ "CUP_V_B_RRV_Medic" },{ "CUP_V_B_RRV_DA1" },{ "CUP_V_B_RRV_DA2" },{ "CUP_V_B_RRV_MG" },{ "CUP_V_B_RRV_Light" },{ "CUP_V_B_RRV_Scout" },{ "CUP_V_B_RRV_Scout2" },{ "CUP_V_B_RRV_Scout3" },{ "CUP_V_B_LHDVest_Blue" },{ "CUP_V_B_LHDVest_Brown" },{ "CUP_V_B_LHDVest_Green" },{ "CUP_V_B_LHDVest_Red" },{ "CUP_V_B_LHDVest_Violet" },{ "CUP_V_B_LHDVest_White" },{ "CUP_V_B_LHDVest_Yellow" },{ "CUP_V_I_Carrier_Belt" },{ "CUP_V_I_Guerilla_Jacket" },{ "CUP_V_I_RACS_Carrier_Vest" },{ "CUP_V_I_RACS_Carrier_Vest_2" },{ "CUP_V_I_RACS_Carrier_Vest_3" },{ "CUP_V_O_SLA_Carrier_Belt" },{ "CUP_V_O_SLA_Carrier_Belt02" },{ "CUP_V_O_SLA_Carrier_Belt03" },{ "CUP_V_O_SLA_Flak_Vest01" },{ "CUP_V_O_SLA_Flak_Vest02" },{ "CUP_V_O_SLA_Flak_Vest03" },{ "CUP_V_O_TK_CrewBelt" },{ "CUP_V_O_TK_OfficerBelt" },{ "CUP_V_O_TK_OfficerBelt2" },{ "CUP_V_O_TK_Vest_1" },{ "CUP_V_O_TK_Vest_2" },{ "CUP_V_OI_TKI_Jacket1_01" },{ "CUP_V_OI_TKI_Jacket1_02" },{ "CUP_V_OI_TKI_Jacket1_03" },{ "CUP_V_OI_TKI_Jacket1_04" },{ "CUP_V_OI_TKI_Jacket1_05" },{ "CUP_V_OI_TKI_Jacket1_06" },{ "CUP_V_OI_TKI_Jacket2_01" },{ "CUP_V_OI_TKI_Jacket2_02" },{ "CUP_V_OI_TKI_Jacket2_03" },{ "CUP_V_OI_TKI_Jacket2_04" },{ "CUP_V_OI_TKI_Jacket2_05" },{ "CUP_V_OI_TKI_Jacket2_06" },{ "CUP_V_OI_TKI_Jacket3_01" },{ "CUP_V_OI_TKI_Jacket3_02" },{ "CUP_V_OI_TKI_Jacket3_03" },{ "CUP_V_OI_TKI_Jacket3_04" },{ "CUP_V_OI_TKI_Jacket3_05" },{ "CUP_V_OI_TKI_Jacket3_06" },{ "CUP_V_OI_TKI_Jacket4_01" },{ "CUP_V_OI_TKI_Jacket4_02" },{ "CUP_V_OI_TKI_Jacket4_03" },{ "CUP_V_OI_TKI_Jacket4_04" },{ "CUP_V_OI_TKI_Jacket4_05" },{ "CUP_V_OI_TKI_Jacket4_06" },{ "CUP_V_BAF_Osprey_Mk2_DDPM_Scout" },{ "CUP_V_BAF_Osprey_Mk2_DDPM_Soldier1" },{ "CUP_V_BAF_Osprey_Mk2_DDPM_Soldier2" },{ "CUP_V_BAF_Osprey_Mk2_DDPM_Grenadier" },{ "CUP_V_BAF_Osprey_Mk2_DDPM_Sapper" },{ "CUP_V_BAF_Osprey_Mk2_DDPM_Medic" },{ "CUP_V_BAF_Osprey_Mk2_DDPM_Officer" },{ "CUP_V_BAF_Osprey_Mk2_DPM_Scout" },{ "CUP_V_BAF_Osprey_Mk2_DPM_Soldier1" },{ "CUP_V_BAF_Osprey_Mk2_DPM_Soldier2" },{ "CUP_V_BAF_Osprey_Mk2_DPM_Grenadier" },{ "CUP_V_BAF_Osprey_Mk2_DPM_Sapper" },{ "CUP_V_BAF_Osprey_Mk2_DPM_Medic" },{ "CUP_V_BAF_Osprey_Mk2_DPM_Officer" },{ "CUP_V_BAF_Osprey_Mk4_MTP_Grenadier" },{ "CUP_V_BAF_Osprey_Mk4_MTP_MachineGunner" },{ "CUP_V_BAF_Osprey_Mk4_MTP_Rifleman" },{ "CUP_V_BAF_Osprey_Mk4_MTP_SquadLeader" }
		};
	};
	class VestsTRYK
	{
		itemType = "item";
		items[] = {
		{ "TRYK_V_PlateCarrier_JSDF" },{ "TRYK_V_ArmorVest_AOR1" },{ "TRYK_V_ArmorVest_AOR2" },{ "TRYK_V_ArmorVest_coyo" },{ "TRYK_V_ArmorVest_Brown" },{ "TRYK_V_ArmorVest_CBR" },{ "TRYK_V_ArmorVest_khk" },{ "TRYK_V_ArmorVest_green" },{ "TRYK_V_ArmorVest_tan" },{ "TRYK_V_ArmorVest_Delta" },{ "TRYK_V_ArmorVest_Ranger" },{ "TRYK_V_ArmorVest_AOR1_2" },{ "TRYK_V_ArmorVest_AOR2_2" },{ "TRYK_V_ArmorVest_coyo2" },{ "TRYK_V_ArmorVest_Brown2" },{ "TRYK_V_ArmorVest_cbr2" },{ "TRYK_V_ArmorVest_khk2" },{ "TRYK_V_ArmorVest_green2" },{ "TRYK_V_ArmorVest_tan2" },{ "TRYK_V_ArmorVest_Delta2" },{ "TRYK_V_ArmorVest_Ranger2" },{ "TRYK_V_harnes_blk_L" },{ "TRYK_V_harnes_od_L" },{ "TRYK_V_harnes_TAN_L" },{ "TRYK_V_PlateCarrier_blk_L" },{ "TRYK_V_PlateCarrier_wood_L" },{ "TRYK_V_PlateCarrier_ACU_L" },{ "TRYK_V_PlateCarrier_coyo_L" },{ "TRYK_V_Bulletproof" },{ "TRYK_V_Bulletproof_BLK" },{ "TRYK_V_Bulletproof_BL" },{ "TRYK_V_PlateCarrier_blk" },{ "TRYK_V_PlateCarrier_coyo" },{ "TRYK_V_PlateCarrier_wood" },{ "TRYK_V_PlateCarrier_ACU" },{ "TRYK_V_TacVest_coyo" },{ "TRYK_V_IOTV_BLK" }
		};
	};
	class HeadgearCUP
	{
		itemType = "item";
		items[] = {
		{ "CUP_H_C_Ushanka_01" },{ "CUP_H_C_Ushanka_02" },{ "CUP_H_C_Ushanka_03" },{ "CUP_H_C_Ushanka_04" },{ "CUP_H_C_Beanie_01" },{ "CUP_H_C_Beanie_02" },{ "CUP_H_C_Beanie_03" },{ "CUP_H_C_Beanie_04" },{ "CUP_H_C_Beret_01" },{ "CUP_H_C_Beret_02" },{ "CUP_H_C_Beret_03" },{ "CUP_H_C_Beret_04" },{ "CUP_H_GER_Boonie_desert" },{ "CUP_H_GER_Boonie_Flecktarn" },{ "CUP_H_NAPA_Fedora" },{ "CUP_H_PMC_PRR_Headset" },{ "CUP_H_PMC_EP_Headset" },{ "CUP_H_PMC_Cap_Tan" },{ "CUP_H_PMC_Cap_Grey" },{ "CUP_H_PMC_Cap_PRR_Tan" },{ "CUP_H_PMC_Cap_PRR_Grey" },{ "CUP_H_RACS_Beret_Blue" },{ "CUP_H_RACS_Helmet_Des" },{ "CUP_H_RACS_Helmet_Goggles_Des" },{ "CUP_H_RACS_Helmet_Headset_Des" },{ "CUP_H_RACS_Helmet_DPAT" },{ "CUP_H_RACS_Helmet_Goggles_DPAT" },{ "CUP_H_RACS_Helmet_Headset_DPAT" },{ "CUP_H_SLA_TankerHelmet" },{ "CUP_H_SLA_Helmet" },{ "CUP_H_SLA_Pilot_Helmet" },{ "CUP_H_SLA_OfficerCap" },{ "CUP_H_SLA_SLCap" },{ "CUP_H_SLA_Boonie" },{ "CUP_H_SLA_Beret" },{ "CUP_H_TK_TankerHelmet" },{ "CUP_H_TK_PilotHelmet" },{ "CUP_H_TK_Helmet" },{ "CUP_H_TK_Lungee" },{ "CUP_H_TK_Beret" },{ "CUP_H_TKI_SkullCap_01" },{ "CUP_H_TKI_SkullCap_02" },{ "CUP_H_TKI_SkullCap_03" },{ "CUP_H_TKI_SkullCap_04" },{ "CUP_H_TKI_SkullCap_05" },{ "CUP_H_TKI_SkullCap_06" },{ "CUP_H_TKI_Lungee_01" },{ "CUP_H_TKI_Lungee_02" },{ "CUP_H_TKI_Lungee_03" },{ "CUP_H_TKI_Lungee_04" },{ "CUP_H_TKI_Lungee_05" },{ "CUP_H_TKI_Lungee_06" },{ "CUP_H_TKI_Lungee_Open_01" },{ "CUP_H_TKI_Lungee_Open_02" },{ "CUP_H_TKI_Lungee_Open_03" },{ "CUP_H_TKI_Lungee_Open_04" },{ "CUP_H_TKI_Lungee_Open_05" },{ "CUP_H_TKI_Lungee_Open_06" },{ "CUP_H_TKI_Pakol_1_01" },{ "CUP_H_TKI_Pakol_1_02" },{ "CUP_H_TKI_Pakol_1_03" },{ "CUP_H_TKI_Pakol_1_04" },{ "CUP_H_TKI_Pakol_1_05" },{ "CUP_H_TKI_Pakol_1_06" },{ "CUP_H_TKI_Pakol_2_01" },{ "CUP_H_TKI_Pakol_2_02" },{ "CUP_H_TKI_Pakol_2_03" },{ "CUP_H_TKI_Pakol_2_04" },{ "CUP_H_TKI_Pakol_2_05" },{ "CUP_H_TKI_Pakol_2_06" },{ "CUP_H_USMC_Officer_Cap" },{ "CUP_H_USMC_HelmetWDL" },{ "CUP_H_USMC_HeadSet_HelmetWDL" },{ "CUP_H_USMC_HeadSet_GoggleW_HelmetWDL" },{ "CUP_H_USMC_Crew_Helmet" },{ "CUP_H_USMC_Goggles_HelmetWDL" },{ "CUP_H_USMC_Helmet_Pilot" },{ "CUP_H_FR_Headset" },{ "CUP_H_FR_Cap_Headset_Green" },{ "CUP_H_FR_Cap_Officer_Headset" },{ "CUP_H_FR_BandanaGreen" },{ "CUP_H_FR_BandanaWdl" },{ "CUP_H_FR_Bandana_Headset" },{ "CUP_H_FR_Headband_Headset" },{ "CUP_H_FR_ECH" },{ "CUP_H_FR_BoonieMARPAT" },{ "CUP_H_FR_BoonieWDL" },{ "CUP_H_FR_BeanieGreen" },{ "CUP_H_FR_PRR_BoonieWDL" },{ "CUP_H_Navy_CrewHelmet_Blue" },{ "CUP_H_Navy_CrewHelmet_Brown" },{ "CUP_H_Navy_CrewHelmet_Green" },{ "CUP_H_Navy_CrewHelmet_Red" },{ "CUP_H_Navy_CrewHelmet_Violet" },{ "CUP_H_Navy_CrewHelmet_White" },{ "CUP_H_Navy_CrewHelmet_Yellow" },{ "CUP_H_BAF_Officer_Beret_PRR_O" },{ "CUP_H_BAF_Helmet_1_DDPM" },{ "CUP_H_BAF_Helmet_2_DDPM" },{ "CUP_H_BAF_Helmet_3_DDPM" },{ "CUP_H_BAF_Helmet_4_DDPM" },{ "CUP_H_BAF_Helmet_1_DPM" },{ "CUP_H_BAF_Helmet_2_DPM" },{ "CUP_H_BAF_Helmet_3_DPM" },{ "CUP_H_BAF_Helmet_4_DPM" },{ "CUP_H_BAF_Helmet_1_MTP" },{ "CUP_H_BAF_Helmet_2_MTP" },{ "CUP_H_BAF_Helmet_3_MTP" },{ "CUP_H_BAF_Helmet_4_MTP" }
		};
	};
	class HeadgearTRYK
	{
		itemType = "item";
		items[] = {
		{ "TRYK_ESS_CAP" },{ "TRYK_ESS_CAP_OD" },{ "TRYK_ESS_CAP_tan" },{ "TRYK_R_CAP_BLK" },{ "TRYK_R_CAP_TAN" },{ "TRYK_R_CAP_OD_US" },{ "TRYK_r_cap_tan_Glasses" },{ "TRYK_r_cap_blk_Glasses" },{ "TRYK_r_cap_od_Glasses" },{ "TRYK_H_headsetcap_Glasses" },{ "TRYK_H_headsetcap_blk_Glasses" },{ "TRYK_H_headsetcap_od_Glasses" },{ "TRYK_H_Helmet_Winter" },{ "TRYK_H_Helmet_Winter_2" },{ "TRYK_US_ESS_Glasses_H" },{ "TRYK_US_ESS_Glasses_NV" },{ "TRYK_US_ESS_Glasses_TAN_NV" },{ "TRYK_Headphone_NV" },{ "TRYK_HRPIGEAR_NV" },{ "TRYK_US_ESS_Glasses_Cover" },{ "TRYK_balaclava_BLACK_NV" },{ "TRYK_balaclava_NV" },{ "TRYK_balaclava_BLACK_EAR_NV" },{ "TRYK_balaclava_EAR_NV" },{ "TRYK_Shemagh_TAN_NV" },{ "TRYK_Shemagh_MESH_NV" },{ "TRYK_Shemagh_G_NV" },{ "TRYK_Shemagh_WH_NV" },{ "TRYK_Shemagh_EAR_NV" },{ "TRYK_Shemagh_EAR_G_NV" },{ "TRYK_Shemagh_EAR_WH_NV" },{ "TRYK_ShemaghESSTAN_NV" },{ "TRYK_ShemaghESSOD_NV" },{ "TRYK_ShemaghESSWH_NV" },{ "TRYK_ESS_BLKTAN_NV" },{ "TRYK_ESS_BLKBLK_NV" },{ "TRYK_Shemagh_shade_MESH" },{ "TRYK_Shemagh_shade_N" },{ "TRYK_Shemagh_shade_G_N" },{ "TRYK_Shemagh_shade_WH_N" },{ "TRYK_Headset_NV" },{ "TRYK_TAC_EARMUFF_SHADE" },{ "TRYK_TAC_EARMUFF" },{ "TRYK_NOMIC_TAC_EARMUFF" },{ "TRYK_TAC_boonie_SET_NV" },{ "TRYK_TAC_SET_NV_TAN" },{ "TRYK_TAC_SET_NV_OD" },{ "TRYK_TAC_SET_NV_WH" },{ "TRYK_TAC_SET_NV_MESH" },{ "TRYK_TAC_SET_NV_TAN_2" },{ "TRYK_TAC_SET_NV_OD_2" },{ "TRYK_TAC_SET_NV_WH_2" },{ "TRYK_TAC_SET_NV_MESH_2" },{ "TRYK_headset2" },{ "TRYK_G_Shades_Black_NV" },{ "TRYK_G_Shades_Blue_NV" },{ "TRYK_G_bala_ess_NV" },{ "TRYK_bandana_NV" },{ "TRYK_SPgearG_NV" },{ "TRYK_SPgear_PHC1_NV" },{ "TRYK_SPgear_PHC2_NV" },{ "TRYK_G_bala_wh_NV" },{ "TRYK_ESS_wh_NV" },{ "TRYK_Kio_Balaclava" },{ "TRYK_H_Helmet_JSDF" },{ "TRYK_H_Helmet_CC" },{ "TRYK_H_Helmet_WOOD" },{ "TRYK_H_Helmet_ACU" },{ "TRYK_H_Helmet_MARPAT_Wood" },{ "TRYK_H_Helmet_MARPAT_Desert" },{ "TRYK_H_Helmet_MARPAT_Desert2" },{ "TRYK_H_Helmet_3C" },{ "TRYK_H_Booniehat_JSDF" },{ "TRYK_H_Booniehat_3CD" },{ "TRYK_H_Booniehat_CC" },{ "TRYK_H_Booniehat_WOOD" },{ "TRYK_H_Booniehat_MARPAT_WOOD" },{ "TRYK_H_Booniehat_MARPAT_Desert" },{ "TRYK_H_Booniehat_AOR1" },{ "TRYK_H_Booniehat_AOR2" },{ "TRYK_H_PASGT_BLK" },{ "TRYK_H_PASGT_OD" },{ "TRYK_H_PASGT_COYO" },{ "TRYK_H_PASGT_TAN" },{ "TRYK_H_Helmet_Snow" },{ "TRYK_H_WH" },{ "TRYK_H_GR" },{ "TRYK_H_AOR1" },{ "TRYK_H_AOR2" },{ "TRYK_H_DELTAHELM_NV" },{ "TRYK_H_EARMUFF" },{ "TRYK_H_TACEARMUFF_H" },{ "TRYK_H_Bandana_H" },{ "TRYK_H_Bandana_wig" },{ "TRYK_H_Bandana_wig_g" },{ "TRYK_H_wig" },{ "TRYK_H_headset2" },{ "TRYK_H_woolhat" },{ "TRYK_H_woolhat_CW" },{ "TRYK_H_woolhat_WH" },{ "TRYK_H_woolhat_br" },{ "TRYK_H_woolhat_cu" },{ "TRYK_H_woolhat_tan" },{ "TRYK_H_headsetcap" },{ "TRYK_H_headsetcap_blk" },{ "TRYK_H_headsetcap_od" },{ "TRYK_H_pakol" },{ "TRYK_H_pakol2" },{ "TRYK_H_LHS_HEL_G" }
		};
	};
};