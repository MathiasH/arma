private["_display"];
if!(isNil "SM_ActionInUse_Paint") exitWith {};
SM_ActionInUse_Paint = true;
try
{
	[SM_PaintThreadID] call ExileClient_system_thread_removeTask;
	_objTextures = getObjectTextures ExileClientModelBoxVehicle;
	_textures = [];
	{
		_defaultTexture = _x;
		_objTexture = _objTextures select _forEachIndex;
		if!(_defaultTexture isEqualTo _objTexture) then
		{
			_textures pushBack [_forEachIndex,_objTexture];
		};
	} forEach SM_LastObjectTexture;
	_prices = [];
	for "_i" from 0 to (count _textures) - 1 do
	{
		_preTexture = false;
		_texture = (_textures select _i) select 1;
		try
		{
			{
				if(_texture isEqualTo (_x select 1)) then
				{
					throw _forEachIndex;
				};
			} forEach SM_PreTextures;
		}
		catch
		{
			_index = SM_PreTextures select _exception;
			_poptabs = _index select 2;
			_respect = _index select 3;
			_prices pushBack [_poptabs,_respect];
			_preTexture = true;
		};
		if!(_preTexture) then
		{
			_prices pushBack [SM_Paint_PoptabsAmount,SM_Paint_RespectAmount];
		};
	};
	_poptabs = 0;
	_respect = 0;
	{
		_p = _x select 0;
		_r = _x select 1;
		if(SM_Paint_Poptabs) then
		{
			_poptabs = _poptabs + _p;
		};
		if(SM_Paint_Repsect) then
		{
			_respect = _respect + _r;
		};
	} forEach _prices;

	if(SM_Paint_Repsect) then
	{
		_currentRespect = player getVariable ["ExileScore",0];
		if!(_currentRespect >= _respect) then
		{
			throw format["You don't have enough respect to paint! (Required %1)",_respect];
		};
	};
	if(SM_Paint_Poptabs) then
	{
		_currentMoney = player getVariable ["ExileMoney",0];
		if!(_currentMoney >= _poptabs) then
		{
			throw format["You don't have enough poptabs to paint! (Required %1)",_poptabs];
		};
		_newMoney = _currentMoney - _poptabs;
		player setVariable ["ExileMoney",_newMoney,true];	
	};

	["updatePaint",[_textures,SM_Paint_Object,SM_ObjectType]] call ExileClient_system_network_send;
	[] call ExileClient_gui_modelBox_destroy;
} catch 
{
	["ErrorTitleAndText",["Base Painting",_exception]] call ExileClient_gui_toaster_addTemplateToast;
};
_display = uiNamespace getVariable ["SM_Paint",displayNull];
if!(isNull _display) then
{
	closeDialog 0;
};
SM_Texture = nil;
SM_ObjectType = nil;
SM_ActionInUse_Paint = nil;
SM_Paint_Object setVariable ["SM_ActionInUse_Paint",nil,true];
true