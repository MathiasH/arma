try
{
	if !((getPlayerUID player) in forumsupporter) exitWith
	{
		if (ExileClientPlayerScore < 1000) then
		{
			["ErrorTitleOnly", ["Base Painting disabled"]] call ExileClient_gui_toaster_addTemplateToast;
		}
		else
		{
			["SuccessTitleAndText", ["Base Painting", localize 'str_points']] call ExileClient_gui_toaster_addTemplateToast;
		};
	};
	if(ExileClientInteractionObject getVariable ["SM_ActionInUse_Paint",false]) then
	{
		throw "This object is already being painted by someone else";
	};
	if((count (getObjectTextures ExileClientInteractionObject)) isEqualTo 0) then
	{
		throw "This object cannot be painted";
	};
	disableSerialization;
	createDialog "SM_Paint";
	_display = uiNamespace getVariable ["SM_Paint",displayNull];
	if(isNull _display) then
 	{
 		throw "The display does not exist";
 	};
 	SM_Paint_Object = ExileClientInteractionObject;
 	SM_Paint_Object setVariable ["SM_ActionInUse_Paint",true,true];
	{
		_x params [["_displayName",""],["_texture",""],["_poptabs",0],["_respect",0]];
		_indexNear = lbAdd [1500, _displayName];
		lbSetData [1500, _indexNear,_forEachIndex];
	} forEach SM_PreTextures;
	_layers = getObjectTextures SM_Paint_Object;
	for "_i" from 0 to ((count _layers) - 1) do
	{
		_indexNear = lbAdd [2100, str _i];
	};
	(_display displayCtrl 2100) lbSetCurSel 0;
	SM_LastObjectTexture = getObjectTextures SM_Paint_Object;
	SM_TextureMode = "Color";
	SM_Texture = "";
	SM_DefaultTexture = "";
	SM_Layer = 0;
	SM_ObjectType = _this;
	sliderSetRange [1903,0.1,1];
	sliderSetSpeed [1903,0.05,1];
	sliderSetRange [1902,0.1,1];
	sliderSetSpeed [1902,0.05,1];
	sliderSetRange [1900,0.1,1];
	sliderSetSpeed [1900,0.05,1];
	sliderSetRange [1901,0.1,1];
	sliderSetSpeed [1901,0.05,1];
	(_display displayCtrl 1500) ctrlAddEventHandler ["LBSelChanged",{if(SM_TextureMode isEqualTo "Texture") then {[] call SM_Paint_OnLBChange}}];
	(_display displayCtrl 2100) ctrlAddEventHandler ["LBSelChanged",{SM_Layer = (lbCurSel 2100)}];
	(_display displayCtrl 1602) ctrlAddEventHandler ["ButtonClick",{closeDialog 0 }];
	[""] call ExileClient_gui_modelBox_create;
	_vehicleClass = typeOf SM_Paint_Object;
	_vehicleClass call ExileClient_gui_modelBox_update;
	SM_DefaultLayers = getObjectTextures ExileClientModelBoxVehicle;
	{
		ExileClientModelBoxVehicle setObjectTexture [_forEachIndex,_x];
	} forEach SM_LastObjectTexture;
	_code =
	{
		disableSerialization;
		_display = uiNamespace getVariable ["SM_Paint",displayNull];
		switch (SM_TextureMode) do
		{
			case "Texture":
			{
				if(isNil "SM_Texture") exitWith {};
				if!(SM_Texture isEqualTo "") then
				{
					ExileClientModelBoxVehicle setObjectTexture [SM_Layer,SM_Texture];
					_selection = lbCurSel 1500;
					if!(isNil "_selection") then
					{
						if!(((count SM_PreTextures) - 1)  <= _selection) exitWith {};
						_texture = SM_PreTextures select _selection;
						_poptabs = 0;
						_respect = 0;
						if(SM_Paint_Poptabs) then
						{
							_poptabs = _texture select 2;
						};
					};
				};
			};
			case "Color":
			{
				_r = sliderPosition 1903;
				_g = sliderPosition 1902;
				_b = sliderPosition 1900;
				_a = sliderPosition 1901;
				If(_r <= 0.1)then {_r = 0.1;};
				If(_g <= 0.1)then {_g = 0.1;};
				If(_b <= 0.1)then {_b = 0.1;};
				If(_a <= 0.1)then {_a = 0.1;};
				SM_Texture = format["#(rgb,8,8,3)color(%1,%2,%3,%4)",_r,_g,_b,_a];
				ExileClientModelBoxVehicle setObjectTexture [SM_Layer,SM_Texture];
				_poptabs = 0;
				_respect = 0;
				if(SM_Paint_Poptabs) then
				{
					_poptabs = SM_Paint_PoptabsAmount;
				};
			};
		};
		(_display displayCtrl 1003) ctrlSetText format["MODE: %1",SM_TextureMode];
	};
	SM_PaintThreadID = [0.5,_code,[],true] call ExileClient_System_Thread_AddTask;
}
catch
{
	["ErrorTitleAndText",["Base Painting",_exception]] call ExileClient_gui_toaster_addTemplateToast;
};
true