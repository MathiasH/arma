SM_Paint_Poptabs = true;
SM_Paint_PoptabsAmount = 0;
SM_Paint_Repsect = false;
SM_Paint_RespectAmount = 0;
ZUID = getPlayerUID player;
SM_PreTexturesZ =
[
	["Zarbon","addons\texture\flag\zarbonr.paa",0,0]
];
SM_PreTexturesX =
[
	["MudRacing","addons\texture\flag\mudracing.paa",0,0]
];
SM_PreTexturesY =
[
	["Georgey","addons\texture\flag\georgey.paa",0,0],
	["Whitey","addons\texture\flag\whitey.paa",0,0]
];
SM_PreTextures =
[
	["Default","Default",0,0],
	["Aqua","addons\texture\flag\aqua.paa",0,0],
	["Australia","\exile_assets\texture\flag\flag_country_au_co.paa",0,0],
	["Belarus","\exile_assets\texture\flag\flag_country_by_co.paa",0,0],
	["Belgium","\exile_assets\texture\flag\flag_country_be_co.paa",0,0],
	["Black and White","addons\texture\flag\blackandwhite_camo.paa",0,0],
	["Black Triangles","addons\texture\flag\blackTri.paa",0,0],
	["Blue","addons\texture\flag\blue_camo.paa",0,0],
	["Blue Lava","addons\texture\flag\bluelava.paa",0,0],
	["Blue Pattern","addons\texture\flag\bluePattern.paa",0,0],
	["Bricks","addons\texture\flag\greybricks.paa",0,0],
	["Brown Bricks","addons\texture\flag\brownbricks.paa",0,0],
	["Canada","\exile_assets\texture\flag\flag_country_cn_co.paa",0,0],
	["Ceramic","addons\texture\flag\ceramic.paa",0,0],
	["Cinder","addons\texture\flag\cinder.paa",0,0],
	["Croatia","\exile_assets\texture\flag\flag_country_hr_co.paa",0,0],
	["Cats","addons\texture\flag\cats.paa",0,0],
	["Czech","\exile_assets\texture\flag\flag_country_cz_co.paa",0,0],
	["Dark Ceramic","addons\texture\flag\darkCeramic.paa",0,0],
	["Dark Metal","addons\texture\flag\darkMetal.paa",0,0],
	["Dark Wood","addons\texture\flag\darkWood.paa",0,0],
	["Death","addons\texture\flag\death.paa",0,0],
	["Denmark","\exile_assets\texture\flag\flag_country_dk_co.paa",0,0],
	["Digital","addons\texture\flag\digital_camo.paa",0,0],
	["England","addons\texture\flag\england.paa",0,0],
	["Finland","\exile_assets\texture\flag\flag_country_fi_co.paa",0,0],
	["Flecked Fall","addons\texture\flag\fleckedfall_camo.paa",0,0],
	["Flecked Green","addons\texture\flag\fleckedgreen_camo.paa",0,0],
	["France","\exile_assets\texture\flag\flag_country_fr_co.paa",0,0],	
	["Germany","\exile_assets\texture\flag\flag_country_de_co.paa",0,0],
	["Green and Red","addons\texture\flag\greenandred_camo.paa",0,0],
	["Green","addons\texture\flag\GreenCamo_co.paa",0,0],
	["Green Stripes","addons\texture\flag\greenstripes_camo.paa",0,0],
	["Hex","addons\texture\flag\hex_camo.paa",0,0],
	["Interference","addons\texture\flag\interference.paa",0,0],
	["Ireland","\exile_assets\texture\flag\flag_country_ir_co.paa",0,0],
	["Italy","addons\texture\flag\italy.paa",0,0],
	["Luxembourg","addons\texture\flag\luxembourg.paa",0,0],
	["Modern Hex","addons\texture\flag\modernHex.paa",0,0],
	["Mosaic","addons\texture\flag\mosaic.paa",0,0],
	["Netherlands","addons\texture\flag\nl.paa",0,0],
	["Poland","\exile_assets\texture\flag\flag_country_pl2_co.paa",0,0],
	["Portugal","addons\texture\flag\portugal.paa",0,0],
	["Rainbow","\exile_assets\texture\flag\flag_misc_rainbow_co.paa",0,0],
	["Red Bricks","addons\texture\flag\redbricks.paa",0,0],
	["Red Green and Black","addons\texture\flag\redgreenblack_camo.paa",0,0],
	["Rubble","addons\texture\flag\rubble.paa",0,0],
	["Russia","\exile_assets\texture\flag\flag_country_ru_co.paa",0,0],
	["Rusty","addons\texture\flag\rusty.paa",0,0],
	["Urban digital","addons\texture\flag\urbandigital_camo.paa",0,0],
	["Sand Stones","addons\texture\flag\sandstone.paa",0,0],
	["Scotland","\exile_assets\texture\flag\flag_country_sct_co.paa",0,0],
	["Sexy","addons\texture\flag\sexy_camo.paa",0,0],
	["Skull","addons\texture\flag\skull_camo.paa",0,0],
	["Splat","addons\texture\flag\splat_camo.paa",0,0],
	["Stones","addons\texture\flag\greystones.paa",0,0],
	["Stripes Multi","addons\texture\flag\stripes.paa",0,0],
	["Sweden","\exile_assets\texture\flag\flag_country_se_co.paa",0,0],
	["UK","\a3\data_f\Flags\flag_uk_co.paa",0,0],
	["Urban","addons\texture\flag\urban.paa",0,0],
	["USA","\a3\data_f\Flags\flag_us_co.paa",0,0],
	["Wales","addons\texture\flag\wales.paa",0,0],
	["White Bricks","addons\texture\flag\whitebricks.paa",0,0],
	["Wood","addons\texture\flag\wood.paa",0,0]
];
if(ZUID in ["76561197960315617"])then
{
	SM_PreTextures append SM_PreTexturesZ;
};
if(ZUID in ["76561197973652846"])then
{
	SM_PreTextures append SM_PreTexturesX;
};
if(ZUID in ["76561198141741114"])then
{
	SM_PreTextures append SM_PreTexturesY;
};
true