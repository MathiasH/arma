class SM_RscPicture
{
    access = 0;
    type = 0;
    style = 1;
    shadow = 0;
    font = "RobotoCondensedLight";
    sizeEx = "pixelW * 19";
    colorText[] = {1,1,1,1};
    colorBackground[] = {0,0,0,0};
    x = 0;
    y = 0;
    w = 0.2;
    h = 0.15;
    tooltipColorText[] = {1,1,1,1};
    tooltipColorBox[] = {1,1,1,1};
    tooltipColorShade[] = {0,0,0,0.65};
};

class SM_RscButton
{
    access = 0;
    type = 1;
    style = 2;
    x = 0;
    y = 0;
    w = 0.055589;
    h = 0.039216;
    shadow = 0;
    font = "RobotoCondensedLight";
    sizeEx = "pixelW * 19";
    colorText[] = {1,1,1,.9};
    colorDisabled[] = {0.4,0.4,0.4,0};
    colorBackground[] = {0,0,0,0.8};
    colorBackgroundDisabled[] = {0,0,0,0.4};
    colorBackgroundActive[] = {0,0,0,0.4};
    colorFocused[] = {0,0,0,0.4};
    colorShadow[] = {0.023529,0,0.0313725,0};
    colorBorder[] = {0.023529,0,0.0313725,1};
    offsetX = 0.003;
    offsetY = 0.003;
    offsetPressedX = 0.002;
    offsetPressedY = 0.002;
    borderSize = 0.0;
    soundEnter[] = {"\A3\ui_f\data\sound\RscButton\soundEnter",0.09,1};
    soundPush[] = {"\A3\ui_f\data\sound\RscButton\soundPush",0.09,1};
    soundClick[] = {"\A3\ui_f\data\sound\RscButton\soundClick",0.09,1};
    soundEscape[] = {"\A3\ui_f\data\sound\RscButton\soundEscape",0.09,1};
};

class SM_RscText
{
    access = 0;
    type = 0;
    idc = -1;
    style = 0x02;
    colorBackground[] = {0,0,0,0.8};
    colorText[] = {1,1,1,1};
    font = "RobotoCondensedLight";
    sizeEx = "pixelW * 19";
    h = 0.05;
    text = "";
};

class SM_RscListBox
{
    access = 0;
    type = 5;
    w = 0.4;
    h = 0.4;
    text = "";
    rowHeight = 0;
    style = 16;
    font = "RobotoCondensedLight";
    sizeEx = "pixelW * 19";
    shadow = 0;
    color[] = {1,1,1,1};
    colorActive[] = {1,0,0,1};
    colorShadow[] = {0,0,0,0.5};
    colorText[] = {1,1,1,1};
    colorDisabled[] = {1,1,1,0.25};
    colorScrollbar[] = {1,0,0,0};
    colorSelect[] = {0,0,0,1};
    colorSelect2[] = {0,0,0,1};
    colorSelectBackground[] = {0.95,0.95,0.95,1};
    colorSelectBackground2[] = {1,1,1,0.5};
    period = 1.2;
    colorBackground[] = {0.19,0.23,0.24,0.6};
    maxHistoryDelay = 1.0;
    colorPicture[] = {1,1,1,1};
    colorPictureSelected[] = {1,1,1,1};
    colorPictureDisabled[] = {1,1,1,0.25};
    colorPictureRight[] = {1,1,1,1};
    colorPictureRightSelected[] = {1,1,1,1};
    colorPictureRightDisabled[] = {1,1,1,0.25};
    colorTextRight[] = {1,1,1,1};
    colorSelectRight[] = {0,0,0,1};
    colorSelect2Right[] = {0,0,0,1};
    tooltipColorText[] = {1,1,1,1};
    tooltipColorBox[] = {1,1,1,1};
    tooltipColorShade[] = {0,0,0,0.65};
    soundSelect[] = {"\A3\ui_f\data\sound\RscListBox\soundSelect",0.09,1};
    class ListScrollBar
    {
        color[] = {1,1,1,0.6};
        colorActive[] = {1,1,1,1};
        colorDisabled[] = {1,1,1,0.3};
        thumb = "\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa";
        arrowEmpty = "\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa";
        arrowFull = "\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa";
        border = "\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa";
        shadow = 0;
        scrollSpeed = 0.06;
        autoScrollEnabled = 0;
        autoScrollSpeed = -1;
        autoScrollDelay = 5;
        autoScrollRewind = 0;
    };
};

class SM_RscSlider
{
    style = "0x400 + 0x10";
    type = 43;
    shadow = 0;
    color[] = {1,1,1,1};
    colorActive[] = {1,1,1,1};
    colorDisabled[] = {1,1,1,1};
    arrowEmpty = "\A3\ui_f\data\gui\cfg\slider\arrowEmpty_ca.paa";
    arrowFull = "\A3\ui_f\data\gui\cfg\slider\arrowFull_ca.paa";
    border = "\A3\ui_f\data\gui\cfg\slider\border_ca.paa";
    thumb = "\A3\ui_f\data\gui\cfg\slider\thumb_ca.paa";
};

class SM_RscCombo 
{
    deletable = 0;
    fade = 0;
    access = 0;
    type = 4;
    colorSelect[] = {0, 0, 0, 1};
    colorText[] = {1, 1, 1, 1};
    colorBackground[] = {0, 0, 0, 1};
    colorScrollbar[] = {1, 0, 0, 1};
    colorDisabled[] = {1, 1, 1, 0.25};
    colorPicture[] = {1, 1, 1, 1};
    colorPictureSelected[] = {1, 1, 1, 1};
    colorPictureDisabled[] = {1, 1, 1, 0.25};
    colorPictureRight[] = {1, 1, 1, 1};
    colorPictureRightSelected[] = {1, 1, 1, 1};
    colorPictureRightDisabled[] = {1, 1, 1, 0.25};
    colorTextRight[] = {1, 1, 1, 1};
    colorSelectRight[] = {0, 0, 0, 1};
    colorSelect2Right[] = {0, 0, 0, 1};
    tooltipColorText[] = {1, 1, 1, 1};
    tooltipColorBox[] = {1, 1, 1, 1};
    tooltipColorShade[] = {0, 0, 0, 0.65};
    soundSelect[] = {"\A3\ui_f\data\sound\RscCombo\soundSelect", 0.1, 1};
    soundExpand[] = {"\A3\ui_f\data\sound\RscCombo\soundExpand", 0.1, 1};
    soundCollapse[] = {"\A3\ui_f\data\sound\RscCombo\soundCollapse", 0.1, 1};
    maxHistoryDelay = 1;
    class ComboScrollBar 
    {
        color[] = {1, 1, 1, 1};
        colorActive[] = {1, 1, 1, 1};
        colorDisabled[] = {1, 1, 1, 0.3};
        thumb = "\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa";
        arrowEmpty = "\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa";
        arrowFull = "\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa";
        border = "\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa";
        shadow = 0;
        scrollSpeed = 0.06;
        width = 0;
        height = 0;
        autoScrollEnabled = 0;
        autoScrollSpeed = -1;
        autoScrollDelay = 5;
        autoScrollRewind = 0;
    };
    style = "0x10 + 0x200";
    font = "RobotoCondensedLight";
    sizeEx = "pixelW * 19";
    shadow = 0;
    x = 0;
    y = 0;
    w = 0.12;
    h = 0.035;
    colorSelectBackground[] = {1, 1, 1, 0.7};
    arrowEmpty = "\A3\ui_f\data\GUI\RscCommon\rsccombo\arrow_combo_ca.paa";
    arrowFull = "\A3\ui_f\data\GUI\RscCommon\rsccombo\arrow_combo_active_ca.paa";
    wholeHeight = 0.45;
    colorActive[] = {1, 0, 0, 1};
};

class SM_Paint
{
	duration = 999999;
	idd = 42056;
	onload = "uinamespace setVariable ['SM_Paint',_this select 0];";
	onunload = "uinamespace setVariable ['SM_Paint',displayNull]; {ExileClientInteractionObject setObjectTexture [_foreachIndex,_x]} forEach SM_LastObjectTexture; [SM_PaintThreadID] call ExileClient_system_thread_removeTask; [] call ExileClient_gui_modelBox_destroy; SM_Paint_Object setVariable ['SM_ActionInUse_Paint',nil,true];";
	class controls
	{
		class RscFrame_1800: SM_RscPicture
		{
			idc = 1800;
			text = "";
			x = 0.0078125 * safezoneW + safezoneX;
			y = 0.038 * safezoneH + safezoneY;
			w = 0.170625 * safezoneW;
			h = 0.80 * safezoneH;
			colorBackground[] = {0.19,0.23,0.24,0.6};
		};
		class Blue: SM_RscSlider
		{
			idc = 1900;
			x = 0.014375 * safezoneW + safezoneX;
			y = 0.5364 * safezoneH + safezoneY;
			w = 0.1575 * safezoneW;
			h = 0.0336 * safezoneH;
			color[] = {0,0,1,1};
			colorActive[] = {0,0,1,1};
		};
		class brightness: SM_RscSlider
		{
			idc = 1901;
			x = 0.014375 * safezoneW + safezoneX;
			y = 0.5728 * safezoneH + safezoneY;
			w = 0.1575 * safezoneW;
			h = 0.0336 * safezoneH;
		};
		class green: SM_RscSlider
		{
			idc = 1902;
			x = 0.014375 * safezoneW + safezoneX;
			y = 0.5 * safezoneH + safezoneY;
			w = 0.1575 * safezoneW;
			h = 0.0336 * safezoneH;
			color[] = {0,1,0,1};
			colorActive[] = {0,1,0,1};
		};
		class red: SM_RscSlider
		{
			idc = 1903;
			x = 0.014375 * safezoneW + safezoneX;
			y = 0.4636 * safezoneH + safezoneY;
			w = 0.1575 * safezoneW;
			h = 0.0336 * safezoneH;
			color[] = {1,0,0,1};
			colorActive[] = {1,0,0,1};
		};
		class listbox: SM_RscListbox
		{
			idc = 1500;
			x = 0.014375 * safezoneW + safezoneX;
			y = 0.2172 * safezoneH + safezoneY;
			w = 0.1575 * safezoneW;
			h = 0.196 * safezoneH;
		};
		class RscText_1000: SM_RscText
		{
			idc = 1000;
			text = "TEXTURES:";
			x = 0.014375 * safezoneW + safezoneX;
			y = 0.1752 * safezoneH + safezoneY;
			w = 0.1575 * safezoneW;
			h = 0.042 * safezoneH;
		};
		class RscText_1001: SM_RscText
		{
			idc = 1001;
			text = "COLOR:";
			x = 0.014375 * safezoneW + safezoneX;
			y = 0.416 * safezoneH + safezoneY;
			w = 0.1575 * safezoneW;
			h = 0.042 * safezoneH;
		};
		class RscButton_1600: SM_RscButton
		{
			idc = 1600;
			text = "CHANGE MODE";
			x = 0.014375 * safezoneW + safezoneX;
			y = 0.6624 * safezoneH + safezoneY;
			w = 0.1575 * safezoneW;
			h = 0.0504 * safezoneH;
			action = "[] call SM_Paint_ChangeMode";
		};
		class RscButton_1601: SM_RscButton
		{
			idc = 1601;
			text = "SELECT";
			x = 0.014375 * safezoneW + safezoneX;
			y = 0.7184 * safezoneH + safezoneY;
			w = 0.1575 * safezoneW;
			h = 0.0504 * safezoneH;
			action = "[] call SM_Paint_Finish";
		};
		class RscButton_1602: SM_RscButton
		{
			idc = 1602;
			text = "CANCEL";
			x = 0.014375 * safezoneW + safezoneX;
			y = 0.7744 * safezoneH + safezoneY;
			w = 0.1575 * safezoneW;
			h = 0.0504 * safezoneH;
		};
		class RscText_1003: SM_RscText
		{
			idc = 1003;
			text = "MODE:";
			x = 0.014375 * safezoneW + safezoneX;
			y = 0.1304 * safezoneH + safezoneY;
			w = 0.1575 * safezoneW;
			h = 0.042 * safezoneH;
		};
		class RscCombo_2100: SM_RscCombo
		{
			idc = 2100;
			x = 0.014375 * safezoneW + safezoneX;
			y = 0.094 * safezoneH + safezoneY;
			w = 0.1575 * safezoneW;
			h = 0.0336 * safezoneH;
		};
		class RscText_1004: SM_RscText
		{
			idc = 1004;
			text = "LAYER DROPDOWN:";
			x = 0.014375 * safezoneW + safezoneX;
			y = 0.052 * safezoneH + safezoneY;
			w = 0.1575 * safezoneW;
			h = 0.042 * safezoneH;
		};
	};
};