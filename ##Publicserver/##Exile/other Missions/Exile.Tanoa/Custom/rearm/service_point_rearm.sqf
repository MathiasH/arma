private ["_vehicle","_args","_servicePoint","_costs","_magazineCount","_weapon","_type","_name","_weaponType","_weaponName","_turret","_magazines","_ammo","_exilew","_veh"];
_vehicle = _this select 0;
_args = _this select 3;
_servicePoint = _args select 0;
_costs = _args select 1;
_weapon = _args select 2;
_type = typeOf _vehicle;
_name = getText(configFile >> "cfgVehicles" >> _type >> "displayName");
_weaponName = _weapon select 0;
_ammoClass = _weapon select 1;
_ammoName = _weapon select 2;
_ammoMAX = _weapon select 3;
_ammoMIN = _weapon select 4;
_turret = _weapon select 5;
_exilew = player getVariable ["ExileMoney", 0];
_veh = vehicle player;
if(ExilePlayerInSafezone)exitWith{["ErrorTitleAndText", ["Rearm Disabled", "You cannot rearm whilst in trader"]] call ExileClient_gui_toaster_addTemplateToast;};
_flags = player nearObjects ['Exile_Construction_Flag_Static', 100];
if!(_flags isEqualTo [])exitWith{["ErrorTitleAndText", ["Rearm Disabled", "You cannot rearm at gas stations near bases"]] call ExileClient_gui_toaster_addTemplateToast;};
if (ExileClientPlayerIsInCombat)exitWith{["ErrorTitleOnly", ["You're in combat!"]] call ExileClient_gui_toaster_addTemplateToast;};
if (!local _vehicle) exitWith {["ErrorTitleOnly", ["Get in vehicle as driver"]] call ExileClient_gui_toaster_addTemplateToast; diag_log format["Error: called service_point_rearm.sqf with non-local vehicle: %1", _vehicle] };
if(count (crew _vehicle) > 1) exitWith {["ErrorTitleOnly", ["Vehicle can not have passengers"]] call ExileClient_gui_toaster_addTemplateToast;};
if(!(_veh isEqualTo player) && !((driver _veh) isEqualTo player)) exitWith {["ErrorTitleOnly", ["You must be in vehicle as driver"]] call ExileClient_gui_toaster_addTemplateToast;};
if (_exilew <_costs) exitWith {["ErrorTitleOnly", ["Not enough poptabs"]] call ExileClient_gui_toaster_addTemplateToast;};
if(_type isKindOf "Air") then 
{
	_ammoMAX = 10;
	_ammoMIN = 1;
} else 
{
	_ammoMAX = _weapon select 3;
	_ammoMIN = _weapon select 4;	
};
_currentmags = magazines _vehicle;
_magscount = count _currentmags;
_ammocount = {_ammoClass == _x}count _currentmags;
/*if(_magscount > _ammoMAX)exitWith{["ErrorTitleOnly", ["Ammo already full"]] call ExileClient_gui_toaster_addTemplateToast;};
if(_ammocount > _ammoMIN)exitWith{["ErrorTitleOnly", ["Ammo already full"]] call ExileClient_gui_toaster_addTemplateToast;};*/
if(_costs > 0 && isTradeEnabled)then{
        takegive_poptab = [player,_costs,true];
        publicVariableServer "takegive_poptab";
};
if(_weaponName == "Flares")exitWith
{
	_vehicle removeWeaponTurret ["CMFlareLauncher",[-1]];
	_vehicle addMagazineTurret ["300Rnd_CMFlare_Chaff_Magazine",[-1]];
	_vehicle addWeaponTurret ["CMFlareLauncher",[-1]];
	_vehicle removeWeaponTurret ["CMFlareLauncher",[0]];
	_vehicle addMagazineTurret ["300Rnd_CMFlare_Chaff_Magazine",[0]];
	_vehicle addWeaponTurret ["CMFlareLauncher",[0]];
	cutText [format["%1 of %2 Rearmed", _weaponName, _name], "PLAIN DOWN"];
};
if(_weaponName == "Smoke Screen")exitWith
{
	_vehicle removeWeaponTurret ["SmokeLauncher",[-1]];
	_vehicle addMagazineTurret ["SmokeLauncherMag",[-1]];
	_vehicle addWeaponTurret ["SmokeLauncher",[-1]];
	_vehicle removeWeaponTurret ["SmokeLauncher",[0]];
	_vehicle addMagazineTurret ["SmokeLauncherMag",[0]];
	_vehicle addWeaponTurret ["SmokeLauncher",[0]];
	cutText [format["%1 of %2 Rearmed", _weaponName, _name], "PLAIN DOWN"];
};
_vehicle addMagazineTurret [_ammoClass, _turret]; 
cutText [format["%1 of %2 Rearmed", _weaponName, _name], "PLAIN DOWN"];