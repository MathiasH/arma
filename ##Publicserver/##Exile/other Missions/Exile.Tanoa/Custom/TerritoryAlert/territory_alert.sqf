private ["_playerUID","_flag","_buildRights","_name","_nextDueDate","_dueDate"];
if(isStreamFriendlyUIEnabled) exitWith {};
_playerUID = getPlayerUID player;
if !((_playerUID) in forumsupporter) exitWith
{
  ["ErrorTitleAndText", ["Mobile Xm8 Email Alerts", "If you enter your email address you will get alerts for your territories"]] call ExileClient_gui_toaster_addTemplateToast;
};
{
  _flag = _x;
  _buildRights = _flag getVariable ["ExileTerritoryBuildRights", []];
  _name = _flag getVariable ["ExileTerritoryName", ""];
  if (_playerUID in _buildRights) then
  {
    if ((_flag getVariable ["ExileFlagStolen", 0]) isEqualTo 1) then
    {
      ["ErrorTitleAndText", ["<t color='#DD2626'>Territory Information</t>", format ["<t size='20' color='#3FD4FC'>'%1' flag is stolen</t>", _name]]] call ExileClient_gui_toaster_addTemplateToast;
    }
    else
    {
      _nextDueDate = _flag getVariable ["ExileTerritoryMaintenanceDue", [0, 0, 0, 0, 0]];
      _dueDate = format ["%3/%2/%1",_nextDueDate select 0,_nextDueDate select 1,_nextDueDate select 2];
      ["InfoTitleAndText", ["<t color='#DD2626'>Territory Due Date</t>", format ["<t size='20' color='#FFFFFF'>YOU HAVE UNTIL:</t><br/><t color='#3FD4FC'>%1</t><br/><t size='20' color='#FFFFFF'>TO PAY FOR:</t><br/><t color='#3FD4FC'>""%2""</t>", _dueDate, _name]]] call ExileClient_gui_toaster_addTemplateToast;
    };
  };
} forEach (allMissionObjects "Exile_Construction_Flag_Static");