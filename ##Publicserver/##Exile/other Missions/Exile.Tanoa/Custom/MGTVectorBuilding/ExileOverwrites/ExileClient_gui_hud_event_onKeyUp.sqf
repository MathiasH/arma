private["_stopPropagation","_caller","_keyCode","_shiftState","_controlState","_altState","_posObject","_step"];
_stopPropagation = false;
_caller = _this select 0;
_keyCode = _this select 1;
_shiftState = _this select 2;
_controlState = _this select 3;
_altState = _this select 4;
if (_keyCode in (actionKeys "TacticalView")) exitWith {true};
if (_keyCode in (actionKeys "User1") && !(_keyCode isEqualTo 0x02)) exitWith
{
	if !(ExileClientIsHandcuffed) then
	{
		call ExileClient_object_vehicle_interaction_keyLock;
	};
	true
};
if (_keyCode in (actionKeys "User2")) exitWith
{
	if (ExileClientAllowPartyMarkers) then
	{
		if !(ExileClientPartyID isEqualTo -1) then
		{
			if !(ExileClientIsHandcuffed) then
			{
				call ExileClient_system_party_updateMyMarker;
			};
		};
	};
	true
};
if (_keyCode in (actionKeys "User3")) exitWith
{
	ExileClientWaypoints = [];
	true
};
if (_keyCode in (actionKeys "User20")) exitWith
{
    if !(ExileClientIsHandcuffed || ExileIsPlayingRussianRoulette) then
    {
        if ((damage player) isEqualTo 0) then
        {
            ["SuccessTitleOnly", ["You're already full health"]] call ExileClient_gui_toaster_addTemplateToast;
        }
        else
        {
            if !(alive player) then
            {
                ["ErrorTitleOnly", ["You're dead"]] call ExileClient_gui_toaster_addTemplateToast;
            }
            else
            {
                if !("Exile_Item_InstaDoc" in (magazines player)) then
                {
                    ["WarningTitleOnly", ["No Instadoc"]] call ExileClient_gui_toaster_addTemplateToast;
                }
                else
                {
                    if ("Exile_Item_InstaDoc" in (magazines player)) then
                    {
                        ["Exile_Item_InstaDoc"] call ExileClient_object_item_consume;
                    };
                };
            };
        };
    };
    true
};
switch (_keyCode) do
{
	case 0x29:
	{
		call ExileClient_gui_hud_toggleStatsBar;
		_stopPropagation = true;
	};
	case 0x11,
	case 0x1E,
	case 0x1F,
	case 0x20:
	{
		if (ExileClientIsAutoRunning) then
		{
			call ExileClient_system_autoRun_stop;
			_stopPropagation = true;
		};
	};
	case 0x0B:	
	{
		if !(ExileClientIsHandcuffed) then
		{
			if !(ExileIsPlayingRussianRoulette) then
			{
				call ExileClient_system_autoRun_toggle;
			};
		};
		_stopPropagation = true;
	};
	case 0x09:
	{
		if(isNil'ENVI_ID')then
		{    
		    enviro =
			{
				if (((getPosASL player) select 2 > 50) || ((getPosATL player) select 2 > 10)) then
				{
					enableEnvironment [false,false];
				}
				else
				{
					enableEnvironment [false,true];
				};
			};
			[1, enviro, [], true] call ExileClient_system_thread_addTask;    
		    ['SuccessTitleOnly', ['Removed environment sounds']] call ExileClient_gui_toaster_addTemplateToast;  
		    ENVI_ID = true;
		}
		else
		{
		    
		    enableEnvironment [false,true];
		    if(!isNil'enviro')then{[enviro] call ExileClient_system_thread_removeTask;enviro=nil;};    
		    ['ErrorTitleOnly', ['Enabled environment sounds']] call ExileClient_gui_toaster_addTemplateToast;  
		    ENVI_ID = nil;
		};
		_stopPropagation = true;
	};
	case 0x0A: 	{ _stopPropagation = true; };
	case 0x3B: 	{ _stopPropagation = true; };
	case 0x3C: 	{ _stopPropagation = true; };
	case 0x3D:	{ _stopPropagation = true; };
	case 0x3E:	{ _stopPropagation = true; };
	case 0x41: 	{ _stopPropagation = true; };
	case 0x42:	{ _stopPropagation = true; };
	case 0x43: 	{ _stopPropagation = true; };
	case 0x44:	{ _stopPropagation = true; };
	case 0x57: 	{ _stopPropagation = true; };
	case 0x58: 	{ _stopPropagation = true; };
	case 0x02:
	{
		if !(ExileClientIsHandcuffed || ExileIsPlayingRussianRoulette) then
		{
			if (ExileClientIsInConstructionMode) then
			{
				ExileClientConstructionObject setObjectTextureGlobal [0, "#(argb,2,2,1)color(0.7,0.93,0,0.6,ca)"];
				ExileClientConstructionCurrentSnapToObject = objNull;
				ExileClientConstructionIsInSelectSnapObjectMode = false;
				ExileClientConstructionPossibleSnapPositions = [];
				ExileClientConstructionMode = 1;
				[] call ExileClient_gui_constructionMode_update;
			}
			else
			{
				if (primaryWeapon player != "") then
				{
					if (primaryWeapon player != currentWeapon player) then
					{
						player selectWeapon (primaryWeapon player);
					};
				};
			};
		};
		_stopPropagation = true;
	};
	case 0x03:
	{
		if !(ExileClientIsHandcuffed || ExileIsPlayingRussianRoulette) then
		{
			if (ExileClientIsInConstructionMode) then
			{
				ExileClientConstructionObject setObjectTextureGlobal [0, "#(argb,2,2,1)color(0.7,0.93,0,0.6,ca)"];
				ExileClientConstructionCurrentSnapToObject = objNull;
				ExileClientConstructionIsInSelectSnapObjectMode = false;
				ExileClientConstructionPossibleSnapPositions = [];
				ExileClientConstructionMode = 2;
				[] call ExileClient_gui_constructionMode_update;
			}
			else
			{
				if (handgunWeapon player != "") then
				{
					if (handgunWeapon player != currentWeapon player) then
					{
						player selectWeapon (handgunWeapon player);
					};
				};
			};
		};
		_stopPropagation = true;
	};
	case 0x04:
	{
		if !(ExileClientIsHandcuffed || ExileIsPlayingRussianRoulette) then
		{
			if (ExileClientIsInConstructionMode) then
			{
				if (ExileClientConstructionSupportSnapMode) then
				{
					if (ExileClientConstructionMode != 3) then
					{
						ExileClientConstructionCurrentSnapToObject = objNull;
						ExileClientConstructionPossibleSnapPositions = [];
						["InfoTitleAndText", ["Snap Mode", "Look at the object you want to snap to, press SPACE to lock on it and then move your object next to a snap point. Press SPACE again to place the object."]] call ExileClient_gui_toaster_addTemplateToast;
					};
					ExileClientConstructionMode = 3;
					ExileClientConstructionIsInSelectSnapObjectMode = true;
					[] call ExileClient_gui_constructionMode_update;
				};
			}
			else
			{
				if (secondaryWeapon player != "") then
				{
					if (secondaryWeapon player != currentWeapon player) then
					{
						player selectWeapon (secondaryWeapon player);
					};
				};
			};
		};
		_stopPropagation = true;
	};
	case 0x05:
	{
		if !(ExileClientIsHandcuffed || ExileIsPlayingRussianRoulette) then
		{
			if (ExileClientIsInConstructionMode) then
			{
				_noPhysics = ["Exile_Item_Flag"];
				if !((ExileClientConstructionKitClassName) in _noPhysics) then
				{
					ExileClientConstructionModePhysx = !ExileClientConstructionModePhysx;
					[] call ExileClient_gui_constructionMode_update;
				};
				_yesPhysics = ["Exile_Item_SafeKit","Exile_Item_SafeSmallKit"];
				if ((ExileClientConstructionKitClassName) in _yesPhysics) then
				{					
					if !(ExileClientConstructionKitClassName in (magazines player))then
					{
						ExileClientConstructionModePhysx = !ExileClientConstructionModePhysx;
						[] call ExileClient_gui_constructionMode_update;
					}
					else
					{
						ExileClientConstructionModePhysx = true;
						[] call ExileClient_gui_constructionMode_update;
						_simulatePhysics = true;
					};
				};
			}
			else
			{
				if (currentWeapon player != "") then
				{
					ExileClientPlayerHolsteredWeapon = currentWeapon player;
					player action["switchWeapon", player, player, 100];
				}
				else
				{
					if (ExileClientPlayerHolsteredWeapon != "") then
					{
						player selectWeapon ExileClientPlayerHolsteredWeapon;
					};
				};
			};
		};
		_stopPropagation = true;
	};
	case 0x06:
	{
		if (ExileClientIsInConstructionMode) then
		{
			ExileClientConstructionShowHint = !ExileClientConstructionShowHint;
			[] call ExileClient_gui_constructionMode_update;
		}
		else
		{
			call ExileClient_system_music_earplugs_toggle;
		};
		_stopPropagation = true;
	};
	case 0x07:
	{
		if (ExileClientIsInConstructionMode) then
		{
			if(ExileClientConstructionLock)then
			{
				ExileClientConstructionLock = false;
				_posObject = position ExileClientConstructionObject;
				ExileClientConstructionOffset = player worldToModel _posObject;
				ExileClientConstructionRotation = (getDir ExileClientConstructionObject) - (getDir player);
			}
			else
			{
				ExileClientConstructionLock = true;
			};
		}
		else
		{
			if (!ExileClientXM8IsVisible) then
			{
				if ("Exile_Item_XM8" in (assignedItems player)) then
				{
					if (alive player) then
					{
						[] call ExileClient_gui_xm8_show;
					};
				};
			};
		};
		_stopPropagation = true;
	};
	case 0x08:
    {
        if !(ExileClientIsHandcuffed) then
		{
			if (ExileClientIsInConstructionMode) then
			{
				if(ExileClientConstructionVectorMode) then
				{
					ExileClientConstructionVectorMode = false;
				}
				else
				{
					_noRotate = ["BlockConcrete_F_Kit"];
					if !((ExileClientConstructionKitClassName) in _noRotate) then
					{					
						ExileClientConstructionVectorMode = true;
					};
				};
				[] call ExileClient_gui_constructionMode_update;
			}
			else
			{
				if(isNil'QUIET_ID')then
				{
				    2 fadeSound 0.01;
				    true call ExileClient_gui_hud_toggleEarplugsIcon;
				    ['SuccessTitleOnly', ['Added extra quiet earplugs']] call ExileClient_gui_toaster_addTemplateToast;			   
				    QUIET_ID = true;
				}
				else
				{
				    2 fadeSound 1;
				    false call ExileClient_gui_hud_toggleEarplugsIcon;
				    ['ErrorTitleOnly', ['Removed extra quiet earplugs']] call ExileClient_gui_toaster_addTemplateToast;			   
				    QUIET_ID = nil;
				};
			};
		};
		_stopPropagation = true;
    };
	case 0x39:
	{
		if (ExileIsPlayingRussianRoulette) then
		{
			if (ExileRussianRouletteCanFire) then
			{
				[] spawn ExileClient_system_russianRoulette_fire;
			};
		}
		else
		{
			if (ExileClientIsInConstructionMode) then
			{
				if (ExileClientConstructionMode == 3) then
				{
					if (ExileClientConstructionIsInSelectSnapObjectMode) then
					{
						if !(isNull ExileClientConstructionCurrentSnapToObject) then
						{
							ExileClientConstructionIsInSelectSnapObjectMode = false;
							[] call ExileClient_gui_constructionMode_update;
						};
					}
					else
					{
						if (ExileClientConstructionCanPlaceObject) then
						{
							ExileClientConstructionResult = 1;
						};
					};
				}
				else
				{
					if (ExileClientConstructionCanPlaceObject) then
					{
						ExileClientConstructionResult = 1;
					};
				};
				_stopPropagation = true;
			};
		};
	};
	case 0x01:
	{
		if (ExileIsPlayingRussianRoulette) then
		{
			_stopPropagation = true;
		}
		else
		{
			if (ExileClientIsInConstructionMode) then
			{
				_stopPropagation = true;
			};
		};
	};
	case 0x10:
	{
        if (ExileClientIsInConstructionMode) then
		{
			_step = 45;
			if (_shiftState) then
			{
				_step = 90;
			}
			else
			{
				if (_controlState) then
				{
					_step = 22.5;
				};
			};
            if(ExileClientConstructionVectorMode) then
            {
				_step = 22.5;
				if (_shiftState) then
				{
					_step = 45;
				}
				else
				{
					if (_controlState) then
					{
						_step = 1;
					};
				};
				ExileClientConstructionBank = ExileClientConstructionBank - _step;
            }
            else
            {
                ExileClientConstructionRotation = (ExileClientConstructionRotation - _step + 360) % 360;
            };
			[] call ExileClient_gui_constructionMode_update;
			_stopPropagation = true;
		};
	};
	case 0x12:
	{
        if (ExileClientIsInConstructionMode) then
    	{
            _step = 45;
            if (_shiftState) then
            {
                _step = 90;
            }
            else
            {
                if (_controlState) then
                {
                    _step = 22.5;
                };
            };
            if(ExileClientConstructionVectorMode) then
            {
				_step = 22.5;
				if (_shiftState) then
				{
					_step = 45;
				}
				else
				{
					if (_controlState) then
					{
						_step = 1;
					};
				};
				ExileClientConstructionBank = ExileClientConstructionBank + _step;
            }
            else
            {
        		ExileClientConstructionRotation = (ExileClientConstructionRotation + _step + 360) % 360;
            };
			[] call ExileClient_gui_constructionMode_update;
            _stopPropagation = true;
    	};
	};
	case 0x19:
	{
		if (ExileClientIsInBush) then
		{
			call ExileClient_object_bush_detach;
			_stopPropagation = true;
		};
	};
	case 0x31:
	{
		if!(isNull ExileClientCameraParentObject)then
		{
			if(ExileClientCameraNVG)then
			{
				camUseNVG false;
			}
			else
			{
				camUseNVG true;
			};
			ExileClientCameraNVG = !ExileClientCameraNVG;
			_stopPropagation = true;
		};
	};
	case 0xC9:
	{
		if (ExileClientIsInConstructionMode) then
		{
			_step = 0.25;
			if (_shiftState) then
			{
				_step = 1;
			}
			else
			{
				if (_controlState) then
				{
					_step = 0.025;
				};
			};
			if(ExileClientConstructionVectorMode) then
            {
				_step = 22.5;
				if (_shiftState) then
				{
					_step = 45;
				}
				else
				{
					if (_controlState) then
					{
						_step = 1;
					};
				};
                ExileClientConstructionPitch = ExileClientConstructionPitch + _step;
            }
            else
            {
                ExileClientConstructionOffset set [2, ((((ExileClientConstructionOffset select 2) + _step) min 6) max -3) ];
            };
			[] call ExileClient_gui_constructionMode_update;
			_stopPropagation = true;
		};
	};
	case 0xD1:
	{
		if (ExileClientIsInConstructionMode) then
		{
			_step = 0.25;
			if (_shiftState) then
			{
				_step = 1;
			}
			else
			{
				if (_controlState) then
				{
					_step = 0.025;
				};
			};
			if(ExileClientConstructionVectorMode) then
            {
				_step = 22.5;
				if (_shiftState) then
				{
					_step = 45;
				}
				else
				{
					if (_controlState) then
					{
						_step = 1;
					};
				};
                ExileClientConstructionPitch = ExileClientConstructionPitch - _step;
            }
            else
            {
                ExileClientConstructionOffset set [2, ((((ExileClientConstructionOffset select 2) - _step) min 6) max -3) ];
    			[] call ExileClient_gui_constructionMode_update;
            };
			[] call ExileClient_gui_constructionMode_update;
			_stopPropagation = true;
		};
	};
	case 0xC7:
	{
		if (ExileClientIsInConstructionMode) then
		{
			_step = 0.25;
			if (_shiftState) then
			{
				_step = 1;
			}
			else
			{
				if (_controlState) then
				{
					_step = 0.025;
				};
			};
			ExileClientConstructionOffset set [1, (((ExileClientConstructionOffset select 1) + _step) min 6) max ExileClientConstructionBoundingRadius ];
			[] call ExileClient_gui_constructionMode_update;
			_stopPropagation = true;
		};
	};
	case 0xCF:
	{
		if (ExileClientIsInConstructionMode) then
		{
			_step = 0.25;
			if (_shiftState) then
			{
				_step = 1;
			}
			else
			{
				if (_controlState) then
				{
					_step = 0.025;
				};
			};
			ExileClientConstructionOffset set [1, (((ExileClientConstructionOffset select 1) - _step) min 6) max ExileClientConstructionBoundingRadius ];
			[] call ExileClient_gui_constructionMode_update;
			_stopPropagation = true;
		};
	};
	case 0xDB:
	{
		switch (ExilePartyEspMode) do
		{
			case 0: 	{ ExilePartyEspMode = 1; };
			case 1: 	{ ExilePartyEspMode = 2; };
			case 2: 	{ ExilePartyEspMode = 0; };
		};
		_stopPropagation = true;
	};
};
_stopPropagation