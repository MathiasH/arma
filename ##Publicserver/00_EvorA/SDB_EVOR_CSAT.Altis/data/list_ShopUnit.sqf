// ["Classname",price,removed from shop,["Weapon that unlocks this unit"]]

[
	[	// Mercenary
		["O_G_Soldier_lite_F",1,false,["arifle_TRG20_F"]],								// Rifleman light
		["O_G_Soldier_F",2,false,["arifle_TRG21_F"]],									// Rifleman
		["O_G_Soldier_A_F",2,true,["arifle_TRG20_F"]],									// Ammo bearer
		["O_G_Soldier_GL_F",3,true,["arifle_TRG21_GL_F"]],								// Grenadier
		["O_G_Soldier_AR_F",3,true,["LMG_Mk200_F"]],									// Autorifleman
		["O_G_Soldier_M_F",4,true,["arifle_Mk20_plain_F"]],								// Marksman
		["O_G_Sharpshooter_F",5,true,["srifle_DMR_06_olive_F","srifle_DMR_06_camo_F"]],	// Sharpshooter
		["O_G_Soldier_LAT2_F",5,true,["launch_MRAWS_olive_F","launch_MRAWS_olive_rail_F"]],	// Rifleman LAT
		["O_G_Soldier_LAT_F",6,true,["launch_RPG32_F"]],								// Rifleman AT
		["O_G_Soldier_exp_F",6,true,["arifle_Mk20C_plain_F"]],							// Explosive specialist
		["O_G_Soldier_TL_F",6,true,["arifle_Mk20_GL_plain_F"]]							// Teamleader
	],[	// CSAT Hex
		["O_crew_F",2,false,["arifle_Katiba_C_F"]],										// Crewman
		["O_helicrew_F",2,false,["arifle_Katiba_C_F"]],									// Heli crew
		["O_helipilot_F",2,true,["SMG_02_F"]],											// Heli pilot
		["O_Pilot_F",2,true,["SMG_02_F"]],												// Pilot
		["O_Soldier_lite_F",2,false,["arifle_Katiba_F"]],								// Rifleman light
		["O_Soldier_F",3,false,["arifle_Katiba_F"]],									// Rifleman
		["O_soldier_PG_F",3,true,["arifle_Katiba_F"]],									// Paratrooper
		["O_Soldier_A_F",3,true,["arifle_Katiba_F"]],									// Ammo bearer
		["O_Soldier_GL_F",4,true,["arifle_Katiba_GL_F"]],								// Grenadier
		["O_Soldier_AR_F",4,true,["LMG_Zafir_F"]],										// Autorifleman
		["O_soldier_M_F",5,true,["srifle_DMR_01_F"]],									// Marksman
		["O_Sharpshooter_F",6,true,["srifle_DMR_05_blk_F","srifle_DMR_05_tan_f","srifle_DMR_05_hex_F"]],	// Sharpshooter
		["O_HeavyGunner_F",6,true,["MMG_01_tan_F","MMG_01_hex_F"]],						// Heavy gunner
		["O_Soldier_LAT_F",7,true,["launch_RPG32_F"]],									// Rifleman AT
		["O_soldier_exp_F",7,true,["arifle_Katiba_C_F"]],								// Explosive specialist
		["O_Soldier_TL_F",7,true,["arifle_Katiba_GL_F"]],								// Teamleader
		["O_Soldier_HAT_F",9,true,["launch_O_Vorona_brown_F"]],							// Missile specialist HAT
		["O_Soldier_AT_F",9,true,["launch_O_Titan_short_F"]],							// Missile specialist AT
		["O_Soldier_AA_F",9,true,["launch_O_Titan_F"]]									// Missile specialist AA
	],[	// CSAT Urban
		["O_soldierU_F",3,true,["arifle_Katiba_F"]],									// Urban rifleman
		["O_soldierU_A_F",3,true,["arifle_Katiba_F"]],									// Urban ammmo bearer
		["O_SoldierU_GL_F",4,true,["arifle_Katiba_GL_F"]],								// Urban grenadier
		["O_soldierU_AR_F",4,true,["LMG_Zafir_F"]],										// Urban autorifleman
		["O_soldierU_M_F",5,true,["srifle_DMR_01_F"]],									// Urban marksman
		["O_Urban_Sharpshooter_F",6,true,["srifle_DMR_05_blk_F","srifle_DMR_05_tan_f","srifle_DMR_05_hex_F"]],	// Urban sharpshooter
		["O_Urban_HeavyGunner_F",6,true,["MMG_01_tan_F","MMG_01_hex_F"]],				// Urban heavy gunner
		["O_soldierU_LAT_F",7,true,["launch_RPG32_F"]],									// Urban rifleman AT
		["O_soldierU_exp_F",7,true,["arifle_Katiba_C_F"]],								// Urban explosive specialist
		["O_soldierU_TL_F",7,true,["arifle_Katiba_GL_F"]],								// Urban teamleader
		["O_soldierU_AT_F",9,true,["launch_O_Titan_short_F"]],							// Urban missile specialist AT
		["O_soldierU_AA_F",9,true,["launch_O_Titan_F"]]									// Urban missile specialist AA
	],[	// CSAT Recon
		["O_diver_F",4,true,["arifle_SDAR_F"]],											// Diver
		["O_diver_exp_F",5,true,["arifle_SDAR_F"]],										// Diver explosive specialist
		["O_diver_TL_F",5,true,["arifle_SDAR_F"]],										// Diver teamleader
		["O_recon_F",6,true,["arifle_Katiba_F"]],										// Recon scout
		["O_recon_M_F",7,true,["srifle_DMR_01_F"]],										// Recon marksman
		["O_Pathfinder_F",7,true,["srifle_DMR_04_F","srifle_DMR_04_Tan_F"]],			// Recon pathfinder
		["O_recon_LAT_F",9,true,["launch_RPG32_F"]],									// Recon scout AT
		["O_recon_JTAC_F",9,true,["arifle_Katiba_GL_F"]],								// Recon JTAC
		["O_recon_exp_F",9,true,["arifle_Katiba_F"]],									// Recon explosive specialist
		["O_soldier_UAV_F",10,true,["arifle_Katiba_F"]],								// UAV operator
		["O_recon_TL_F",10,true,["arifle_Katiba_F"]],									// Recon teamleader
		["O_spotter_F",10,true,["srifle_GM6_F","srifle_GM6_camo_F"]],					// Spotter
		["O_sniper_F",13,true,["srifle_GM6_F","srifle_GM6_camo_F"]],					// Sniper
		["O_ghillie_lsh_F",13,true,["srifle_GM6_F","srifle_GM6_camo_F"]],				// Sniper (Lush)
		["O_ghillie_sard_F",13,true,["srifle_GM6_F","srifle_GM6_camo_F"]],				// Sniper (Semi-arid)
		["O_ghillie_ard_F",13,true,["srifle_GM6_F","srifle_GM6_camo_F"]]				// Sniper (arid)
	],[	// CSAT Viper
		["O_V_Soldier_hex_F",11,true,["arifle_ARX_blk_F","arifle_ARX_hex_F"]],			// Viper operative
		["O_V_Soldier_M_hex_F",13,true,["arifle_ARX_blk_F","arifle_ARX_hex_F"]],		// Viper marksman
		["O_V_Soldier_LAT_hex_F",15,true,["arifle_ARX_blk_F","arifle_ARX_hex_F"]],		// Viper rifleman AT
		["O_V_Soldier_JTAC_hex_F",15,true,["arifle_ARX_blk_F","arifle_ARX_hex_F"]],		// Viper JTAC
		["O_V_Soldier_Exp_hex_F",15,true,["arifle_ARX_blk_F","arifle_ARX_hex_F"]],		// Viper explosive specialist
		["O_V_Soldier_TL_hex_F",15,true,["arifle_ARX_blk_F","arifle_ARX_hex_F"]]		// Viper teamleader
	]
]