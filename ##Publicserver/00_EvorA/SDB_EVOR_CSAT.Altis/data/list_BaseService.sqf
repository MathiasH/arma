// Defines array of service objects for vehicles types

[
	[	// Vehicle service
		base0_veh0,
		base0_veh1,
		base1_veh,
		base2_veh,
		base3_veh
	],[	// Boat service
		base0_boat,
		base1_boat,
		base2_boat,
		base3_boat
	],[	// Helicopter service
		base0_heli0,
		base0_heli1,
		base0_heli2,
		base1_supply,
		base2_supply,
		base3_supply
	],[	// Jet & VTOL service
		base0_heli0,
		base0_heli1,
		base0_heli2,
		base1_supply,
		base2_supply,
		base3_supply
	]
];