// ["Classname",price,removed from shop,[params],"Custom name"]

[
	[	// Supplies
		["Box_East_Support_F",3,false,[]],											// Supportbox
		["Box_East_Ammo_F",5,false,[]],												// Ammobox
		["Box_East_AmmoOrd_F",5,false,[]],											// Explosivesbox
		["Box_East_Wps_F",10,false,[]],												// Weaponbox
		["O_supplyCrate_F",20,false,[]],											// Cargonet
		["Land_Pod_Heli_Transport_04_fuel_F",10,false,[]],							// Fuel
		["Land_Pod_Heli_Transport_04_repair_F",10,false,[]],						// Repair
		["Land_Pod_Heli_Transport_04_ammo_F",10,false,[]],							// Ammo
		["Land_Pod_Heli_Transport_04_box_F",25,false,[],"Taru Arsenal Pod"]			// Arsenal
	],[	// Motorized
		["C_Truck_02_covered_F",50,false,["BlueBlue"],"Construction Truck"],		// MHQ Truck
		["C_Kart_01_F",2,false,[]],													// Kart
		["O_Quadbike_01_F",3,false,[]],												// Quad (CSAT)
		["C_Hatchback_01_F",5,false,[]],											// Hatchback
		["C_Hatchback_01_sport_F",7,false,[]],										// Hatchback sport
		["C_SUV_01_F",6,false,[]],													// SUV
		["C_Offroad_02_unarmed_F",8,false,[]],										// 4WD
		["I_C_Offroad_02_LMG_F",12,false,[]],										// 4WD (LMG)
		["I_C_Offroad_02_AT_F",15,false,[]],										// 4WD (AT)
		["O_G_Offroad_01_F",8,false,[]],											// Offroad
		["O_G_Offroad_01_armed_F",13,false,[]],										// Offroad (HMG)
		["O_G_Offroad_01_AT_F",15,false,[]],										// Offroad (AT)
		["O_G_Van_02_transport_F",10,false,[]],										// Van (Infantry)
		["O_G_Van_02_vehicle_F",10,false,[]],										// Van (Vehicle)
		["O_UGV_01_F",15,false,[]],													// Saif
		["O_UGV_01_rcws_F",25,false,[]],											// Saif (RCWS)
		["O_LSV_02_unarmed_F",12,false,[]],											// Qilin
		["O_LSV_02_armed_F",17,false,[]],											// Qilin (GAT)
		["O_LSV_02_AT_F",22,false,[]],												// Qilin (AT)
		["I_MRAP_03_F",18,false,[]],												// Strider
		["I_MRAP_03_hmg_F",23,false,[]],											// Strider (HMG)
		["I_MRAP_03_gmg_F",23,false,[]],											// Strider (GMG)
		["O_MRAP_02_F",18,false,[]],												// Ifrit
		["O_MRAP_02_hmg_F",23,false,[]],											// Ifrit (HMG)
		["O_MRAP_02_gmg_F",23,false,[]],											// Ifrit (GMG)
		["I_LT_01_scout_F",20,false,["Indep_03"]],																// Nyx (Recon)
		["I_LT_01_cannon_F",30,false,["Indep_03"]],																// Nyx (CAN)
		["I_LT_01_cannon_F",33,false,["Indep_03",["showSLATHull",1]],"%1 (SLAT)"],								// Nyx (CAN) (SLAT)
		["I_LT_01_AT_F",35,false,["Indep_03"]],																	// Nyx (AT)
		["I_LT_01_AT_F",38,false,["Indep_03",["showSLATHull",1]],"%1 (SLAT)"],									// Nyx (AT) (SLAT)
		["I_LT_01_AA_F",35,false,["Indep_03"]],																	// Nyx (AA)
		["O_APC_Wheeled_02_rcws_v2_F",35,false,[]],																// Marid
		["O_APC_Wheeled_02_rcws_v2_F",38,false,[false,["showSLATHull",1]],"%1 (SLAT)"],							// Marid (SLAT)
		["I_APC_Wheeled_03_cannon_F",40,false,["Indep_03"]],													// Gorgon
		["I_APC_Wheeled_03_cannon_F",43,false,["Indep_03",["showSLATHull",1]],"%1 (SLAT)"],						// Gorgon (SLAT)
		["I_APC_tracked_03_cannon_F",40,false,["Indep_03"]],													// Mora
		["I_APC_tracked_03_cannon_F",43,false,["Indep_03",["showSLATHull",1,"showSLATTurret",1]],"%1 (SLAT)"],	// Mora (SLAT)
		["O_APC_Tracked_02_cannon_F",45,false,[]],																// Kamysh
		["O_APC_Tracked_02_cannon_F",48,false,[false,["showSLATHull",1]],"%1 (SLAT)"],							// Kamysh (SLAT)
		["O_APC_Tracked_02_AA_F",55,false,[]],										// Tigris
		["O_MBT_02_cannon_F",60,false,[]],											// T100
		["I_MBT_03_cannon_F",65,false,["Indep_03"]],								// Kuma
		["O_MBT_04_cannon_F",70,false,[]],											// Angara
		["O_MBT_04_command_F",75,false,[]],											// Angara-K
		["O_MBT_02_arty_F",80,false,[]],											// Sochor
		["I_Truck_02_MRL_F",80,false,["Opfor"]],									// Zamak MRL
		["O_Truck_03_transport_F",15,false,[]],										// Flatbed
		["O_Truck_03_covered_F",15,false,[]],										// Covered
		["O_Truck_03_medical_F",15,false,[]],										// Medical
		["O_Truck_03_fuel_F",20,false,[]],											// Fuel
		["O_Truck_03_repair_F",20,false,[]],										// Repair
		["O_Truck_03_ammo_F",20,false,[]]											// Ammo
	],[	// Naval
		["C_Scooter_Transport_01_F",5,false,[]],									// Water scooter
		["O_Boat_Transport_01_F",8,false,[]],										// Dinghy
		["C_Boat_Civil_01_F",10,false,[]],											// Speedboat
		["I_C_Boat_Transport_02_F",12,false,[]],									// RHIB
		["I_Boat_Armed_01_minigun_F",20,false,["Opfor"]],							// Gunboat (GAT)
		["O_Boat_Armed_01_hmg_F",20,false,[]],										// Gunboat (HMG)
		["O_SDV_01_F",12,false,[]]													// Submarine
	],[	// Rotary wing & VTOL
		["C_Heli_Light_01_civil_F",10,false,[]],									// M900
		["O_Heli_Light_02_unarmed_F",15,false,["Opfor"]],							// Orca
		["O_Heli_Light_02_dynamicLoadout_F",40,false,[]],							// Orca (Armed)
		["I_Heli_light_03_unarmed_F",15,false,["Indep"]],							// Hellcat
		["I_Heli_light_03_dynamicLoadout_F",55,false,[]],							// Hellcat (Armed)
		["O_Heli_Transport_04_F",25,false,[]],										// Taru empty
		["O_Heli_Transport_04_covered_F",25,false,[]],								// Taru transport
		["I_Heli_Transport_02_F",30,false,[]],										// Mohawk
		["B_T_UAV_03_dynamicLoadout_F",120,false,[]],								// Falcon
		["O_Heli_Attack_02_dynamicLoadout_F",150,false,[]]							// Kajman
	],[	// Fixed wing
		["C_Plane_Civil_01_F",15,false,[]],											// Caesar
		["C_Plane_Civil_01_racing_F",20,false,[]],									// Caesar (Racing)
		["O_T_UAV_04_CAS_F",75,false,[]],											// Fenghuang
		["O_UAV_02_dynamicLoadout_F",100,false,[]],									// Abadil
		["B_UAV_05_F",125,false,[false,"wing_fold_l"]],								// Sentinel
		["I_Plane_Fighter_03_dynamicLoadout_F",125,false,["Hex"]],					// Buzzard
		["I_Plane_Fighter_04_F",150,false,[]],										// Gryphon
		["O_Plane_CAS_02_dynamicLoadout_F",150,false,[]],							// Neophron
		["O_T_VTOL_02_infantry_dynamicLoadout_F",175,false,["Hex"]],				// Xi'an (Infantry)
		["O_T_VTOL_02_vehicle_dynamicLoadout_F",175,false,["Grey"]],				// Xi'an (Vehicle)
		["O_Plane_Fighter_02_Stealth_F",200,false,["CamoBlue"]],					// Shikra (Stealth)
		["O_Plane_Fighter_02_F",200,false,[]]										// Shikra
	]
];