/*
	Defines list of bases
	Must be adjusted for each map

	Example: ["baseMarker",baseRadius,teleportObject,[supplySpawn,vehSpawn,boatSpawn,heliSpawn,jetSpawn],attackFlags];
		- xSpawn must be a non-null object or an array containing non-null objects to enable shop.
		- attackFlags handles types of attack on this base (can only happen on first base in list)
			* 0 - No base attacks happen
			* 1 - Mortar attacks (only set this when there is land closer than 2500 m)
			* 2 - Infantry attacks (only set this when base is connected to mainland)
			* 4 - Motorized attacks (only set this when base is connected to mainland)
			* 8 - Naval attacks (only set this when base has a shore!)
*/

[	
	["base0",150,base0_teleport,[base0_supply,[base0_veh0,base0_veh1],objNull,[base0_heli0,base0_heli1],[base0_jet0,base0_jet1]],2+4],
	["base1",100,base1_teleport,[base1_supply,base1_veh,objNull,base1_heli,objNull],0],
	["base2",100,base2_teleport,[base2_supply,base2_veh,[base2_boat0,base2_boat1],base2_heli,objNull],0],
	["base3",150,base3_teleport,[base3_supply,base3_veh,objNull,base3_heli0,objNull],0],
	["base4",100,base4_teleport,[base4_supply,base4_veh,objNull,objNull,objNull],0]
];