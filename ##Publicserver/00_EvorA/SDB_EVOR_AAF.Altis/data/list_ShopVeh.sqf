// ["Classname",price,removed from shop,[params],"Custom name"]

[
	[	// Supplies
		["Box_IND_Support_F",3,false,[]],											// Supportbox
		["Box_IND_Ammo_F",5,false,[]],												// Ammobox
		["Box_IND_AmmoOrd_F",5,false,[]],											// Explosivesbox
		["Box_IND_Wps_F",10,false,[]],												// Weaponbox
		["I_supplyCrate_F",10,false,[]],											// Cargonet
		["B_Slingload_01_Fuel_F",10,false,[]],										// Fuel
		["B_Slingload_01_Repair_F",10,false,[]],									// Repair
		["B_Slingload_01_Ammo_F",10,false,[]],										// Ammo
		["B_Slingload_01_Cargo_F",15,false,[],"Huron Arsenal Container"]			// Cargo
	],[	// Motorized
		["C_Truck_02_covered_F",50,false,["BlueBlue"],"Construction Truck"],		// MHQ Truck
		["C_Kart_01_F",2,false,[]],													// Kart
		["I_Quadbike_01_F",3,false,[]],												// Quad (AAF)
		["C_Hatchback_01_F",5,false,[]],											// Hatchback
		["C_Hatchback_01_sport_F",7,false,[]],										// Hatchback sport
		["C_SUV_01_F",6,false,[]],													// SUV
		["I_C_Offroad_02_unarmed_F",8,false,[]],									// 4WD
		["I_C_Offroad_02_LMG_F",12,false,[]],										// 4WD (LMG)
		["I_C_Offroad_02_AT_F",15,false,[]],										// 4WD (AT)
		["I_G_Offroad_01_F",8,false,[]],											// Offroad
		["I_G_Offroad_01_armed_F",13,false,[]],										// Offroad (HMG)
		["I_G_Offroad_01_AT_F",15,false,[]],										// Offroad (AT)
		["I_G_Van_02_transport_F",10,false,[]],										// Van (Infantry)
		["I_G_Van_02_vehicle_F",10,false,[]],										// Van (Vehicle)
		["I_UGV_01_F",15,false,[]],													// Stomper
		["I_UGV_01_rcws_F",25,false,[]],											// Stomper (RCWS)
		["O_T_LSV_02_unarmed_F",12,false,["Black"]],								// Qilin
		["O_T_LSV_02_armed_F",17,false,["Black"]],									// Qilin (GAT)
		["O_LSV_02_AT_F",22,false,["Black"]],										// Qilin (AT)
		["I_MRAP_03_F",18,false,[]],												// Strider
		["I_MRAP_03_hmg_F",23,false,[]],											// Strider (HMG)
		["I_MRAP_03_gmg_F",23,false,[]],											// Strider (GMG)
		["I_LT_01_scout_F",20,false,[]],																		// Nyx (Recon)
		["I_LT_01_cannon_F",30,false,[]],																		// Nyx (CAN)
		["I_LT_01_cannon_F",33,false,[false,["showSLATHull",1]],"%1 (SLAT)"],									// Nyx (CAN) (SLAT)
		["I_LT_01_AT_F",35,false,[]],																			// Nyx (AT)
		["I_LT_01_AT_F",38,false,[false,["showSLATHull",1]],"%1 (SLAT)"],										// Nyx (AT) (SLAT)
		["I_LT_01_AA_F",35,false,[]],																			// Nyx (AA)
		["I_APC_Wheeled_03_cannon_F",40,false,[]],																// Gorgon
		["I_APC_Wheeled_03_cannon_F",43,false,[false,["showSLATHull",1]],"%1 (SLAT)"],							// Gorgon (SLAT)
		["I_APC_tracked_03_cannon_F",40,false,[]],																// Mora
		["I_APC_tracked_03_cannon_F",43,false,[false,["showSLATHull",1,"showSLATTurret",1]],"%1 (SLAT)"],		// Mora (SLAT)
		["I_MBT_03_cannon_F",65,false,[]],											// Kuma
		["O_MBT_04_cannon_F",70,false,["Grey"]],									// Angara
		["O_MBT_04_command_F",75,false,["Grey"]],									// Angara-K
		["I_Truck_02_MRL_F",80,false,[]],											// Zamak MRL
		["I_Truck_02_transport_F",15,false,[]],										// Flatbed
		["I_Truck_02_covered_F",15,false,[]],										// Covered
		["I_Truck_02_medical_F",15,false,[]],										// Medical
		["I_Truck_02_fuel_F",20,false,[]],											// Fuel
		["I_Truck_02_box_F",20,false,[]],											// Repair
		["I_Truck_02_ammo_F",20,false,[]]											// Ammo
	],[	// Naval
		["C_Scooter_Transport_01_F",5,false,[]],									// Water scooter
		["I_Boat_Transport_01_F",8,false,[]],										// Dinghy
		["C_Boat_Civil_01_F",10,false,[]],											// Speedboat
		["I_C_Boat_Transport_02_F",12,false,[]],									// RHIB
		["I_Boat_Armed_01_minigun_F",20,false,[]],									// Gunboat (GAT)
		["I_SDV_01_F",12,false,[]]													// Submarine
	],[	// Rotary wing & VTOL
		["C_Heli_Light_01_civil_F",10,false,[]],									// M900
		["I_Heli_light_03_unarmed_F",15,false,[]],									// Hellcat
		["I_Heli_light_03_dynamicLoadout_F",55,false,[]],							// Hellcat (Armed)
		["I_Heli_Transport_02_F",30,false,[]],										// Mohawk
		["O_Heli_Attack_02_dynamicLoadout_F",150,false,["Black"]]					// Kajman
	],[	// Fixed wing
		["C_Plane_Civil_01_F",15,false,[]],											// Caesar BTT
		["C_Plane_Civil_01_racing_F",20,false,[]],									// Caesar BTT (Racing)
		["I_UAV_02_dynamicLoadout_F",100,false,[]],									// Abadil
		["I_Plane_Fighter_03_dynamicLoadout_F",125,false,[]],						// Buzzard
		["I_Plane_Fighter_04_F",150,false,["CamoGrey"]]								// Gryphon
	]
];