// Defines array of service objects for vehicles types

[
	[	// Vehicle service
		base0_veh0,
		base0_veh1,
		base1_veh,
		base2_veh,
		base3_veh,
		base4_veh
	],[	// Boat service
		base2_boat0,
		base2_boat1
	],[	// Helicopter service
		base0_heli0,
		base0_heli1,
		base1_heli,
		base2_heli,
		base3_heli0,
		base3_heli1
	],[	// Jet & VTOL service
		base0_heli0,
		base0_heli1,
		base1_heli,
		base2_heli,
		base3_heli0,
		base3_heli1,
		base0_jet0,
		base0_jet1,
		base0_service
	]
];