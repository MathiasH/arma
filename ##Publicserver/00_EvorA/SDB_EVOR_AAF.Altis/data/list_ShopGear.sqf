// ["Classname",price,removed from shop,[params],"Custom name"]

[
	[	// Weapons
		// Pistols
		["hgun_Rook40_F",2,false],						// Rook-40
		["hgun_ACPC2_F",4,true],						// ACP-C2
		["hgun_Pistol_Signal_F",10,false],				// Starter Pistol
		// SMGs
		["hgun_PDW2000_F",7,true],						// PDW2000
		["SMG_05_F",9,false],							// Protector
		["SMG_02_F",10,false],							// Sting
		// Rifles
		["arifle_Mk20C_F",12,true],						// Mk20 C (Camo)
		["arifle_Mk20_F",15,true],						// Mk20 (Camo)
		["arifle_Mk20_GL_F",18,false],					// Mk20 GL (Camo)
		["arifle_TRG20_F",15,true],						// TRG C
		["arifle_TRG21_F",18,true],						// TRG
		["arifle_TRG21_GL_F",21,false],					// TRG GL
		["arifle_AK12_F",21,false],						// AK-12
		["arifle_AK12_GL_F",24,false],					// AK-12 GL
		["arifle_Katiba_C_F",18,false],					// Katiba C
		["arifle_Katiba_F",21,false],					// Katiba
		["arifle_Katiba_GL_F",24,false],				// Katiba GL
		["arifle_SDAR_F",20,false],						// SDAR
		// Snipers
		["srifle_DMR_06_olive_F",23,true],				// Mk14 (Olive)
		["srifle_EBR_F",30,false],						// Mk18
		["srifle_DMR_03_F",35,false],					// Mk-1 EMR (Black)
		["srifle_DMR_03_khaki_F",35,false],				// Mk-1 EMR (Khaki)
		["srifle_DMR_04_F",35,false],					// ASP-1 Kir (Black)
		["srifle_DMR_05_blk_F",40,false],				// Cyrus (Black)
		["srifle_GM6_F",50,false],						// Lynx (Black)
		// Machineguns
		["LMG_03_F",28,false],							// LIM-85
		["LMG_Mk200_F",30,false],						// Mk200
		["LMG_Zafir_F",34,false],						// Zafir
		["MMG_01_tan_F",42,false],						// Navid (Tan)
		// launchers
		["launch_RPG7_F",10,true],						// RPG7
		["launch_MRAWS_olive_rail_F",20,false],			// MAAWS (Olive)
		["launch_MRAWS_olive_F",23,false],				// MAAWS NVS (Olive)
		["launch_NLAW_F",27,false],						// PCML
		["launch_O_Vorona_green_F",30,false],			// Vorona AT (Green)
		["launch_I_Titan_short_F",40,false],			// Titan AT (Olive)
		["launch_I_Titan_F",40,false]					// Titan AA (Digi)
	],[	// Items
		// Misc
		["ItemMap",1,true],								// Map
		["ItemWatch",1,true],							// Watch
		["ItemCompass",1,true],							// Compass
		["ItemRadio",1,true],							// Radio
		["ItemGPS",5,true],								// GPS
		["I_UavTerminal",30,false],						// UAV terminal
		// Support
		["FirstAidKit",1,true],							// First Aid Kit
		["ToolKit",5,true],								// Toolkit
		["MineDetector",5,false],						// Minedetector
		// Vision
		["Binocular",2,true],							// Binocular
		["Rangefinder",5,false],						// Rangefinder
		["Laserdesignator_03",10,false],				// Laser designator (Olive)
		["NVGoggles_OPFOR",10,false],					// NVG (Black)
		["NVGoggles_INDEP",10,false],					// NVG (Green)
		["NVGogglesB_blk_F",30,false],					// ENVG (Black)
		["NVGogglesB_grn_F",30,false],					// ENVG (Green)
		// Barrel
		["acc_flashlight_pistol",1,true],				// Pistol Flashlight
		["acc_flashlight",1,true],						// Flashlight
		["acc_pointer_IR",2,true],						// Laser Pointer
		// Optics
		["optic_Aco_smg",5,false],						// ACO SMG (Red)
		["optic_ACO_grn_smg",5,false],					// ACO SMG (Green)
		["optic_Aco",7,false],							// ACO (Red)
		["optic_ACO_grn",7,true],						// ACO (Green)
		["optic_MRCO",10,true],							// MRCO
		["optic_Arco",15,false],						// ARCO (Tan)
		["optic_Arco_blk_F",15,false],					// ARCO (Black)
		["optic_Hamr",15,false],						// RCO
		["optic_ERCO_blk_F",15,false],					// ERCO (Black)
		["optic_DMS",20,false],							// DMS	
		["optic_SOS",20,false],							// MOS
		["optic_LRPS",25,false],						// LRPS (Black)
		["optic_NVS",25,false],							// NVS
		["optic_KHS_old",30,false],						// Kahlia (Old)
		["optic_KHS_blk",30,false],						// Kahlia (Black)
		["optic_AMS",33,false],							// AMS (Black)
		["optic_tws",33,false],							// TWS
		["optic_tws_mg",33,false],						// TWS MG
		["optic_Nightstalker",40,false],				// Night-stalker
		// Suppressors
		["muzzle_snds_L",2,true],						// 9 mm
		["muzzle_snds_acp",3,false],					// .45
		["muzzle_snds_M",3,false],						// 5.56 mm (Black)
		["muzzle_snds_H",5,false],						// 6.5 mm (Black)
		["muzzle_snds_B",7,false],						// 7.62 mm (Black)
		["muzzle_snds_93mmg",10,false],					// 9.3 mm (Black)
		// Bipods
		["bipod_03_F_oli",2,false],						// AAF (oli)
		["bipod_03_F_blk",2,false]						// AAF (Black)
	],[	// Clothes
		// Uniforms
		["U_BG_Guerilla1_1",5,true],					// Guer (Garment)
		["U_BG_Guerilla1_2_F",5,true],					// Guer (Garment Olive)
		["U_BG_Guerilla2_2",5,true],					// Guer (Pattern)
		["U_BG_Guerilla2_3",5,true],					// Guer (Plain, Light)
		["U_BG_Guerilla2_1",5,true],					// Guer (Plain, Dark)
		["U_BG_Guerilla3_1",6,true],					// Guer (Smocks)
		["U_BG_leader",6,true],							// Guer (Uniform)
		["U_BG_Guerrilla_6_1",7,true],					// Guer (Apparel)
		["U_Tank_green_F",8,false],						// AAF Tanker
		["U_I_CombatUniform_shortsleeve",8,true],		// AAF (Rolled)
		["U_I_CombatUniform",8,true],					// AAF 
		["U_I_OfficerUniform",9,false],					// AAF (Officer)
		["U_I_Wetsuit",15,false],						// AAF Wetsuit
		["U_I_HeliPilotCoveralls",15,false],			// AAF Heli pilot
		["U_I_pilotCoveralls",15,false],				// AAF Pilot
		["U_I_GhillieSuit",15,false],					// AAF Ghillie
		["U_I_FullGhillie_sard",20,false],				// AAF Ghillie (Lush)
		// Vests
		["V_Pocketed_black_F",4,true],					// Pockets (Black)
		["V_Pocketed_olive_F",4,true],					// Pockets (Olive)
		["V_BandollierB_oli",5,true],					// Bandolier (Olive)
		["V_BandollierB_blk",5,true],					// Bandolier (Black)
		["V_TacChestrig_oli_F",6,true],					// Tactical chest rig (Olive)
		["V_TacChestrig_grn_F",6,true],					// Tactical chest rig (Green)
		["V_Chestrig_oli",7,false],						// Chest Rig (Olive)
		["V_Chestrig_rgr",7,false],						// Chest Rig (Green)
		["V_Chestrig_blk",7,false],						// Chest Rig (Black)
		["V_TacVest_oli",10,true],						// Tactical (Olive)
		["V_TacVest_camo",10,false],					// Tactical (Camo)
		["V_TacVest_blk",10,false],						// Tactical (Black)
		["V_TacVestIR_blk",15,false],					// Raven
		["V_RebreatherIA",15,false],					// AAF Rebreather
		["V_EOD_olive_F",23,false],						// EOD (Olive)
		["V_PlateCarrierIA1_dgtl",21,true],				// AAF Light
		["V_PlateCarrierIA2_dgtl",24,false],			// AAF Rig
		["V_PlateCarrierIAGL_dgtl",26,false],			// AAF GL (Digi)
		// Headgear
		["H_HeadBandage_clean_F",1,true],				// Bandage (Clean)
		["H_HeadBandage_stained_F",1,true],				// Bandage (Stained)
		["H_HeadBandage_bloody_F",1,true],				// Bandage (Bloody)
		["H_Cap_blk_Raven",1,true],						// Cap (AAF)
		["H_Cap_oli",1,true],							// Cap (Olive)
		["H_Cap_oli_hs",3,false],						// Cap (Olive, Headset)
		["H_Cap_headphones",3,false],					// Cap (Rangemaster)
		["H_Bandanna_sgg",2,true],						// Bandanna (Sage)
		["H_Bandanna_camo",2,true],						// Bandanna (Woodland)
		["H_Bandanna_gry",2,true],						// Bandanna (Black)
		["H_Shemag_olive",5,true],						// Shemag (Olive)
		["H_Shemag_olive_hs",5,false],					// Shemag (Olive, Headset)
		["H_Booniehat_oli",6,true],						// Booniehat (Olive)
		["H_Booniehat_dgtl",6,false],					// Booniehat (Digi)
		["H_Watchcap_camo",7,false],					// Beanie (Green)
		["H_Watchcap_khk",7,false],						// Beanie (Khaki)
		["H_Watchcap_blk",7,false],						// Beanie (Black)
		["H_MilCap_dgtl",10,true],						// Military Cap (Digi)
		["H_PASGT_basic_olive_F",12,false],				// Basic (Olive)
		["H_PASGT_basic_black_F",12,false],				// Basic (Black)
		["H_HelmetIA",12,false],						// Modular helmet
		["H_HelmetCrew_I",15,false],					// AAF Crew
		["H_CrewHelmetHeli_I",15,false],				// AAF Heli Crew
		["H_PilotHelmetHeli_I",15,false],				// AAF Heli Pilot
		["H_PilotHelmetFighter_I",17,false]				// AAF Jet Pilot
	],[	// Goggles
		["G_Shades_Black",2,true],						// Shades (Black)
		["G_Sport_Blackred",2,true],					// Sport Shades (Black Red)
		["G_Bandanna_oli",2,true],						// Bandanna (oli)
		["G_Bandanna_blk",2,true],						// Bandanna (Black)
		["G_Bandanna_shades",3,false],					// Bandanna (Shades)
		["G_Bandanna_sport",3,false],					// Bandanna (Sport)
		["G_Bandanna_aviator",4,false],					// Bandanna (Aviator)
		["G_Bandanna_beast",4,false],					// Bandanna (Beast)
		["G_Aviator",3,false],							// Aviator
		["G_Lowprofile",4,false],						// Low Profile Goggles
		["G_Combat",5,false],							// Combat Goggles
		["G_Balaclava_blk",5,false],					// Balaclava (Black)
		["G_Balaclava_oli",5,false],					// Balaclava (Olive)
		["G_Balaclava_lowprofile",6,false],				// Balaclava (Low Profile)
		["G_Balaclava_combat",6,false],					// Balaclava (Combat)
		["G_I_Diving",6,false],							// AAF Diving Goggles
		["G_Balaclava_TI_blk_F",8,false],				// Stealth balaclava (Black)
		["G_Balaclava_TI_G_blk_F",10,false]				// Stealth balaclava (Black, Goggles)
	],[	// Backpacks
		// Cargo backpacks
		["B_Parachute",5,true],							// Parachute
		["B_LegStrapBag_black_F",1,true],				// Leg strap (Black)
		["B_LegStrapBag_olive_F",1,true],				// Leg strap (Olive)
		["B_AssaultPack_khk",3,true],					// Assault (Khaki)
		["B_AssaultPack_dgtl",3,true],					// Assault (Digi)
		["B_AssaultPack_rgr",3,true],					// Assault (Green)
		["B_AssaultPack_blk",3,false],					// Assault (Black)
		["B_FieldPack_khk",5,false],					// Field (Khaki)
		["B_FieldPack_oli",5,true],						// Field (Olive)
		["B_TacticalPack_oli",7,false],					// Tactical (Olive)
		["B_TacticalPack_rgr",7,false],					// Tactical (Green)
		["B_TacticalPack_blk",7,false],					// Tactical (Black)
		["B_Kitbag_rgr",10,false],						// Kit (Green)
		["B_Kitbag_cbr",10,false],						// Kit (Coyote)
		["B_Carryall_oli",15,false],					// Carryall (Olive)
		// Turret backpacks
		["I_UAV_06_backpack_F",10,false],				// AAF UAV Utility
		["I_UAV_06_medical_backpack_F",15,false],		// AAF UAV Medical
		["I_UAV_01_backpack_F",15,false],				// AAF UAV
		["C_IDAP_UAV_06_antimine_backpack_F",20,false],	// IDAP UAV Demining
		["I_HMG_01_support_F",5,false],					// AAF Tripod
		["I_HMG_01_support_high_F",5,false],			// AAF Tripod Raised
		["I_HMG_01_weapon_F",20,false],					// AAF MG
		["I_HMG_01_high_weapon_F",20,false],			// AAF MG Raised
		["I_HMG_01_A_weapon_F",35,false],				// AAF MG Auto
		["I_GMG_01_weapon_F",20,false],					// AAF GMG
		["I_GMG_01_high_weapon_F",20,false],			// AAF GMG Raised
		["I_GMG_01_A_weapon_F",35,false],				// AAF GMG Auto
		["I_Mortar_01_support_F",10,false],				// AAF Mortar Base
		["I_Mortar_01_weapon_F",20,false],				// AAF Mortar Tube
		["I_AT_01_weapon_F",30,false],					// AAF AT
		["I_AA_01_weapon_F",30,false]					// AAF AA
	]
];