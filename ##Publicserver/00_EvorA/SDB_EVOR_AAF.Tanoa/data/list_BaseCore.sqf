/*
	Defines list of bases
	Must be adjusted for each map

	Example: ["baseMarker",baseRadius,teleportObject,[supplySpawn,vehSpawn,boatSpawn,heliSpawn,jetSpawn],attackFlags];
		- xSpawn must be a non-null object or an array containing non-null objects to enable shop.
		- attackFlags handles types of attack on this base (can only happen on first base in list)
			* 0 - No base attacks happen
			* 1 - Mortar attacks (only set this when there is land closer than 2500 m)
			* 2 - Infantry attacks (only set this when base is connected to mainland)
			* 4 - Motorized attacks (only set this when base is connected to mainland)
			* 8 - Naval attacks (only set this when base has a shore!)
*/

[	
	["base0",150,base0_teleport,[base0_supply,base0_veh,objNull,base0_heli0,objNull],1],
	["base1",100,base1_teleport,[base1_supply,base1_veh,[base1_boat0,base1_boat1],base1_heli,objNull],0],
	["base2",100,base2_teleport,[base2_supply,base2_veh,objNull,base2_heli,objNull],0],
	["base3",100,base3_teleport,[base3_supply,base3_veh,objNull,objNull,objNull],0]
];