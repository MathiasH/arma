// Defines array of service objects for vehicles types

[
	[	// Vehicle service
		base0_veh,
		base1_veh,
		base2_veh,
		base3_veh
	],[	// Boat service
		base1_boat0,
		base1_boat1
	],[	// Helicopter service
		base0_heli0,
		base0_heli1,
		base1_heli,
		base2_heli
	],[	// Jet & VTOL service
		base0_heli0,
		base0_heli1,
		base1_heli,
		base2_heli
	]
];