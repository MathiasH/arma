// ["Classname",price,removed from shop,["Weapon that unlocks this unit"]]

[
	[	// Bandits
		["I_C_Pilot_F",1,true,["arifle_AKS_F"]],											// Pilot
		["I_C_Soldier_Bandit_5_F",2,true,["arifle_AKM_F"]],									// Scout
		["I_C_Soldier_Bandit_6_F",3,true,["arifle_AK12_GL_F"]],								// Grenadier
		["I_C_Soldier_Bandit_3_F",3,true,["LMG_03_F"]],										// Autorifleman
		["I_C_Soldier_Bandit_2_F",3,true,["launch_RPG7_F"]],								// Rifleman AT
		["I_C_Soldier_Bandit_8_F",3,true,["arifle_AKS_F"]]									// Explosive specialist
	],[	// AAF Regulars
		["I_crew_F",2,true,["arifle_Mk20C_F"]],												// Crewman
		["I_helicrew_F",2,true,["arifle_Mk20C_F"]],											// Heli crew
		["I_helipilot_F",2,true,["hgun_PDW2000_F"]],										// Heli pilot
		["I_pilot_F",2,true,["hgun_PDW2000_F"]],											// Pilot
		["I_soldier_F",3,true,["arifle_Mk20_F"]],											// Rifleman
		["I_Soldier_A_F",3,true,["arifle_Mk20_F"]],											// Ammo bearer
		["I_Soldier_GL_F",4,true,["arifle_Mk20_GL_F"]],										// Grenadier
		["I_Soldier_AR_F",4,true,["LMG_Mk200_F"]],											// Autorifleman
		["I_Soldier_M_F",5,true,["srifle_EBR_F"]],											// Marksman
		["I_Soldier_LAT2_F",6,true,["launch_MRAWS_olive_F","launch_MRAWS_olive_rail_F"]],	// Rifleman LAT
		["I_Soldier_LAT_F",7,true,["launch_NLAW_F"]],										// Rifleman AT
		["I_Soldier_exp_F",7,true,["arifle_Mk20_F"]],										// Explosive specialist
		["I_Soldier_TL_F",7,true,["arifle_Mk20_GL_F"]],										// Teamleader
		["I_Soldier_AT_F",9,true,["launch_I_Titan_short_F"]],								// Missile specialist AT
		["I_Soldier_AA_F",9,true,["launch_I_Titan_F"]]										// Missile specialist AA
	],[	// FIA
		["I_G_Soldier_lite_F",1,false,["arifle_TRG20_F"]],									// Rifleman light
		["I_G_Soldier_F",2,false,["arifle_TRG21_F"]],										// Rifleman
		["I_G_Soldier_A_F",2,true,["arifle_TRG20_F"]],										// Ammo bearer
		["I_G_Soldier_GL_F",3,true,["arifle_TRG21_GL_F"]],									// Grenadier
		["I_G_Soldier_AR_F",3,true,["LMG_Mk200_F"]],										// Autorifleman
		["I_G_Soldier_M_F",4,true,["arifle_Mk20_F"]],										// Marksman
		["I_G_Sharpshooter_F",5,true,["srifle_DMR_06_olive_F"]],							// Sharpshooter
		["I_G_Soldier_LAT2_F",5,true,["launch_MRAWS_olive_F","launch_MRAWS_olive_rail_F"]], // Rifleman LAT
		["I_G_Soldier_exp_F",6,true,["arifle_Mk20C_F"]],									// Explosive specialist
		["I_G_Soldier_TL_F",6,true,["arifle_Mk20_GL_F"]]									// Teamleader
	],[	// AAF Recon
		["I_diver_F",4,true,["arifle_SDAR_F"]],												// Diver
		["I_diver_exp_F",5,true,["arifle_SDAR_F"]],											// Diver explosive specialist
		["I_diver_TL_F",5,true,["arifle_SDAR_F"]],											// Diver teamleader
		["I_Spotter_F",10,true,["srifle_GM6_F"]],											// Spotter
		["I_Sniper_F",13,true,["srifle_GM6_F"]],											// Sniper
		["I_ghillie_lsh_F",13,true,["srifle_GM6_F"]]										// Sniper (Lush)
	],[	// Syndikat Paramilitary
		["I_C_Helipilot_F",2,true,["arifle_AKS_F"]],										// Syndikat Rifleman AKSU
		["I_C_Soldier_Para_7_F",2,true,["arifle_AKM_F"]],									// Syndikat Rifleman Light AKM
		["I_C_Soldier_Para_1_F",3,true,["arifle_AK12_F"]],									// Syndikat Rifleman Light AK12
		["I_C_Soldier_Para_2_F",3,true,["arifle_AKM_F"]],									// Syndikat Rifleman Heavy AK12
		["I_C_Soldier_Para_6_F",4,true,["arifle_AK12_GL_F"]],								// Syndikat grenadier
		["I_C_Soldier_Para_4_F",4,true,["LMG_03_F"]],										// Syndikat autorifleman
		["I_C_Soldier_Para_5_F",4,true,["launch_RPG7_F"]],									// Syndikat rifleman AT
		["I_C_Soldier_Para_8_F",4,true,["arifle_AKM_F"]]									// Syndikat explosive specialist
	]
];