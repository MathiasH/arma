// ["Classname",price,removed from shop,[params],"Custom name"]

[
	[	// Weapons
		// Pistols
		["hgun_Pistol_01_F",1,true],					// PM
		["hgun_Rook40_F",2,false],						// Rook-40
		["hgun_P07_khk_F",2,false],						// P07
		["hgun_ACPC2_F",4,false],						// ACP-C2
		["hgun_Pistol_heavy_02_F",6,false],				// Zubr
		["hgun_Pistol_heavy_01_F",8,false],				// 4-five
		["hgun_Pistol_Signal_F",10,false],				// Starter Pistol
		// SMGs
		["hgun_PDW2000_F",7,false],						// PDW2000
		["SMG_05_F",9,false],							// Protector
		["SMG_02_F",10,false],							// Sting
		["SMG_01_F",13,false],							// Vermin
		// Rifles
		["arifle_AKS_F",5,true],						// AKS
		["arifle_AKM_F",10,true],						// AKM
		["arifle_Mk20C_F",12,false],					// Mk20 C (Camo)
		["arifle_Mk20_F",15,false],						// Mk20 (Camo)
		["arifle_Mk20_GL_F",18,false],					// Mk20 GL (Camo)
		["arifle_TRG20_F",15,false],					// TRG C
		["arifle_TRG21_F",18,false],					// TRG
		["arifle_TRG21_GL_F",21,false],					// TRG GL
		["arifle_AK12_F",21,false],						// AK-12
		["arifle_AK12_GL_F",24,false],					// AK-12 GL
		["arifle_CTAR_ghex_F",21,false],				// Car-95 (gHex)
		["arifle_CTAR_GL_ghex_F",24,false],				// Car-95 GL (gHex)
		["arifle_SPAR_01_blk_F",21,false],				// SPAR-16 (Black)
		["arifle_SPAR_01_GL_blk_F",24,false],			// SPAR-16 GL (Black)
		["arifle_Katiba_C_F",18,false],					// Katiba C
		["arifle_Katiba_F",21,false],					// Katiba
		["arifle_Katiba_GL_F",24,false],				// Katiba GL
		["arifle_MXC_Black_F",21,false],				// MX C (Black)
		["arifle_MX_Black_F",24,false],					// MX (Black)
		["arifle_MX_GL_Black_F",30,false],				// MX GL (Black)
		["arifle_ARX_ghex_F",40,false],					// Type 115 (gHex)
		["arifle_SDAR_F",20,false],						// SDAR
		// Snipers
		["srifle_DMR_01_F",20,false],					// Rahim
		["srifle_DMR_06_olive_F",23,true],				// Mk14 (Olive)
		["srifle_DMR_07_ghex_F",25,false],				// CMR (gHex)
		["arifle_MXM_Black_F",26,false],				// MXM (Black)
		["srifle_EBR_F",30,false],						// Mk18
		["arifle_SPAR_03_blk_F",33,false],				// SPAR-17 (Black)
		["srifle_DMR_03_F",35,false],					// Mk-1 EMR (Black)
		["srifle_DMR_03_khaki_F",35,false],				// Mk-1 EMR (Khaki)
		["srifle_DMR_04_F",35,false],					// ASP-1 Kir (Black)
		["srifle_DMR_05_blk_F",40,false],				// Cyrus (Black)
		["srifle_DMR_02_F",42,false],					// Mar-10 (Black)
		["srifle_LRR_camo_F",45,false],					// M320 (Camo)
		["srifle_GM6_F",50,false],						// Lynx (Black)
		// Machineguns
		["arifle_CTARS_ghex_F",24,false],				// Car-95 MG (gHex)
		["arifle_MX_SW_Black_F",26,false],				// MXM (Black)
		["arifle_SPAR_02_blk_F",27,false],				// SPAR-16 MG (Black)
		["LMG_03_F",28,false],							// LIM-85
		["LMG_Mk200_F",30,false],						// Mk200
		["LMG_Zafir_F",34,false],						// Zafir
		["MMG_02_black_F",40,false],					// SPMG (Black)
		["MMG_01_tan_F",42,false],						// Navid (Tan)
		// launchers
		["launch_RPG7_F",10,true],						// RPG7
		["launch_RPG32_ghex_F",20,false],				// RPG32 (gHex)
		["launch_MRAWS_green_rail_F",20,false],			// MAAWS (Green)
		["launch_MRAWS_green_F",23,false],				// MAAWS NVS (Green)
		["launch_NLAW_F",27,false],						// PCML
		["launch_O_Vorona_green_F",30,false],			// Vorona AT (Green)
		["launch_I_Titan_short_F",40,false],			// Titan AT (Olive)
		["launch_I_Titan_F",40,false]					// Titan AA (Digi)
	],[	// Items
		// Misc
		["ItemMap",1,true],								// Map
		["ItemWatch",1,true],							// Watch
		["ItemCompass",1,true],							// Compass
		["ItemRadio",1,true],							// Radio
		["ItemGPS",5,true],								// GPS
		["I_UavTerminal",30,false],						// UAV terminal
		// Support
		["FirstAidKit",1,true],							// First Aid Kit
		["ToolKit",5,true],								// Toolkit
		["MineDetector",5,false],						// Minedetector
		// Vision
		["Binocular",2,true],							// Binocular
		["Rangefinder",5,false],						// Rangefinder
		["Laserdesignator_03",10,false],				// Laser designator (Hex)
		["NVGoggles_OPFOR",10,false],					// NVG (Black)
		["NVGoggles_INDEP",10,false],					// NVG (Green)
		["O_NVGoggles_ghex_F",10,false],				// NVG Compact (gHex)
		["NVGogglesB_blk_F",30,false],					// ENVG (Black)
		["NVGogglesB_grn_F",30,false],					// ENVG (Green)
		// Barrel
		["acc_flashlight_pistol",1,false],				// Pistol Flashlight
		["acc_flashlight",1,true],						// Flashlight
		["acc_pointer_IR",2,false],						// Laser Pointer
		// Optics
		["optic_MRD",3,false],							// MRD
		["optic_Yorris",3,false],						// Yorris
		["optic_Aco_smg",5,false],						// ACO SMG (Red)
		["optic_ACO_grn_smg",5,false],					// ACO SMG (Green)
		["optic_Aco",7,false],							// ACO (Red)
		["optic_ACO_grn",7,false],						// ACO (Green)
		["optic_Holosight_smg_khk_F",5,false],			// Holo SMG (Green)
		["optic_Holosight_smg_blk_F",5,false],			// Holo SMG (Black)
		["optic_Holosight_khk_F",7,false],				// Holo (Green)
		["optic_Holosight_blk_F",7,false],				// Holo (Black)
		["optic_MRCO",10,false],						// MRCO
		["optic_Arco_ghex_F",15,false],					// ARCO (ghex)
		["optic_Arco_blk_F",15,false],					// ARCO (Black)
		["optic_Hamr",15,false],						// RCO
		["optic_ERCO_khk_F",15,false],					// ERCO (Green)
		["optic_ERCO_blk_F",15,false],					// ERCO (Black)
		["optic_DMS",20,false],							// DMS	
		["optic_SOS",20,false],							// MOS
		["optic_LRPS",25,false],						// LRPS (Black)
		["optic_LRPS_tna_F",25,false],					// LRPS (Camo)
		["optic_NVS",25,false],							// NVS
		["optic_KHS_old",30,false],						// Kahlia (Old)
		["optic_KHS_blk",30,false],						// Kahlia (Black)
		["optic_AMS",33,false],							// AMS (Black)
		["optic_AMS_khk",33,false],						// AMS (khk)
		["optic_tws",33,false],							// TWS
		["optic_tws_mg",33,false],						// TWS MG
		["optic_Nightstalker",40,false],				// Night-stalker
		// Suppressors
		["muzzle_snds_L",2,false],						// 9 mm
		["muzzle_snds_acp",3,false],					// .45
		["muzzle_snds_M",3,false],						// 5.56 mm (Black)
		["muzzle_snds_m_khk_F",3,false],				// 5.56 mm (Green)
		["muzzle_snds_58_blk_F",4,false],				// 5.8 mm (Black)
		["muzzle_snds_58_ghex_F",4,false],				// 5.8 mm (gHex)
		["muzzle_snds_H",5,false],						// 6.5 mm (Black)
		["muzzle_snds_H_khk_F",5,false],				// 6.5 mm (Green)
		["muzzle_snds_65_TI_ghex_F",6,false],			// 6.5 mm Stealth (gHex)
		["muzzle_snds_B",7,false],						// 7.62 mm (Black)
		["muzzle_snds_B_khk_F",7,false],				// 7.62 mm (Green)
		["muzzle_snds_338_black",10,false],				// .338 (Black)
		["muzzle_snds_338_green",10,false],				// .338 (Green)
		["muzzle_snds_93mmg",10,false],					// 9.3 mm (Black)
		// Bipods
		["bipod_03_F_oli",2,false],						// AAF (oli)
		["bipod_03_F_blk",2,false],						// AAF (Black)
		["bipod_01_F_khk",2,false]						// Nato (Green)
	],[	// Clothes
		// Uniforms
		["U_I_C_Soldier_Bandit_1_F",5,true],			// Bandit (Polo)
		["U_I_C_Soldier_Bandit_2_F",5,true],			// Bandit (Skull)
		["U_I_C_Soldier_Bandit_3_F",5,true],			// Bandit (Tee)
		["U_I_C_Soldier_Bandit_4_F",5,true],			// Bandit (Checkered)
		["U_I_C_Soldier_Bandit_5_F",5,true],			// Bandit (Tank top)
		["U_BG_Guerilla1_2_F",5,false],					// Guer (Garment Olive)
		["U_BG_Guerilla2_2",5,false],					// Guer (Pattern)
		["U_BG_Guerilla2_3",5,false],					// Guer (Plain, Light)
		["U_BG_Guerilla3_1",6,false],					// Guer (Smocks)
		["U_I_C_Soldier_Para_1_F",6,true],				// Para (Tee)
		["U_I_C_Soldier_Para_2_F",6,true],				// Para (Jacket)
		["U_I_C_Soldier_Para_3_F",6,true],				// Para (Shirt)
		["U_I_C_Soldier_Para_4_F",6,true],				// Para (Tank top)
		["U_I_C_Soldier_Para_5_F",6,true],				// Para (Shorts)
		["U_BG_leader",6,false],						// Guer (Uniform)
		["U_BG_Guerrilla_6_1",7,false],					// Guer (Apparel)
		["U_I_C_Soldier_Camo_F",7,false],				// Syndikat Uniform
		["U_Tank_green_F",8,false],						// AAF Tanker
		["U_I_CombatUniform_shortsleeve",8,false],		// AAF (Rolled)
		["U_I_CombatUniform",8,false],					// AAF 
		["U_I_OfficerUniform",9,false],					// AAF (Officer)
		["U_I_Wetsuit",15,false],						// AAF Wetsuit
		["U_I_pilotCoveralls",15,false],				// AAF Pilot
		["U_I_GhillieSuit",15,false],					// AAF Ghillie
		["U_I_FullGhillie_lsh",20,false],				// AAF Ghillie (Lush)
		["U_O_T_Soldier_F",12,false],					// CSAT Fatigues (gHex)
		["U_O_V_Soldier_Viper_F",30,false],				// Viper Fatigues (gHex)
		// Vests
		["V_Pocketed_black_F",4,true],					// Pockets (Black)
		["V_Pocketed_olive_F",4,true],					// Pockets (Olive)
		["V_BandollierB_ghex_F",5,true],				// Bandolier (gHex)
		["V_BandollierB_oli",5,true],					// Bandolier (Olive)
		["V_BandollierB_blk",5,true],					// Bandolier (Black)
		["V_TacChestrig_oli_F",6,true],					// Tactical chest rig (Olive)
		["V_TacChestrig_grn_F",6,true],					// Tactical chest rig (Green)
		["V_Chestrig_oli",7,false],						// Chest Rig (Olive)
		["V_Chestrig_rgr",7,false],						// Chest Rig (Green)
		["V_Chestrig_blk",7,false],						// Chest Rig (Black)
		["V_TacVest_brn",10,false],						// Tactical (Brown)
		["V_TacVest_oli",10,false],						// Tactical (Olive)
		["V_TacVest_camo",10,false],					// Tactical (Camo)
		["V_TacVest_blk",10,false],						// Tactical (Black)
		["V_TacVestIR_blk",15,false],					// Raven
		["V_RebreatherIA",15,false],					// AAF Rebreather
		["V_HarnessOGL_ghex_F",17,false],				// LBV GL (gHex)
		["V_HarnessO_ghex_F",19,false],					// LBV (gHex)
		["V_EOD_olive_F",23,false],						// EOD (Olive)
		["V_PlateCarrierIA1_dgtl",21,false],			// AAF Light
		["V_PlateCarrierIA2_dgtl",24,false],			// AAF Rig
		["V_PlateCarrierIAGL_dgtl",26,false],			// AAF GL (Digi)
		// Headgear
		["H_EarProtectors_black_F",1,false],			// Ear protectors
		["H_HeadSet_black_F",1,false],					// Ear protectors + headset
		["H_Cap_blk",1,true],							// Cap (Black)
		["H_Cap_blk_Raven",1,true],						// Cap (AAF)
		["H_Cap_oli",1,true],							// Cap (Olive)
		["H_Cap_oli_hs",3,false],						// Cap (Olive, Headset)
		["H_Cap_headphones",3,false],					// Cap (Rangemaster)
		["H_Bandanna_sgg",2,true],						// Bandanna (Sage)
		["H_Bandanna_camo",2,true],						// Bandanna (Woodland)
		["H_Bandanna_gry",2,true],						// Bandanna (Black)
		["H_Shemag_olive",5,true],						// Shemag (Olive)
		["H_Shemag_olive_hs",5,false],					// Shemag (Olive, Headset)
		["H_Booniehat_oli",6,true],						// Booniehat (Olive)
		["H_Booniehat_dgtl",6,false],					// Booniehat (Digi)
		["H_Watchcap_camo",7,false],					// Beanie (Green)
		["H_Watchcap_khk",7,false],						// Beanie (Khaki)
		["H_Watchcap_blk",7,false],						// Beanie (Black)
		["H_Hat_Safari_olive_F",7,false],				// Safari (olive)
		["H_Helmet_Skate",8,false],						// Skate helmet
		["H_MilCap_dgtl",10,false],						// Military Cap (Digi)
		["H_MilCap_ghex_F",10,false],					// Military Cap (gHex)
		["H_PASGT_basic_olive_F",12,false],				// Basic (Olive)
		["H_PASGT_basic_black_F",12,false],				// Basic (Black)
		["H_HelmetIA",12,false],						// Modular helmet
		["H_HelmetCrew_I",15,false],					// AAF Crew
		["H_CrewHelmetHeli_I",15,false],				// AAF Heli Crew
		["H_PilotHelmetFighter_I",17,false],			// AAF Pilot
		["H_HelmetCrew_O_ghex_F",15,false],				// CSAT Crew
		["H_HelmetO_ghex_F",13,false],					// Protector (gHex)
		["H_HelmetSpecO_ghex_F",15,false],				// Assassin (gHex)
		["H_HelmetLeaderO_ghex_F",17,false],			// Defender (gHex)
		["H_HelmetO_ViperSP_ghex_F",40,false]			// Viper (gHex)
	],[	// Goggles
		["G_Shades_Black",2,true],						// Shades (Black)
		["G_Sport_Blackred",2,true],					// Sport Shades (Black Red)
		["G_Bandanna_oli",2,true],						// Bandanna (oli)
		["G_Bandanna_blk",2,true],						// Bandanna (Black)
		["G_Bandanna_shades",3,false],					// Bandanna (Shades)
		["G_Bandanna_sport",3,false],					// Bandanna (Sport)
		["G_Bandanna_aviator",4,false],					// Bandanna (Aviator)
		["G_Bandanna_beast",4,false],					// Bandanna (Beast)
		["G_Aviator",3,false],							// Aviator
		["G_Lowprofile",4,false],						// Low Profile Goggles
		["G_Combat",5,false],							// Combat Goggles
		["G_Balaclava_blk",5,false],					// Balaclava (Black)
		["G_Balaclava_oli",5,false],					// Balaclava (Olive)
		["G_Balaclava_lowprofile",6,false],				// Balaclava (Low Profile)
		["G_Balaclava_combat",6,false],					// Balaclava (Combat)
		["G_I_Diving",6,false],							// AAF Diving Goggles
		["G_Balaclava_TI_blk_F",8,false],				// Stealth balaclava (Black)
		["G_Balaclava_TI_G_blk_F",10,false]				// Stealth balaclava (Black, Goggles)
	],[	// Backpacks
		// Cargo backpacks
		["B_Parachute",5,true],							// Parachute
		["B_LegStrapBag_black_F",1,true],				// Leg strap (Black)
		["B_LegStrapBag_olive_F",1,true],				// Leg strap (Olive)
		["B_Messenger_Black_F",2,true],					// Messenger (Black)
		["B_Messenger_Olive_F",2,true],					// Messenger (Olive)
		["B_AssaultPack_khk",3,true],					// Assault (Khaki)
		["B_AssaultPack_dgtl",3,true],					// Assault (Digi)
		["B_AssaultPack_rgr",3,true],					// Assault (Green)
		["B_AssaultPack_blk",3,false],					// Assault (Black)
		["B_FieldPack_khk",5,false],					// Field (Khaki)
		["B_FieldPack_oli",5,false],					// Field (Olive)
		["B_FieldPack_ghex_F",5,false],					// Field (gHex)
		["B_TacticalPack_oli",7,false],					// Tactical (Olive)
		["B_TacticalPack_rgr",7,false],					// Tactical (Green)
		["B_TacticalPack_blk",7,false],					// Tactical (Black)
		["B_Kitbag_rgr",10,false],						// Kit (Green)
		["B_Kitbag_cbr",10,false],						// Kit (Coyote)
		["B_ViperLightHarness_oli_F",12,false],			// Viper light (Olive)
		["B_ViperLightHarness_blk_F",12,false],			// Viper light (Black)
		["B_ViperLightHarness_ghex_F",12,false],		// Viper light (gHex)
		["B_ViperHarness_oli_F",13,false],				// Viper (Olive)
		["B_ViperHarness_blk_F",13,false],				// Viper (Black)
		["B_ViperHarness_ghex_F",13,false],				// Viper (gHex)
		["B_Carryall_oli",15,false],					// Carryall (Olive)
		["B_Carryall_ghex_F",15,false],					// Carryall (gHex)
		["B_Bergen_dgtl_F",25,false],					// Bergen (Digi)
		// Turret backpacks
		["I_UAV_06_backpack_F",10,false],				// AAF UAV Utility
		["I_UAV_06_medical_backpack_F",15,false],		// AAF UAV Medical
		["I_UAV_01_backpack_F",15,false],				// AAF UAV
		["C_IDAP_UAV_06_antimine_backpack_F",20,false],	// IDAP UAV Demining
		["I_HMG_01_support_F",5,false],					// AAF Tripod
		["I_HMG_01_support_high_F",5,false],			// AAF Tripod Raised
		["I_HMG_01_weapon_F",20,false],					// AAF MG
		["I_HMG_01_high_weapon_F",20,false],			// AAF MG Raised
		["I_HMG_01_A_weapon_F",35,false],				// AAF MG Auto
		["I_GMG_01_weapon_F",20,false],					// AAF GMG
		["I_GMG_01_high_weapon_F",20,false],			// AAF GMG Raised
		["I_GMG_01_A_weapon_F",35,false],				// AAF GMG Auto
		["I_Mortar_01_support_F",10,false],				// AAF Mortar Base
		["I_Mortar_01_weapon_F",20,false],				// AAF Mortar Tube
		["I_AT_01_weapon_F",30,false],					// AAF AT
		["I_AA_01_weapon_F",30,false]					// AAF AA
	]
];