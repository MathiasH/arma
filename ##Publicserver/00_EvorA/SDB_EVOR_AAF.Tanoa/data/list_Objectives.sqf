// ["Name",[pos],radius,type]
// type: DECIMAL BITFLAG (1 - airfield, 2 - island, 4 - nontraversable)

[
	[	// Near
		["Roadhouse",					[8275,12275],400,4],
		["Savu",						[8649,13553],350,0],
		["Saint-Paul",					[7950,13300],300,0],
		["Petit Nicolet",				[6800,13150],300,0],
		["Nicolet",						[6450,12650],300,0],
		["Coral Reef",					[7691,14845],250,2]
	],[	// Medium
		["Saint-Julien",				[5650,11200],350,0],
		["Railway Depot",				[4400,8425],350,0],
		["Regina",						[5150,8725],350,0],
		["Lobaka",						[6300,8625],350,0],
		["Diesel Power Plant",			[7500,8550],450,0],
		["Lami",						[8025,7650],400,0],
		["La Rochelle",					[9700,13325],400,0],
		["Nasau",						[11350,12325],350,0],
		["Mount Tanoa",					[10000,12075],400,4],
		["Tanouka",						[8700,10225],500,0],
		["Red Spring Surface Mine",		[11925,10350],600,0],
		["Vagalala",					[11000,9600],350,0],
		["Temple Ruins",				[11100,8500],350,4],
		["Kotomo",						[10900,6525],500,0],
		["Ouméré",						[12750,7650],400,4],
		["Nandai",						[14175,8825],350,0],
		["Luganville",					[13750,8450],350,0]
	],[	// Far
		["Tuvanaka",					[2200,12100],500,0],
		["Belfort",						[3075,11250],300,0],
		["Leqa",						[1925,8150],500,0],
		["Tavu",						[1175,7600],350,4],
		["Balavu",						[2575,7150],350,0],
		["Yanukka",						[2975,3300],400,0],
		["Katkoula",					[5400,4150],400,0],
		["Imuri Island",				[1700,850],300,2],
		["Tuadua Island",				[8475,1325],300,2],
		["Moddergat",					[9425,3900],400,0],
		["La Foa",						[8950,4425],350,4],
		["Harcourt",					[11225,5100],350,0],
		["Bua Bua",						[13225,2875],350,4],
		["Ile Saint George",			[13425,5200],250,2],
		["Doodstil",					[12600,4700],350,0],
		["Forest Clearing",				[12175,4000],400,4]
	],[	// Finish
		["Georgetown",					[6000,10250],400,0],
		["Blue Pearl Industrial Port",	[13600,11900],500,0],
		["Lijnhaven",					[11775,2425],450,0],
		["Sosuvu Island",				[2800,9125],400,2]
	],[	// Airfield
		["Tanoa",						[7225,7525],500,1],
		["Bala",						[2225,3550],400,1],
		["Military",					[2100,13125],400,1],
		["SaintGeorge",					[11975,3125],350,1],
		["Rochelle",					[11825,12925],400,1]
	]
];