[
	[	// Start
		[	// Infantry
			"B_G_Soldier_AR_F",
			"B_G_Soldier_LAT_F",
			"B_G_Soldier_LAT2_F",
			"B_GEN_Soldier_F",
			"B_GEN_Soldier_F",
			"B_GEN_Commander_F"
		],[	// Static
			"B_G_Mortar_01_F"
		],[	// APC
			"B_GEN_Offroad_01_gen_F",
			"B_GEN_Van_02_transport_F"
		],[	// Tank
			"B_G_Offroad_01_AT_F"
		],[	// Anti-air
			"B_G_Offroad_01_armed_F"
		],[	// Air support
			"B_Heli_Light_01_dynamicLoadout_F"
		],[	// Air transport
			"B_Heli_Light_01_F"
		],[	// Boat attack
			"B_T_Boat_Armed_01_minigun_F"
		],[	// Boat transport
			"B_G_Boat_Transport_01_F"
		]
	],[	// Easy
		[	// Infantry
			"B_G_Soldier_A_F",
			"B_G_Soldier_AR_F",
			"B_G_medic_F",
			"B_G_Soldier_GL_F",
			"B_G_Soldier_M_F",
			"B_G_Soldier_F",
			"B_G_Soldier_LAT_F",
			"B_G_Soldier_LAT2_F",
			"B_G_Soldier_lite_F",
			"B_G_Sharpshooter_F",
			"B_G_Soldier_SL_F"
		],[	// Static
			"B_G_Mortar_01_F"
		],[	// APC
			"B_G_Van_01_transport_F",
			"B_G_Van_02_transport_F",
			"B_G_Offroad_01_armed_F",
			"B_T_LSV_01_armed_F"
		],[	// Tank
			"B_G_Offroad_01_AT_F",
			"B_T_LSV_01_AT_F"
		],[	// Anti-air
			"B_G_Offroad_01_armed_F",
			"B_T_APC_Tracked_01_AA_F"
		],[	// Air support
			"B_Heli_Light_01_dynamicLoadout_F",
			"B_T_UAV_03_dynamicLoadout_F",
			"B_T_VTOL_01_armed_F"
		],[	// Air transport
			"B_Heli_Light_01_F",
			"B_Heli_Transport_01_F"
		],[	// Boat attack
			"B_T_Boat_Armed_01_minigun_F"
		],[	// Boat transport
			"B_G_Boat_Transport_01_F"
		]
	],[	// Medium
		[	// Infantry
			"B_T_Soldier_A_F",
			"B_T_Soldier_AR_F",
			"B_T_Medic_F",
			"B_T_Crew_F",
			"B_T_Soldier_GL_F",
			"B_T_soldier_M_F",
			"B_T_Soldier_AA_F",
			"B_T_Soldier_AT_F",
			"B_T_Soldier_LAT2_F",
			"B_T_Soldier_F",
			"B_T_Soldier_LAT_F"
		],[	// Static
			"B_T_HMG_01_F",
			"B_T_GMG_01_F",
			"B_T_Mortar_01_F",
			"B_T_Static_AA_F",
			"B_T_Static_AT_F"
		],[	// APC
			"B_T_LSV_01_armed_F",
			"B_T_APC_Tracked_01_rcws_F"
		],[	// Tank
			"B_T_LSV_01_AT_F",
			"B_T_AFV_Wheeled_01_cannon_F",
			"B_T_APC_Wheeled_01_cannon_F"
		],[	// Anti-air
			"B_G_Offroad_01_armed_F",
			"B_T_APC_Tracked_01_AA_F",
			"B_T_APC_Tracked_01_AA_F"
		],[	// Air support
			"B_T_UAV_03_dynamicLoadout_F",
			"B_Plane_Fighter_01_F",
			"B_T_VTOL_01_armed_F"
		],[	// Air transport
			"B_Heli_Transport_01_F",
			"B_Heli_Transport_03_F"
		],[	// Boat attack
			"B_T_Boat_Armed_01_minigun_F"
		],[	// Boat transport
			"B_T_Boat_Transport_01_F",
			"B_T_Boat_Armed_01_minigun_F"
		]
	],[	// Hard
		[	// Infantry
			"B_T_Soldier_A_F",
			"B_T_Soldier_AR_F",
			"B_T_Medic_F",
			"B_T_Crew_F",
			"B_T_Soldier_GL_F",
			"B_T_soldier_M_F",
			"B_T_Soldier_AA_F",
			"B_T_Soldier_AT_F",
			"B_T_Soldier_LAT2_F",
			"B_T_Soldier_F",
			"B_T_Soldier_LAT_F",
			"B_T_Recon_TL_F",
			"B_T_Recon_LAT_F",
			"B_T_Recon_F",
			"B_T_Recon_Medic_F",
			"B_T_Recon_M_F",
			"B_T_Recon_JTAC_F",
			"B_T_Spotter_F",
			"B_T_Sniper_F",
			"B_CTRG_Soldier_AR_tna_F",
			"B_CTRG_Soldier_JTAC_tna_F",
			"B_CTRG_Soldier_M_tna_F",
			"B_CTRG_Soldier_Medic_tna_F",
			"B_CTRG_Soldier_tna_F",
			"B_CTRG_Soldier_LAT_tna_F",
			"B_CTRG_Soldier_LAT2_tna_F",
			"B_CTRG_Soldier_TL_tna_F"
		],[	// Static
			"B_T_HMG_01_F",
			"B_T_GMG_01_F",
			"B_T_Mortar_01_F",
			"B_T_Static_AA_F",
			"B_T_Static_AT_F",
			"B_AAA_System_01_F"
		],[	// APC
			"B_CTRG_LSV_01_light_F",
			"B_T_APC_Wheeled_01_cannon_F",
			"B_T_APC_Tracked_01_rcws_F"
		],[	// Tank
			"B_T_MBT_01_cannon_F",
			"B_T_APC_Wheeled_01_cannon_F",
			"B_T_AFV_Wheeled_01_up_cannon_F"
		],[	// Anti-air
			"B_T_APC_Tracked_01_AA_F"
		],[	// Air support
			"B_Heli_Attack_01_dynamicLoadout_F",
			"B_Plane_CAS_01_dynamicLoadout_F",
			"B_Plane_Fighter_01_F",
			"B_T_VTOL_01_armed_F"
		],[	// Air transport
			"B_CTRG_Heli_Transport_01_tropic_F",
			"B_Heli_Transport_03_F",
			"B_T_VTOL_01_infantry_F"
		],[	// Boat attack
			"B_T_Boat_Armed_01_minigun_F"
		],[	// Boat transport
			"B_T_Boat_Transport_01_F",
			"B_T_Boat_Armed_01_minigun_F"
		]
	]
];