[
	[	// Start
		[	// Infantry
			"B_G_Soldier_LAT_F",
			"B_G_Soldier_LAT2_F",
			"B_G_Soldier_SL_F",
			"B_G_Soldier_A_F",
			"B_G_Soldier_AR_F",
			"B_G_Soldier_GL_F",
			"B_G_medic_F",
			"B_G_Soldier_M_F",
			"B_G_Soldier_F",
			"B_G_Soldier_lite_F",
			"B_G_Sharpshooter_F"
		],[	// Static
			"B_G_Mortar_01_F"
		],[	// APC
			"B_G_Offroad_01_F",
			"B_G_Van_01_transport_F",
			"B_G_Van_02_transport_F",
			"B_G_Offroad_01_armed_F"
		],[	// Tank
			"B_G_Offroad_01_AT_F"
		],[	// Anti-air
			"B_G_Offroad_01_armed_F"
		],[	// Air support
			"B_Heli_Light_01_dynamicLoadout_F"
		],[	// Air transport
			"B_Heli_Light_01_F"
		],[	// Boat attack
			"B_Boat_Armed_01_minigun_F"
		],[	// Boat transport
			"B_G_Boat_Transport_01_F"
		]
	],[	// Easy
		[	// Infantry
			"B_soldier_LAT_F",
			"B_soldier_LAT2_F",
			"B_soldier_AT_F",
			"B_soldier_AA_F",
			"B_Soldier_TL_F",
			"B_Soldier_SL_F",
			"B_Soldier_A_F",
			"B_soldier_AR_F",
			"B_medic_F",
			"B_Soldier_GL_F",
			"B_HeavyGunner_F",
			"B_soldier_M_F",
			"B_Soldier_F",
			"B_Soldier_lite_F",
			"B_Sharpshooter_F",
			"B_G_Soldier_LAT_F",
			"B_G_Soldier_LAT2_F",
			"B_G_Soldier_SL_F",
			"B_G_Soldier_A_F",
			"B_G_Soldier_AR_F",
			"B_G_Soldier_GL_F",
			"B_G_medic_F",
			"B_G_Soldier_M_F",
			"B_G_Soldier_F",
			"B_G_Soldier_lite_F",
			"B_G_Sharpshooter_F"
		],[
			"B_Mortar_01_F",
			"B_HMG_01_F",
			"B_GMG_01_F"
		],[	// APC
			"B_G_Offroad_01_F",
			"B_G_Van_02_transport_F",
			"B_LSV_01_armed_sand_F",
			"B_MRAP_01_hmg_F",
			"B_MRAP_01_gmg_F",
			"B_APC_Tracked_01_rcws_F"
		],[	// Tank
			"B_LSV_01_AT_F",
			"B_G_Offroad_01_AT_F",
			"B_APC_Wheeled_01_cannon_F",
			"B_AFV_Wheeled_01_cannon_F",
			"B_UGV_01_rcws_F"
		],[	// Anti-air
			"B_APC_Tracked_01_AA_F",
			"B_G_Offroad_01_armed_F"
		],[	// Air support
			"B_Heli_Light_01_dynamicLoadout_F",
			"B_T_UAV_03_dynamicLoadout_F",
			"B_Heli_Attack_01_dynamicLoadout_F"
		],[	// Air transport
			"B_Heli_Transport_01_camo_F",
			"B_Heli_Transport_01_camo_F",
			"B_Heli_Transport_03_F"
		],[	// Boat attack
			"B_Boat_Armed_01_minigun_F"
		],[	// Boat transport
			"B_G_Boat_Transport_01_F",
			"B_Boat_Transport_01_F",
			"B_Boat_Armed_01_minigun_F"
		]
	],[	// Medium
		[	// Infantry
			"B_soldier_LAT_F",
			"B_soldier_LAT2_F",
			"B_soldier_AT_F",
			"B_soldier_AA_F",
			"B_Soldier_TL_F",
			"B_Soldier_SL_F",
			"B_Soldier_A_F",
			"B_soldier_AR_F",
			"B_medic_F",
			"B_Soldier_GL_F",
			"B_HeavyGunner_F",
			"B_soldier_M_F",
			"B_Soldier_F",
			"B_Soldier_lite_F",
			"B_Sharpshooter_F",
			"B_recon_F",
			"B_recon_LAT_F",
			"B_recon_medic_F",
			"B_recon_TL_F",
			"B_recon_M_F",
			"B_recon_JTAC_F",
			"B_Recon_Sharpshooter_F"
		],[
			"B_Mortar_01_F",
			"B_Mortar_01_F",
			"B_static_AT_F",
			"B_static_AA_F",
			"B_HMG_01_high_F",
			"B_GMG_01_high_F"
		],[	// APC
			"B_LSV_01_armed_sand_F",
			"B_MRAP_01_hmg_F",
			"B_MRAP_01_gmg_F",
			"B_APC_Tracked_01_rcws_F"
		],[	// Tank
			"B_LSV_01_AT_F",
			"B_APC_Wheeled_01_cannon_F",
			"B_AFV_Wheeled_01_cannon_F",
			"B_AFV_Wheeled_01_up_cannon_F",
			"B_MBT_01_cannon_F",
			"B_MBT_01_TUSK_F",
			"B_UGV_01_rcws_F"
		],[	// Anti-air
			"B_APC_Tracked_01_AA_F"
		],[	// Air support
			"B_Heli_Attack_01_dynamicLoadout_F",
			"B_Plane_CAS_01_dynamicLoadout_F",
			"B_T_UAV_03_dynamicLoadout_F"
		],[	// Air transport
			"B_Heli_Transport_01_camo_F",
			"B_Heli_Transport_03_F",
			"B_T_VTOL_01_infantry_F"
		],[	// Boat attack
			"B_Boat_Armed_01_minigun_F"
		],[	// Boat transport
			"B_Boat_Transport_01_F",
			"B_Boat_Armed_01_minigun_F"
		]
	],[	// Hard
		[	// Infantry
			"B_soldier_LAT_F",
			"B_soldier_LAT2_F",
			"B_soldier_AT_F",
			"B_soldier_AA_F",
			"B_Soldier_TL_F",
			"B_Soldier_SL_F",
			"B_Soldier_A_F",
			"B_soldier_AR_F",
			"B_medic_F",
			"B_Soldier_GL_F",
			"B_HeavyGunner_F",
			"B_soldier_M_F",
			"B_Soldier_F",
			"B_Sharpshooter_F",
			"B_CTRG_Soldier_AR_tna_F",
			"B_CTRG_Soldier_JTAC_tna_F",
			"B_CTRG_Soldier_M_tna_F",
			"B_CTRG_Soldier_Medic_tna_F",
			"B_CTRG_Soldier_tna_F",
			"B_CTRG_Soldier_LAT_tna_F",
			"B_CTRG_Soldier_TL_tna_F",
			"B_recon_F",
			"B_recon_LAT_F",
			"B_recon_medic_F",
			"B_recon_TL_F",
			"B_recon_M_F",
			"B_recon_JTAC_F",
			"B_Recon_Sharpshooter_F"
		],[
			"B_Mortar_01_F",
			"B_Mortar_01_F",
			"B_static_AT_F",
			"B_static_AA_F",
			"B_HMG_01_A_F",
			"B_GMG_01_A_F",
			"B_HMG_01_high_F",
			"B_GMG_01_high_F"
		],[	// APC
			"B_LSV_01_armed_black_F",
			"B_MRAP_01_hmg_F",
			"B_MRAP_01_gmg_F",
			"B_APC_Tracked_01_rcws_F",
			"B_APC_Wheeled_01_cannon_F"
		],[	// Tank
			"B_UGV_01_rcws_F",
			"B_AFV_Wheeled_01_up_cannon_F",
			"B_MBT_01_cannon_F",
			"B_MBT_01_TUSK_F"
		],[	// Anti-air
			"B_APC_Tracked_01_AA_F"
		],[	// Air support
			"B_Heli_Attack_01_dynamicLoadout_F",
			"B_Plane_CAS_01_dynamicLoadout_F",
			"B_T_UAV_03_dynamicLoadout_F",
			"B_Plane_Fighter_01_F"
		],[	// Air transport
			"B_CTRG_Heli_Transport_01_sand_F",
			"B_Heli_Transport_03_F",
			"B_T_VTOL_01_infantry_F"
		],[	// Boat attack
			"B_Boat_Armed_01_minigun_F"
		],[	// Boat transport
			"B_Boat_Transport_01_F",
			"B_Boat_Armed_01_minigun_F"
		]
	]
];