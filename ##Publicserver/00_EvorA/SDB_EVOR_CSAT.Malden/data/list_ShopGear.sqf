// ["Classname",price,removed from shop,[params],"Custom name"]

[
	[	// Weapons
		// Pistols
		["hgun_Pistol_01_F",1,true],					// PM
		["hgun_Rook40_F",2,true],						// Rook-40
		["hgun_P07_F",2,false],							// P07
		["hgun_ACPC2_F",4,false],						// ACP-C2
		["hgun_Pistol_heavy_02_F",6,false],				// Zubr
		["hgun_Pistol_heavy_01_F",8,false],				// 4-five
		["hgun_Pistol_Signal_F",10,false],				// Starter Pistol
		// SMGs
		["hgun_PDW2000_F",7,true],						// PDW2000
		["SMG_05_F",9,false],							// Protector
		["SMG_02_F",10,false],							// Sting
		["SMG_01_F",13,false],							// Vermin
		// Rifles
		["arifle_AKS_F",5,true],						// AKS
		["arifle_AKM_F",10,true],						// AKM
		["arifle_Mk20C_plain_F",12,true],				// Mk20 C (Sand)
		["arifle_Mk20_plain_F",15,false],				// Mk20 (Sand)
		["arifle_Mk20_GL_plain_F",18,false],			// Mk20 GL (Sand)
		["arifle_TRG20_F",15,false],					// TRG C
		["arifle_TRG21_F",18,false],					// TRG
		["arifle_TRG21_GL_F",21,false],					// TRG GL
		["arifle_AK12_F",21,false],						// AK-12
		["arifle_AK12_GL_F",24,false],					// AK-12 GL
		["arifle_CTAR_blk_F",21,false],					// Car-95 (Black)
		["arifle_CTAR_hex_F",21,false],					// Car-95 (Hex)
		["arifle_CTAR_GL_blk_F",24,false],				// Car-95 GL (Black)
		["arifle_CTAR_GL_hex_F",24,false],				// Car-95 GL (Hex)
		["arifle_SPAR_01_blk_F",21,false],				// SPAR-16 (Black)
		["arifle_SPAR_01_GL_blk_F",24,false],			// SPAR-16 GL (Black)
		["arifle_Katiba_C_F",18,false],					// Katiba C
		["arifle_Katiba_F",21,false],					// Katiba
		["arifle_Katiba_GL_F",24,false],				// Katiba GL
		["arifle_MXC_Black_F",21,false],				// MX C (Black)
		["arifle_MX_Black_F",24,false],					// MX (Black)
		["arifle_MX_GL_Black_F",30,false],				// MX GL (Black)
		["arifle_ARX_blk_F",40,false],					// Type 115 (Black)
		["arifle_ARX_hex_F",40,false],					// Type 115 (Hex)
		["arifle_SDAR_F",20,false],						// SDAR
		// Snipers
		["srifle_DMR_01_F",20,true],					// Rahim
		["srifle_DMR_06_olive_F",23,false],				// Mk14 (Olive)
		["srifle_DMR_06_camo_F",23,false],				// Mk14 (Camo)
		["srifle_DMR_07_blk_F",25,false],				// CMR (Black)
		["srifle_DMR_07_hex_F",25,false],				// CMR (Hex)
		["arifle_MXM_Black_F",26,false],				// MXM (Black)
		["srifle_EBR_F",30,false],						// Mk18
		["arifle_SPAR_03_blk_F",33,false],				// SPAR-17 (Black)
		["srifle_DMR_03_F",35,false],					// Mk-1 EMR (Black)
		["srifle_DMR_03_khaki_F",35,false],				// Mk-1 EMR (Khaki)
		["srifle_DMR_03_tan_F",35,false],				// Mk-1 EMR (Sand)
		["srifle_DMR_03_woodland_F",35,false],			// Mk-1 EMR (Woodland)
		["srifle_DMR_04_F",35,false],					// ASP-1 Kir (Black)
		["srifle_DMR_04_Tan_F",35,false],				// ASP-1 Kir (Tan)
		["srifle_DMR_05_blk_F",40,false],				// Cyrus (Black)
		["srifle_DMR_05_tan_f",40,false],				// Cyrus (Tan)
		["srifle_DMR_05_hex_F",40,false],				// Cyrus (Hex)
		["srifle_DMR_02_F",42,false],					// Mar-10 (Black)
		["srifle_DMR_02_sniper_F",42,false],			// Mar-10 (Sand)
		["srifle_LRR_F",45,false],						// M320 (Black)
		["srifle_GM6_F",50,false],						// Lynx (Black)
		["srifle_GM6_camo_F",50,false],					// Lynx (Hex)
		// Machineguns
		["arifle_CTARS_blk_F",24,false],				// Car-95 MG (Black)
		["arifle_CTARS_hex_F",24,false],				// Car-95 MG (Hex)
		["arifle_MX_SW_Black_F",26,false],				// MXM (Black)
		["arifle_SPAR_02_blk_F",27,false],				// SPAR-16 MG (Black)
		["LMG_03_F",28,false],							// LIM-85
		["LMG_Mk200_F",30,false],						// Mk200
		["LMG_Zafir_F",34,false],						// Zafir
		["MMG_02_black_F",40,false],					// SPMG (Black)
		["MMG_02_sand_F",40,false],						// SPMG (Sand)
		["MMG_01_tan_F",42,false],						// Navid (Tan)
		["MMG_01_hex_F",42,false],						// Navid (Hex)
		// launchers
		["launch_RPG7_F",10,false],						// RPG7
		["launch_RPG32_F",20,false],					// RPG32
		["launch_MRAWS_olive_rail_F",20,false],			// MAAWS (Olive)
		["launch_MRAWS_olive_F",23,false],				// MAAWS NVS (Olive)
		["launch_NLAW_F",27,false],						// PCML
		["launch_O_Vorona_brown_F",30,false],			// Vorona AT (Brown)
		["launch_O_Titan_short_F",40,false],			// Titan AT (Hex)
		["launch_O_Titan_F",40,false]					// Titan AA (Hex)
	],[	// Items
		// Misc
		["ItemMap",1,true],								// Map
		["ItemWatch",1,true],							// Watch
		["ItemCompass",1,true],							// Compass
		["ItemRadio",1,true],							// Radio
		["ItemGPS",5,false],							// GPS
		["O_UavTerminal",30,false],						// UAV terminal
		// Support
		["FirstAidKit",1,true],							// First Aid Kit
		["ToolKit",5,true],								// Toolkit
		["MineDetector",5,false],						// Minedetector
		// Vision
		["Binocular",2,true],							// Binocular
		["Rangefinder",5,false],						// Rangefinder
		["Laserdesignator_02",10,false],				// Laser designator (Hex)
		["NVGoggles_OPFOR",10,false],					// NVG (Black)
		["O_NVGoggles_hex_F",10,false],					// NVG Compact (Hex)
		["O_NVGoggles_urb_F",10,false],					// NVG Compact (Urban)
		["NVGogglesB_gry_F",30,false],					// ENVG (Gray)
		["NVGogglesB_blk_F",30,false],					// ENVG (Black)
		// Barrel
		["acc_flashlight_pistol",1,false],				// Pistol Flashlight
		["acc_flashlight",1,true],						// Flashlight
		["acc_pointer_IR",2,false],						// Laser Pointer
		// Optics
		["optic_MRD",3,false],							// MRD
		["optic_Yorris",3,false],						// Yorris
		["optic_Aco_smg",5,false],						// ACO SMG (Red)
		["optic_ACO_grn_smg",5,false],					// ACO SMG (Green)
		["optic_Aco",7,false],							// ACO (Red)
		["optic_ACO_grn",7,false],						// ACO (Green)
		["optic_Holosight_smg",5,false],				// Holo SMG
		["optic_Holosight_smg_blk_F",5,false],			// Holo SMG (Black)
		["optic_Holosight",7,false],					// Holo (Sand)
		["optic_Holosight_blk_F",7,false],				// Holo (Black)
		["optic_MRCO",10,false],						// MRCO
		["optic_Arco",15,false],						// ARCO (Tan)
		["optic_Arco_blk_F",15,false],					// ARCO (Black)
		["optic_Hamr",15,false],						// RCO
		["optic_ERCO_snd_F",15,false],					// ERCO (Sand)
		["optic_ERCO_blk_F",15,false],					// ERCO (Black)
		["optic_DMS",20,false],							// DMS	
		["optic_SOS",20,false],							// MOS
		["optic_LRPS",25,false],						// LRPS
		["optic_NVS",25,false],							// NVS
		["optic_KHS_old",30,false],						// Kahlia (Old)
		["optic_KHS_blk",30,false],						// Kahlia (Black)
		["optic_KHS_tan",30,false],						// Kahlia (Tan)
		["optic_KHS_hex",30,false],						// Kahlia (Hex)
		["optic_AMS",33,false],							// AMS (Black)
		["optic_AMS_snd",33,false],						// AMS (Sand)
		["optic_tws",33,false],							// TWS
		["optic_tws_mg",33,false],						// TWS MG
		["optic_Nightstalker",40,false],				// Night-stalker
		// Suppressors
		["muzzle_snds_L",2,false],						// 9 mm
		["muzzle_snds_acp",3,false],					// .45
		["muzzle_snds_M",3,false],						// 5.56 mm (Black)
		["muzzle_snds_m_snd_F",3,false],				// 5.56 mm (Sand)
		["muzzle_snds_58_blk_F",4,false],				// 5.8 mm (Black)
		["muzzle_snds_58_hex_F",4,false],				// 5.8 mm (Hex)
		["muzzle_snds_H",5,false],						// 6.5 mm (Black)
		["muzzle_snds_H_snd_F",5,false],				// 6.5 mm (Sand)
		["muzzle_snds_65_TI_blk_F",6,false],			// 6.5 mm Stealth (Black)
		["muzzle_snds_65_TI_hex_F",6,false],			// 6.5 mm Stealth (Hex)
		["muzzle_snds_B",7,false],						// 7.62 mm (Black)
		["muzzle_snds_B_snd_F",7,false],				// 7.62 mm (Sand)
		["muzzle_snds_338_black",10,false],				// .338 (Black)
		["muzzle_snds_338_sand",10,false],				// .338 (Sand)
		["muzzle_snds_93mmg",10,false],					// 9.3 mm (Black)
		["muzzle_snds_93mmg_tan",10,false],				// 9.3 mm (Tan)
		// Bipods
		["bipod_01_F_snd",2,false],						// NATO (Sand)
		["bipod_01_F_blk",2,false],						// NATO (Black)
		["bipod_02_F_blk",2,false],						// CSAT (Black)
		["bipod_02_F_tan",2,false],						// CSAT (Tan)
		["bipod_02_F_hex",2,false]						// CSAT (Hex)
	],[	// Clothes
		// Uniforms
		["U_C_Poor_1",3,false],							// Civ (Worn)
		["U_C_Poloshirt_tricolour",3,false],			// Civ (Tricolor)
		["U_OrestesBody",3,false],						// Civ (Jacket & Shorts)
		["U_C_Man_casual_1_F",3,false],					// Casual (Navy)
		["U_C_Man_casual_2_F",3,false],					// Casual (Blue)
		["U_C_Man_casual_3_F",3,false],					// Casual (Green)
		["U_Competitor",5,false],						// Civ (Competitor)
		["U_I_C_Soldier_Bandit_2_F",5,true],			// Bandit (Skull)
		["U_I_C_Soldier_Bandit_3_F",5,true],			// Bandit (Tee)
		["U_I_C_Soldier_Bandit_5_F",5,true],			// Bandit (Tank top)
		["U_BG_Guerilla1_1",5,true],					// Guer (Garment)
		["U_BG_Guerilla1_2_F",5,true],					// Guer (Garment Olive)
		["U_BG_Guerilla2_2",5,true],					// Guer (Pattern)
		["U_BG_Guerilla2_3",5,true],					// Guer (Plain, Light)
		["U_BG_Guerilla2_1",5,true],					// Guer (Plain, Dark)
		["U_BG_Guerilla3_1",6,false],					// Guer (Smocks)
		["U_BG_leader",6,false],						// Guer (Uniform)
		["U_I_C_Soldier_Para_1_F",6,false],				// Para (Tee)
		["U_I_C_Soldier_Para_2_F",6,false],				// Para (Jacket)
		["U_I_C_Soldier_Para_3_F",6,false],				// Para (Shirt)
		["U_I_C_Soldier_Para_4_F",6,false],				// Para (Tank top)
		["U_I_C_Soldier_Para_5_F",6,false],				// Para (Shorts)
		["U_I_C_Soldier_Camo_F",7,false],				// Syndikat Uniform
		["U_BG_Guerrilla_6_1",7,false],					// Guer (Apparel)
		["U_B_GEN_Soldier_F",7,false],					// Gendarmerie (Soldier)
		["U_B_GEN_Commander_F",8,false],				// Gendarmerie (Officer)
		["U_O_officer_noInsignia_hex_F",8,false],		// CSAT Fatigues (Light)
		["U_O_OfficerUniform_ocamo",9,false],			// CSAT Officer
		["U_O_CombatUniform_ocamo",12,false],			// CSAT Fatigues (Hex)
		["U_O_CombatUniform_oucamo",12,false],			// CSAT Fatigues (Urban)
		["U_O_Wetsuit",15,false],						// CSAT Wetsuit
		["U_O_PilotCoveralls",15,false],				// CSAT Pilot
		["U_O_GhillieSuit",15,false],					// CSAT Ghillie
		["U_O_FullGhillie_lsh",20,false],				// CSAT Ghillie (Lush)
		["U_O_FullGhillie_sard",20,false],				// CSAT Ghillie (Semi Arid)
		["U_O_FullGhillie_ard",20,false],				// CSAT Ghillie (Arid)
		["U_I_G_resistanceLeader_F",20,false],			// Guer (Stavrou)
		["U_C_Driver_1_white",25,false],				// Racing (White)
		["U_C_Driver_1_black",25,false],				// Racing (Black)
		["U_C_Driver_4",25,false],						// Racing (VRana)
		["U_O_V_Soldier_Viper_hex_F",30,false],			// Viper (Hex)
		// Vests
		["V_Rangemaster_belt",3,true],					// Belt (Rangemaster)
		["V_Pocketed_black_F",4,false],					// Pockets (Black)
		["V_Pocketed_coyote_F",4,false],				// Pockets (Coyote)
		["V_Pocketed_olive_F",4,false],					// Pockets (Olive)
		["V_BandollierB_khk",5,true],					// Bandolier (Khaki)
		["V_BandollierB_oli",5,true],					// Bandolier (Olive)
		["V_BandollierB_rgr",5,true],					// Bandolier (Green)
		["V_BandollierB_blk",5,true],					// Bandolier (Black)
		["V_BandollierB_cbr",5,true],					// Bandolier (Coyote)
		["V_TacChestrig_cbr_F",6,false],				// Tactical chest rig (Coyote)
		["V_TacChestrig_oli_F",6,false],				// Tactical chest rig (Olive)
		["V_TacChestrig_grn_F",6,false],				// Tactical chest rig (Green)
		["V_Chestrig_khk",7,false],						// Chest Rig (Khaki)
		["V_Chestrig_oli",7,false],						// Chest Rig (Olive)
		["V_Chestrig_rgr",7,false],						// Chest Rig (Green)
		["V_Chestrig_blk",7,false],						// Chest Rig (Black)
		["V_TacVest_khk",10,false],						// Tactical (Khaki)
		["V_TacVest_brn",10,false],						// Tactical (Brown)
		["V_TacVest_oli",10,false],						// Tactical (Olive)
		["V_TacVest_camo",10,false],					// Tactical (Camo)
		["V_TacVest_gen_F",10,false],					// Tactical (Gendarmerie)
		["V_I_G_resistanceLeader_F",10,false],			// Tactical (Stavrou)
		["V_TacVest_blk",10,false],						// Tactical (Black)
		["V_TacVestIR_blk",15,false],					// Raven
		["V_RebreatherIR",15,false],					// CSAT Rebreather
		["V_HarnessOGL_brn",17,false],					// LBV GL (Brown)
		["V_HarnessOGL_gry",17,false],					// LBV GL (Gray)
		["V_HarnessO_brn",19,false],					// LBV (Brown)
		["V_HarnessO_gry",19,false],					// LBV (Gray)
		["V_PlateCarrierIA1_dgtl",21,false],			// AAF Light
		["V_PlateCarrier1_rgr_noflag_F",22,false],		// NATO Light (No flag)
		["V_PlateCarrier1_blk",22,false],				// NATO Light (Black)
		["V_EOD_coyote_F",23,false],					// EOD (Coyote)
		["V_EOD_olive_F",23,false],						// EOD (Olive)
		["V_PlateCarrierIA2_dgtl",24,false],			// AAF Rig
		["V_PlateCarrier2_rgr_noflag_F",25,false],		// NATO Rig (No flag)
		["V_PlateCarrier2_blk",25,false],				// NATO Rig (Black)
		["V_PlateCarrierIAGL_oli",26,false],			// AAF GL (Olive)
		["V_PlateCarrierGL_blk",27,false],				// NATO GL (Black)
		["V_PlateCarrierSpec_blk",30,false],			// NATO Special (Black)
		// Headgear
		["H_HeadBandage_clean_F",1,false],				// Bandage (Clean)
		["H_HeadBandage_stained_F",1,false],			// Bandage (Stained)
		["H_HeadBandage_bloody_F",1,false],				// Bandage (Bloody)
		["H_EarProtectors_black_F",1,false],			// Ear protectors
		["H_HeadSet_black_F",1,false],					// Ear protectors + headset
		["H_Cap_tan",1,true],							// Cap (Tan)
		["H_Cap_oli",1,true],							// Cap (Olive)
		["H_Cap_blk",1,true],							// Cap (Black)
		["H_Cap_blk_ION",3,false],						// Cap (ION)
		["H_Cap_brn_SPECOPS",3,false],					// Cap (Hex)
		["H_Cap_oli_hs",3,false],						// Cap (Olive, Headset)
		["H_Cap_headphones",3,false],					// Cap (Rangemaster)
		["H_Bandanna_sgg",2,true],						// Bandanna (Sage)
		["H_Bandanna_khk",2,true],						// Bandanna (Khaki)
		["H_Bandanna_khk_hs",2,false],					// Bandanna (Khaki, Headset)
		["H_Bandanna_cbr",2,false],						// Bandanna (Coyote)
		["H_Bandanna_sand",2,false],					// Bandanna (Sand)
		["H_Bandanna_camo",2,false],					// Bandanna (Woodland)
		["H_Bandanna_gry",2,false],						// Bandanna (Black)
		["H_Shemag_olive",5,true],						// Shemag (Olive)
		["H_Shemag_olive_hs",5,false],					// Shemag (Olive, Headset)
		["H_ShemagOpen_khk",5,false],					// Shemag (White)
		["H_ShemagOpen_tan",5,false],					// Shemag (Tan)
		["H_Booniehat_oli",6,false],					// Booniehat (Olive)
		["H_Booniehat_khk",6,false],					// Booniehat (Khaki)
		["H_Booniehat_khk_hs",6,false],					// Booniehat (Khaki, Headset)
		["H_Watchcap_cbr",7,false],						// Beanie (Coyote)
		["H_Watchcap_camo",7,false],					// Beanie (Green)
		["H_Watchcap_khk",7,false],						// Beanie (Khaki)
		["H_Watchcap_blk",7,false],						// Beanie (Black)
		["H_Hat_Safari_sand_F",7,false],				// Safari (Sand)
		["H_Helmet_Skate",8,false],						// Skate helmet
		["H_MilCap_gen_F",10,false],					// Military Cap (Gendarmerie)
		["H_MilCap_ocamo",10,false],					// Military Cap (Hex)
		["H_MilCap_gry",10,false],						// Military Cap (Gray)
		["H_Tank_black_F",11,false],					// Tanker (CSAT)
		["H_PASGT_basic_blue_F",12,false],				// Basic (Blue)
		["H_PASGT_basic_olive_F",12,false],				// Basic (Olive)
		["H_PASGT_basic_black_F",12,false],				// Basic (Black)
		["H_HelmetIA",12,false],						// Modular helmet
		["H_HelmetO_ocamo",13,false],					// Protector (Hex)
		["H_HelmetO_oucamo",13,false],					// Protector (Urban)
		["H_HelmetSpecO_ocamo",15,false],				// Assassin (Hex)
		["H_HelmetSpecO_blk",15,false],					// Assassin (Black)
		["H_HelmetCrew_O",15,false],					// CSAT Crew
		["H_PilotHelmetHeli_O",15,false],				// CSAT Heli Pilot
		["H_PilotHelmetHeli_B",15,false],				// NATO Heli Pilot
		["H_CrewHelmetHeli_O",15,false],				// CSAT Heli Crew
		["H_CrewHelmetHeli_B",15,false],				// NATO Heli Crew
		["H_PilotHelmetFighter_O",17,false],			// CSAT Pilot
		["H_HelmetLeaderO_ocamo",17,false],				// Defender (Hex)
		["H_HelmetLeaderO_oucamo",17,false],			// Defender (Urban)
		["H_Beret_gen_F",17,false],						// Beret (Gendarmerie)
		["H_Beret_blk",17,false],						// Beret (CSAT)
		["H_RacingHelmet_1_white_F",20,false],			// Racing (White)
		["H_RacingHelmet_1_black_F",20,false],			// Racing (Black)
		["H_RacingHelmet_4_F",20,false],				// Racing (VRana)
		["H_RacingHelmet_3_F",20,false],				// Racing (Redstone)
		["H_HelmetO_ViperSP_hex_F",40,false],			// Viper (Hex)
		["H_Hat_camo",100,false,nil,"Tactical Fedora"]	// Fedora (Camo)
	],[	// Goggles
		["G_Shades_Black",2,true],						// Shades (Black)
		["G_Sport_Blackred",2,true],					// Sport Shades (Black Red)
		["G_Bandanna_khk",2,false],						// Bandanna (Khaki)
		["G_Bandanna_tan",2,false],						// Bandanna (Tan)
		["G_Bandanna_blk",2,false],						// Bandanna (Black)
		["G_Bandanna_shades",3,false],					// Bandanna (Shades)
		["G_Bandanna_sport",3,false],					// Bandanna (Sport)
		["G_Bandanna_aviator",4,false],					// Bandanna (Aviator)
		["G_Bandanna_beast",4,false],					// Bandanna (Beast)
		["G_Aviator",3,false],							// Aviator
		["G_Tactical_Clear",4,false],					// Tactical Glasses
		["G_Tactical_Black",4,false],					// Tactical Shades
		["G_Lowprofile",4,false],						// Low Profile Goggles
		["G_Combat",5,false],							// Combat Goggles
		["G_Balaclava_blk",5,false],					// Balaclava (Black)
		["G_Balaclava_lowprofile",6,false],				// Balaclava (Low Profile)
		["G_Balaclava_combat",6,false],					// Balaclava (Combat)
		["G_O_Diving",6,false],							// CSAT Diving Goggles
		["G_Balaclava_TI_blk_F",8,false],				// Stealth balaclava (Black)
		["G_Balaclava_TI_G_blk_F",10,false]				// Stealth balaclava (Black, Goggles)
	],[	// Backpacks
		// Cargo backpacks
		["B_Parachute",5,true],							// Parachute
		["B_LegStrapBag_black_F",1,false],				// Leg strap (Black)
		["B_LegStrapBag_coyote_F",1,false],				// Leg strap (Coyote)
		["B_LegStrapBag_olive_F",1,false],				// Leg strap (Olive)
		["B_Messenger_Black_F",2,false],				// Messenger (Black)
		["B_Messenger_Coyote_F",2,false],				// Messenger (Coyote)
		["B_Messenger_Olive_F",2,false],				// Messenger (Olive)
		["B_AssaultPack_khk",3,true],					// Assault (Khaki)
		["B_AssaultPack_sgg",3,true],					// Assault (Sage)
		["B_AssaultPack_cbr",3,true],					// Assault (Coyote)
		["B_AssaultPack_rgr",3,false],					// Assault (Green)
		["B_AssaultPack_ocamo",3,false],				// Assault (Hex)
		["B_AssaultPack_blk",3,false],					// Assault (Black)
		["B_FieldPack_khk",5,false],					// Field (Khaki)
		["B_FieldPack_oli",5,false],					// Field (Olive)
		["B_FieldPack_cbr",5,false],					// Field (Coyote)
		["B_FieldPack_ocamo",5,false],					// Field (Hex)
		["B_FieldPack_oucamo",5,false],					// Field (Urban)
		["B_FieldPack_blk",5,false],					// Field (Black)
		["B_TacticalPack_oli",7,false],					// Tactical (Olive)
		["B_TacticalPack_rgr",7,false],					// Tactical (Green)
		["B_TacticalPack_ocamo",7,false],				// Tactical (Hex)
		["B_TacticalPack_blk",7,false],					// Tactical (Black)
		["B_Kitbag_sgg",10,false],						// Kit (Sage)
		["B_Kitbag_rgr",10,false],						// Kit (Green)
		["B_Kitbag_cbr",10,false],						// Kit (Coyote)
		["B_ViperLightHarness_khk_F",12,false],			// Viper light (Khaki)
		["B_ViperLightHarness_oli_F",12,false],			// Viper light (Olive)
		["B_ViperLightHarness_blk_F",12,false],			// Viper light (Black)
		["B_ViperLightHarness_hex_F",12,false],			// Viper light (Hex)
		["B_ViperHarness_khk_F",13,false],				// Viper (Khaki)
		["B_ViperHarness_oli_F",13,false],				// Viper (Olive)
		["B_ViperHarness_blk_F",13,false],				// Viper (Black)
		["B_ViperHarness_hex_F",13,false],				// Viper (Hex)
		["B_Carryall_khk",15,false],					// Carryall (Khaki)
		["B_Carryall_oli",15,false],					// Carryall (Olive)
		["B_Carryall_ocamo",15,false],					// Carryall (Hex)
		["B_Carryall_oucamo",15,false],					// Carryall (Urban)
		["B_Bergen_dgtl_F",25,false],					// Bergen (Digi)
		["B_Bergen_hex_F",25,false],					// Bergen (Hex)
		// Turret backpacks
		["O_Static_Designator_02_weapon_F",10,false],	// CSAT Static Designator
		["O_UAV_06_backpack_F",10,false],				// CSAT UAV Utility
		["O_UAV_06_medical_backpack_F",15,false],		// CSAT UAV Medical
		["C_IDAP_UAV_06_antimine_backpack_F",20,false],	// IDAP UAV Demining
		["O_UAV_01_backpack_F",15,false],				// CSAT UAV
		["O_HMG_01_support_F",5,false],					// CSAT Tripod
		["O_HMG_01_support_high_F",5,false],			// CSAT Tripod Raised
		["O_HMG_01_weapon_F",20,false],					// CSAT MG
		["O_HMG_01_high_weapon_F",20,false],			// CSAT MG Raised
		["O_HMG_01_A_weapon_F",35,false],				// CSAT MG Auto
		["O_GMG_01_weapon_F",20,false],					// CSAT GMG
		["O_GMG_01_high_weapon_F",20,false],			// CSAT GMG Raised
		["O_GMG_01_A_weapon_F",35,false],				// CSAT GMG Auto
		["O_Mortar_01_support_F",10,false],				// CSAT Mortar Base
		["O_Mortar_01_weapon_F",20,false],				// CSAT Mortar Tube
		["O_AT_01_weapon_F",30,false],					// CSAT AT
		["O_AA_01_weapon_F",30,false]					// CSAT AA
	]
];