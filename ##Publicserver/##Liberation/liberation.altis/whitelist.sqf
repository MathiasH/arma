// Here you can allow people to use the commander slot. It will only be enforced if you activate the related mission option.
// When editing be careful with quotes and commas

// Allowed team tags, as defined in your team's squad.xml
// This isn't very secure but efficient to whitelist a lot of people at once.
GRLIB_whitelisted_tags = [

];

// Allowed individual players based on their SteamID64. This is the most secure way to do.
// For example: "76561198016642627"
// To know that information: https://steamid.io/
GRLIB_whitelisted_steamids = [
	"76561198041043249",	//XlrT
	"76561197973996366",	//Barrazal
	"76561198001767153",	//ColdZero
	"76561197990557284",	//Phoenix
	"76561198026891445",	//Elchsocke
	"76561198086210494" 	//DrSnuggles
];

// Allowed individual player names. Note that this method is not very secure contrary to SteamIDs.
// For exemple: "Zbug"
GRLIB_whitelisted_names = [

];
