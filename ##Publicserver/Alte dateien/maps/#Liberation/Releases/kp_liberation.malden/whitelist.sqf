// Here you can allow people to use the commander slot. It will only be enforced if you activate the related mission option.
// When editing be careful with quotes and commas

// Allowed team tags, as defined in your team's squad.xml
// This isn't very secure but efficient to whitelist a lot of people at once.
GRLIB_whitelisted_tags = [

];

// Allowed individual players based on their SteamID64. This is the most secure way to do.
// For example: "76561198016642627"
// To know that information: https://steamid.io/
GRLIB_whitelisted_steamids = [
"76561197973996366", //Barrazal
"76561198126166020", //Preacher
"76561198343861261", //Benny
"76561197970870725", //Nachoman
"76561198050420274", //Bodochecker
"76561198241220806", //Christian
/*"76561197971067406", //Branco - EUCOR Mitglied und Publicspieler
"76561197988791038", //Scrappy - EUCOR Mitglied und Publicspieler
"76561198034373817", //donny aka donchau - EUCOR Mitglied und Publicspieler */
"76561198031740542", // basti aka lol.de
"76561198086210494", // DrSnuggles
"76561198069231227" // Martin
];

// Allowed individual player names. Note that this method is not very secure contrary to SteamIDs.
// For exemple: "Zbug"
GRLIB_whitelisted_names = [

];