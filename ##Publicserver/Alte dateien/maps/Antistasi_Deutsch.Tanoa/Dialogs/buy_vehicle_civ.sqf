private ["_display","_childControl"];
_nul = createDialog "civ_vehicle";

sleep 1;
disableSerialization;

_display = findDisplay 100;

if (str (_display) != "no display") then
{
	_ChildControl = _display displayCtrl 104;
	_ChildControl  ctrlSetTooltip format ["Cost: %1 €",[civCar] call vehiclePrice];
	_ChildControl = _display displayCtrl 105;
	_ChildControl  ctrlSetTooltip format ["Cost: %1 €",[civTruck] call vehiclePrice];
	_ChildControl = _display displayCtrl 106;
	_ChildControl  ctrlSetTooltip format ["Cost: %1 €",[civHeli] call vehiclePrice];
	_ChildControl = _display displayCtrl 107;
	//Requires to be near shoreline
	_ChildControl  ctrlSetTooltip format ["Cost: %1 €. Muss nahe am Wasser liegen",[vehSDKBoat] call vehiclePrice];
};