execVM "scripts\nre_earplugs.sqf";

_igiload = execVM "IgiLoad\IgiLoadInit.sqf";

tawvd_maxRange = 8000;
tawvd_disablenone = true;

if (isServer) then
{
	//ZeusModule = your curator module name; true = boolean, if civilians should be editable by zeus as well - set to false if you don't want civilians to be editable.
	[gm1,true] execVM "scripts\ADV_zeus.sqf";
	[gm2,true] execVM "scripts\ADV_zeus.sqf";
};

MAC_fnc_switchMove = {
    private["_object","_anim"];
    _object = _this select 0;
    _anim = _this select 1;

    _object switchMove _anim;
    
};