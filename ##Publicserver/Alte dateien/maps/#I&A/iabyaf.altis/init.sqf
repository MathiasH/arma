/*
@filename: init.sqf
Author:
	
	Quiksilver

Last modified:

	12/05/2014
	
Description:

	Things that may run on both server and client.
	Deprecated initialization file, still using until the below is correctly partitioned between server and client.
______________________________________________________*/

["Initialize"] call BIS_fnc_dynamicGroups;

for [ {_i = 0}, {_i < count(paramsArray)}, {_i = _i + 1} ] do {
	call compile format
	[
		"PARAMS_%1 = %2",
		(configName ((missionConfigFile >> "Params") select _i)),
		(paramsArray select _i)
	];
};


call compile preprocessFile "scripts\=BTC=_revive\=BTC=_revive_init.sqf";		// revive

execVM "fn_advancedSlingLoadingInit.sqf";
execVM "scripts\NRE_earplugs.sqf";
//_igiload = execVM "IgiLoad\IgiLoadInit.sqf";

missionNamespace setVariable ["SA_ASL_HEAVY_LIFTING_ENABLED",true,true];

_admin = ["admin1", "admin2", "admin3"];

if (!isServer && isMultiplayer) then {
 _whiteList = ["76561198055505556","76561198046660999","76561198072528201","76561198138033405"]; // enter ID's here
 waitUntil {!isNull player};
 if ((str player) in _admin) then {
   waitUntil {(getPlayerUID player) != ""};
   _id = getPlayerUID player;
   if (!(_id in _whiteList)) then {
     sleep 1;
     endMission "LOSER";
   }; 
 };
};
onPlayerConnected "if (_uid == 76561198072528201) then { cutText [""Admin Redthor Joined the game."", ""PLAIN""]; };";
onPlayerConnected "if (_uid == 76561198046660999) then { cutText [""Admin Shadow Joined the game."", ""PLAIN""]; };";
onPlayerConnected "if (_uid == 76561198055505556) then { cutText [""Admin Retaliator Joined the game."", ""PLAIN""]; };";
onPlayerConnected "if (_uid == 76561198138033405) then { cutText [""Admin Kyle Joined the game."", ""PLAIN""]; };";