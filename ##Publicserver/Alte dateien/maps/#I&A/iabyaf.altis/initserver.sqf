/*
@filename: initServer.sqf
Author:
	
	Quiksilver

Last modified:

	23/10/2014 ArmA 1.32 by Quiksilver
	
Description:

	Server scripts such as missions, modules, third party and clean-up.
	
______________________________________________________*/

//------------------------------------------------ Handle parameters

for [ {_i = 0}, {_i < count(paramsArray)}, {_i = _i + 1} ] do {
	call compile format
	[
		"PARAMS_%1 = %2",
		(configName ((missionConfigFile >> "Params") select _i)),
		(paramsArray select _i)
	];
};

//-------------------------------------------------- Server scripts

if (PARAMS_AO == 1) then { _null = [] execVM "mission\main\missionControl.sqf"; };						// Main AO
if (PARAMS_SideObjectives == 1) then { _null = [] execVM "mission\side\missionControl.sqf";};			// Side objectives		
_null = [] execVM "scripts\eos\OpenMe.sqf";																// EOS (urban mission and defend AO)
_null = [] execVM "scripts\misc\airbaseDefense.sqf";													// Airbase air defense
_null = [] execVM "scripts\misc\cleanup.sqf";															// cleanup
_null = [] execVM "scripts\misc\islandConfig.sqf";														// prep the island for mission
_null = [] execVM "scripts\misc\zeusupdater.sqf";														// zeus unit updater loop
if (PARAMS_EasterEggs == 1) then {_null = [] execVM "scripts\easterEggs.sqf";};							// Spawn easter eggs around the island
adminCurators = allCurators;
enableEnvironment FALSE;
BACO_ammoSuppAvail = true; publicVariable "BACO_ammoSuppAvail";

_admin = ["admin1", "admin2", "admin3"];

if (!isServer && isMultiplayer) then {
 _whiteList = ["76561198055505556","76561198072528201","76561198046660999","76561198138033405"]; // enter ID's here
 waitUntil {!isNull player};
 if ((str player) in _admin) then {
   waitUntil {(getPlayerUID player) != ""};
   _id = getPlayerUID player;
   if (!(_id in _whiteList)) then {
     sleep 1;
     endMission "LOSER";
   }; 
 };
};
onPlayerConnected "if (_uid == 76561198072528201) then { cutText [""Admin Redthor Joined the game."", ""PLAIN""]; };";
onPlayerConnected "if (_uid == 76561198046660999) then { cutText [""Admin Shadow Joined the game."", ""PLAIN""]; };";
onPlayerConnected "if (_uid == 76561198055505556) then { cutText [""Admin Retaliator Joined the game."", ""PLAIN""]; };";
onPlayerConnected "if (_uid == 76561198138033405) then { cutText [""Admin Kyle Joined the game."", ""PLAIN""]; };";