Simple demo mission to show basics of FFE setup. 

Preview in editor, use binoculars to keep enemy vehicle known, then wait. 
May pass about minute or more before first salvo, so be patient or accelerate time. 
Use map to follow debug markers, click on map to activate ShellView toll, that allows to see shells on the fly.  

See init.sqf file to learn how to customize and init FFE script. 