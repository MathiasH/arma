hint "Box script executed";

_crate = _this select 0;

_crate allowdamage false;   
_crate addAction ["<t color='#FF7722'>---------EMPTY----</t>", 			"leer.sqf"];
_crate addAction ["<t color='#3333FF'>---------------------</t>", 		""];
_crate addAction ["<t color='#3333FF'>-------STAFF-------</t>",			""];
_crate addAction ["<t color='#FF7722'>- Commander -</t>",				"loadouts\commander.sqf"];
_crate addAction ["<t color='#3333FF'>---------------------</t>", 		""];
_crate addAction ["<t color='#3333FF'>-------F�hrung-------</t>", 		""];
_crate addAction ["<t color='#FF7722'>- Team Leader </t>", 				"loadouts\squadleader.sqf"];
_crate addAction ["<t color='#FF7722'>- Team Leader (Funk) </t>", 		"loadouts\squadleader_funk.sqf"];
_crate addAction ["<t color='#3333FF'>---------------------</t>", 		""];
_crate addAction ["<t color='#3333FF'>----Mannschaften-----</t>", 		""];		
_crate addAction ["<t color='#FF7722'>- Schuetze </t>", 					"loadouts\schuetze.sqf"];
_crate addAction ["<t color='#FF7722'>- Schuetze (MG) </t>",				"loadouts\schuetze_mg.sqf"];	
_crate addAction ["<t color='#FF7722'>- Schuetze (AT) </t>",				"loadouts\schuetze_at.sqf"];
_crate addAction ["<t color='#FF7722'>- Funker </t>",			 		"loadouts\funker.sqf"];
_crate addAction ["<t color='#FF7722'>- Sanitaeter </t>",				"loadouts\sani.sqf"];	
_crate addAction ["<t color='#FF7722'>- Grenadier </t>",				"loadouts\sgrenadier.sqf"];
_crate addAction ["<t color='#FF7722'>- Marksmen </t>",					"loadouts\marksmen.sqf"];
_crate addAction ["<t color='#3333FF'>---------------------</t>", 		""];
_crate addAction ["<t color='#3333FF'>-------Sonderkr�fte-------</t>", 		""];
_crate addAction ["<t color='#FF7722'>- Sniper </t>",					"loadouts\sniper.sqf"];
_crate addAction ["<t color='#FF7722'>- Spotter </t>",					"loadouts\spotter.sqf"];
_crate addAction ["<t color='#FF7722'>- Pilot </t>",					"loadouts\pilot.sqf"];
_crate addAction ["<t color='#FF7722'>- Pionier </t>",					"loadouts\pionier.sqf"];
_crate addAction ["<t color='#FF7722'>- PanzerBesatzung (Kd/Funk) </t>",	"loadouts\panzer_funker.sqf"];
_crate addAction ["<t color='#FF7722'>- PanzerBesatzung (Repair) </t>",	"loadouts\panzer_repair.sqf"];
_crate addAction ["<t color='#3333FF'>---------------------</t>", 		""];

// läuft: nul =[this] execVM "loadoutbox.sqf"; für kiste
//_this select 1 addAction ["loadoutbox", execVM "loadoutbox.sqf", rad