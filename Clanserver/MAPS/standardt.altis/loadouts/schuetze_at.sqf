comment "Exported from Arsenal by [SOAF]Barrazal";

comment "Remove existing items";
removeAllWeapons player;
removeAllItems player;
removeAllAssignedItems player;
removeUniform player;
removeVest player;
removeBackpack player;
removeHeadgear player;
removeGoggles player;

comment "Add containers";
player forceAddUniform "U_B_CTRG_1";
for "_i" from 1 to 4 do {player addItemToUniform "SmokeShell";};
for "_i" from 1 to 2 do {player addItemToUniform "SmokeShellGreen";};
player addItemToUniform "SmokeShellRed";
for "_i" from 1 to 2 do {player addItemToUniform "16Rnd_9x21_Mag";};
player addVest "V_PlateCarrier1_rgr";
for "_i" from 1 to 4 do {player addItemToVest "FirstAidKit";};
player addItemToVest "Chemlight_blue";
for "_i" from 1 to 2 do {player addItemToVest "HandGrenade";};
player addItemToVest "Chemlight_green";
for "_i" from 1 to 4 do {player addItemToVest "30Rnd_65x39_caseless_mag";};
player addBackpack "B_AssaultPack_rgr";
for "_i" from 1 to 2 do {player addItemToBackpack "NLAW_F";};
player addHeadgear "H_HelmetB_grass";

comment "Add weapons";
player addWeapon "arifle_MX_F";
player addPrimaryWeaponItem "optic_Hamr";
player addWeapon "launch_NLAW_F";
player addWeapon "hgun_P07_F";
player addWeapon "Rangefinder";

comment "Add items";
player linkItem "ItemMap";
player linkItem "ItemCompass";
player linkItem "tf_microdagr";
player linkItem "tf_anprc152_7";
player linkItem "ItemGPS";
player linkItem "NVGoggles_INDEP";
