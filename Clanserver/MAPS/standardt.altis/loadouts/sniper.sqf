comment "Exported from Arsenal by [SOAF]Barrazal";

comment "Remove existing items";
removeAllWeapons player;
removeAllItems player;
removeAllAssignedItems player;
removeUniform player;
removeVest player;
removeBackpack player;
removeHeadgear player;
removeGoggles player;

comment "Add containers";
player forceAddUniform "U_B_FullGhillie_sard";
for "_i" from 1 to 3 do {player addItemToUniform "FirstAidKit";};
player addItemToUniform "Chemlight_blue";
player addItemToUniform "Chemlight_green";
player addItemToUniform "SmokeShellBlue";
player addItemToUniform "SmokeShellGreen";
for "_i" from 1 to 2 do {player addItemToUniform "SmokeShell";};
player addItemToUniform "SmokeShellPurple";
player addVest "V_Chestrig_oli";
for "_i" from 1 to 6 do {player addItemToVest "7Rnd_408_Mag";};
for "_i" from 1 to 2 do {player addItemToVest "16Rnd_9x21_Mag";};
player addBackpack "B_AssaultPack_rgr";
for "_i" from 1 to 2 do {player addItemToBackpack "APERSTripMine_Wire_Mag";};
player addItemToBackpack "ClaymoreDirectionalMine_Remote_Mag";

comment "Add weapons";
player addWeapon "srifle_LRR_camo_F";
player addPrimaryWeaponItem "optic_LRPS";
player addWeapon "hgun_P07_F";
player addHandgunItem "muzzle_snds_L";
player addWeapon "Laserdesignator";

comment "Add items";
player linkItem "ItemMap";
player linkItem "ItemCompass";
player linkItem "tf_microdagr";
player linkItem "tf_anprc152_8";
player linkItem "ItemGPS";
player linkItem "NVGoggles_INDEP";
