comment "Exported from Arsenal by [SOAF]Barrazal";

comment "Remove existing items";
removeAllWeapons player;
removeAllItems player;
removeAllAssignedItems player;
removeUniform player;
removeVest player;
removeBackpack player;
removeHeadgear player;
removeGoggles player;

comment "Add containers";
player forceAddUniform "U_B_CombatUniform_mcam_vest";
player addItemToUniform "FirstAidKit";
player addItemToUniform "SmokeShellBlue";
player addItemToUniform "16Rnd_9x21_Mag";
player addVest "V_Chestrig_khk";
player addBackpack "B_AssaultPack_mcamo";
player addItemToBackpack "ToolKit";
player addHeadgear "H_HelmetCrew_B";

comment "Add weapons";
player addWeapon "SMG_01_F";
player addPrimaryWeaponItem "optic_ACO_grn_smg";
player addWeapon "hgun_P07_F";

comment "Add items";
player linkItem "ItemMap";
player linkItem "ItemCompass";
player linkItem "tf_microdagr";
player linkItem "tf_anprc152_14";
player linkItem "ItemGPS";
player linkItem "NVGoggles_INDEP";
