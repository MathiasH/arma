
if (!isNil 'patient1') then {

    [patient1, true] call BIS_fnc_VRSpawnEffect;

    sleep 2;
    deleteVehicle patient1;

    systemChat 'Patient abgefertigt.'
} else {

    systemChat 'Kein Patient, bitte erst einen am Schlachtfeld suchen.';

}
