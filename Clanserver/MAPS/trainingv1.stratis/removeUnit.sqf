
if (!isNil 'patient') then {

    [patient, true] call BIS_fnc_VRSpawnEffect;

    sleep 2;
    deleteVehicle patient;

    systemChat 'Patient abgefertigt.'
} else {

    systemChat 'Kein Patient, bitte erst einen am Schlachtfeld suchen.';

}
