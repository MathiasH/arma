BattlEye Filters are time consuming and have to be changed depending on every small addon/script you do to your server/mission

Here is a guide on how to change them manually:
http://www.exilemod.com/topic/74-how-to-battleye-filters-do-it-yourself/?do=findComment&comment=1077
https://pastebin.com/9FBdjS1u

This is a program from one of the developers of Exilemod:
http://www.exilemod.com/topic/15394-battleye-automatic-script-exception-generator/
https://github.com/eraser1/BE_AEG
it automatically updates your scripts.txt / adds exceptions needed

I am not affiliated with Battleye in any way and if the filters provided by me are not working with your modified server / mission - you have to deal with it yourself.
