// PZF SCHÜTZE FLECKTARN
private ["_unit","_remote","_default"];


_unit = _this select 1;
_weapon =  (_this select 3) select 0;
_mag = (_this select 3) select 1;
_mag2 = (_this select 3) select 2;
_scope = (_this select 3) select 3;
_vest = (_this select 3) select 4;

_uavs = ["B_UAV_02_F","B_UAV_02_CAS_F","B_UAV_01_F","B_UGV_01_rcws_F","B_UGV01_F"];
if (typeOf _unit in _uavs) exitWith { hint "Netter versuch, aber das wird nichts";};
_uavs_AI = ["B_UAV_AI"];
if (typeOf _unit in _uavs_AI) exitWith { hint (localize "STR_GSG_UAVLoadout");};

hint (localize "STR_GSG_Container");
_remote = _unit;

//LOAD DEFAULT LOADOUT
//_default = [_remote, _weapon, _mag, _mag2, _scope, _vest] execVM "loadout\default_mtp.sqf";
//waitUntil {scriptDone _default};

removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

_unit forceAddUniform "U_B_Wetsuit";
_unit addItemToUniform "FirstAidKit";
_unit addItemToUniform "FirstAidKit";
_unit addItemToUniform "FirstAidKit";

hint "Krame in Container. Ahh meine Weste. Bitte warten ....";

_unit addVest _vest;

_unit addBackpack "B_AssaultPack_blk";
_unit addItemToBackpack "SmokeShell";
_unit addItemToBackpack "SmokeShell";
_unit addItemToBackpack "SmokeShell";
_unit addItemToBackpack "SmokeShell";
_unit addItemToBackpack "SmokeShellGreen";
_unit addItemToBackpack "SmokeShellGreen";
_unit addItemToBackpack "SmokeShellBlue";
_unit addItemToBackpack _mag;
_unit addItemToBackpack _mag;
_unit addItemToBackpack _mag;
_unit addItemToBackpack _mag;
_unit addItemToBackpack _mag;
_unit addItemToBackpack _mag;
_unit addItemToBackpack _mag;
_unit addItemToBackpack _mag2;
_unit addItemToBackpack _mag2;
_unit addItemToBackpack _mag2;
_unit addItemToBackpack _mag2;
_unit addItemToBackpack "HandGrenade";
_unit addItemToBackpack "HandGrenade";
_unit addItemToBackpack "DemoCharge_Remote_Mag";

hint "Krame in Container. Ahh Waffen. Bitte warten ....";

_unit addWeapon _weapon;
//_unit addPrimaryWeaponItem _scope;

_unit linkItem "ItemCompass";
_unit linkItem "ItemWatch";
_unit linkItem "ItemRadio";
_unit linkItem "NVGoggles";
_unit linkItem "ItemMap";
_unit linkItem "ItemGPS";

_unit addItemToUniform "16Rnd_9x21_Mag";
_unit addItemToUniform "16Rnd_9x21_Mag";

_unit addWeapon "hgun_P07_F";

_unit addHeadgear "H_HelmetSpecB_paint2";
_unit addGoggles "G_B_Diving";
_unit addWeapon "Binocular";

hint (localize "STR_GSG_LoadoutDone");
sleep 15;
hint "";