/*
* Copyright (c) 2016, GSG Grenzschutzgruppe
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted.
*
* _box SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF _box SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*    Author:           Apus
*    Version:          1.00
*    Date:             29.11.2016
*/


_box = _this select 0;
//_class = _this select 1;

if (isServer) then 
{  
	clearWeaponCargoGlobal _box; 
	clearMagazineCargoGlobal _box; 
	clearItemCargoGlobal _box; 
	clearBackpackCargoGlobal _box;
	_box setVariable ["R3F_LOG_disabled", true];
};     

//Sch�tze
//null = [this,schuetze] execVM "loadout\init_box.sqf";
if (_this select 1 == schuetze) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP    ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>--------- Manschaften --------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>--------- 	Sch�tze	 ---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["MX","loadout\schuetze.sqf", 			["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["SPAR 16","loadout\schuetze.sqf", 		["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["TRG 21","loadout\schuetze.sqf", 		["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["MK 20","loadout\schuetze.sqf", 		["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];     
	_box addAction ["Katiba","loadout\schuetze.sqf",		["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95","loadout\schuetze.sqf",		["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
};

//Grenadier
//null = [this,grenadier] execVM "loadout\init_box.sqf";
if (_this select 1 == grenadier) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP     ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>--------- Manschaften ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>---------  Grenadier  ---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["MX","loadout\gren.sqf", 				["arifle_MX_GL_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["SPAR 16","loadout\gren.sqf", 			["arifle_SPAR_01_GL_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["TRG 21","loadout\gren.sqf", 			["arifle_TRG21_GL_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20","loadout\gren.sqf", 			["arifle_Mk20_GL_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Katiba","loadout\gren.sqf", 			["arifle_Katiba_GL_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95","loadout\gren.sqf", 			["arifle_CTAR_GL_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
};  

//Panzerabwehr
//null = [this,pzf] execVM "loadout\init_box.sqf";
if (_this select 1 == pzf) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP      ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>--------- Manschaften  ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>--------- Panzerabwehr ---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["MX + PCML","loadout\atpcml.sqf", 		["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["SPAR 16 + PCML","loadout\atpcml.sqf", 	["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["TRG 21 + PCML","loadout\atpcml.sqf", 	["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20 + PCML","loadout\atpcml.sqf", 	["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Katiba + PCML","loadout\atpcml.sqf", 	["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95 + PCML","loadout\atpcml.sqf", 	["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["MX + Titan","loadout\attitan.sqf", 	["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["SPAR 16 + Titan","loadout\attitan.sqf",["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["TRG 21 + Titan","loadout\attitan.sqf", ["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20 + Titan","loadout\attitan.sqf", 	["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Katiba + Titan","loadout\attitan.sqf", ["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95 + Titan","loadout\attitan.sqf", ["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
};

//Flugabwehr
//null = [this,aa] execVM "loadout\init_box.sqf";
if (_this select 1 == aa) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP     ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>--------- Manschaften ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>--------- Flugabwehr  ---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["MX","loadout\aa.sqf", 					["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["SPAR 16","loadout\aa.sqf", 			["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["TRG 21","loadout\aa.sqf", 				["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20","loadout\aa.sqf", 				["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Katiba","loadout\aa.sqf", 				["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95","loadout\aa.sqf", 				["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
};

//Pionier
//null = [this,pio] execVM "loadout\init_box.sqf";
if (_this select 1 == pio) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP     ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>--------- Manschaften ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>---------   Pionier   ---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["MX","loadout\pio.sqf", 				["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["SPAR 16","loadout\pio.sqf", 			["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["TRG 21","loadout\pio.sqf", 			["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20","loadout\pio.sqf", 				["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Katiba","loadout\pio.sqf", 			["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95","loadout\pio.sqf", 			["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
};

//Sanit�ter
//null = [this,sani] execVM "loadout\init_box.sqf";
if (_this select 1 == sani) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP     ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>--------- Manschaften ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>---------  Sanit�ter  ---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["MX","loadout\sani.sqf", 				["arifle_MXC_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["SPAR 16","loadout\sani.sqf", 			["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["TRG 21","loadout\sani.sqf", 			["arifle_TRG20_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20","loadout\sani.sqf", 			["arifle_Mk20C_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Katiba","loadout\sani.sqf", 			["arifle_Katiba_C_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95","loadout\sani.sqf", 			["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
};

//GrpScharfsch�tze
//null = [this,gss] execVM "loadout\init_box.sqf";
if (_this select 1 == gss) then
{
	_box addAction ["<t color='#2E8B57'>---------     	MTP   	   ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>--------- 	Manschaften    ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>--------- GrpScharfsch�tze ---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["MK 18","loadout\gss.sqf", 				["srifle_EBR_F","20Rnd_762x51_Mag","20Rnd_762x51_Mag","optic_DMS","V_PlateCarrier1_rgr","bipod_01_F_blk"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["Cyrus","loadout\gss.sqf", 				["srifle_DMR_05_tan_f","10Rnd_93x64_DMR_05_Mag","10Rnd_93x64_DMR_05_Mag","optic_AMS","V_PlateCarrier1_rgr","bipod_01_F_blk"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK-1","loadout\gss.sqf", 				["srifle_DMR_03_tan_F","20Rnd_762x51_Mag","20Rnd_762x51_Mag","optic_AMS_snd","V_PlateCarrier1_rgr","bipod_01_F_snd"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK14","loadout\gss.sqf", 				["srifle_DMR_06_olive_F","20Rnd_762x51_Mag","20Rnd_762x51_Mag","optic_AMS_khk","V_PlateCarrier1_rgr","bipod_01_F_khk"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MXM","loadout\gss.sqf", 				["arifle_MXM_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_DMS","V_PlateCarrier1_rgr","bipod_01_F_snd"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["SPAR 17","loadout\gss.sqf", 			["arifle_SPAR_03_snd_F","20Rnd_762x51_Mag","20Rnd_762x51_Mag","optic_AMS_snd","V_PlateCarrier1_rgr","bipod_01_F_snd"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["CMR-76","loadout\gss.sqf", 			["srifle_DMR_07_blk_F","20Rnd_650x39_Cased_Mag_F","20Rnd_650x39_Cased_Mag_F","optic_AMS","V_PlateCarrier1_rgr","bipod_01_F_blk"],1,false,true,"","_this distance _target < 2"];   
};

//Scharfsch�tze
//null = [this,sniper] execVM "loadout\init_box.sqf";
if (_this select 1 == sniper) then
{
	_box addAction ["<t color='#2E8B57'>---------      MTP      ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>---------  Manschaften  ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>--------- Scharfsch�tze ---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["M320 LRR","loadout\sniper.sqf", 		["srifle_LRR_F","7Rnd_408_Mag","7Rnd_408_Mag","optic_LRPS","V_PlateCarrier1_rgr","U_B_GhillieSuit"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["Lynx","loadout\sniper.sqf", 			["srifle_GM6_F","5Rnd_127x108_Mag","5Rnd_127x108_APDS_Mag","optic_LRPS","V_PlateCarrier1_rgr","U_B_GhillieSuit"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MAR 10","loadout\sniper.sqf", 			["srifle_DMR_02_F","10Rnd_338_Mag","10Rnd_338_Mag","optic_LRPS","V_PlateCarrier1_rgr","U_B_FullGhillie_sard"],1,false,true,"","_this distance _target < 2"];   
};

//Spotter
//null = [this,spotter] execVM "loadout\init_box.sqf";
if (_this select 1 == spotter) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP     ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>--------- Manschaften ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>--------- 	Spotter   ---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["MK 18","loadout\spotter.sqf", 			["srifle_EBR_F","20Rnd_762x51_Mag","20Rnd_762x51_Mag","optic_DMS","V_PlateCarrier1_rgr","U_B_GhillieSuit","bipod_01_F_blk"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["Cyrus","loadout\spotter.sqf", 			["srifle_DMR_05_tan_f","10Rnd_93x64_DMR_05_Mag","10Rnd_93x64_DMR_05_Mag","optic_AMS","V_PlateCarrier1_rgr","U_B_FullGhillie_sard","bipod_01_F_blk"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK-1","loadout\spotter.sqf", 			["srifle_DMR_03_tan_F","20Rnd_762x51_Mag","20Rnd_762x51_Mag","optic_AMS_snd","V_PlateCarrier1_rgr","U_B_FullGhillie_sard","bipod_01_F_snd"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK14","loadout\spotter.sqf", 			["srifle_DMR_06_olive_F","20Rnd_762x51_Mag","20Rnd_762x51_Mag","optic_AMS_khk","V_PlateCarrier1_rgr","U_B_FullGhillie_sard","bipod_01_F_khk"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MXM","loadout\spotter.sqf", 			["arifle_MXM_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_DMS","V_PlateCarrier1_rgr","U_B_GhillieSuit","bipod_01_F_khk"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["SPAR 17","loadout\spotter.sqf", 		["arifle_SPAR_03_snd_F","20Rnd_762x51_Mag","20Rnd_762x51_Mag","optic_AMS_snd","V_PlateCarrier1_rgr","U_B_FullGhillie_sard","bipod_01_F_khk"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["CMR-76","loadout\spotter.sqf", 		["srifle_DMR_07_blk_F","20Rnd_650x39_Cased_Mag_F","20Rnd_650x39_Cased_Mag_F","optic_AMS","V_PlateCarrier1_rgr","U_B_FullGhillie_sard","bipod_01_F_khk"],1,false,true,"","_this distance _target < 2"];   
};

//UAV
//null = [this,uav] execVM "loadout\init_box.sqf";
if (_this select 1 == uav) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP     ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>--------- Manschaften ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>---------  	 UAV 	  ---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["MX","loadout\uav.sqf", 				["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["SPAR 16","loadout\uav.sqf", 			["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["TRG 21","loadout\uav.sqf", 			["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20","loadout\uav.sqf", 				["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Katiba","loadout\uav.sqf", 			["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95","loadout\uav.sqf", 			["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
};

//Gruppenf�hrer
//null = [this,grpfhr] execVM "loadout\init_box.sqf";
if (_this select 1 == grpfhr) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP    ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>--------- 	F�hrung --------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>---------Gruppenf�hrer---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["MX","loadout\grpfhr.sqf", 				["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["SPAR 16","loadout\grpfhr.sqf", 		["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["TRG 21","loadout\grpfhr.sqf", 			["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20","loadout\grpfhr.sqf", 			["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Katiba","loadout\grpfhr.sqf",			["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95","loadout\grpfhr.sqf",			["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
};

//Truppenf�hrer
//null = [this,trpfhr] execVM "loadout\init_box.sqf";
if (_this select 1 == trpfhr) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP    ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>--------- 	F�hrung --------</t>","", [],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["<t color='#2E8B57'>---------Truppenf�hrer---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["MX","loadout\trpfhr.sqf", 				["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["SPAR 16","loadout\trpfhr.sqf", 		["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["TRG 21","loadout\trpfhr.sqf", 			["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20","loadout\trpfhr.sqf", 			["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Katiba","loadout\trpfhr.sqf",			["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95","loadout\trpfhr.sqf",			["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
};

//JTAC
//null = [this,jtac] execVM "loadout\init_box.sqf";
if (_this select 1 == jtac) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP    ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>--------- Manschaften --------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>---------    JTAC    ---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["MX","loadout\jtac.sqf", 				["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["SPAR 16","loadout\jtac.sqf", 			["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["TRG 21","loadout\jtac.sqf", 			["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20","loadout\jtac.sqf", 			["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Katiba","loadout\jtac.sqf",			["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95","loadout\jtac.sqf",			["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
};

//Taucher
//null = [this,taucher] execVM "loadout\init_box.sqf";
if (_this select 1 == taucher) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP    ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>--------- Manschaften --------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>---------   Taucher  ---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["SDAR","loadout\taucher.sqf", 			["arifle_SDAR_F","20Rnd_556x45_UW_mag","30Rnd_556x45_Stanag_red","optic_Arco_blk_F","V_RebreatherB"],1,false,true,"","_this distance _target < 2"];
};

//MG - Sch�tze
//null = [this,mg] execVM "loadout\init_box.sqf";
if (_this select 1 == mg) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP      ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>--------- Manschaften  ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>--------- MG - Sch�tze ---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["LIM-85","loadout\mg.sqf", 				["LMG_03_F","200Rnd_556x45_Box_F","200Rnd_556x45_Box_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr","bipod_01_F_blk"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["MK 200","loadout\mg.sqf", 				["LMG_Mk200_F","200Rnd_65x39_cased_Box","200Rnd_65x39_cased_Box_Tracer","optic_Arco","V_PlateCarrier1_rgr","bipod_01_F_blk"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Navid","loadout\mg.sqf", 				["MMG_01_tan_F","150Rnd_93x64_Mag","150Rnd_93x64_Mag","optic_Arco","V_PlateCarrier1_rgr","bipod_01_F_blk"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["SPMG","loadout\mg.sqf", 				["MMG_02_sand_F","130Rnd_338_Mag","130Rnd_338_Mag","optic_Arco","V_PlateCarrier1_rgr","bipod_01_F_snd"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Zafir","loadout\mg.sqf", 				["LMG_Zafir_F","150Rnd_762x54_Box","150Rnd_762x54_Box_Tracer","optic_Arco","V_PlateCarrier1_rgr","bipod_01_F_blk"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95-1","loadout\mg.sqf", 			["arifle_CTARS_blk_F","100Rnd_580x42_Mag_F","100Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr","bipod_01_F_blk"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["MX-SW","loadout\mg.sqf", 				["arifle_MX_SW_F","100Rnd_65x39_caseless_mag","100Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","bipod_01_F_snd"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["SPAR 16-S","loadout\mg.sqf", 			["arifle_SPAR_02_snd_F","150Rnd_556x45_Drum_Mag_F","150Rnd_556x45_Drum_Mag_Tracer_F","optic_Arco","V_PlateCarrier1_rgr","bipod_01_F_snd"],1,false,true,"","_this distance _target < 2"];
};

//MG -  Sch�tze 2
//null = [this,mg2] execVM "loadout\init_box.sqf";
if (_this select 1 == mg2) then
{
	_box addAction ["<t color='#2E8B57'>---------      MTP      ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>---------  Manschaften  ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>--------- MG2 - Sch�tze ---------</t>","", [],1,false,true,"","_this distance _target < 2"];
	_box addAction ["<t color='#2E8B57'>--------- MX ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["MX/LIM-85","loadout\mg2.sqf", 			["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","200Rnd_556x45_Box_F","200Rnd_556x45_Box_Tracer_F"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["MX/MK 200","loadout\mg2.sqf", 			["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","200Rnd_65x39_cased_Box","200Rnd_65x39_cased_Box_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MX/Navid","loadout\mg2.sqf", 			["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","150Rnd_93x64_Mag","150Rnd_93x64_Mag"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MX/SPMG","loadout\mg2.sqf", 			["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","130Rnd_338_Mag","130Rnd_338_Mag"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MX/Zafir","loadout\mg2.sqf", 			["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","150Rnd_762x54_Box","150Rnd_762x54_Box_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MX/CAR 95-1","loadout\mg2.sqf", 		["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","100Rnd_580x42_Mag_F","100Rnd_580x42_Mag_Tracer_F"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["MX/MX-SW","loadout\mg2.sqf", 			["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","100Rnd_65x39_caseless_mag","100Rnd_65x39_caseless_mag_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MX/SPAR 16-S","loadout\mg2.sqf", 		["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","150Rnd_556x45_Drum_Mag_F","150Rnd_556x45_Drum_Mag_Tracer_F"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["<t color='#2E8B57'>--------- SPAR 16 ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["SPAR 16/LIM-85","loadout\mg2.sqf", 	["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","200Rnd_556x45_Box_F","200Rnd_556x45_Box_Tracer_F"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["SPAR 16/MK 200","loadout\mg2.sqf", 	["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","200Rnd_65x39_cased_Box","200Rnd_65x39_cased_Box_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["SPAR 16/Navid","loadout\mg2.sqf", 		["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","150Rnd_93x64_Mag","150Rnd_93x64_Mag"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["SPAR 16/SPMG","loadout\mg2.sqf", 		["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","130Rnd_338_Mag","130Rnd_338_Mag"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["SPAR 16/Zafir","loadout\mg2.sqf", 		["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","150Rnd_762x54_Box","150Rnd_762x54_Box_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["SPAR 16/CAR 95-1","loadout\mg2.sqf", 	["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","100Rnd_580x42_Mag_F","100Rnd_580x42_Mag_Tracer_F"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["SPAR 16/MX-SW","loadout\mg2.sqf", 		["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","100Rnd_65x39_caseless_mag","100Rnd_65x39_caseless_mag_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["SPAR 16/SPAR 16-S","loadout\mg2.sqf", 	["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","150Rnd_556x45_Drum_Mag_F","150Rnd_556x45_Drum_Mag_Tracer_F"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["<t color='#2E8B57'>--------- TRG 21 ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["TRG 21/LIM-85","loadout\mg2.sqf", 		["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","200Rnd_556x45_Box_F","200Rnd_556x45_Box_Tracer_F"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["TRG 21/MK 200","loadout\mg2.sqf", 		["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","200Rnd_65x39_cased_Box","200Rnd_65x39_cased_Box_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["TRG 21/Navid","loadout\mg2.sqf", 		["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","150Rnd_93x64_Mag","150Rnd_93x64_Mag"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["TRG 21/SPMG","loadout\mg2.sqf", 		["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","130Rnd_338_Mag","130Rnd_338_Mag"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["TRG 21/Zafir","loadout\mg2.sqf", 		["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","150Rnd_762x54_Box","150Rnd_762x54_Box_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["TRG 21/CAR 95-1","loadout\mg2.sqf", 	["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","100Rnd_580x42_Mag_F","100Rnd_580x42_Mag_Tracer_F"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["TRG 21/MX-SW","loadout\mg2.sqf", 		["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","100Rnd_65x39_caseless_mag","100Rnd_65x39_caseless_mag_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["TRG 21/SPAR 16-S","loadout\mg2.sqf", 	["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","150Rnd_556x45_Drum_Mag_F","150Rnd_556x45_Drum_Mag_Tracer_F"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["<t color='#2E8B57'>--------- MK 20 ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["MK 20/LIM-85","loadout\mg2.sqf", 		["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","200Rnd_556x45_Box_F","200Rnd_556x45_Box_Tracer_F"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["MK 20/MK 200","loadout\mg2.sqf", 		["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","200Rnd_65x39_cased_Box","200Rnd_65x39_cased_Box_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20/Navid","loadout\mg2.sqf", 		["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","150Rnd_93x64_Mag","150Rnd_93x64_Mag"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20/SPMG","loadout\mg2.sqf", 		["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","130Rnd_338_Mag","130Rnd_338_Mag"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20/Zafir","loadout\mg2.sqf", 		["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","150Rnd_762x54_Box","150Rnd_762x54_Box_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20/CAR 95-1","loadout\mg2.sqf", 	["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","100Rnd_580x42_Mag_F","100Rnd_580x42_Mag_Tracer_F"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["MK 20/MX-SW","loadout\mg2.sqf", 		["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","100Rnd_65x39_caseless_mag","100Rnd_65x39_caseless_mag_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20/SPAR 16-S","loadout\mg2.sqf", 	["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","150Rnd_556x45_Drum_Mag_F","150Rnd_556x45_Drum_Mag_Tracer_F"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["<t color='#2E8B57'>--------- Katiba ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["Katiba/LIM-85","loadout\mg2.sqf", 		["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","200Rnd_556x45_Box_F","200Rnd_556x45_Box_Tracer_F"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["Katiba/MK 200","loadout\mg2.sqf", 		["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","200Rnd_65x39_cased_Box","200Rnd_65x39_cased_Box_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Katiba/Navid","loadout\mg2.sqf", 		["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","150Rnd_93x64_Mag","150Rnd_93x64_Mag"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Katiba/SPMG","loadout\mg2.sqf", 		["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","130Rnd_338_Mag","130Rnd_338_Mag"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Katiba/Zafir","loadout\mg2.sqf", 		["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","150Rnd_762x54_Box","150Rnd_762x54_Box_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Katiba/CAR 95-1","loadout\mg2.sqf", 	["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","100Rnd_580x42_Mag_F","100Rnd_580x42_Mag_Tracer_F"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["Katiba/MX-SW","loadout\mg2.sqf", 		["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","100Rnd_65x39_caseless_mag","100Rnd_65x39_caseless_mag_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Katiba/SPAR 16-S","loadout\mg2.sqf", 	["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","150Rnd_556x45_Drum_Mag_F","150Rnd_556x45_Drum_Mag_Tracer_F"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["<t color='#2E8B57'>--------- CAR 95 ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["CAR 95/LIM-85","loadout\mg2.sqf", 		["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr","200Rnd_556x45_Box_F","200Rnd_556x45_Box_Tracer_F"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["CAR 95/MK 200","loadout\mg2.sqf", 		["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr","200Rnd_65x39_cased_Box","200Rnd_65x39_cased_Box_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95/Navid","loadout\mg2.sqf", 		["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr","150Rnd_93x64_Mag","150Rnd_93x64_Mag"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95/SPMG","loadout\mg2.sqf", 		["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr","130Rnd_338_Mag","130Rnd_338_Mag"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95/Zafir","loadout\mg2.sqf", 		["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr","150Rnd_762x54_Box","150Rnd_762x54_Box_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95/CAR 95-1","loadout\mg2.sqf", 	["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr","100Rnd_580x42_Mag_F","100Rnd_580x42_Mag_Tracer_F"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["CAR 95/MX-SW","loadout\mg2.sqf", 		["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr","100Rnd_65x39_caseless_mag","100Rnd_65x39_caseless_mag_Tracer"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95/SPAR 16-S","loadout\mg2.sqf", 	["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr","150Rnd_556x45_Drum_Mag_F","150Rnd_556x45_Drum_Mag_Tracer_F"],1,false,true,"","_this distance _target < 2"];
};

//Helikopterpilot
//null = [this,hpil] execVM "loadout\init_box.sqf";
if (_this select 1 == hpil) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP    ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>---------   Logistik --------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>---------Helikopterpilot---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["MXC","loadout\pil.sqf", 				["arifle_MXC_F","30Rnd_65x39_caseless_mag","U_B_HeliPilotCoveralls","optic_Holosight_blk_F","V_TacVestIR_blk","H_PilotHelmetHeli_B","B_AssaultPack_blk"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["PDW","loadout\pil.sqf", 				["hgun_PDW2000_F","30Rnd_9x21_Mag","U_B_HeliPilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetHeli_B","B_AssaultPack_blk"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MP5","loadout\pil.sqf", 				["SMG_05_F","30Rnd_9x21_Mag_SMG_02","U_B_HeliPilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetHeli_B","B_AssaultPack_blk"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Sting","loadout\pil.sqf", 				["SMG_02_F","30Rnd_9x21_Mag_SMG_02","U_B_HeliPilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetHeli_B","B_AssaultPack_blk"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Vermin","loadout\pil.sqf",				["SMG_01_F","30Rnd_45ACP_Mag_SMG_01","U_B_HeliPilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetHeli_B","B_AssaultPack_blk"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20C","loadout\pil.sqf",				["arifle_Mk20C_plain_F","30Rnd_556x45_Stanag","U_B_HeliPilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetHeli_B","B_AssaultPack_blk"],1,false,true,"","_this distance _target < 2"];
};

//Jetpilot
//null = [this,jpil] execVM "loadout\init_box.sqf";
if (_this select 1 == jpil) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP    ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>---------   Logistik --------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>---------	Jetpilot---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["MXC","loadout\pil.sqf", 				["arifle_MXC_F","30Rnd_65x39_caseless_mag","U_B_PilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetFighter_B","B_Parachute"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["PDW","loadout\pil.sqf", 				["hgun_PDW2000_F","30Rnd_9x21_Mag","U_B_PilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetFighter_B","B_Parachute"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MP5","loadout\pil.sqf", 				["SMG_05_F","30Rnd_9x21_Mag_SMG_02","U_B_PilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetFighter_B","B_Parachute"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Sting","loadout\pil.sqf", 				["SMG_02_F","30Rnd_9x21_Mag_SMG_02","U_B_PilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetFighter_B","B_Parachute"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Vermin","loadout\pil.sqf",				["SMG_01_F","30Rnd_45ACP_Mag_SMG_01","U_B_PilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetFighter_B","B_Parachute"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20C","loadout\pil.sqf",				["arifle_Mk20C_plain_F","30Rnd_556x45_Stanag","U_B_PilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetFighter_B","B_Parachute"],1,false,true,"","_this distance _target < 2"];
};

//Ingenieur
//null = [this,ing] execVM "loadout\init_box.sqf";
if (_this select 1 == ing) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP    ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>---------   Logistik ---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["<t color='#2E8B57'>---------  Ingenieur ---------</t>","", [],1,false,true,"","_this distance _target < 2"]; 
	_box addAction ["MX","loadout\ing.sqf", 				["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["SPAR 16","loadout\ing.sqf", 			["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["TRG 21","loadout\ing.sqf", 			["arifle_TRG21_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["MK 20","loadout\ing.sqf", 				["arifle_Mk20_plain_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Katiba","loadout\ing.sqf",				["arifle_Katiba_F","30Rnd_65x39_caseless_green","30Rnd_65x39_caseless_green_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["CAR 95","loadout\ing.sqf",				["arifle_CTAR_blk_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_Tracer_F","optic_Arco_blk_F","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 2"];
};

//Zugf�hrung
//null = [this,zugfhr] execVM "loadout\init_box.sqf";
if (_this select 1 == zugfhr) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP   ---------</t>","", [],1,false,true,"","_this distance _target < 2"];     
	_box addAction ["<t color='#2E8B57'>--------- 	F�hrung ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>--------- Zugf�hrung---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["4-five .45 ACP","loadout\zugfhr.sqf", 	["hgun_Pistol_heavy_01_F","11Rnd_45ACP_Mag","U_B_CombatUniform_mcam_vest","optic_MRD","V_Rangemaster_belt"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["ACP-C2 .45 ACP","loadout\zugfhr.sqf", 	["hgun_ACPC2_F","9Rnd_45ACP_Mag","U_B_CombatUniform_mcam_vest","optic_MRD","V_Rangemaster_belt"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["P07","loadout\zugfhr.sqf", 			["hgun_P07_F","16Rnd_9x21_Mag","U_B_CombatUniform_mcam_vest","optic_MRD","V_Rangemaster_belt"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["PM 9mm","loadout\zugfhr.sqf", 			["hgun_Pistol_01_F","10Rnd_9x21_Mag","U_B_CombatUniform_mcam_vest","optic_MRD","V_Rangemaster_belt"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Rook-40 9mm","loadout\zugfhr.sqf",		["hgun_Rook40_F","16Rnd_9x21_Mag","U_B_CombatUniform_mcam_vest","optic_MRD","V_Rangemaster_belt"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Zubr .45 ACP","loadout\zugfhr.sqf",	["hgun_Pistol_heavy_02_F","6Rnd_45ACP_Cylinder","U_B_CombatUniform_mcam_vest","optic_Yorris","V_Rangemaster_belt"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["<t color='#2E8B57'>---------    Dunkel   ---------</t>","", [],1,false,true,"","_this distance _target < 2"];     
	_box addAction ["<t color='#2E8B57'>--------- 	F�hrung ---------</t>","", [],1,false,true,"","_this distance _target < 2"];    
	_box addAction ["<t color='#2E8B57'>--------- Zugf�hrung---------</t>","", [],1,false,true,"","_this distance _target < 2"];  
	_box addAction ["4-five .45 ACP","loadout\zugfhr.sqf", 	["hgun_Pistol_heavy_01_F","11Rnd_45ACP_Mag","U_B_GEN_Commander_F","optic_MRD","V_Rangemaster_belt"],1,false,true,"","_this distance _target < 2"];
	_box addAction ["ACP-C2 .45 ACP","loadout\zugfhr.sqf", 	["hgun_ACPC2_F","9Rnd_45ACP_Mag","U_B_GEN_Commander_F","optic_MRD","V_Rangemaster_belt"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["P07","loadout\zugfhr.sqf", 			["hgun_P07_F","16Rnd_9x21_Mag","U_B_GEN_Commander_F","optic_MRD","V_Rangemaster_belt"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["PM 9mm","loadout\zugfhr.sqf", 			["hgun_Pistol_01_F","10Rnd_9x21_Mag","U_B_GEN_Commander_F","optic_MRD","V_Rangemaster_belt"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Rook-40 9mm","loadout\zugfhr.sqf",		["hgun_Rook40_F","16Rnd_9x21_Mag","U_B_GEN_Commander_F","optic_MRD","V_Rangemaster_belt"],1,false,true,"","_this distance _target < 2"];   
	_box addAction ["Zubr .45 ACP","loadout\zugfhr.sqf",	["hgun_Pistol_heavy_02_F","6Rnd_45ACP_Cylinder","U_B_GEN_Commander_F","optic_Yorris","V_Rangemaster_belt"],1,false,true,"","_this distance _target < 2"];
};
