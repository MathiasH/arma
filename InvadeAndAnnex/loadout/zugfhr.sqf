// PZF SCHÜTZE FLECKTARN
private ["_unit","_remote","_default"];


_unit = _this select 1;
_weapon =  (_this select 3) select 0;
_mag = (_this select 3) select 1;
_uniform = (_this select 3) select 2;
_scope = (_this select 3) select 3;
_vest = (_this select 3) select 4;

_uavs = ["B_UAV_02_F","B_UAV_02_CAS_F","B_UAV_01_F","B_UGV_01_rcws_F","B_UGV01_F"];
if (typeOf _unit in _uavs) exitWith { hint "Netter versuch, aber das wird nichts";};
_uavs_AI = ["B_UAV_AI"];
if (typeOf _unit in _uavs_AI) exitWith { hint (localize "STR_GSG_UAVLoadout");};

removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

hint (localize "STR_GSG_Container");
_remote = _unit;

//LOAD DEFAULT LOADOUT
//_default = [_remote, _weapon, _mag, _mag2, _scope, _vest] execVM "loadout\default_mtp.sqf";
//waitUntil {scriptDone _default};

_unit forceAddUniform _uniform;
_unit addItemToUniform "FirstAidKit";
_unit addItemToUniform "FirstAidKit";
_unit addItemToUniform "FirstAidKit";
_unit addItemToUniform _mag;
_unit addItemToUniform _mag;

_unit addVest _vest;
_unit addItemToVest _mag;
_unit addItemToVest _mag;
_unit addItemToVest "NVGoggles";
_unit addItemToVest "SmokeShell";

_unit addWeapon _weapon;
_unit addHandgunItem _scope;

_unit addHeadgear "H_Beret_blk";

_unit addWeapon "Rangefinder";

_unit linkItem "ItemCompass";
_unit linkItem "ItemWatch";
_unit linkItem "ItemRadio";
_unit linkItem "ItemMap";
_unit linkItem "ItemGPS";

hint (localize "STR_GSG_LoadoutDone");
sleep 15;
hint "";