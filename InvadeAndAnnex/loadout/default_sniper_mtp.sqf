private ["_unit", "_this", "_vest"];

_unit = _this select 0;
_weapon = _this select 1;
_mag = _this select 2;
_mag2 = _this select 3;
_scope = _this select 4;
_vest = _this select 5;
_uniform = _this select 6;

removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

_unit forceAddUniform _uniform;
_unit addItemToUniform "FirstAidKit";
_unit addItemToUniform "FirstAidKit";
_unit addItemToUniform "FirstAidKit";

hint "Krame in Container. Ahh meine Weste. Bitte warten ....";

_unit addVest _vest;

_unit addItemToVest "SmokeShell";
_unit addItemToVest "SmokeShell";
_unit addItemToVest "SmokeShell";
_unit addItemToVest "SmokeShell";
_unit addItemToVest "SmokeShellGreen";
_unit addItemToVest "SmokeShellGreen";
_unit addItemToVest "SmokeShellBlue";
_unit addItemToVest _mag;
_unit addItemToVest _mag;
_unit addItemToVest _mag;
_unit addItemToVest _mag;
_unit addItemToVest _mag;
_unit addItemToVest _mag2;
_unit addItemToVest _mag2;
_unit addItemToVest "HandGrenade";
_unit addItemToVest "HandGrenade";

hint "Krame in Container. Ahh Waffen. Bitte warten ....";

_unit addWeapon _weapon;
_unit addPrimaryWeaponItem _scope;

_unit linkItem "ItemCompass";
_unit linkItem "ItemWatch";
_unit linkItem "ItemRadio";
_unit linkItem "NVGoggles";
_unit linkItem "ItemMap";
_unit linkItem "ItemGPS";