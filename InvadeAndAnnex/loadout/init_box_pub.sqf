_box = _this select 0;

if (isServer) then 
{  
	clearWeaponCargoGlobal _box; 
	clearMagazineCargoGlobal _box; 
	clearItemCargoGlobal _box; 
	clearBackpackCargoGlobal _box;
	_box setVariable ["R3F_LOG_disabled", true];
};

//logistik
//null = [this,logistik] execVM "loadout\init_box_pub.sqf";
if (_this select 1 == logistik) then
{  
	_box addAction ["<t color='#2E8B57'>---------   Logistik --------</t>","", [],1,false,true,"","_this distance _target < 8"];  
	_box addAction ["<t color='#2E8B57'>---------Helikopterpilot---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MXC","loadout\pil.sqf", 				["arifle_MXC_F","30Rnd_65x39_caseless_mag","U_B_HeliPilotCoveralls","optic_Holosight_blk_F","V_TacVestIR_blk","H_PilotHelmetHeli_B","B_AssaultPack_blk"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["PDW","loadout\pil.sqf", 				["hgun_PDW2000_F","30Rnd_9x21_Mag","U_B_HeliPilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetHeli_B","B_AssaultPack_blk"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["MP5","loadout\pil.sqf", 				["SMG_05_F","30Rnd_9x21_Mag_SMG_02","U_B_HeliPilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetHeli_B","B_AssaultPack_blk"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["Sting","loadout\pil.sqf", 				["SMG_02_F","30Rnd_9x21_Mag_SMG_02","U_B_HeliPilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetHeli_B","B_AssaultPack_blk"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["Vermin","loadout\pil.sqf",				["SMG_01_F","30Rnd_45ACP_Mag_SMG_01","U_B_HeliPilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetHeli_B","B_AssaultPack_blk"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["<t color='#2E8B57'>---------	Jetpilot---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MXC","loadout\pil.sqf", 				["arifle_MXC_F","30Rnd_65x39_caseless_mag","U_B_PilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetFighter_B","B_Parachute"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["PDW","loadout\pil.sqf", 				["hgun_PDW2000_F","30Rnd_9x21_Mag","U_B_PilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetFighter_B","B_Parachute"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["MP5","loadout\pil.sqf", 				["SMG_05_F","30Rnd_9x21_Mag_SMG_02","U_B_PilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetFighter_B","B_Parachute"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["Sting","loadout\pil.sqf", 				["SMG_02_F","30Rnd_9x21_Mag_SMG_02","U_B_PilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetFighter_B","B_Parachute"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["Vermin","loadout\pil.sqf",				["SMG_01_F","30Rnd_45ACP_Mag_SMG_01","U_B_PilotCoveralls","optic_Holosight_smg_blk_F","V_TacVestIR_blk","H_PilotHelmetFighter_B","B_Parachute"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["<t color='#2E8B57'>---------  Ingenieur ---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MX","loadout\ing.sqf", 				["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["SPAR 16","loadout\ing.sqf", 			["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];   
};

//Panzerfahrer
//null = [this,panzer] execVM "loadout\init_box_pub.sqf";
if (_this select 1 == panzer) then
{
	_box addAction ["<t color='#2E8B57'>---------     MTP    ---------</t>","", [],1,false,true,"","_this distance _target < 8"];    
	_box addAction ["<t color='#2E8B57'>---------Panzerfahrer---------</t>","", [],1,false,true,"","_this distance _target < 8"];  
	_box addAction ["MXC","loadout\panzer.sqf", 			["arifle_MXC_F","30Rnd_65x39_caseless_mag","U_B_CombatUniform_mcam_vest","optic_Holosight_blk_F","V_BandollierB_khk","H_HelmetCrew_B"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["PDW","loadout\panzer.sqf", 			["hgun_PDW2000_F","30Rnd_9x21_Mag","U_B_CombatUniform_mcam_vest","optic_Holosight_smg_blk_F","V_BandollierB_khk","H_HelmetCrew_B"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["MP5","loadout\panzer.sqf", 			["SMG_05_F","30Rnd_9x21_Mag_SMG_02","U_B_CombatUniform_mcam_vest","optic_Holosight_smg_blk_F","V_BandollierB_khk","H_HelmetCrew_B"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["Sting","loadout\panzer.sqf", 			["SMG_02_F","30Rnd_9x21_Mag_SMG_02","U_B_CombatUniform_mcam_vest","optic_Holosight_smg_blk_F","V_BandollierB_khk","H_HelmetCrew_B"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["Vermin","loadout\panzer.sqf",			["SMG_01_F","30Rnd_45ACP_Mag_SMG_01","U_B_CombatUniform_mcam_vest","optic_Holosight_smg_blk_F","V_BandollierB_khk","H_HelmetCrew_B"],1,false,true,"","_this distance _target < 8"];    
	_box addAction ["<t color='#2E8B57'>---------  Ingenieur ---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MXC","loadout\ingp.sqf", 				["arifle_MXC_F","30Rnd_65x39_caseless_mag","U_B_CombatUniform_mcam_vest","optic_Holosight_blk_F","V_BandollierB_khk","H_HelmetCrew_B","B_AssaultPack_mcamo"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["PDW","loadout\ingp.sqf", 				["hgun_PDW2000_F","30Rnd_9x21_Mag","U_B_CombatUniform_mcam_vest","optic_Holosight_smg_blk_F","V_BandollierB_khk","H_HelmetCrew_B","B_AssaultPack_mcamo"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["MP5","loadout\ingp.sqf", 				["SMG_05_F","30Rnd_9x21_Mag_SMG_02","U_B_CombatUniform_mcam_vest","optic_Holosight_smg_blk_F","V_BandollierB_khk","H_HelmetCrew_B","B_AssaultPack_mcamo"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["Sting","loadout\ingp.sqf", 			["SMG_02_F","30Rnd_9x21_Mag_SMG_02","U_B_CombatUniform_mcam_vest","optic_Holosight_smg_blk_F","V_BandollierB_khk","H_HelmetCrew_B","B_AssaultPack_mcamo"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["Vermin","loadout\ingp.sqf",			["SMG_01_F","30Rnd_45ACP_Mag_SMG_01","U_B_CombatUniform_mcam_vest","optic_Holosight_smg_blk_F","V_BandollierB_khk","H_HelmetCrew_B","B_AssaultPack_mcamo"],1,false,true,"","_this distance _target < 8"];
};

//fuehrung
//null = [this,fuehrung] execVM "loadout\init_box_pub.sqf";
if (_this select 1 == fuehrung) then
{
	_box addAction ["<t color='#2E8B57'>--------- 	Führung --------</t>","", [],1,false,true,"","_this distance _target < 8"];  
	_box addAction ["<t color='#2E8B57'>---------Gruppenführer---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MX","loadout\grpfhr.sqf", 				["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["SPAR 16","loadout\grpfhr.sqf", 		["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["<t color='#2E8B57'>---------Truppenführer---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MX","loadout\trpfhr.sqf", 				["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["SPAR 16","loadout\trpfhr.sqf", 		["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];   
};

//aufklaerer
//null = [this,aufklaerer] execVM "loadout\init_box_pub.sqf";
if (_this select 1 == aufklaerer) then
{
	_box addAction ["<t color='#2E8B57'>---------  Aufklärer  ---------</t>","", [],1,false,true,"","_this distance _target < 8"];  
	_box addAction ["<t color='#2E8B57'>--------- Scharfschütze ---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["M320 LRR","loadout\sniper.sqf", 		["srifle_LRR_F","7Rnd_408_Mag","7Rnd_408_Mag","optic_LRPS","V_PlateCarrier1_rgr","U_B_GhillieSuit"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["MAR 10","loadout\sniper.sqf", 			["srifle_DMR_02_F","10Rnd_338_Mag","10Rnd_338_Mag","optic_LRPS","V_PlateCarrier1_rgr","U_B_FullGhillie_sard"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["<t color='#2E8B57'>--------- 	Spotter   ---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MK 18","loadout\spotter.sqf", 			["srifle_EBR_F","20Rnd_762x51_Mag","20Rnd_762x51_Mag","optic_DMS","V_PlateCarrier1_rgr","U_B_GhillieSuit","bipod_01_F_blk"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["MK-1","loadout\spotter.sqf", 			["srifle_DMR_03_tan_F","20Rnd_762x51_Mag","20Rnd_762x51_Mag","optic_AMS_snd","V_PlateCarrier1_rgr","U_B_FullGhillie_sard","bipod_01_F_snd"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["MK14","loadout\spotter.sqf", 			["srifle_DMR_06_olive_F","20Rnd_762x51_Mag","20Rnd_762x51_Mag","optic_AMS_khk","V_PlateCarrier1_rgr","U_B_FullGhillie_sard","bipod_01_F_khk"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["MXM","loadout\spotter.sqf", 			["arifle_MXM_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_DMS","V_PlateCarrier1_rgr","U_B_GhillieSuit","bipod_01_F_khk"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["SPAR 17","loadout\spotter.sqf", 		["arifle_SPAR_03_snd_F","20Rnd_762x51_Mag","20Rnd_762x51_Mag","optic_AMS_snd","V_PlateCarrier1_rgr","U_B_FullGhillie_sard","bipod_01_F_khk"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["<t color='#2E8B57'>---------  	 UAV 	  ---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MX","loadout\uav.sqf", 				["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["SPAR 16","loadout\uav.sqf", 			["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["<t color='#2E8B57'>---------    JTAC    ---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MX","loadout\jtac.sqf", 				["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["SPAR 16","loadout\jtac.sqf", 			["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];   
};

//uav
//null = [this,uav] execVM "loadout\init_box_pub.sqf";
if (_this select 1 == uav) then
{
	_box addAction ["<t color='#2E8B57'>---------  Aufklärer  ---------</t>","", [],1,false,true,"","_this distance _target < 8"];  
	_box addAction ["<t color='#2E8B57'>---------  	 UAV 	  ---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MX","loadout\uav.sqf", 				["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["SPAR 16","loadout\uav.sqf", 			["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];   
};

//mannschaften
//null = [this,mannschaften] execVM "loadout\init_box_pub.sqf";
if (_this select 1 == mannschaften) then
{ 
	_box addAction ["<t color='#2E8B57'>--------- Manschaften --------</t>","", [],1,false,true,"","_this distance _target < 8"];  
	_box addAction ["<t color='#2E8B57'>--------- 	Schütze	 ---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MX","loadout\schuetze.sqf", 			["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];  
	_box addAction ["SPAR 16","loadout\schuetze.sqf", 		["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];    
	_box addAction ["<t color='#2E8B57'>---------  Grenadier  ---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MX","loadout\gren.sqf", 				["arifle_MX_GL_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["SPAR 16","loadout\gren.sqf", 			["arifle_SPAR_01_GL_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["<t color='#2E8B57'>--------- Panzerabwehr ---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MX + PCML","loadout\atpcml.sqf", 		["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["SPAR 16 + PCML","loadout\atpcml.sqf", 	["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["MX + Titan","loadout\attitan.sqf", 	["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["SPAR 16 + Titan","loadout\attitan.sqf",["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["<t color='#2E8B57'>--------- Flugabwehr  ---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MX","loadout\aa.sqf", 					["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["SPAR 16","loadout\aa.sqf", 			["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["<t color='#2E8B57'>---------   Pionier   ---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MX","loadout\pio.sqf", 				["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["SPAR 16","loadout\pio.sqf", 			["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["<t color='#2E8B57'>---------  Sanitäter  ---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MX","loadout\sani.sqf", 				["arifle_MXC_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["SPAR 16","loadout\sani.sqf", 			["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["<t color='#2E8B57'>--------- GrpScharfschütze ---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MK 18","loadout\gss.sqf", 				["srifle_EBR_F","20Rnd_762x51_Mag","20Rnd_762x51_Mag","optic_DMS","V_PlateCarrier1_rgr","bipod_01_F_blk"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["MK-1","loadout\gss.sqf", 				["srifle_DMR_03_tan_F","20Rnd_762x51_Mag","20Rnd_762x51_Mag","optic_AMS_snd","V_PlateCarrier1_rgr","bipod_01_F_snd"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["MK14","loadout\gss.sqf", 				["srifle_DMR_06_olive_F","20Rnd_762x51_Mag","20Rnd_762x51_Mag","optic_AMS_khk","V_PlateCarrier1_rgr","bipod_01_F_khk"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["MXM","loadout\gss.sqf", 				["arifle_MXM_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_DMS","V_PlateCarrier1_rgr","bipod_01_F_snd"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["SPAR 17","loadout\gss.sqf", 			["arifle_SPAR_03_snd_F","20Rnd_762x51_Mag","20Rnd_762x51_Mag","optic_AMS_snd","V_PlateCarrier1_rgr","bipod_01_F_snd"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["<t color='#2E8B57'>--------- MG - Schütze ---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MK 200","loadout\mg.sqf", 				["LMG_Mk200_F","200Rnd_65x39_cased_Box","200Rnd_65x39_cased_Box_Tracer","optic_Arco","V_PlateCarrier1_rgr","bipod_01_F_blk"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["SPMG","loadout\mg.sqf", 				["MMG_02_sand_F","130Rnd_338_Mag","130Rnd_338_Mag","optic_Arco","V_PlateCarrier1_rgr","bipod_01_F_snd"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["MX-SW","loadout\mg.sqf", 				["arifle_MX_SW_F","100Rnd_65x39_caseless_mag","100Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","bipod_01_F_snd"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["SPAR 16-S","loadout\mg.sqf", 			["arifle_SPAR_02_snd_F","150Rnd_556x45_Drum_Mag_F","150Rnd_556x45_Drum_Mag_Tracer_F","optic_Arco","V_PlateCarrier1_rgr","bipod_01_F_snd"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["<t color='#2E8B57'>--------- MG2 - Schütze ---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["MX/MK 200","loadout\mg2.sqf", 			["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","200Rnd_65x39_cased_Box","200Rnd_65x39_cased_Box_Tracer"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["MX/SPMG","loadout\mg2.sqf", 			["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","130Rnd_338_Mag","130Rnd_338_Mag"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["MX/MX-SW","loadout\mg2.sqf", 			["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","100Rnd_65x39_caseless_mag","100Rnd_65x39_caseless_mag_Tracer"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["MX/SPAR 16-S","loadout\mg2.sqf", 		["arifle_MX_F","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_mag_Tracer","optic_Arco","V_PlateCarrier1_rgr","150Rnd_556x45_Drum_Mag_F","150Rnd_556x45_Drum_Mag_Tracer_F"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["SPAR 16/MK 200","loadout\mg2.sqf", 	["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","200Rnd_65x39_cased_Box","200Rnd_65x39_cased_Box_Tracer"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["SPAR 16/SPMG","loadout\mg2.sqf", 		["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","130Rnd_338_Mag","130Rnd_338_Mag"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["SPAR 16/MX-SW","loadout\mg2.sqf", 		["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","100Rnd_65x39_caseless_mag","100Rnd_65x39_caseless_mag_Tracer"],1,false,true,"","_this distance _target < 8"];   
	_box addAction ["SPAR 16/SPAR 16-S","loadout\mg2.sqf", 	["arifle_SPAR_01_snd_F","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Green","optic_Arco","V_PlateCarrier1_rgr","150Rnd_556x45_Drum_Mag_F","150Rnd_556x45_Drum_Mag_Tracer_F"],1,false,true,"","_this distance _target < 8"];
	_box addAction ["<t color='#2E8B57'>---------   Taucher  ---------</t>","", [],1,false,true,"","_this distance _target < 8"]; 
	_box addAction ["SDAR","loadout\taucher.sqf", 			["arifle_SDAR_F","20Rnd_556x45_UW_mag","30Rnd_556x45_Stanag_red","optic_Arco_blk_F","V_RebreatherB"],1,false,true,"","_this distance _target < 8"];
};
