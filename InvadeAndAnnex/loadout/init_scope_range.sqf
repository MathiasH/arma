//null = [this] execVM "loadout\init_scope_range.sqf";
_box = _this select 0;

if (isServer) then 
{
	clearWeaponCargoGlobal _box; 
	clearMagazineCargoGlobal _box; 
	clearItemCargoGlobal _box; 
	clearBackpackCargoGlobal _box;  
	_box setVariable ["R3F_LOG_disabled", true];
	_box addItemCargoGlobal ["optic_AMS_khk", 15];
	_box addItemCargoGlobal ["optic_AMS", 15];
	_box addItemCargoGlobal ["optic_AMS_snd", 15];
	_box addItemCargoGlobal ["optic_DMS", 15];
	_box addItemCargoGlobal ["optic_KHS_old", 15];
	_box addItemCargoGlobal ["optic_KHS_blk", 15];
	_box addItemCargoGlobal ["optic_KHS_tan", 15];
	_box addItemCargoGlobal ["optic_LRPS", 15];
	_box addItemCargoGlobal ["optic_LRPS_tna_F", 15];
	_box addItemCargoGlobal ["optic_SOS", 15];
	_box addItemCargoGlobal ["optic_SOS_khk_F", 15];
	_box addItemCargoGlobal ["optic_NVS", 15];
};




