//null = [this] execVM "loadout\init_scope.sqf";
_box = _this select 0;

if (isServer) then 
{
	clearWeaponCargoGlobal _box; 
	clearMagazineCargoGlobal _box; 
	clearItemCargoGlobal _box; 
	clearBackpackCargoGlobal _box;  
	_box setVariable ["R3F_LOG_disabled", true];
	_box addItemCargoGlobal ["optic_Arco", 25];
	_box addItemCargoGlobal ["optic_Arco_blk_F", 25];
	_box addItemCargoGlobal ["optic_MRCO", 25];
	_box addItemCargoGlobal ["optic_ERCO_khk_F", 25];
	_box addItemCargoGlobal ["optic_ERCO_snd_F", 25];
	_box addItemCargoGlobal ["optic_ERCO_blk_F", 25];
	_box addItemCargoGlobal ["optic_Hamr", 25];
	_box addItemCargoGlobal ["optic_Hamr_khk_F", 25];
	_box addItemCargoGlobal ["optic_NVS", 25];
	_box addItemCargoGlobal ["optic_ACO_grn", 25];
	_box addItemCargoGlobal ["optic_Aco", 25];
	_box addItemCargoGlobal ["optic_Holosight", 25];
	_box addItemCargoGlobal ["optic_Holosight_smg", 25];
	_box addItemCargoGlobal ["optic_Holosight_khk_F", 25];
	_box addItemCargoGlobal ["optic_Holosight_blk_F", 25];
	_box addItemCargoGlobal ["optic_Holosight_smg_blk_F", 25];
};