// PZF SCHÜTZE FLECKTARN
private ["_unit","_remote","_default"];


_unit = _this select 1;
_weapon =  (_this select 3) select 0;
_mag = (_this select 3) select 1;
_mag2 = (_this select 3) select 2;
_scope = (_this select 3) select 3;
_vest = (_this select 3) select 4;

_uavs = ["B_UAV_02_F","B_UAV_02_CAS_F","B_UAV_01_F","B_UGV_01_rcws_F","B_UGV01_F"];
if (typeOf _unit in _uavs) exitWith { hint "Netter versuch, aber das wird nichts";};
_uavs_AI = ["B_UAV_AI"];
if (typeOf _unit in _uavs_AI) exitWith { hint (localize "STR_GSG_UAVLoadout");};

hint (localize "STR_GSG_Container");
_remote = _unit;

//LOAD DEFAULT LOADOUT
_default = [_remote, _weapon, _mag, _mag2, _scope, _vest] execVM "loadout\default_mtp.sqf";
waitUntil {scriptDone _default};

_unit addItemToUniform "16Rnd_9x21_Mag";
_unit addItemToUniform "16Rnd_9x21_Mag";

_unit addBackpack "B_Static_Designator_01_weapon_F";

_unit addPrimaryWeaponItem "acc_pointer_IR";
_unit addWeapon "hgun_P07_F";
_unit linkItem "B_UavTerminal";
_unit addHeadgear "H_HelmetSpecB_paint2";

_unit addWeapon "Laserdesignator";
for "_i" from 1 to 2 do {_unit addItemToUniform "Laserbatteries";};


hint (localize "STR_GSG_LoadoutDone");
sleep 15;
hint "";