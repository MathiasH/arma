player createDiarySubject ["Server-Regeln","Server-Regeln"];
player createDiaryRecord ["Server-Regeln",["Unterstützung (§15 - §22)","
<br /> § 15 Piloten/Logistiker:
<br /> - Die Pilotenplätze und Kopilotenplätze in den Luftfahrzeugen <br />   können nur von Piloten/Logistikern benutzt werden! 
<br /> - Jeder Pilot ist für sein Fluggerät verantwortlich und es sollte <br />   nicht im Einsatzraum verbleiben. 
<br /> - Falls der Helikopter nicht mehr in der Lage ist zurück zu <br />   fliegen, muss dieser von dem zuständigen Piloten/Logistiker gesprengt <br />   werden. 
<br /> - Bei häufiger, absichtlicher Zerstörung eines Fluggerätes durch <br />   einen Piloten/Logistiker kann dieser aus dem Slot und vom Server <br />   entfernt werden.
<br /> 
<br /> § 16 Panzer Unterstützung:
<br /> - Die Panzer sind zur Unterstützung der Infanterie an der AO <br />   gedacht. 
<br /> - Panzer sind grundsätzlich von zwei bis drei Mann zu besetzen <br />   und nicht alleine zu führen. 
<br /> - Panzer sollten wieder zum HQ zurück gebracht werden und <br />   nicht im alten Einsatzgebiet zurückgelassen werden. 
<br /> - Wer einen Panzer nutzt, hat sich im Teamspeak einzufinden, <br />   für alle Panzerführer herrscht Teamspeak Pflicht. 
<br /> - Als Panzer zählt jedes mittel bis schwer gepanzertes Fahrzeug.
<br /> - Panzerbesatzungen haben sich auf den dafür ausgewiesenen Slots zu befinden <br /> (Ausgenommen bei den Side-Belohnungs-Fahrzeugen).
<br /> 
<br /> § 17 Luftunterstützung:
<br /> - Luftunterstützung gegen Bodenziele ist grundsätzlich erst nach<br />   der Anforderung im Side Chat durch die Infanterieeinheit im<br />   Einsatzgebiet durchzuführen, die Anforderung kann ebenfalls<br />   durch roten Rauch erfolgen. 
<br /> - Piloten/Logistiker und UAV-Operator sind von dem Recht zu Anforderung <br />   ausgeschlossen. 
<br /> - Wenn ein Pilot roten Rauch eindeutig identifiziert, kann er <br />   selbstständig entscheiden ob er die Ziele bekämpft. 
<br /> - Luftunterstützung gegen Luftziele ist in eigener Verantwortung<br />   durchzuführen, solange sie eigene Einheiten nicht gefährden.
<br /> - Alle Starts und Landungen sind in schriftlicher Form im <br />   Side-Chat anzumelden. Beispiel:<br />   'Start von 22 L' oder 'Landung auf 04 R'. 
<br /> - Die Luftunterstützung hat alle Anforderungen auf <br />   Durchführbarkeit und Gefahrenlage zu prüfen. 
<br /> - Außerdem hat die Luftunterstützung Anforderungen des JTac <br />   bevorzugt zu behandeln und in der von dieser angegebenen <br />   Reihenfolge durchzuführen, sie darf Anforderungen des JTac <br />   nur mit Begründung auf Basis der Durchführbarkeit oder der <br />   Gefahrenlage ablehnen.
<br /> - Die Buzzard, die F/A-181 Black Wasp II (Tarn), <br />  sowie die TO-201 Shirka (Tarn) sind nur für Luft-Luft Angriffe zu nutzen.
<br /> 
<br /> § 17.1 Eigensicherung
<br /> - Die Luftunterstützung darf Bodenziele ohne Anforderung erst <br />   angreifen, wenn diese bereits von besagten Bodenzielen<br />   angegriffen werden.
<br /> - Diese Regelung gilt nicht, wenn die Luftunterstützung den <br />   Angriff durch beispielsweise Tief - oder Tiefstflug provoziert <br />   und somit Feindbeschuss auf sich zieht! 
<br /> - Die Aufklärungsflughöhe beträgt mindestens 1300m (3000ft), <br />   tiefere Anflüge gelten als Tiefflug. 
<br /> - Ein Abweichen dieser Mindesthöhe ist nur im Angriffsfall, <br />   auf durch Infanterie, angeforderte Ziele zu verlassen!
<br /> 
<br /> § 18 Clan-Flugzeuge/Fahrzeuge:
<br /> - Benutzer die ein Clanflugzeug, Clanhelikopter oder einen<br />   Clanpanzer nutzen möchten, müssen sich im Forum für eine der<br />   dazugehörigen Lizenzen bewerben. 
<br /> - Nach der Bewerbung muss der Spieler verschiedene Prüfungen<br />   ablegen, welche von einem Moderator abgenommen werden.
<br /> 
<br /> §19 HEMTT-Munition:
<br /> - Der HEMTT-Munition ist ein Nachfüllfahrzeug welches au <br />   Anforderung an eine Position gebracht werden kann <br />   (vorzugsweise durch logistische Kräfte), um dort andere<br />   Fahrzeuge/Stellungen neu aufzufüllen. 
<br /> - Es hat nach dem Auffüllvorgang wieder zurück zur Basis oder <br />   FOB gebracht zu werden und nicht am Einsatzort zu <br />   verbleiben. 
<br /> - Stellungen/Fahrzeuge die am Auffüllvorgang beteiligt sind <br />   dürfen währen dieses Vorganges ausschließlich zur <br />   Eigensicherung schießen.
<br />
<br />
<br /> §20 FOB:
<br /> - Eine FOB hat niemals alleine errichtet zu werden. 
<br /> - Des Weiteren muss immer eine Kraft in der FOB verbleiben. 
<br /> - Eine FOB hat in sicherem Gebiet errichtet zu werden, <br />   d.h. außerhalb einer AO.
<br /> 
<br /> §21 JTac:
<br /> - Der JTac muss sich bei den Luftunterstützungseinheiten <br />   anmelden, damit diese auch wissen, dass er da ist, bestenfalls <br />   hat er sich im selben Ts-Chanel wie diese zu befinden. 
<br /> - Er hat keine Anforderungen auf Ziele zu geben, die nicht in <br />   seiner AO liegen. 
<br /> - Auch hier haben Anforderungen im Side zu erfolgen.
<br /> 
<br /> §22 Neustartzeiten des Servers:
<br /> Der Publikserver wird zu folgenden Zeiten automatisch neu <br />  gestartet:
<br /> - 02:00 Uhr
<br /> - 06:00 Uhr
<br /> - 12:00 Uhr
<br /> - 18:00 Uhr
<br /> 
<br />Um einen Reibungslosen Ablauf auf unserem Publikserver zu gewährleisten, ist jeder Benutzer dazu angehalten die hier aufgeführten Regeln zu lesen und umzusetzen. 
<br />Gleichzeitig bleiben alle weiteren Regeln der GSG, nachzulesen unter dem Reiter 'Die GSG im Überblick - Zentrale Dienstvorschrift - Regeln' in Kraft!
<br /> 
<br />Die Grenzschutzgruppe wünscht allen Benutzern viel Spaß auf unseren Servern!
<br /> 
" ]];
player createDiaryRecord ["Server-Regeln",["Allgemeines (§1 - §14)","

<br /> Regeln auf dem Public Arma 3-Server der Grenzschutzgruppe
<br /> 
<br /> § 1 Benutzer:
<br /> - Die folgenden Regeln gelten für alle Benutzer unseres<br />   Publikservers und werden mit Betreten des Servers akzeptiert.
 - Verstösse gegen die Regeln führen zum Ausschluss vom <br />   Server.
<br /> - Alle Benutzer haben sich an die Anweisungen der Moderatoren <br />   des Publikservers zuhalten.
<br /> - Bei Missverständnissen oder Problemen können die Clanführung <br />   oder Mitglieder mit der nötigen Befugnis hinzugezogen werden. 
<br /> - Grundsätzlich besteht für jeden Benutzer die Möglichkeit sich <br />   über Missbrauch oder Fehlverhalten unserer Moderatoren im <br />   Forum zu beschweren.
<br /> 
<br /> § 2 Beleidigung:
<br /> - Wer keinen vernünftigen Umgangston beherrscht und andere<br />   Benutzer oder unsere Moderatoren beleidigt, ob nun direkt oder<br />   subtil, im Sidechat oder im Teamspeak, wird vom Server und /<br />  oder Teamspeak entfernt!
<br /> 
<br /> § 3 Pornographie, Nazismus und Rassismus:
<br /> - Pornographische, nazistische oder rassistische Aussagen oder<br />   Nicknames werden nicht geduldet und sind grundsätzlich<br />   untersagt!
<br /> - Zu Wieder Handlungen führen zu einem sofortigen, permanenten<br />   Bann!
<br />
<br /> § 4 Werbung:
<br /> - Jegliche Art von Werbung, An - oder Abwerbung von<br />   Mitgliedern ist verboten und führt zu einem sofortigen,<br />   permanenten Bann!
<br /> 
<br /> § 5 Meldepflicht:
<br /> - Bei Regelverstößen durch andere Benutzer ist dies umgehend <br />   einem Moderator oder der Clanleitung zu melden und zu <br />   belegen!
<br /> 
<br /> § 6 Datenschutz:
<br /> - Private Daten, wie Telefonnummern, Adressen oder Passwörter<br />   usw. dürfen nicht öffentlich ausgetauscht werden! 
<br /> - Kein Moderator oder Teammitglied wird jemals nach Deinen<br />   Persönlichen Daten (z.B. deinem Passwort) fragen.
<br />
<br /> § 7 Streitigkeiten:
<br /> - Private Missverständnisse und Streitigkeiten sind auch privat <br />   zu klären, sollte die Situation dennoch eskalieren, werdet Ihr <br />   vom Server entfernt, um für alle anderen Benutzer weiterhin <br />   einen reibungslosen Ablauf zu garantieren.
<br /> 
<br /> § 8 Slots mit TS-Pflicht:
<br /> - Zu den Slots mit TS-Pflicht gehören alle Logistiker, Piloten, <br />    Panzer, JTac und UAV-Operator-Slots.
<br /> - Alle Benutzer die einen solchen Slot belegen, müssen auf <br />   unserem Teamspeak im Publik-Bereich unter ihrem <br />   In-Game-Namen immer erreichbar sein. 
<br /> - Sollte ein solcher Benutzer nicht in diesem Bereich erreichbar <br />   sein oder nicht reagieren, kann dieser auch ohne Aufforderung <br />   vom Slot und vom Server entfernt werden.
<br />
<br /> § 9 VR Kleidung:
<br /> - Die VR Kleidung ist auf Grund der Tatsache, dass eine einzelne <br />   Person das halbe Arsenal mit sich herumtragen kann,<br />   auf unserem Publikserver verboten.
<br /> 
<br /> § 10 CSAT Uniformen:
<br /> - Wer eine CSAT Uniform trägt und sich damit in der AO <br />   aufhält, muss damit rechnen, dass er durch Verbündeten <br />   Beschuss getötet wird.
<br /> - Der Schütze kann dafür nicht belangt werden da er auf <br />   erkannten Feind gefeuert hat.
<br /> - Anspruch auf Beschwerde oder Disziplinarmaßnahmen gibt es <br />   nicht!
<br /> - Bitte unterlasst soche Provokationen von vorne rein!
<br /> 
<br /> § 11 Teamplay:
<br /> - Teamplay ist auf Unserem Publikserver sehr erwünscht und <br />   steht an oberster Stelle. 
<br /> - Bitte organisiert Euch selbst und bildet Gruppen mit - oder<br />   ohne Führung und agiert als Team zusammen!
<br /> 
<br /> § 12 Transportables Virtuelles Arsenal:
<br /> - Das transportable Virtuelle Arsenal ist AUSSCHLIESSLICH für <br />   die Errichtung einer FOB vorgesehen. 
<br />
<br /> § 13 Bobcats:
<br /> - Die Bobcats, die in der Base stehen, sind auch dort zu <br />   belassen, da sie für die Reperaturen an der Base benötigt <br />   werden. 
<br /> - Sei es für Beschädigte Helikopter oder für die zerstörte <br />   Landebahn. 
<br /> 
<br /> § 14 Bugusing:
<br /> - Bugs sind zu melden und nicht auszunutzen.
<br />
" ]];
