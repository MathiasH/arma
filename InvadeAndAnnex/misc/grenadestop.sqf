/*
	GrenadeStop v0.8 for ArmA 3 Alpha by Bake (tweaked slightly by Rarek)
	
	DESCRIPTION:
	Stops players from throwing grenades in safety zones.
	
	INSTALLATION:
	Move grenadeStop.sqf to your mission's folder. Then add the
	following line to your init.sqf file (create one if necessary):
	execVM "grenadeStop.sqf";
	
	CONFIGURATION:
	Edit the #defines below.
*/

#define SAFETY_ZONES	[["Base_Protect_1", 200],["Base_Protect_2",250],["Base_Protect_3",150]] // Syntax: [["marker1", radius1], ["marker2", radius2], ...]
#define MESSAGE "Placing / throwing items and firing at base is STRICTLY PROHIBITED!"
#define MORTAR_MESSAGE	"No point you putting that up, soldier; we're fresh out of ammo for those things."
#define AA_MESSAGE	"Sorry, solider! All AA missiles are disabled!"

if (isDedicated) exitWith {};
waitUntil {!isNull player};

/* player addEventHandler ["Fired", 
{
	if ({(_this select 0) distance getMarkerPos (_x select 0) < _x select 1} count SAFETY_ZONES > 0) then
	{
		deleteVehicle (_this select 6);
		titleText [MESSAGE, "BLACK FADED"];
		titleCut [MESSAGE, "BLACK IN", 3];
	};
}]; */


{_x addEventHandler ["GetIn", 
{
	//systemchat "test";
	vehicle player addEventHandler ["Fired", 
	{
		_projectile = _this select 6;
		_mag = _this select 5;
		_flares = ["CMflareAmmo","CMflare_Chaff_Ammo"];
		_flaremag = [
						"60Rnd_CMFlareMagazine",
						"120Rnd_CMFlareMagazine",
						"240Rnd_CMFlareMagazine",
						"60Rnd_CMFlare_Chaff_Magazine",
						"120Rnd_CMFlare_Chaff_Magazine",
						"240Rnd_CMFlare_Chaff_Magazine",
						"192Rnd_CMFlare_Chaff_Magazine",
						"168Rnd_CMFlare_Chaff_Magazine",
						"300Rnd_CMFlare_Chaff_Magazine"
					];
		if (({(_this select 0) distance getMarkerPos (_x select 0) < _x select 1} count SAFETY_ZONES > 0) && !(_mag in _flaremag)) then
		{
			deleteVehicle _projectile;
			titleText [MESSAGE, "BLACK FADED"];
			titleCut [MESSAGE, "BLACK IN", 3];	
		};
	}];
}];} foreach vehicles;

/* player addEventHandler ["WeaponAssembled", 
{
	deleteVehicle _this select 1;
		titleText [MORTAR_MESSAGE, "BLACK FADED"];
		titleCut [MORTAR_MESSAGE, "BLACK IN", 3];
}]; */