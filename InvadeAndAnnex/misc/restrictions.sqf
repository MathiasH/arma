[] spawn
{
	while {true} do
	{
		_pWeap = primaryWeapon player;
		_sWeap = secondaryWeapon player;
		_hWeap = handgunWeapon player;
		_pWeap_items = primaryWeaponItems player; 
		_pWeap_items_scope = _pWeap_items select 2;
		_pWeap_items_schalli = _pWeap_items select 0;
		_hWeap_items = handgunItems player; 
		_assItems = assignedItems player;
		_headgear = headgear player;
		_backpack = Backpack player;
		_uniform = uniform player;
		
		_Sniper =
		[
			"B_sniper_F",
			"B_spotter_F",
			"B_Protagonist_VR_F"
		];
		_Sniper_weapons = 
		[
			"srifle_GM6_F",
			"srifle_LRR_F",
			"srifle_LRR_camo_F",
			"srifle_LRR_tna_F",
			"srifle_DMR_02_camo_F",
			"srifle_DMR_02_F",
			"srifle_DMR_02_sniper_F"
		];
		_Sniper_items =
		[
			"optic_LRPS_tna_F",
			"optic_LRPS"
		];
		_Marksman =
		[
			"B_sniper_F",
			"B_spotter_F",
			"B_soldier_M_F",
			"B_Protagonist_VR_F"
		];
		_Marksman_weapons = 
		[
			"srifle_DMR_05_blk_F",
			"srifle_DMR_05_tan_f"
		];
		_Autorifle =
		[
			"B_soldier_AR_F",
			"B_Protagonist_VR_F"
		];
		_Autorifle_weapons = 
		[
			"MMG_01_tan_F",
			"MMG_02_black_F",
			"MMG_02_camo_F",
			"MMG_02_sand_F"
		];
		_AT = 
		[
			"B_soldier_LAT_F",
			"B_Protagonist_VR_F"
		];
		_AT_weapons = 
		[
			"launch_NLAW_F",
			"launch_I_Titan_short_F",
			"launch_B_Titan_short_F",
			"launch_B_Titan_F",
			"launch_B_Titan_tna_F",
			"launch_B_Titan_short_tna_F"
		];
		_UAV =
		[
			"B_soldier_UAV_F",
			"B_Protagonist_VR_F"
		];
		_UAV_items =
		[
			"B_UavTerminal"
		];
		_JTAC = 
		[
			"B_recon_JTAC_F",
			"B_Protagonist_VR_F"
		];
		_JTAC_items =
		[
		];
		_UAVJTAC =
		[
			"B_soldier_UAV_F",
			"B_recon_JTAC_F",
			"B_Protagonist_VR_F"
		];
		_San = 
		[
			"B_medic_F"
		];
		_vWeap =
		[
			"arifle_ARX_ghex_F",
			"arifle_ARX_blk_F",
			"arifle_ARX_hex_F",
			"arifle_CTARS_hex_F",
			"srifle_DMR_07_ghex_F",
			"srifle_DMR_07_hex_F",
			"arifle_CTAR_GL_hex_F",
			"arifle_CTARS_ghex_F",
			"arifle_CTAR_hex_F",
			"arifle_CTAR_GL_ghex_F",
			"arifle_CTAR_ghex_F",
			"MMG_01_hex_F",
			"srifle_GM6_camo_F",
			"srifle_GM6_ghex_F",
			"srifle_DMR_05_hex_F"
		];
		_vScope =
		[
			"optic_KHS_hex",
			"optic_Arco_ghex_F",
			"optic_Hamr_ghex_F",
			"optic_DMS_ghex_F",
			"optic_LRPS_ghex_F"
		];
		_vSchalli =
		[
			"muzzle_snds_65_TI_ghex_f",
			"muzzle_snds_65_TI_hex_f",
			"muzzle_snds_58_ghex_F",
			"muzzle_snds_58_hex_F"
		];
		_vLauncher =
		[
			"launch_I_Titan_F",
			"launch_O_Titan_ghex_F",
			"launch_O_Titan_F",
			"launch_O_Titan_short_ghex_F",
			"launch_O_Titan_short_F"
		];
		_vHeadgear =
		[
			"H_HelmetO_ViperSP_ghex_F",
			"H_HelmetO_ViperSP_hex_F"
		];
		_vBackpack =
		[
			"B_Mortar_01_support_F",
			"B_Mortar_01_weapon_F",
			"O_Mortar_01_support_F",
			"O_Mortar_01_weapon_F",
			"I_Mortar_01_support_F",
			"I_Mortar_01_weapon_F",
			"B_HMG_01_support_F",
			"B_HMG_01_support_high_F",
			"O_HMG_01_support_F",
			"O_HMG_01_support_high_F",
			"I_HMG_01_support_F",
			"I_HMG_01_support_high_F"
		];
		_vUniform =
		[
			"U_B_Protagonist_VR",
			"U_I_Protagonist_VR",
			"U_O_Protagonist_VR"
		];
		_mod =
		[
			"B_Protagonist_VR_F"
		];
		_Sanvweap =
		[
			"LMG_03_F",
			"LMG_Mk200_F",
			"LMG_Zafir_F"
		];
		_Sanvlaunch =
		[
			"launch_RPG32_F",
			"launch_RPG32_ghex_F",
			"launch_RPG7_F"
		];
	
		if (_sWeap in _AT_weapons) then
		{
			if (!(playerSide == west && typeOf player in _AT)) then
			{
				player removeWeapon _sWeap;
				hint "Only AT Soldiers are trained in missile launcher operations. Launcher removed.";
				player globalChat "Only AT Soldiers are trained in missile launcher operations. Launcher removed.";
			};
		};
		if (_pWeap in _Sniper_weapons) then
		{
			if (!(playerSide == west && typeOf player in _Sniper)) then
			{
				player removeWeapon _pWeap;
				hint "Only Snipers are trained with this weapon. Sniper rifle removed.";
				player globalChat "Only Snipers are trained with this weapon. Sniper rifle removed.";
			};
		};
		if (_pWeap in _Marksman_weapons) then
		{
			if (!(playerSide == west && typeOf player in _Marksman)) then
			{
				player removeWeapon _pWeap;
				hint "Only Marksman are trained with this weapon. Rifle removed.";
				player globalChat "Only Marksman are trained with this weapon. Rifle removed.";
			};
		};
		if (_pWeap in _Autorifle_weapons) then
		{
			if (!(playerSide == west && typeOf player in _Autorifle)) then
			{
				player removeWeapon _pWeap;
				hint "Only Autorifleman are trained with this weapon. Rifle removed.";
				player globalChat "Only Autorifleman are trained with this weapon. Rifle removed.";
			};
		};
		if (("B_UavTerminal" in _assItems)) then
		{
			if (!(playerSide == west && typeOf player in _UAVJTAC)) then
			{
				player unassignItem "B_UavTerminal";
				hint "Only UAV Operators and JTACs are trained with this weaponsystem. UAV-Terminal removed.";
				player globalChat "Only UAV Operators and JTACs are trained with this weaponsystem. UAV-Terminal removed.";
			};
		};
		//Verbotene Items
 		if (
				("NVGogglesB_gry_F" in _assItems) || 
				("NVGogglesB_grn_F" in _assItems) || 
				("NVGogglesB_blk_F" in _assItems) || 
				("O_NVGoggles_ghex_F" in _assItems) || 
				("O_NVGoggles_hex_F" in _assItems) || 
				("O_NVGoggles_urb_F" in _assItems) || 
				("Laserdesignator_02" in _assItems) ||
				("Laserdesignator_02_ghex_F" in _assItems) || 
				("O_UavTerminal" in _assItems) || 
				("I_UavTerminal" in _assItems)
			) then
		{
			if (!(playerSide == west && typeOf player in _mod)) then
			{
				player unassignItem "NVGogglesB_gry_F";
				player unassignItem "NVGogglesB_grn_F";
				player unassignItem "NVGogglesB_blk_F";
				player unassignItem "O_NVGoggles_ghex_F";
				player unassignItem "O_NVGoggles_hex_F";
				player unassignItem "O_NVGoggles_urb_F";
				player removeItem "NVGogglesB_gry_F";
				player removeItem "NVGogglesB_grn_F";
				player removeItem "NVGogglesB_blk_F";
				player removeItem "O_NVGoggles_ghex_F";
				player removeItem "O_NVGoggles_hex_F";
				player removeItem "O_NVGoggles_urb_F";
				player removeWeapon "Laserdesignator_02_ghex_F";
				player removeWeapon "Laserdesignator_02";
				player unassignItem "O_UavTerminal";
				player unassignItem "I_UavTerminal";
				hint "Item is forbidden. Item removed.";
				player globalChat "Item is forbidden. Item removed.";
			};
		};	
		//verbotene Waffen
		if (_pWeap in _vWeap) then
		{
			if (!(playerSide == west && typeOf player in _mod)) then
			{
				player removeWeapon _pWeap;
				hint "Weapon is forbidden. Weapon removed.";
				player globalChat "Weapon is forbidden. Weapon removed.";
			};
		};
		//verbotene Werfer
		if (_sWeap in _vLauncher) then
		{
			if (!(playerSide == west && typeOf player in _mod)) then
			{
				player removeWeapon _sWeap;
				hint "Launcher is forbidden. Launcher removed.";
				player globalChat "Launcher is forbidden. Launcher removed.";
			};
		};
		//verbotene Visiere
		if (_pWeap_items_scope in _vScope) then
		{
			if (!(playerSide == west && typeOf player in _mod)) then
			{
				player removePrimaryWeaponItem _pWeap_items_scope;
				hint "Scope is forbidden. Scope removed.";
				player globalChat "Scope is forbidden. Scope removed.";
			};
		};
		//verbotene Schalldämpfer
		if (_pWeap_items_schalli in _vSchalli) then
		{
			if (!(playerSide == west && typeOf player in _mod)) then
			{
				player removePrimaryWeaponItem _pWeap_items_schalli;
				hint "Silencer is forbidden. Silencer removed.";
				player globalChat "Silencer is forbidden. Silencer removed.";
			};
		};
		//verbotene Helme
		if (_headgear in _vHeadgear) then
		{
			if (!(playerSide == west && typeOf player in _mod)) then
			{
				removeHeadgear player;
				hint "Headgear is forbidden. Headgear removed.";
				player globalChat "Headgear is forbidden. Headgear removed.";
			};
		};
		//verbotene Rucksäcke
		if (_backpack in _vBackpack) then
		{
			if (!(playerSide == west && typeOf player in _mod)) then
			{
				removeBackpack player;
				hint "Backpack is forbidden. Backpack removed.";
				player globalChat "Backpack is forbidden. Backpack removed.";
			};
		};
		//verbotene Helme
		if (_uniform in _vUniform) then
		{
			if (!(playerSide == west && typeOf player in _mod)) then
			{
				removeUniform player;
				player forceAddUniform "U_B_CombatUniform_mcam";
				hint "Uniform is forbidden. Uniform replaced.";
				player globalChat "Uniform is forbidden. Uniform replaced.";
			};
		};
		if (_sWeap in _Sanvlaunch) then
		{
			if ((playerSide == west && typeOf player in _San)) then
			{
				player removeWeapon _sWeap;
				hint "Medics are not trained with launcher. Launcher removed.";
				player globalChat "Medics are not trained with launcher. Launcher removed.";
			};
		};
		if (_pWeap in _Sanvweap) then
		{
			if ((playerSide == west && typeOf player in _San)) then
			{
				player removeWeapon _pWeap;
				hint "Medics are not trained with this weapon. Weapon removed.";
				player globalChat "Medics are not trained with this weapon. Weapon removed.";
			};
		};
		sleep 3.0;
	} foreach allUnits;
};