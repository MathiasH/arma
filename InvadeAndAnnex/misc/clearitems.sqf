
private ["_itemsToClear","_obj","_rad","_delay"];
_obj = getMarkerPos "respawn_west"; // get spawn - might as well
_rad = 1500;  //  radius outwards from center point to clear items.
_delay = 150; // amount of time in-between clean-ups //150
//_dekowaffen = [waffe_1,waffe_2,waffe_3,waffe_4,waffe_5,waffe_6,waffe_7,waffe_8,waffe_9,waffe_10,waffe_11,waffe_12,waffe_13];
 
while {true} do
{
	sleep _delay;
	debugMessage = "Clearing items from spawn...";
	publicVariable "debugMessage";
	_itemsToClear = nearestObjects [_obj,["weaponholder"],_rad];
	{
		_varname = vehicleVarName _x;
		//diag_log format ["%1    -- %2    -- %3",_x,_varname,_dekowaffen];
		if (!(vehicleVarName _x in ["waffe_1","waffe_2","waffe_3","waffe_4","waffe_5","waffe_6","waffe_7","waffe_8","waffe_9","waffe_10","waffe_11","waffe_12","waffe_13","Scope_1"])) then
		{
			deleteVehicle _x;
		}; 
	} forEach _itemsToClear;
	debugMessage = "Items cleared.";
	publicVariable "debugMessage";
	debugMessage = "Deleting empty groups...";
	publicVariable "debugMessage";
	 {
		if (count units _x==0) then {deleteGroup _x}
	} forEach allGroups;
	["gsg\gsg_deletegroups.sqf","BIS_fnc_execVM",true,true] call BIS_fnc_MP;
	debugMessage = "Empty groups deletet.";
	publicVariable "debugMessage";
};