	//if (PARAMS_ClanTank == 0) exitWith {};
_Tank = _this select 0; // Object that had the Action (also _target in the addAction command)
_place = _this select 1; // Unit that used the Action (also _this in the addAction command)
_actID = _this select 2; // ID of the Action

private ["_namesArray","_nameCheck","_heliplace","_forbidden"];
//test2=_actID;
_player = Player;
_uid = getPlayerUID _player;

_namesArray = TankLizenz + ClanLizenz + ClanTankLizenz + ModLizenz;

_nameCheck = false;
//test=[_nameCheck, _Tank, _place, _actID];

/*if (_uid in _namesArray) then {
_nameCheck = true; 
};*/

{if(getPlayerUID _x in _namesArray) then {_nameCheck = true};} foreach [driver _Tank, gunner _Tank, commander _Tank];

if !(_nameCheck) then { 
					_forbidden = [driver _Tank, gunner _Tank, commander _Tank];
					if (_player in _forbidden) then  
						{
						_player action ["eject", _Tank];
						_player action["Getout", _Tank];						
						//test= (vehicle player == player);
						waitUntil {sleep 1; vehicle player == player};		
						systemChat "Only Clanmember and Friends or Tanklizenzmember!";
						};
					_nameCheck = false;
				};
