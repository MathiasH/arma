
/* =============================================== */
/* =============================================== */
/* ============ SERVER INITIALISATION ============ */
/* =============================================== */
/* =============================================== */

private ["_null","_start","_handle"];

//Groups
["Initialize"] call BIS_fnc_dynamicGroups;

_start = diag_tickTime;
if (isserver) then {diag_log format["Serverinitialisierung gestartet in %1",(diag_tickTime - _start)];};

//Set random Daytime
skipTime random 34;
if (isserver) then {diag_log format["Serverzeit eingestellt %1",(diag_tickTime - _start)];};

//Set a few blank variables for event handlers and solid vars for SM
call compile preprocessFileLineNumbers "sm\Var\ini_global_variable.sqf";
if (isserver) then {diag_log format["ini_global_variable gestartet in %1",(diag_tickTime - _start)];};

//Run a few miscellaneous server-side scripts
_null = [] execVM "misc\clearitems.sqf";
if(isServer) then {[40,50,55,60] execVM "scripts\bodyRemoval.sqf";};

// AI retake AO start
[]execVM "eos\OpenMe.sqf";

//unflip AI vehicles
[] execVM "gsg\gsg_turnVehicle.sqf";

// Water Missions
if (PARAMS_WaterMissions == 1) then { _null=[1380] execVM "sm\sideMission_Init.sqf"; if (isserver) then { diag_log format["Watermission gestartet in %1",(diag_tickTime - _start)];};};

//Begin generating priority targets and Spawn random wrecks
if (PARAMS_PriorityTargets == 1) then 
{
	[] spawn GSG_fnc_pt_wracks;
	[] spawn GSG_fnc_priorityTargets;
	if (isserver) then {diag_log format["PT wird geladen in %1",(diag_tickTime - _start)];};
};

//Begin generating side missions
[] spawn GSG_fnc_sideBlueMissions; if (isserver) then {diag_log format["Sidemission wird geladen in %1",(diag_tickTime - _start)];};

if (isserver) then {diag_log format["Serverinitialisierung abgeschlossen in %1",(diag_tickTime - _start)];};

//Start der Hauptmission
[] spawn GSG_fnc_Main;

//l�sche Einheit nach Disconnect
addMissionEventHandler ['HandleDisconnect',
{
	[(_this select 0)] spawn 
	{
		_unit = _this select 0;
		//_name = _this select 3;
		hideBody _unit;
		_removeTime = time + 8;
		while {getPosATL _unit select 2 < 0.2 && time < _removeTime} do {sleep 0.1;};
		_holder = objNull;
		_holders = getPosATL _unit nearEntities ["WeaponHolderSimulated", 10];
		if (count _holders > 0) then
		{
			_holder = _holders select 0;
		};
		deleteVehicle _holder;
		
		sleep 5;
		deleteVehicle (_this select 0);
	};
}];