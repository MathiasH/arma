	//this addEventHandler ["GetIn", {[_this select 0,_this select 1,_this select 2] execVM"gsg\vehcheck\gsg_vehcheckServer.sqf"}];
	

    private ["_sanheliplayer","_SanHeli","_pilots","_aircraft_nocopilot","_black", "_uid","_iampilot","_oldvehicle","_veh","_forbidden"];
	_pilots = ["B_Helipilot_F","B_helicrew_F","B_Pilot_F","B_helicrew_F","O_helipilot_F","O_helicrew_F","O_Pilot_F","I_helipilot_F","I_helicrew_F","I_Pilot_F","B_Protagonist_VR_F"];
	_jetpilots = ["B_Pilot_F","B_helicrew_F","O_helicrew_F","O_Pilot_F","I_helicrew_F","I_Pilot_F","B_Protagonist_VR_F"];
    _aircraft_nocopilot = ["B_Heli_Transport_01_camo_F", "B_Heli_Transport_01_F", "I_Heli_Transport_02_F", "O_Heli_Light_02_F", "O_Heli_Light_02_unarmed_F", "B_Heli_Light_01_armed_F", "I_Heli_light_03_unarmed_F"];
	_moerserteam = ["B_support_Mort_F","B_support_AMort_F","O_support_Mort_F","O_support_AMort_F","I_support_Mort_F","I_support_AMort_F","B_Protagonist_VR_F"];
	_bobcatteam = ["B_crew_F","B_engineer_F","B_Protagonist_VR_F","B_Helipilot_F","B_helicrew_F","B_Pilot_F","B_helicrew_F","O_helipilot_F","O_helicrew_F","O_Pilot_F","I_helipilot_F","I_helicrew_F","I_Pilot_F","B_soldier_UAV_F"];
	//_uav = ["B_soldier_UAV_F"];
	
	_veh = _this select 0;
	_seat = _this select 1;
	_player = _this select 2;
	
	
    waitUntil {player == player};
	
    _iampilot = ({typeOf player == _x} count _pilots) > 0;
	_iamjetpilot = ({typeOf player == _x} count _jetpilots) > 0;
	_iammoerser = ({typeOf player == _x} count _moerserteam) > 0;
	_iambob = ({typeOf player == _x} count _bobcatteam) > 0;
	//_iamuav = ({typeOf player == _x} count _uav) > 0;
	

            //Heli & Jetpilot can be pilot seat only on the Heli
            if((_veh isKindOf "Air") && !(_veh isKindOf "ParachuteBase")) then 
			{
				if(_veh isKindof "B_Heli_Attack_01_F") then 
				{
					_forbidden = [gunner _veh];
					if((player in _forbidden) and (isnull driver _veh)) exitWith 
					{
						systemChat "wait for pilot";
						sleep 0.5;
						player action ["eject", _veh];
						player action["Getout", _veh];
					};
				};
				if(_veh isKindof "B_Heli_Transport_01_F") then 
				{
					_forbidden = [gunner _veh];
					if((player in _forbidden) and (isnull driver _veh)) exitWith 
					{
						systemChat "wait for pilot";
						sleep 0.5;
						player action ["eject", _veh];
						player action["Getout", _veh];
					};
				};
				if(_veh isKindof "I_Heli_light_03_F") then 
				{
					_forbidden = [gunner _veh];
					if((player in _forbidden) and (isnull driver _veh)) exitWith 
					{
						systemChat "wait for pilot";
						sleep 0.5;
						player action ["eject", _veh];
						player action["Getout", _veh];
					};
				};
				if(_veh isKindof "I_Heli_light_03_unarmed_F") then 
				{
					_forbidden = [gunner _veh];
					if((player in _forbidden) and (isnull driver _veh)) exitWith 
					{
						systemChat "wait for pilot";
						sleep 0.5;
						player action ["eject", _veh];
						player action["Getout", _veh];
					};
				};
				if(_veh isKindof "B_T_VTOL_01_infantry_F") then 
				{
					_forbidden = [gunner _veh];
					if((player in _forbidden) and (isnull driver _veh)) exitWith 
					{
						systemChat "wait for pilot";
						sleep 0.5;
						player action ["eject", _veh];
						player action["Getout", _veh];
					};
				};
						
				if(!_iampilot) then 
				{
					_forbidden = [driver _veh, _veh turretUnit [0]];
					if ((player in _forbidden) and !(player == gunner _veh)) then 
					{
						systemChat "You must be a pilot to fly this aircraft";
						sleep 0.5;
						player action ["eject", _veh];
						player action["Getout", _veh];
					};
				};
            };
			// Jetpilot can be pilot seat only on the Jet
			if((vehicleVarName _veh in ["MH_3", "MH_4", "MH_5", "MH_6", "Wipeout", "BlackWasp"]) && !(_veh isKindOf "ParachuteBase")) then 
			{
				if(!_iamjetpilot) then 
				{
					_forbidden = [driver _veh, _veh turretUnit [0]];
					if(player in _forbidden) then 
					{
						systemChat "You must be a licencepilot to fly this aircraft";
						sleep 0.5;
						player action ["eject", _veh];
						player action["Getout", _veh];
					};
				};
            };
			if((vehicleVarName _veh in ["MH_2", "LB_1"]) && !(_veh isKindOf "ParachuteBase")) then 
			{
				if(!_iamjetpilot) then 
				{
					_forbidden = [driver _veh];
					if(player in _forbidden) then 
					{
						systemChat "You must be a licencepilot to fly this aircraft";
						sleep 0.5;
						player action ["eject", _veh];
						player action["Getout", _veh];
					};
				};
            };
			if((_veh isKindof "I_Plane_Fighter_04_F") OR (_veh isKindof "O_Plane_Fighter_02_F")) then 
			{
				if(!_iamjetpilot) then 
				{
					_forbidden = [driver _veh, _veh turretUnit [0]];
					if(player in _forbidden) then 
					{
						systemChat "You must be a licencepilot to fly this aircraft";
						sleep 0.5;
						player action ["eject", _veh];
						player action["Getout", _veh];
					};
				};
			};
			//Bobcat
			if((_veh isKindof "B_APC_Tracked_01_CRV_F") && !(_veh isKindOf "ParachuteBase")) then 
			{
				if(!_iambob) then {
					_forbidden = [driver _veh, gunner _veh, commander _veh];
					if(player in _forbidden) then 
					{
						systemChat "You must be a Logistician to drive this vehicle";
						sleep 0.5;
						player action ["eject", _veh];
						player action["Getout", _veh];
					};
				};
            };
