// by Figuri001
// null = [this] execVM "GSG\gsg_rules.sqf";

_board = _this select 0;
_board addaction ["GSG Panzerregeln/Tankrules",{
_player = _this select 1;
sleep 0.5;
_txt = composeText [
"==============================================",
lineBreak,
lineBreak,
lineBreak,
parseText Format["<t size='1.7'>Hallo %1!</t>",name _player],
lineBreak,
lineBreak,
parseText"<t size='1'><a color='#FFFFFF'> - Panzerbesatzungen haben sich auf unserem TS³ einzufinden (ts.grenzschutzgruppe.de)! </a></t>",
lineBreak,
parseText"<t size='1'><a color='#FFFFFF'> - Panzer müssen mit mindestens 2 Personen besetzt sein! </a></t>",
lineBreak,
parseText"<t size='1'><a color='#FFFFFF'> - Panzer sind nur unterstützend einzusetzen! </a></t>",
lineBreak,
parseText"<t size='1'><a color='#FFFFFF'> - Panzer sind nach dem Einsatz wieder zurück in das HQ zu verbringen und abzuschließen! </a></t>",
lineBreak,
parseText"<t size='1'><a color='#FFFFFF'> - Die ausführlichen Regeln sind unter M-->Serverregeln oder in unserem Forum zu finden. </a></t>",
lineBreak,
lineBreak,
parseText"<t size='1'><img image='signs\GSGneu.paa' /><a color='#FFFFFF' href='http://www.grenzschutzgruppe.de'> Link GSG Website </a></t>",
lineBreak,
lineBreak,
parseText"<t size='1'><img image='signs\GSGneu.paa' /><a color='#FFFFFF' href='http://www.grenzschutzgruppe.de/index.php?bewerben'> Link Bewerbung für den GSG Clan</a></t>",
lineBreak,
lineBreak,
parseText"<t size='1'><img image='signs\GSGneu.paa' /><a color='#FFFFFF' href='http://www.grenzschutzgruppe.de'> Link Spenden</a></t>",
lineBreak,
lineBreak,
parseText"<t size='1'><img image='signs\GSGneu.paa' /><a color='#FFFFFF' href='http://www.grenzschutzgruppe.de/index.php?forum'> Link GSG Forum</a></t>",
lineBreak
];
"GSG Publicserver Regeln/Rules" hintC _txt;
hintC_arr_EH = findDisplay 72 displayAddEventHandler ["unload", { 0 = _this spawn { _this select 0 displayRemoveEventHandler ["unload", hintC_arr_EH]; hintSilent ""; }; }];
diag_log composeText ["Nutzung Panzeregeln von ",name _player];
}];