/*
null=[vehicle,position,raduis,type] execVM "gsg_vehmovetonow.sqf";

type:
"MOVE"
"DESTROY"
"GETIN"
"SAD"
"GETOUT"
"UNLOAD"
 
"HOLD"
...
*/

private["_veh","_target","_radius","_wparray","_wptype","_wp"];
_veh=[_this,0,objNull,[objNull]] call BIS_fnc_param;
_target=[_this,1,[0,0,0],[objNull,[]]] call BIS_fnc_param;
_radius=[_this,2,15,[0]] call BIS_fnc_param;
_wptype=[_this,3,"MOVE",[""]] call BIS_fnc_param;

if ((typename _target)=="OBJECT") then {
_target = getPosATL _target;
};
_wparray = waypoints group _veh;
reverse _wparray;
{deleteWaypoint _x;} foreach _wparray;
_wp = group _veh addWaypoint [_target, _radius];
_wp setWaypointType _wptype;