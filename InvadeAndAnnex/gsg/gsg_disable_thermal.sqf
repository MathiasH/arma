

[] spawn
{
_layer = 85125;
_items2 = primaryWeaponItems player;

while {true} do
{	
//waitUntil {inputAction "nightVision" > 0};
{
     if (("optic_Nightstalker" in primaryWeaponItems player) || ("optic_tws" in primaryWeaponItems player) || ("optic_tws_mg" in primaryWeaponItems player)) then
	 {
		if (currentVisionMode player == 2) then
		{
			_layer cutText ["Thermal is currently OFFLINE!","BLACK",-1];
			if (( vehicle player != player ) OR !(isNull (getConnectedUav player))) then
			{
				hint format ["To use Thermal in Vehicles remove your thermal optic"]
			};
			waituntil {currentVisionMode player != 2};
			_layer cutText ["", "PLAIN"];
		};
	};
} forEach _items2;
sleep 0.1;
};
};