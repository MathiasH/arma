/*
* Copyright (c) 2016, GSG Grenzschutzgruppe
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*    Author:           Apus
*    Version:          1.00
*    Date:             29.11.2016
*/

_unit = _this select 0;// unit with addaction
_player = _this select 1;// player who selects addaction
_addaction = _this select 2;// the aDDACTION	

	_panzerteam = ["B_crew_F","B_engineer_F","B_Protagonist_VR_F"];
	waitUntil {player == player};
	_iampanzer = ({typeOf player == _x} count _panzerteam) > 0;
	if(_iampanzer) then 
	{
		_unit setVehicleLock "LOCKED";
		systemChat (localize "STR_GSG_CLOSE");
	};
	if!(_iampanzer) then 
	{
		systemChat (localize "STR_GSG_NOKEY");
	};
	