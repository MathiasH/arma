/*
by Figuri001[GSG]
---------dieses in die Init.sqf kopieren------------
if (!isDedicated) then
{
	waitUntil {!isNull player && isPlayer player};
	[] spawn (compile preprocessFile "GSG\gsg_check_Lizenz.sqf");
};
----------------------------------------------------
*/
private["_uid", "_name","_slot1","_slot2","_slot3","_slot4","_slot5","_slot6","_slot7"]; 

_uid = getPlayerUID player;
_name = name player;
_slot1 = Clanlizenz + ClanJetlizenz; // Array mit UserID für die Berechtigung
_slot2 = ClanTankLizenz + TankLizenz + Clanlizenz; // Array mit UserID für die Berechtigung
_slot3 = ClanJetlizenz + Jetlizenz + Clanlizenz;
_slot4 = Clanlizenz + ClanTankLizenz;
_slot5 = ModLizenz; 
_slot6 = BlackListPilot; 
_slot7 = BlackListTank; 
_pilots = ["B_Helipilot_F","O_helipilot_F","B_Pilot_F","B_soldier_UAV_F","B_support_Mort_F","B_support_AMort_F","B_helicrew_F","B_recon_JTAC_F"];
_tanks = ["B_crew_F","B_engineer_F"];

waituntil {Alive Player};

switch (side player) do 
{ 

	case west: 
	{ 
		if (str player in ["GSG_Heli_1", "GSG_Heli_2"]) then 
		{
			if (_uid in _slot1) exitWith {sleep 2; [West,"HQ"] sideChat format["Berechtigung für %1 vorhanden!",_name];};
			if (true) exitWith 
			{
				[West,"HQ"] sideChat format["%1 ist nicht berechtigt für GSG-Clan Slots",_name];
				sleep 5;
				//endMission "forceEnd";
				"KeineBerechtigung" call BIS_fnc_endMission;
			}; 
		};
		
		if (str player in ["GSG_Liz_Heli_1", "GSG_Liz_Heli_2"]) then {
			if (_uid in _slot3) exitWith {sleep 2; [West,"HQ"] sideChat format["Berechtigung für %1 vorhanden!",_name];};
			if (true) exitWith {
				[West,"HQ"] sideChat format["%1 ist nicht berechtigt für Lizenz Platz",_name];
				sleep 5;
				endMission "forceEnd";
			}; 
		};

		if (str player in ["GSG_Tank_1","GSG_Tank_2","GSG_Tank_3"]) then 
		{
			if (_uid in _slot4) exitWith {sleep 2; [West,"HQ"] sideChat format["Berechtigung für %1 vorhanden!",_name];};
			if (true) exitWith 
			{
				[West,"HQ"] sideChat format["%1 ist nicht berechtigt für GSG-Clan Slots",_name];
				sleep 5;
				//endMission "forceEnd";
				"KeineBerechtigung" call BIS_fnc_endMission;
			}; 
		};
		
/* 		if (str player in ["GSG_Tank_4","GSG_Tank_5","GSG_Tank_6"]) then 
		{
			if (_uid in _slot2) exitWith {sleep 2; [West,"HQ"] sideChat format["Berechtigung für %1 vorhanden!",_name];};
			if (true) exitWith 
			{
				[West,"HQ"] sideChat format["%1 ist nicht berechtigt für GSG-Clan Slots",_name];
				sleep 5;
				endMission "forceEnd";
			}; 
		}; */
		
		if (str player in ["GSG_Mod_1", "GSG_Mod_2", "GSG_Mod_3", "GSG_Mod_4"]) then 
		{
			if (_uid in _slot5) exitWith {sleep 2; [West,"HQ"] sideChat format["Berechtigung für %1 vorhanden!",_name];};
			if (true) exitWith
			{
				[West,"HQ"] sideChat format["%1 ist nicht berechtigt für GSG-Moderatoren Slots",_name];
				sleep 5;
				//endMission "forceEnd";
				"KeineBerechtigung" call BIS_fnc_endMission;
			}; 
		};
		if (typeOf player in _pilots) then 
		{
			if !(_uid in _slot6) exitWith {sleep 2; [West,"HQ"] sideChat format["%1 steht nicht auf der Blacklist!",_name];};
			if (true) exitWith 
			{
				[West,"HQ"] sideChat format["%1 steht auf der Blacklist",_name];
				sleep 5;
				//endMission "forceEnd";
				"Blacklist" call BIS_fnc_endMission;
			};
		};

		if (typeOf player in _tanks) then 
		{
			if !(_uid in _slot7) exitWith {sleep 2; [West,"HQ"] sideChat format["%1 steht nicht auf der Blacklist!",_name];};
			if (true) exitWith 
			{
				[West,"HQ"] sideChat format["%1 steht auf der Blacklist",_name];
				sleep 5;
				//endMission "forceEnd";
				"Blacklist" call BIS_fnc_endMission;
			};
		};
	};
};

if (typeOf player in _pilots) then 
{
	cutText ["Logistiker/Piloten/UAV Operator/JTAC müssen sich im TeamSpeak3 unter IP: " + GSG_TS_IP + " anmelden!","PLAIN DOWN",3];

	// Pilotrules
	rulepilot = true;
	if(!createDialog "Pilotrules") exitWith {};
	waitUntil{!isNull (findDisplay 32154)}; //Wait for the spawn selection to be open.
	waitUntil{isNull (findDisplay 32154)}; //Wait for the spawn selection to be done.
	sleep 1;
	if(!rulepilot)then 
	{      
		diag_log format["%1 hat die Regeln nicht bestätigt",_name];	
        player enableSimulation false;
        ["Pilotrules",false,true] call BIS_fnc_endMission;
        sleep 35;
	};
	rulepilot = nil;
};

if (typeOf player in _tanks) then 
{
	cutText ["Panzerfahrer müssen sich im TeamSpeak3 unter IP: " + GSG_TS_IP + " anmelden!","PLAIN DOWN",3];

	// Tankrules
	ruletank = true;
	if(!createDialog "Tankrules") exitWith {};
	waitUntil{!isNull (findDisplay 32164)}; //Wait for the spawn selection to be open.
	waitUntil{isNull (findDisplay 32164)}; //Wait for the spawn selection to be done.
	sleep 1;
	if(!ruletank)then 
	{ 
		diag_log format["%1 hat die Regeln nicht bestätigt",_name];	
        player enableSimulation false;
        ["Tankrules",false,true] call BIS_fnc_endMission;
        sleep 35;
	};
	ruletank = nil;
};
