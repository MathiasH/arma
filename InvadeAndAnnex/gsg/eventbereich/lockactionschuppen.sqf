/*
* Copyright (c) 2016, GSG Grenzschutzgruppe
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*    Author:           Apus
*    Version:          1.00
*    Date:             29.11.2016
*/
if (isserver) then 
{
	_bar = _this select 0;
	_bar1 = _bar select 0;
	_bar2 = _bar select 1;

	_door = _this select 1;
	_door1 = _door select 0;
	_door2 = _door select 1;
	sleep 0.5;
	//schuppen setVariable [_door1, 1, true];
	//schuppen setVariable [_door2, 1, true];
	
	_dooranim = _this select 2;
	_dooranim1 = _dooranim select 0;
	_dooranim2 = _dooranim select 1;
	schuppen animate [_dooranim1, 0];
	schuppen animate [_dooranim2, 0];
	sleep 1.5;
	playSound3D ["a3\sounds_f\vehicles\air\noises\SL_1hookLock.wss", _bar1];
	
	_bar1 hideObjectGlobal false; 
	_bar2 hideObjectGlobal false; 
};