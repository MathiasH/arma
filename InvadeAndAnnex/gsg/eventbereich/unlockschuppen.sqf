/*
* Copyright (c) 2016, GSG Grenzschutzgruppe
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*    Author:           Apus
*    Version:          1.00
*    Date:             29.11.2016
*/

private["_uid", "_name","_Mod","_obj","_caller","_id"]; 

_uid = getPlayerUID player;
_name = name player;
_Mod = ModLizenz; 
_obj = _this select 0;
_caller = _this select 1;
_id = _this select 2;
_bar =  (_this select 3) select 0;
_door = (_this select 3) select 1;
_dooranim = (_this select 3) select 2;

	if (_uid in _Mod) then
	{
		hint (localize "STR_GSG_Offen");
		_bar1 = _bar select 0;
		playSound3D ["a3\sounds_f\vehicles\air\Heli_Attack_01\blackfoot_gear_down_ext.wss", _bar1];
		[[_bar,_door,_dooranim], 'gsg\eventbereich\unlockactionschuppen.sqf'] remoteExec ["execVM",0,false];
	}
	else
	{
		playSound3D ["a3\sounds_f\vehicles\air\noises\heli_alarm_bluefor.wss", _obj];
		hint (localize "STR_GSG_Berechtigung");
	};

