
private ["_number","_Paracutes","_ctn"];
_heli = _this select 0;
_player = _this select 1;
_ctn = 0;

if (isNull (unitBackpack _player)) exitwith {hint (localize "STR_GSG_NoBackpack")};
if (backpack _player == "B_Parachute") exitwith {hint (localize "STR_GSG_HasParachute")};

_cargo = backpackCargo _heli;

_Paracutes = ["B_Parachute"];
_cnt = {_x == "B_Parachute"} count _cargo;

_ctn = _cnt - 1;

if (_ctn < 1 && !("B_Parachute" in _cargo)) exitwith {hint (localize "STR_GSG_NoParachutes");};

clearBackpackCargoGlobal _heli;

{
if !(_x in _Paracutes) then {_heli addBackpackCargoGlobal [_x,1];};
} foreach _cargo;
_heli addBackpackCargoGlobal ["B_Parachute",_ctn];

_backpackp = backpack _player;
_bpackitems = backpackItems _player;
removeBackpack _player;

_player addbackpack "B_Parachute";
_player setVariable ["GSG_BackpackonChest", "1"];
_player forceWalk true;

private _obj = createSimpleObject [_backpackp, [0,0,0]];

_obj attachTo [_player, [0.02,-0.04,-0.3], "pelvis"];
_obj setDir 180;

_player addAction["<t color='#09810b'>Put backpack on back</t>","gsg\gsg_backpackonback.sqf",[_bpackitems,_backpackp,_obj],1.5,true,true,"","!(vehicle player != player)"];

_player addEventHandler ["Killed",
{
	_player = _this select 0;
	_player setVariable ["GSG_BackpackonChest", "0"];
	_player forceWalk false;
}];

waituntil {!(alive _player) || !(alive _obj)};

	deleteVehicle _obj;
	removeBackpack player;
	player addbackpack _backpackp;
	{
		player addItemToBackpack _x;
	} foreach _bpackitems;


