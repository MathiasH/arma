
_player = _this select 1;
_id = _this select 2;
_arg = _this select 3;

_bpackitems = _arg select 0;
_backpack = _arg select 1;
_obj = _arg select 2;

_player addbackpack _backpack;
_player setVariable ["GSG_BackpackonChest", "0"];
_player forceWalk false;
deleteVehicle _obj;

_player removeaction _id;

/* {
	_player addItemToBackpack _x;
} foreach _bpackitems; */