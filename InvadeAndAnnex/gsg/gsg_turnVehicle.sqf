private ["_vector","_vehlist","_vehicletypes"];

while {true} do 
{
	_vehlist = vehicles;		//<--- Erstellt eine Liste aller Fahrzeuge auf der Karte
	_vehicletypes = ["O_APC_Tracked_02_AA_F","O_APC_Tracked_02_cannon_F","O_APC_Wheeled_02_rcws_F","O_MBT_02_cannon_F","O_Truck_02_covered_F","O_Truck_02_transport_F","O_MRAP_02_gmg_F","O_MRAP_02_hmg_F"];
	{
		/* if ((_x isKindOf "O_APC_Tracked_02_AA_F")||(_x isKindOf "O_APC_Tracked_02_cannon_F")||(_x isKindOf "O_APC_Wheeled_02_rcws_F")||(_x isKindOf "O_MBT_02_cannon_F")||(_x isKindOf "O_Truck_02_covered_F")||(_x isKindOf "O_Truck_02_transport_F")||(_x isKindOf "O_MRAP_02_gmg_F")||(_x isKindOf "O_MRAP_02_hmg_F")||(_x isKindOf "O_MBT_02_arty_F")) then {		//<--- Kontrolliert die Fahrzeugliste auf zu Prüfende Fahrzeuge */
		if (typeOf _x in _vehicletypes) then 
		{
			_vector = vectorUp _x;
	
			If (_vector select 2 <= 0.60) then //<--- Prüft die Rotation des Fahrzeuges
			{		

				_x setDir getDir _x;
				_x setPos [(getPos _x select 0), (getPos _x select 1), ((getPos _x select 2) + 3)];
	
			};
		};
	} forEach _vehlist;
	sleep 15;
};