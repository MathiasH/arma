/*
randomPos = [getMarkerPos "test1", 50] call aw_fnc_randomPos;
spawngroup =  createGroup east;
spawnGroup = [randomPos, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OIA_InfSquad")] call BIS_fnc_spawnGroup;
"O_Soldier_AA_F" createUnit [randomPos, spawnGroup];
"O_spotter_F" createUnit [randomPos, spawnGroup];
truck = "O_Truck_02_transport_F" createVehicle randomPos;
units spawngroup select (count units spawngroup -1) moveInDriver truck;
{_x moveInCargo Truck} foreach units spawngroup;
 null=[truck,getpos player,50,"UNLOAD"] execVM "GSG\gsg_vehmovetonow.sqf";
[spawnGroup, getPos Player,50] call aw_fnc_spawn2_perimeterPatrol;
*/
//[Gruppe,Art der Patrolie] execVM "GSG\gsg_inf_to_ao.sqf";

private["_handel","_randomVeh","_randomPos","_spawngroup","_spawngroupD","_truck","_startpoint","_gopoints","_crew","_veh","_units","_roads","_randomVehT","_randomPosT","_roadsT","_time","_a"];


_spawngroup = [_this,0,grpNull,[grpNull]] call BIS_fnc_param;
_patrol = [_this,1,"",[""]] call BIS_fnc_param;

// zufälliges Fahrzeug als Transportmittel
_randomVeh = ["O_Truck_02_transport_F","O_Truck_02_covered_F","O_Truck_03_transport_F","O_Truck_03_covered_F","O_APC_Tracked_02_cannon_F"] call BIS_fnc_selectRandom;

// alle möglichen Startpositionen
_gopoints = call compile preprocessFileLineNumbers "sm\Var\gopoints.sqf";

// Startpunkt finden
_a=false;
while {!_a} do {
_startpoint = _gopoints call BIS_fnc_selectRandom;
_a=((getMarkerPos _startpoint distance getMarkerPos currentAO) < 5000 and _startpoint != currentAO);
};

// Straße am Startpunkt finden
while {true} do {
_roundPos = [getMarkerPos _startpoint, 1000] call aw_fnc_randomPos;
_roads = _roundPos nearRoads 75;
	if (count _roads > 0) exitWith{
	_randomPos = getPos	(_roads select floor random (count _roads));
	};
};

waitUntil{!isNil "_randomPos"};

//Transportfahrzeug und Crew erstellen und der Gruppe zuweisen
//==========================================================================================================================================================
_veh = [_randomVeh,_randomPos] call {_truck = (_this select 0) createVehicle (_this select 1); createVehicleCrew _truck; _units = crew _truck; [_truck,_units]};
waitUntil{!isNil "_veh"};
_truck = _veh select 0;
_crew = _veh select 1;


_crew join _spawngroup;
{_x moveInCargo _truck;} foreach units _spawngroup;
//==========================================================================================================================================================

//Straße am Zielpunkt finden
while {true} do {
_roundPosT = [getMarkerPos currentAO, 1000] call aw_fnc_randomPos;
_roadsT = _roundPosT nearRoads 75;
	if (count _roadsT > 0) exitWith{
	_randomPosT = getPos	(_roadsT select floor random (count _roadsT));
	};
};

waitUntil{!isNil "_randomPosT"};

//Script "Befehl zum Entladen der Einheiten am Zielort"
_handel= [_truck,_randomPosT,1,"UNLOAD"] execVM "GSG\gsg_vehmovetonow.sqf";
waitUntil {scriptDone _handel};

//Abfragen angekommen
if (typeof _truck == "O_APC_Tracked_02_cannon_F") then
	{
			waitUntil{sleep 2;count crew _truck <=3 and _truck distance getMarkerPos currentAO < 1200;};
			[driver _truck] join createGroup east;
			[gunner _truck] join driver _truck;
			[commander _truck] join driver _truck;
	}
	else
	{ 
			waitUntil{sleep 2;count crew _truck <=1 and _truck distance getMarkerPos currentAO < 1200;};
			[driver _truck] join createGroup east;
	};
//abgesetzte Gruppe Patrolienbefehl
if (_patrol == "Perimeter") then 
	{
	[_spawngroup,getMarkerPos currentAO,PARAMS_AOSize] call aw_fnc_spawn2_perimeterPatrol;
	}
else
	{
	[_spawnGroup, getMarkerPos currentAO, PARAMS_AOSize] call aw_fnc_spawn2_randomPatrol;
	};

Waituntil{sleep 10;true};
// Truck fährt zurück zum Ausgangsort
group _truck addWaypoint [getMarkerPos _startpoint, 40];

// Truck und Crew werden nach Standartzeit gelöscht
[crew _truck +[_truck]] spawn AW_fnc_deleteOldUnitsAndVehicles;
[crew _truck +[_truck]] spawn AW_fnc_deleteOldSMUnits;
