/*
randomPos = [getMarkerPos "test1", 50] call aw_fnc_randomPos;
spawngroup =  createGroup east;
spawnGroup = [randomPos, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OIA_InfSquad")] call BIS_fnc_spawnGroup;
"O_Soldier_AA_F" createUnit [randomPos, spawnGroup];
"O_spotter_F" createUnit [randomPos, spawnGroup];
truck = "O_Truck_02_transport_F" createVehicle randomPos;
units spawngroup select (count units spawngroup -1) moveInDriver truck;
{_x moveInCargo Truck} foreach units spawngroup;
 null=[truck,getpos player,50,"UNLOAD"] execVM "GSG\gsg_vehmovetonow.sqf";
[spawnGroup, getPos Player,50] call aw_fnc_spawn2_perimeterPatrol;
*/
private["_handel","_randomVeh","_randomPos","_spawngroup","_spawngroupD","_truck","_startpoint","_gopoints"];
// zufälliges Fahrzeug als Transportmittel
_randomVeh = ["O_Truck_02_transport_F","O_Truck_02_covered_F","O_Truck_03_transport_F","O_Truck_03_covered_F","O_APC_Tracked_02_cannon_F"] call BIS_fnc_selectRandom;
// zufällige Startposition
_gopoints = [
	"Kalochori",
	"Sofia",
	"Dome",
	"Feres",
	"Pyrgos",
	"Skopos",
	"Neri",
	"Factory",
	"Syrta",
	"Zaros",
	"Chalkeia",
	"Pyrsos",
	"Dump",
	"Outpost",
	"Frini",
	"Swamp",
	"Rodopoli",
	"Charkia",
	"Alikampos",
	"Kavala",
	"Aggelochori",
	"Agios Dionysios",
	"Poliakko",
	"Paros",
	"Molos",
	"The wind turbines",
	"Therisa",
	"Delfinaki outpost",
	"Panochori",
	"The Stadium",
	"The Xirolimni Dam",
	"The small industrial complex",
	"Negades",
	"Fotia",
	"Galati",
	"Ghosthotel",
	"Ioannina",
	"Kore"
];
waituntil {
_startpoint = _gopoints call BIS_fnc_selectRandom;
(getMarkerPos _startpoint distance getMarkerPos currentAO) < 5000 and _startpoint != currentAO; 
};
_randomPos = [getMarkerPos _startpoint, 50] call aw_fnc_randomPos;

_spawngroup =  createGroup east;
_spawngroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OIA_InfSquad")] call BIS_fnc_spawnGroup;
"O_Soldier_AA_F" createUnit [_randomPos, _spawngroup];
"O_engineer_F" createUnit [_randomPos, _spawngroup];
_truck = _randomVeh createVehicle _randomPos;
units _spawngroup select (count units _spawngroup -1) moveInDriver _truck;
{_x moveInCargo _truck;} foreach units _spawngroup;
_handel= [_truck,getMarkerPos currentAO,150,"UNLOAD"] execVM "GSG\gsg_vehmovetonow.sqf";
waitUntil {scriptDone _handel};
if (typeof _truck == "O_APC_Tracked_02_cannon_F") then
	{
			waitUntil{sleep 1;count crew _truck <=3 and _truck distance getMarkerPos currentAO < 250;};
			[driver _truck] join createGroup east;
			[gunner _truck] join driver _truck;
			[commander _truck] join driver _truck;
	}
	else
	{ 
			waitUntil{sleep 1;count crew _truck <=1 and _truck distance getMarkerPos currentAO < 250;};
			[driver _truck] join createGroup east;
	};

group _truck addWaypoint [getMarkerPos _startpoint, 40];
[_spawngroup,getMarkerPos currentAO,PARAMS_AOSize] call aw_fnc_spawn2_perimeterPatrol;
waitUntil {_truck distance getMarkerPos _startpoint < 50};
{_truck deleteVehicleCrew _x} forEach crew _truck;
deleteVehicle _truck;