class Pilotrules
{
	idd = 32154;
	name = "Pilotrules";
	movingEnable = false;
	enableSimulation = false;
    
	class controlsBackground
	{
		class GSG_RscTitleBackground : GSG_RscText
		{
			colorBackground[] = {0, 0, 0, 0.7};
			idc = -1;
			x = 0.0875;
			y = 0.06;
			w = 0.825;
			h = 0.04;
		};
		
		class GSG_RscBackground : GSG_RscText
		{
			colorBackground[] = {0.431,0.431,0.431,0.9};
			idc = -1;
			x = 0.0875;
			y = 0.12;
			w = 0.825;
			h = 0.82;
		};
		
		class GSG_RscTitleText : GSG_RscTitle
		{
			colorBackground[] = {0, 0, 0, 0};
			idc = -1;
			text = $STR_GSG_Pilot;
			x = 0.0875;
			y = 0.06;
			w = 0.825;
			h = 0.04;
            class Attributes 
			{
				align = "center";
			};
		};
				
		class GSG_RsclolStatus : GSG_RscStructuredText
		{
			idc = -1;
			colorBackground[] = {0, 0, 0, 0};
            sizeEx = 0.0001;
			text = $STR_GSG_Pilotrules;
			x = 0.1125;
			y = 0.16;
			w = 0.775;
			h = 0.66;
		};	
	};
    
    	class Controls
	{
		class naz : GSG_RscButtonMenu
		{
			idc = -1;
			text = $STR_GSG_NOTACCEPT;
			colorBackground[] = {0.886,0,0,0.7};
			onButtonClick = "rulepilot = false; closeDialog 0;";
			x = 0.1075;
            y = 0.88;
            w = 0.3;
            h = 0.04;
            class Attributes 
			{
				align = "center";
			};
		};
		
		
		class az : GSG_RscButtonMenu
		{
			idc = -1;
			text = $STR_GSG_ACCEPT;
			colorBackground[] = {0.161,0.741,0,0.7};
			onButtonClick = "rulepilot = true; closeDialog 0;";
            x = 0.5825;
            y = 0.88;
            w = 0.3;
            h = 0.04;
            class Attributes 
			{
				align = "center";
			};    
		};
	};
};