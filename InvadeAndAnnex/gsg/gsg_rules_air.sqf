// by Figuri001
// null = [this] execVM "GSG\gsg_rules.sqf";

_board = _this select 0;
_board addaction ["GSG Pilotenegeln/Pilotrules",{
_player = _this select 1;
sleep 0.5;
_txt = composeText [
"==============================================",
lineBreak,
lineBreak,
lineBreak,
parseText Format["<t size='1.7'>Hallo %1!</t>",name _player],
lineBreak,
lineBreak,
parseText"<t size='1'><a color='#FFFFFF'> - Logistiker, Piloten, UAV-Operator, and JTAC haben sich auf unserem TS³ Server einzufinden (ts.grenzschutzgruppe.de)! </a></t>",
lineBreak,
parseText"<t size='1'><a color='#FFFFFF'> - Piloten sind für ihr Fluggerät verantwortlich! </a></t>",
lineBreak,
parseText"<t size='1'><a color='#FFFFFF'> - CAS muss per Sidechat oder durch roten Rauch durch die infanterie im Einsatzgebiet angefordert werden! </a></t>",
lineBreak,
parseText"<t size='1'><a color='#FFFFFF'> - Starts und Landungen müssen im Sidechat angekündigt werden! </a></t>",
lineBreak,		
parseText"<t size='1'><a color='#FFFFFF'> - Das ist ein Multiplayer Server, keine Flugschule! </a></t>",
lineBreak,		
parseText"<t size='1'><a color='#FFFFFF'> - CAS Anforderungen durch den JTAC müssen priorisiert behandelt werden! </a></t>",
lineBreak,		
parseText"<t size='1'><a color='#FFFFFF'> - Die ausführlichen Regeln sind unter M-->Serverregeln oder in unserem Forum zu finden. </a></t>",
lineBreak,
lineBreak,
parseText"<t size='1'><img image='signs\GSGneu.paa' /><a color='#FFFFFF' href='http://www.grenzschutzgruppe.de'> Link GSG Website </a></t>",
lineBreak,
lineBreak,
parseText"<t size='1'><img image='signs\GSGneu.paa' /><a color='#FFFFFF' href='http://www.grenzschutzgruppe.de/index.php?bewerben'> Link Bewerbung für den GSG Clan</a></t>",
lineBreak,
lineBreak,
parseText"<t size='1'><img image='signs\GSGneu.paa' /><a color='#FFFFFF' href='http://www.grenzschutzgruppe.de'> Link Spenden</a></t>",
lineBreak,
lineBreak,
parseText"<t size='1'><img image='signs\GSGneu.paa' /><a color='#FFFFFF' href='http://www.grenzschutzgruppe.de/index.php?forum'> Link GSG Forum</a></t>",
lineBreak
];
"GSG Publicserver Regeln/Rules" hintC _txt;
hintC_arr_EH = findDisplay 72 displayAddEventHandler ["unload", { 0 = _this spawn { _this select 0 displayRemoveEventHandler ["unload", hintC_arr_EH]; hintSilent ""; }; }];
diag_log composeText ["Nutzung Pilotenregeln von ",name _player];
}];