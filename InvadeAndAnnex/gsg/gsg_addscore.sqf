_object = _this select 0;
_Killer = _this select 1;


	_type = typeOf _object;
	_Objectname = 
	{
		if ( isClass( configFile >> _x >> _type ) ) exitWith 
		{
			getText( configFile >> _x >> _type >> "displayName" )
		};
	}forEach [ "CfgVehicles", "CfgNonAIVehicles" ];


diag_log format["%1 destroyed %2",name _Killer,_Objectname];
//_Killer sidechat format ["Congratulations, %2 destroyed %1 and got 5 Points",_Objectname, name _Killer];
[West,"HQ"] sidechat format ["Good Job, %2 destroyed %1 and got 5 Points",_Objectname, name _Killer];

[_killer,5] call BIS_fnc_addScore;
