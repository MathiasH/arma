/*
=====================================================================================================
=						Landschaftsreparaturscript - Landscaperepairscript							=
=																									=
=	Skript zum Entfernen von Krater in der Landschaft.												=
=																									=
=	diesen Code in die Init-Zeile des Reparaturfahrzeugs schreiben:									=
=																									=
= [this] execVM "\...\gsg_bobcat_landscape_repair.sqf";												=
=																									=
= Script by (GSG)Figuri001[GER]
=====================================================================================================
*/

private ["_gsg_unit","_gsg_obj"];
_gsg_unit = _this select 0;

while{true} do {
	_gsg_obj=nearestObjects [position _gsg_unit , ["craterlong","craterlong_small"], 7];
	if (count _gsg_obj > 0) then {
		{
		deleteVehicle _x;
		} foreach _gsg_obj;
	};
	sleep 1;
};
