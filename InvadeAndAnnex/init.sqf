/*
      ::: ::: :::             ::: :::             :::
     :+: :+:   :+:           :+:   :+:           :+:
    +:+ +:+     +:+         +:+     +:+         +:+
   +#+ +#+       +#+       +#+       +#+       +#+
  +#+ +#+         +#+     +#+         +#+     +#+
 #+# #+#           #+#   #+#           #+#   #+#
### ###             ### ###             ### ###

| AHOY WORLD | ARMA 3 ALPHA | STRATIS DOMI VER 2.82 |

Creating working missions of this complexity from
scratch is difficult and time consuming, please
credit http://www.ahoyworld.co.uk for creating and
distibuting this mission when hosting!

This version of Domination was lovingly crafted by
Jack Williams (Rarek) for Ahoy World!
*/

//Headless Client + Blacklist
[true,30,false,true,30,3,true,
[
	"O_MBT_02_arty_F",
	"O_static_AA_F",
	"arti_1",
	"arti_2",
	"B_UAV_AI",
	"O_Truck_02_transport_F",
	"O_Truck_02_covered_F",
	"O_Truck_03_transport_F",
	"O_Truck_03_covered_F",
	"O_APC_Tracked_02_cannon_F",
	"O_T_VTOL_02_infantry_hex_F",
	"O_Heli_Light_02_F"
]] execVM "WerthlesHeadless.sqf";

//Wetter
execVM "misc\randomWeather2.sqf";

private ["_pos","_uavAction","_isAdmin","_i","_isPerpetual","_accepted","_position","_randomWreck","_validTarget","_targetsLeft","_flatPos","_targetStartText","_radioTowerDownText","_targetCompleteText","_null","_unitSpawnPlus","_unitSpawnMinus","_missionCompleteText","_start","_handle"];
if (isServer) then {_start = diag_tickTime};

//was tut es? ohne startet die Mission nicht
leaFunction =  compile preprocessFileLineNumbers "lea\loadout-init.sqf"; 
call leaFunction;

// JIP Check (This code should be placed first line of init.sqf file)
if (!isServer && isNull player) then {isJIP=true;} else {isJIP=false;};

//Grab parameters and put them into readable variables
for [ {_i = 0}, {_i < count(paramsArray)}, {_i = _i + 1} ] do
{
	call compile format
	[
		"PARAMS_%1 = %2",
		(configName ((missionConfigFile >> "Params") select _i)),
		(paramsArray select _i)
	];
};

//Set GSG Lizenzen
if (isMultiplayer) then 
{
	if (isServer) then 
	{
		GSG_Lizenzen = {call compile preprocessFile "\GSG\syncGSGLizenzen.sqf";};
		call GSG_Lizenzen;
		"GSG_Lizenzen_update" addPublicVariableEventHandler {call GSG_Lizenzen;};		
	}
	else
	{
		publicVariableServer "GSG_Lizenzen_update";
	};
}
else
{
	AUID = "_SP_PLAYER";
	stAUID = AUID;
	TankLizenz = [];
	Jetlizenz = [];
	ClanLizenz =["_SP_PLAYER_"];
	ClanJetlizenz = [];
	ClanTankLizenz = [];
	ModLizenz = [];
	BlackListTank = [];
	BlackListPilot = [];
	GSG_TS_IP = "ts.grenzschutzgruppe.de";
}; // Singleplayermodus

[] spawn compile preprocessFile "=BTC=_TK_punishment\=BTC=_tk_init.sqf";

_handle = [] execVM "aw_functions.sqf";
waitUntil{scriptDone _handle};

if (isserver) then {diag_log format ["aw_functions geladen in %1",(diag_tickTime - _start)];};

_igiload = [] execVM "IgiLoad\IgiLoadInit.sqf";

if(isMultiplayer) then
{
	if(PARAMS_DebugMode == 1) then {DEBUG = true} else {DEBUG = false};
	if(PARAMS_DebugMode2 == 1) then {DEBUG2 = true} else {DEBUG2 = false};
} else {DEBUG = true};

// Disable saving to save time
enableSaving [false, false];

// Disable automatic radio messages
enableSentences false;

// Eventhandler werden bei jedem geladen
call GSG_fnc_allEH;

if (isserver) then {diag_log format ["EH geladen in %1",(diag_tickTime - _start)];};

/* =============================================== */
/* ================ PLAYER SCRIPTS =============== */

execVM "misc\gsg_deletegroups.sqf";

//Camshake deaktiviert aufgrund Fehler der Version 1.38
enableCamShake false; 

[player] execVM "scripts\crew\crew.sqf";

if (PARAMS_ReviveEnabled == 1) then {call compile preprocessFile "=BTC=_revive\=BTC=_revive_init.sqf";};
if (PARAMS_PlayerMarkers == 1) then { _null = [] execVM "misc\playerMarkers.sqf"; };
	
call GSG_fnc_mp_funktionen;

call compileFinal preprocessFileLineNumbers "Script_XENO_Taru_Pod\XENO_Taru_Pod_Mod.sqf";

if (!isServer) exitWith
{
	_uavAction = player addAction
	[
		"<t color='#FFCF11'>Activate Personal UAV</t>",
		"uavView.sqf",
		[
			currentAO
		],
		0,
		false,
		true
	];
};


