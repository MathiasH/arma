class GSG
{
	tag = "GSG";
	class Group_Spawn
	{
		file = "sm\Funktionen\groups";
		class reconpatrol {};
		class squadsdefend {};
		class squadspatrol {};
		class teampatrol {};
		class blu_sol {};
		class opf_sol {};		
	};
	class Mission
	{
		file = "sm";
		class Main {};
		class aoRetake {};
		class airDefense {};
		class heloDefense {};
		class openSM {};
		class priorityTargets {};
		class sideBlueMissions {};
		//class sideMissions {};
	};
	class Scripts
	{
		file = "sm\Scripts";
		class pt_wracks {};
		class aapost {};
		class radarTower {};
		class radioTower {};		
	};
	class Funktionen
	{
		file = "sm\Funktionen\gsg";
		class reinforcements {};
		class delete_null {};		
		class deleteRuins {};
		class createMarker {};
		class startpunkt {};
		class AnzUnitsAO {};
		class group_change {};
		class mp_funktionen {};
		class ao_retake_spawn_Units {};
		class allEH {};
		class AnzPlayer {};
	};
};

class AW
{
	tag = "AW";
	class Funktionen
	{
		file="sm\Funktionen\aw";
		class spawnUnits {};
		class rewardPlusHintMI {};
		class rewardPlusHintJet {};
		class rewardPlusHintBLUE {};
		class rewardPlusHintBlueOld {};
		class rewardPlusHint {};
		class deleteSingleUnit {};
		class deleteOldSMUnits {};
		class deleteOldUnitsAndVehicles {};
		class deleteOldAOUnits {};
		class minefield {};
		class garrisonBuildings {};
	};
};