/*	Marker erstellen, ändern oder löschen
	by Figuri001
	
	_marker = compile preprocessFileLineNumbers "gsg_createMarker.sqf";
	[Art,Markername,Mittelpunkt,MarkerType,MarkerFarbe,[Länge,Breite] oder Markertext] call _marker;
	
	Art 				= 	0 = "ICON" / 1 = "RECTANGLE" / 2 = "ELLIPSE" / 3 = löschen

	Markername 			= 	String
	
	Mittelpunkt 		= 	Position
	
	MarkerType/Brush 	= 	String MarkerType/Brush default "Empty"
							Type 	- https://community.bistudio.com/wiki/cfgMarkers
							Brush 	- https://community.bistudio.com/wiki/setMarkerBrush
							
	MarkerFarbe			= 	String 	- https://community.bistudio.com/wiki/setMarkerColor
	
	[Länge,Breite] 		=   Größe des Markerbrush	
	or
	Markertext			=	String - Text zum Marker (getht nur bei "ICON"
	
	Bsp.: 	[2,"Player",getPos Player, "DiagGrid","ColorRed",[100,100]] call compile preprocessFileLineNumbers "sm\Funktionen\gsg_createMarker.sqf";
			[0,"Player",getPos Player, "mil_dot","ColorBlack","TEXT"] call _marker;
			[4,"Player"] call _marker;
	*/
Private ["_marker","_markername","_markertxt","_mid","_typbru","_laenge","_breite","_farbe","_farben","_groesse"]; 

_farben=["Default","ColorBlack","ColorGrey","ColorRed","ColorGreen","ColorBlue","ColorYellow","ColorOrange","ColorWhite","ColorPink","ColorBrown","ColorKhaki","ColorWEST","ColorEAST","ColorGUER","ColorCIV","ColorUNKNOWN","Color1_FD_F","Color2_FD_F","Color3_FD_F","Color4_FD_F","ColorBLUFOR","ColorCivilian","ColorIndependent","ColorOPFOR"];

_art = [_this, 0, 0,[0]] call BIS_fnc_param;			
_markername = [_this, 1, "Empty Marker",[""]] call BIS_fnc_param;
_mid = [_this, 2, [0,0,0],[[]],[3]] call BIS_fnc_param;
_typbru = [_this, 3, "o_unknown", [""]] call BIS_fnc_param;
_farbe = [_this, 4, "Default", [""]] call BIS_fnc_param;
_groesse = [_this, 5, [50,50],["",[]],[2]] call BIS_fnc_param;

switch (typeName _groesse) do {
			case "ARRAY":
			{
			_laenge = [_groesse, 0, 50,[0]] call BIS_fnc_paramIn;
			_breite = [_groesse, 1, 50,[0]] call BIS_fnc_paramIn;
			_markertxt="";
			};
			case "STRING":
			{
			_laenge = 1;
			_breite = 1;
			_markertxt = _groesse;
			};
};

if !(_markername in allMapMarkers) then {
		_marker = createMarker[_markername,_mid];
}
	else
{
		_marker = _markername;
};

if !(_farbe in _farben) then {_farbe = "Default";};

switch (_art) do {
    
	case 0: {
	//ICON
	if !(isClass (configFile >> "CfgMarkers" >> _typbru)) then {_typbru = "o_unknown";}; 
	
	_marker setMarkerShape "ICON";
	_marker setMarkerPos _mid;
	_marker setMarkerType _typbru;
	_marker setMarkerColor _farbe;
	_marker setMarkerSize [1, 1];
	_marker setMarkerText _markertxt;
	};
	
    case 1: {
	//RECTANGLE
	if !(isClass (configFile >> "cfgMarkerBrushes" >> _typbru)) then {_typbru = "DiagGrid";}; 
	_marker setMarkerShape "RECTANGLE";
	_marker setMarkerPos _mid;
	_marker setMarkerBrush _typbru;
	_marker setMarkerSize [_laenge, _breite];
	_marker setMarkerColor _farbe;
};
	
	case 2: {
	//ELLIPSE
	if !(isClass (configFile >> "cfgMarkerBrushes" >> _typbru)) then {_typbru = "DiagGrid";}; 
	_marker setMarkerShape "ELLIPSE";
	_marker setMarkerPos _mid;
	_marker setMarkerBrush _typbru;
	_marker setMarkerSize [_laenge, _breite];
	_marker setMarkerColor _farbe;
	};
    
	case 3: {
	//löschen des Markers
	deleteMarker _marker;
	};
};
