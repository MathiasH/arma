private ["_randomPos","_spawnGroup","_pos","_units","_armourGroup","_armour","_airGroup","_air_veh","_airType","_type"];
_pos = getMarkerPos (_this select 0);
	_enemiesArray = [];

	_units = 0;
	for "_units" from 1 to PARAMS_SquadsPatrol do {
		sleep 0.5;
		_randomPos = [getMarkerPos currentAO, PARAMS_AOSize] call aw_fnc_randomPos;
		_randomPos = [_pos, PARAMS_AOSize, random 360] call BIS_fnc_relPos;
		_randomPos = [_randomPos,0,20,5,1,20,0] call BIS_fnc_findSafePos;
		if (surfaceiswater _randomPos) then {_type=1;}else{_type=0;};
		_pool=[0,_type] call compile preprocessFileLineNumbers "EOS\EOS_UnitPools.sqf";
		_spawnGroup = [_randomPos, EAST, _pool] call BIS_fnc_spawnGroup;
		//[_spawnGroup,"Random"] execVM "GSG\gsg_inf_to_ao.sqf"; //or "Perimeter"
		//[_spawnGroup, getMarkerPos currentAO, PARAMS_AOSize] call aw_fnc_spawn2_randomPatrol;
		[(units _spawnGroup)] call aw_setGroupSkill;
		[_spawnGroup, getMarkerPos currentAO, PARAMS_AOSize / 2] call aw_fnc_spawn2_randomPatrol;
			
		
		if(DEBUG) then
		{
			_name = format ["%1%2",name (leader _spawnGroup),_units];
			createMarker [_name,getPos (leader _spawnGroup)];
			_name setMarkerType "o_unknown";
			_name setMarkerText format ["GSG Squad Patrol %1",_units];
			_name setMarkerColor "ColorRed";
			[_spawnGroup,_name] spawn
			{
				private["_group","_marker"];
				_group = _this select 0;
				_marker = _this select 1;

				while{count (units _group) > 0} do
				{
					_marker setMarkerPos (getPos (leader _group));
					sleep 0.1;
				};
				deleteMarker _marker;
			};
		};

		_enemiesArray = _enemiesArray + [_spawnGroup];
	};

	_units = 0;
	for "_units" from 1 to PARAMS_SquadsDefend do {
		sleep 0.5;
		_randomPos = [getMarkerPos currentAO, PARAMS_AOSize] call aw_fnc_randomPos;
		_randomPos = [_pos, PARAMS_AOSize, random 360] call BIS_fnc_relPos;
		_randomPos = [_randomPos,0,20,5,1,20,0] call BIS_fnc_findSafePos;
		if (surfaceiswater _randomPos) then {_type=1;}else{_type=0;};
		_pool=[0,_type] call compile preprocessFileLineNumbers "EOS\EOS_UnitPools.sqf";
		_spawnGroup = [_randomPos, EAST, _pool] call BIS_fnc_spawnGroup;
		//[_spawnGroup,"Random"] execVM "GSG\gsg_inf_to_ao.sqf"; //or "Perimeter"
		//[_spawnGroup, getMarkerPos currentAO, PARAMS_AOSize] call aw_fnc_spawn2_randomPatrol;
		[(units _spawnGroup)] call aw_setGroupSkill;
		[_spawnGroup, getMarkerPos currentAO, PARAMS_AOSize / 2] call aw_fnc_spawn2_randomPatrol;
			
		
		if(DEBUG) then
		{
			_name = format ["%1%2",name (leader _spawnGroup),_units];
			createMarker [_name,getPos (leader _spawnGroup)];
			_name setMarkerType "o_unknown";
			_name setMarkerText format ["GSG Squad Patrol %1",_units];
			_name setMarkerColor "ColorRed";
			[_spawnGroup,_name] spawn
			{
				private["_group","_marker"];
				_group = _this select 0;
				_marker = _this select 1;

				while{count (units _group) > 0} do
				{
					_marker setMarkerPos (getPos (leader _group));
					sleep 0.1;
				};
				deleteMarker _marker;
			};
		};

		_enemiesArray = _enemiesArray + [_spawnGroup];
	};

	_units = 0;
	
	for "_units" from 1 to PARAMS_TeamsPatrol do {
	sleep 0.5;
		_randomPos = [getMarkerPos currentAO, PARAMS_AOSize] call aw_fnc_randomPos;
		_randomPos = [_pos, PARAMS_AOSize, random 360] call BIS_fnc_relPos;
		_randomPos = [_randomPos,0,20,5,1,20,0] call BIS_fnc_findSafePos;
		if (surfaceiswater _randomPos) then {_type=1;}else{_type=0;};
		_pool=[0,_type] call compile preprocessFileLineNumbers "EOS\EOS_UnitPools.sqf";
		_spawnGroup = [_randomPos, EAST, _pool] call BIS_fnc_spawnGroup;
		//[_spawnGroup,"Random"] execVM "GSG\gsg_inf_to_ao.sqf"; //or "Perimeter"
		//[_spawnGroup, getMarkerPos currentAO, PARAMS_AOSize] call aw_fnc_spawn2_randomPatrol;
		[(units _spawnGroup)] call aw_setGroupSkill;
		[_spawnGroup, getMarkerPos currentAO, PARAMS_AOSize / 2] call aw_fnc_spawn2_randomPatrol;
			
		
		if(DEBUG) then
		{
			_name = format ["%1%2",name (leader _spawnGroup),_units];
			createMarker [_name,getPos (leader _spawnGroup)];
			_name setMarkerType "o_unknown";
			_name setMarkerText format ["GSG Squad Patrol %1",_units];
			_name setMarkerColor "ColorRed";
			[_spawnGroup,_name] spawn
			{
				private["_group","_marker"];
				_group = _this select 0;
				_marker = _this select 1;

				while{count (units _group) > 0} do
				{
					_marker setMarkerPos (getPos (leader _group));
					sleep 0.1;
				};
				deleteMarker _marker;
			};
		};

		_enemiesArray = _enemiesArray + [_spawnGroup];
	};
	
	/*
	_units = 0;
	for "_units" from 1 to PARAMS_CarsPatrol do {
		_randomPos = [getMarkerPos currentAO, PARAMS_AOSize,6] call aw_fnc_randomPos;
		_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Motorized_MTP" >> "OIA_MotInf_Team")] call BIS_fnc_spawnGroup;
		[_spawnGroup, getMarkerPos currentAO, PARAMS_AOSize] call aw_fnc_spawn2_randomPatrol;
		(vehicle (leader _spawnGroup)) spawn aw_fnc_fuelMonitor;
		[(units _spawnGroup)] call aw_setGroupSkill;

		if(DEBUG) then
		{
			_name = format ["%1%2",name (leader _spawnGroup),_units];
			createMarker [_name,getPos (leader _spawnGroup)];
			_name setMarkerType "o_unknown";
			_name setMarkerText format ["Car %1",_units];;
			_name setMarkerColor "ColorRed";
			[_spawnGroup,_name] spawn
			{
				private["_group","_marker"];
				_group = _this select 0;
				_marker = _this select 1;

				while{count (units _group) > 0} do
				{
					_marker setMarkerPos (getPos (leader _group));
					sleep 0.1;
				};
				deleteMarker _marker;
			};
		};

		_enemiesArray = _enemiesArray + [_spawnGroup];
	};
*/
	for "_units" from 1 to ( 2 + floor random PARAMS_DefendArmourPatrol) do {
		//armour_veh = createGroup east;
		_randomPos = [getMarkerPos currentAO, PARAMS_AOSize] call aw_fnc_randomPos;
		_randomPos = [_pos, PARAMS_AOSize, random 360] call BIS_fnc_relPos;
		_randomPos = [_randomPos,0,100,5,0,20,0] call BIS_fnc_findSafePos;
		if (surfaceiswater _randomPos) then {_type=1;}else{_type=0;};
		_armour =["O_APC_Tracked_02_AA_F", "O_MBT_02_cannon_F", "O_APC_Tracked_02_cannon_F","O_MRAP_02_hmg_F","O_APC_Wheeled_02_rcws_F"];// select floor random 4 createVehicle _randomPos;
		_armour_Array = [_randomPos, (random 360), _armour call BIS_fnc_selectRandom, east] call BIS_fnc_spawnVehicle;
		_armour_veh = _armour_Array select 0;
		_armour_crew = _armour_Array select 1;
		_armour_group = _armour_Array select 2;
		[_armour_group,getMarkerPos currentAO,(PARAMS_AOSize / 2)] call aw_fnc_spawn2_randomPatrol;
		_armour_veh spawn aw_fnc_fuelMonitor;
		_enemiesArray = _enemiesArray + [_armour_group,_armour_veh];
		[(units _armour_group)] call aw_setGroupSkill;

		_armour_veh lock true;
		if(DEBUG) then
		{
			_name = format ["%1%2",name (leader _armour_veh),_units];
			createMarker [_name,getPos (leader _armour_veh)];
			_name setMarkerType "o_unknown";
			_name setMarkerText format ["APC %1",_units];;
			_name setMarkerColor "ColorRed";
			[_armour_veh,_name] spawn
			{
				private["_group","_marker"];
				_group = _this select 0;
				_marker = _this select 1;

				while{count (units _group) > 0} do
				{
					_marker setMarkerPos (getPos (leader _group));
					sleep 0.1;
				};
				deleteMarker _marker;
			};
		};
	};

	/*
	if((random 10 <= PARAMS_AirPatrol))
		then {
		//_air_group = createGroup east;
		_randomPos = [getMarkerPos currentAO, PARAMS_AOSize] call aw_fnc_randomPos;
		_randomPos deleteRange [2,2];
		_randomPos = _randomPos + [550];
		_airType = ["O_Heli_Attack_02_F","O_Heli_Light_02_F"];
		_air_Array = [_randomPos, 90, _airType call BIS_fnc_selectRandom, east] call BIS_fnc_spawnVehicle;
		_air_veh = _air_Array select 0; //Fahrzeug
		_air_crew = _air_Array select 1; //Crew
		_air_group = _air_Array select 2; //Gruppe
		[_air_group, getMarkerPos currentAO, (2 * (PARAMS_AOSize / 3))] call BIS_fnc_taskPatrol;
		_air_veh spawn aw_fnc_fuelMonitor;
		_enemiesArray = _enemiesArray + [_air_group,_air_veh];
		[(units _air_group)] call aw_setGroupSkill;

		if(DEBUG) then
		{
			_name = format ["%1%2",name (leader _air_group),_units];
			createMarker [_name,getPos (leader _air_group)];
			_name setMarkerType "o_unknown";
			_name setMarkerText format ["Air %1",_units];;
			_name setMarkerColor "ColorRed";
			[_air_group,_name] spawn
			{
				private["_group","_marker"];
				_group = _this select 0;
				_marker = _this select 1;

				while{count (units _group) > 0} do
				{
					_marker setMarkerPos (getPos (leader _group));
					sleep 0.1;
				};
				deleteMarker _marker;
			};
		};
	};

	{
		_newGrp = [_x] call AW_fnc_garrisonBuildings;
		if (!isNull _newGrp) then { _enemiesArray = _enemiesArray + [_newGrp]; };
	} forEach (getMarkerPos currentAO nearObjects ["House", 600]);
*/
_enemiesArray