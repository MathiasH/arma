//by Figuri001
// [GSG_AO_Groups] call GSG_fnc_AnzUnitsAO;

#define SPmax 8
#define SDmax 9
#define TPmax 4

private["_array","_units","_gsg_units","_gsg_groups","_groups","_grouID","_gsg_txt","_gsg_sol","_SP","_SD","_TP","_color","_anz","_SPmax","_SDmax","_TPmax"];
_array = [_this, 0,[],[[]]] call BIS_fnc_param;

_gsg_group=[];
_groups=[];
_units=[];
_anz=0;
_SPmax = SPmax;
_SDmax = SDmax;
_TPmax = TPmax;

{
call compile format["_%1=[]",_x];
for "_a" from 0 to (count (_array select _anz)- 1) do {
		call compile format["_%1 = _%1 + [(_array select _anz) select _a];",_x];
	};
	_anz = _anz + 1;
	_z = compile format ["player sidechat '%1: ' + str(_%1) + str (_%1max);",_x];
	call _z;
}forEach ["SP","SD","TP"];

if (debug) then {
	_gsg_units="";
	{
		_gsg_units = _gsg_units + format["<br/><br/>%1<br/>",_x];
			_a = compile format ["
			for '_z' from 0 to (count _%1 - 1) do {
				_groupID = _%1 select _z;
				_anz = {(alive _x)} count units _groupID;
				switch (true) do {
				case (_anz >= (_%1max/2)): {_color = '#3ADF00';};
				case (_anz < (_%1max/2) && _anz >= (_%1max/4)): {_gsg_group = _gsg_group + [[_groupID,""%1""]];_color = '#FFFF00';};
				case (_anz < (_%1max/4)): {_gsg_group = _gsg_group + [[_groupID,""%1""]];_color = '#FF0000';};
				default {_color = '#FFFFFF';};
				};
				_gsg_units = _gsg_units +""<br/>(%1)<t color='"" + _color + ""'>"" + str(_groupID) + ""</t>: "" + str(_anz);
			};",_x];
			call _a;
	}forEach ["SP","SD","TP"];
	_gsg_txt = _gsg_units;
	_gsg_txt = parseText _gsg_txt;
	_gsg_txt spawn {sleep 0.5; "TEST" hintC _this;};
};
_gsg_group