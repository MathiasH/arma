/* ---------------------------------------------------------------------------------------------------------

File: gsg_reinforcements.sqf
Author: Figuri001
       
Parameter(s):
_this select 0 <number> - 1=Truck; 2=Tank; 3=Helikopter; 4=Car; 0 and default=direkt to Marker
_this select 1 <String> - Unitsgroup ("SP" - Squadpatrol, "SD" - Squaddefend", "TP" - Teampatrol and "RP" - ReconPatrol)
_this select 2 <Array> - [Marker: (spawn position), Marker: (Landing Zone), Size]
_this select 3 <bool> - Debug Mode: (True = Enabled, False = Disabled (default))

Usage:
_nul = [Fahrzeug, [Group Alt, Einheiten neu],["spawnMrk", "LZMrk", Radius], false] call GSG_fnc_Group_Change; 
_grp = [815,"Ziel",100] call GSG_fnc_reconpatrol;
gsg_test = [3, [_grp, "SP"],["Kalochori", "Ziel"], false] call GSG_fnc_Group_Change;
 ---------------------------------------------------------------------------------------------------------*/
 
 if (!isServer) exitWith {};
 private ["_ranGrp","_veh","_infgrp","_randomPos","_veh","_mod","_side","_line","_spawnMrk","_LZMrk","_LZRad","_debugMode","_vehCrew","_startpoint","_flatPos"];
 //arguments definitions 
_veh_mod = [_this,0,0,[0]] call BIS_fnc_param;
_side = [_this,1,["","TP"],[[]],[2]] call BIS_fnc_param;
_line = [_this,2,[[""],[""],0],[[]],[2,3]] call BIS_fnc_param;
_spawnMrk = [_line, 0, "",[""]] call BIS_fnc_paramIn;
_LZMrk = [_line, 1, "",[""]] call BIS_fnc_paramIn;
_LZRad = [_line, 2, 300,[0]] call BIS_fnc_paramIn; 
_debugMode = [_this,3,false,[true]] call BIS_fnc_param;

_group_old = [_side,0,grpNull,[grpNull]] call BIS_fnc_paramIn;
_group_new = [_side,1,"",[""]] call BIS_fnc_paramIn;

player sidechat str(_group_old);

if (isNull _group_old) exitWith {hint "Keine Gruppe vorhanden"};

gsg_waypoints = {
	private ["_nearestEnemies","_infGrp","_LZMrk","_LZRad","_debugMode"];
	_infGrp = _this select 0;
	_LZMrk = _this select 1;
	_LZRad = _this select 2;
	_debugMode = _this select 3;


		_nearestEnemies = [];
		_nearestMen = nearestObjects [getPosATL leader _infGrp, ["Man"], 500];
			{
				if ( (side _x getFriend (side leader (_infGrp))) < 0.6 && {side _x != CIVILIAN} ) then {
					_nearestEnemies = _nearestEnemies + [_x];
				};
			} forEach _nearestMen;
	if (count _nearestEnemies > 0) then {
				_enemy = _nearestEnemies call bis_fnc_selectRandom;
				_attkPos = [_enemy, random 100, random 360] call BIS_fnc_relPos;
				_infWp = _infGrp addWaypoint [_attkPos, 0];
				_infWp setWaypointType "SAD";
				_infWp setWaypointBehaviour "AWARE";
				_infWp setWaypointCombatMode "RED";
				_infWp setWaypointSpeed "FULL";
				[_infgrp, getMarkerPos _LZMrk, _LZRad,10] call aw_fnc_spawn2_randomPatrol;
				
				 if (_debugMode) then {
					player globalChat "Target Found. Setting SAD waypoint";
					_colorTarget = [(((side _enemy) call bis_fnc_sideID) call bis_fnc_sideType),true] call bis_fnc_sidecolor;
					_mrkTarget = createMarker [format ["%1", random 10000], _attkPos];
					_mrkTarget setMarkerShape "ICON";
					_mrkTarget setMarkerType "mil_dot";
					_mrkTarget setMarkerSize [1,1];
					_mrkTarget setMarkerColor _colorTarget;
					_mrkTarget setMarkerText "SAD Target Area";
				};
	}else{
				
				
				//[_infGrp, getPosATL (leader _infGrp), 200] call BIS_fnc_taskPatrol;
				[_infgrp, getMarkerPos _LZMrk, _LZRad,10] call aw_fnc_spawn2_randomPatrol;
				if (_debugMode) then {player globalChat "No targets found. Patrol mode enabled";};
			
	};
};

if (_debugMode) then {
	_null = [] spawn{
    sleep 1;
    hint format ["Debug mode is enabled %1 (SP ONLY!!). Mapclick teleport, invincibility and marker tracking are enabled.", name player];
    player globalChat format ["Group Old: %1 Group New: %2", _group_old, _group_new];
	player globalChat format ["Spawn Position: %1 ", _spawnMrk];
	player globalChat format ["Landing Zone: %1", _LZMrk];
	player globalChat format ["AI Skill: %1", _skill];
	player globalChat format ["SAD Mode: %1", _sadMode];
	player globalChat format ["Body Deletion: %1", _bodyDelete];
	player globalChat format ["Cycle Mode: %1", _cycleMode];
	player globalChat format ["Debug Mode: %1", _debugMode];
	};
};
_startpoint = _spawnMrk;

// Straße am Startpunkt finden
while {true} do {
_roundPos = [getMarkerPos _startpoint, 1000] call aw_fnc_randomPos;
_roads = _roundPos nearRoads 75;
	if (count _roads > 0) exitWith{
	_randomPos = getPos	(_roads select floor random (count _roads));
	};
};
//waitUntil{!isNil "_randomPos"};


switch (_group_new) do {
	//Squadpatrol
	case "SP":{
	_infgrp = [floor random count allGroups,_LZMrk] call GSG_fnc_squadspatrol;
	};
	//Squaddefend
	case "SD":{
	_infgrp = [floor random count allGroups,_LZMrk] call GSG_fnc_squadsdefend;
	};
	// Teampatrol
	case "TP":{
	_infgrp = [floor random count allGroups,_LZMrk] call GSG_fnc_teampatrol;
	};
	// ReconPatrol
	case "RP":{
	_infgrp = [floor random count allGroups,_LZMrk] call GSG_fnc_reconpatrol;
	};
	
	default {
	    _ranGrp = ["OI_reconPatrol","OI_reconSentry","OI_reconTeam","OIA_InfSentry","OIA_InfSquad","OIA_InfSquad_Weapons","OIA_InfTeam","OIA_InfTeam_AA","OIA_InfTeam_AT"] call BIS_fnc_selectRandom;
		_infgrp = [getMarkerPos _LZMrk, EAST, (configFile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> _ranGrp)] call BIS_fnc_spawnGroup;
	};
};

// Fahrzeugauswahl
if (((count units _infgrp) > 5 OR (count units _group_old) >5 ) AND (_veh_mod == 4)) then {_veh_mod = 1};
switch (_veh_mod) do {
	case 1 :{
	_randomVeh = ["O_Truck_02_transport_F","O_Truck_02_covered_F","O_Truck_03_transport_F","O_Truck_03_covered_F"] call BIS_fnc_selectRandom;
	// _veh = [_randomVeh,_randomPos] call {_truck = (_this select 0) createVehicle (_this select 1); createVehicleCrew _truck; _units = crew _truck; [_truck,_units]};
	_veh = [_randomPos, (random 360), _randomVeh, EAST,false] call BIS_FNC_spawnVehicle;
	};
	
	case 2 :{
	_randomVeh = ["O_APC_Tracked_02_cannon_F"] call BIS_fnc_selectRandom;
	//_veh = [_randomVeh,_randomPos] call {_truck = (_this select 0) createVehicle (_this select 1); createVehicleCrew _truck; _units = crew _truck; [_truck,_units]};
	_veh = [_randomPos, (random 360), _randomVeh, EAST,false] call BIS_FNC_spawnVehicle;
	};
	
	case 3 :{
	_randomVeh = ["O_Heli_Light_02_F","B_Heli_Transport_01_F"] call BIS_fnc_selectRandom;
	_veh = [_randomPos, (random 360), _randomVeh, EAST,false] call BIS_FNC_spawnVehicle;
	};
	
	case 4 :{
	_randomVeh = ["O_G_Offroad_01_F"] call BIS_fnc_selectRandom;
	// _veh = [_randomVeh,_randomPos] call {_truck = (_this select 0) createVehicle (_this select 1); createVehicleCrew _truck; _units = crew _truck; [_truck,_units]};
	_veh = [_randomPos, (random 360), _randomVeh, EAST,false] call BIS_FNC_spawnVehicle;
	};
	
	default {
	_veh = [objNull];
	_randomPos = [getMarkerPos _LZMrk , 0, 400, 10, 0, 0.3, 0] call BIS_fnc_findSafePos;
	};
};



if (isNull (_veh select 0)) exitWith {[_infgrp, getMarkerPos _LZMrk, 300,10] call aw_fnc_spawn2_randomPatrol;[_infgrp]};

//Debug output of the vehicle + cargo
if (_debugMode) then {
    player globalChat format ["Group Selection: %1", _ranGrp];
    player globalChat format ["Vehicle Array: %1", _veh];
    player globalChat format ["Cargo Group: %1", _infGrp];
};

//Find a flat position around the LZ marker & create an HPad there.
_flatPos = [getPos leader _group_old, 50, 150, 10, 0, 0.3, 0] call BIS_fnc_findSafePos;
_null  = [_group_old, _veh, _flatPos, _debugMode] spawn {
_group_old = _this select 0; 
_veh = _this select 1;
_flatPos = _this select 2;
_debugMode = _this select 3;
_group_old setVariable ["_flatPos",_flatPos, false];
_group_old setVariable ["_veh",_veh select 0, false];
//player sideChat str(_group_old getVariable "_flatPos");
				
				for "_a" from 0 to count waypoints (_group_old) - 1 do {deleteWaypoint [_group_old, 0];};
				_wp = _group_old addWaypoint [_flatPos, 10];
 				_wp setWaypointType "MOVE";
				_wp setWaypointBehaviour "AWARE";
				_wp setWaypointCombatMode "GREEN";
				
				_wp setWaypointCompletionRadius 15;
				_wp setWaypointFormation "DIAMOND";
				_wp setWaypointSpeed "FULL";
				_wp setWaypointStatements ["true", "_null = this spawn {_pos = group _this getVariable ""_flatPos""; _veh = group _this getVariable ""_veh"";
				if !(_veh iskindof ""AIR"") exitWith{player sidechat ""kein Heli"";};
				waitUntil{sleep 1; ((getPos _veh distance _pos) < 750);};
				if (daytime < 6 || daytime > 20) then {
				{
				_x playMove ""AwopPercMstpSgthWnonDnon_start"";_x playMove ""AwopPercMstpSgthWnonDnon_throw"";_x playMove ""AwopPercMstpSgthWnonDnon_end"";
				_vehpos =[_pos, 4.5 + random 1, ((_foreachindex / count (units group _this)) * 360) ] call BIS_fnc_relPos;  
				smoke = 'Chemlight_blue' createVehicle _vehpos;
				}foreach units group _this;
				}
				else 
				{
				{
				_x playMove ""AwopPercMstpSgthWnonDnon_start"";_x playMove ""AwopPercMstpSgthWnonDnon_throw"";_x playMove ""AwopPercMstpSgthWnonDnon_end"";
				_vehpos =[_pos, 4.5 + random 1, ((_foreachindex / count (units group _this)) * 360) ] call BIS_fnc_relPos;  
				_smoke = 'SmokeShellPurple' createVehicle _vehpos;
				}foreach units group _this;
				};};"];
};
_null = [_infgrp,_veh, _spawnMrk,_randomPos, _flatPos,_LZRad,_LZMrk,_group_old,_debugMode] spawn {
	_infgrp = _this select 0;
	_veh = _this select 1;
	_spawnMrk = _this select 2;
	_randomPos = _this select 3;
	_flatPos = _this select 4;
	_LZRad = _this select 5;
	_LZMrk = _this select 6;
	_group_old = _this select 7;
	_debugMode = _this select 8;
	

//Assign the crew to a group & assign cargo to the vehicle
_vehCrew = _veh select 2;
{[_x] joinSilent _vehCrew;} forEach crew (_veh select 0);	

//Find a flat position around the LZ marker & create an HPad there.
//_flatPos = [getMarkerPos _LZMrk , 0, 400, 10, 0, 0.3, 0] call BIS_fnc_findSafePos;

switch (true) do {
	case (_veh select 0 isKindOf "Air"):{

		
		_hPad = createVehicle ["Land_HelipadEmpty_F", _flatPos, [], 0, "NONE"];
		{_x moveInCargo (_veh select 0); _x assignAsCargo (_veh select 0);} forEach units _infgrp; 
			//disable collision to avoid deaths and setup the paradrop
		{_x disableCollisionWith (_veh Select 0)} forEach units _infGrp; 
		
	_heloWp = _vehCrew addWaypoint [_flatPos, 50];
    _heloWp setWaypointType "MOVE";
    _heloWp setWaypointBehaviour "CARELESS";
    _heloWp setWaypointCombatMode "BLUE";
    _heloWp setWaypointSpeed "FULL";
	_heloWp setWaypointStatements ["true", "(vehicle this) LAND 'LAND';"];
	Player sidechat "Vor isTouching";
	//wait until the helicopter is touching the ground before ejecting the cargo	
    waitUntil {isTouchingGround (_veh select 0) || {!canMove (_veh select 0)}}; 
	//deleteVehicle _hPad;
	units _infgrp orderGetIn false;
	waitUntil {sleep 1; Player sideChat "260 - Alle raus gehen"; {!alive _x || !(_x in (_veh select 0))} count (units _infGrp) == count (units _infGrp)};
    Player sideChat "261 - Alle raus gehen";
	{unAssignVehicle _x;} forEach units _infgrp; //Eject the cargo
	//waitUntil {sleep 0.5; !(count assignedCargo == count units _group_old)};
		{Player sideChat Format["264 - assignAsCargo: %1",_x];_x assignAsCargo (_veh select 0);} forEach units _group_old;
		Player sideChat "265 - OrderGetIn"; units _group_old orderGetIn true;
		waitUntil {sleep 1;
				player sidechat str({!alive _x || (_x in (_veh select 0))} count (units _group_old)) + ":" + str (count (units _group_old));
				{!alive _x || (_x in (_veh select 0))} count (units _group_old) == count (units _group_old)
				};
  player globalChat format ["Helo Cargo Count: %1", {alive _x && (_x in (_veh select 0))} count (units _group_old)];
			_heloWp = _vehCrew addWaypoint [getMarkerPos _spawnMrk, 50];
            _heloWp setWaypointType "MOVE";
            _heloWp setWaypointBehaviour "CARELESS";
            _heloWp setWaypointCombatMode "BLUE";
            _heloWp setWaypointSpeed "FULL";
            _heloWp setWaypointStatements ["true", "{deleteVehicle _x;} forEach crew (vehicle this) + [vehicle this];"];
			deleteVehicle _hPad;

		_null = [_infGrp,_LZMrk,_LZRad,_debugMode] spawn gsg_waypoints;
	};
	case (_veh select 0 isKindOf "Tank"):{
		{_x assignAsCargo (_veh select 0); _x moveInCargo (_veh select 0);} forEach units _infgrp; 
		(units _infgrp) orderGetIn true; 
		_vehWp = _vehCrew addWaypoint [_flatPos, 0];
		_vehWp setWaypointType "MOVE";
		_vehWp setWaypointBehaviour "CARELESS";
		_vehWp setWaypointCombatMode "BLUE";
		_vehWp setWaypointSpeed "FULL";
		_vehWp setWaypointStatements ["true", "vehicle this spawn {assignedCargo _this orderGetIn false;};"];
		waitUntil {sleep 0.5; {!alive _x || !(_x in (_veh select 0))} count (units _infGrp) == count (units _infGrp) || {!canMove (_veh select 0)}};
		player sideChat "alle abgestiegen";
		{unAssignVehicle _x; sleep 0.5;} forEach units _infgrp; //Eject the cargo
		player sideChat "alle unAssignVehicle";
		{_x assignAsCargo (_veh select 0);} forEach units _group_old;
		player sideChat "neu assignAsCargo";
		units _group_old orderGetIn true;
		player sideChat "neu orderGetIn";
		waitUntil {	sleep 1;
				player sidechat str({!alive _x || (_x in (_veh select 0))} count (units _group_old)) + ":" + str (count (units _group_old));
				{!alive _x || (_x in (_veh select 0))} count (units _group_old) == count (units _group_old)
				};
		player globalChat format ["Helo Cargo Count: %1", {alive _x && (_x in (_veh select 0))} count (units _group_old)];	
		player sideChat str(leader _vehcrew);
		  _vehWp = _vehcrew addWaypoint [getMarkerPos _spawnMrk, 0];
				_vehWp setWaypointType "MOVE";
				_vehWp setWaypointBehaviour "CARELESS";
				_vehWp setWaypointCombatMode "BLUE";
				_vehWp setWaypointSpeed "FULL";
				_vehWp setWaypointStatements ["true", "{deleteVehicle _x;} forEach crew (vehicle this) + [vehicle this];"];
		player sideChat "WP eingeleitet";
		_null = [_infgrp,_LZMrk,_LZRad,_debugMode] spawn gsg_waypoints;
		};
	case (_veh select 0 isKindOf "Car"): {
		
		{_x assignAsCargo (_veh select 0); _x moveInCargo (_veh select 0);} forEach units _infgrp; 
		(units _infgrp) orderGetIn true; 
		
		_vehWp = _vehCrew addWaypoint [_flatPos, 0];
		_vehWp setWaypointType "MOVE";
		_vehWp setWaypointBehaviour "CARELESS";
		_vehWp setWaypointCombatMode "BLUE";
		_vehWp setWaypointSpeed "FULL";
		_vehWp setWaypointStatements ["true", "vehicle this spawn {_infgrp =[]; {if (_this getCargoIndex _x >= 0) then {_infgrp = _infgrp +[_x];}} count crew _this; _infgrp orderGetIn false;};"];
			
		//wait until the helicopter is touching the ground before ejecting the cargo	
		waitUntil {sleep 0.5; {!alive _x || !(_x in (_veh select 0))} count (units _infGrp) == count (units _infGrp) || {!canMove (_veh select 0)}};
		player sideChat "alle abgestiegen";
		//waitUntil {sleep 1; count assignedCargo (_veh select 0) == 0 || {!canMove (_veh select 0)}};
		{unAssignVehicle _x; sleep 0.5;} forEach units _infgrp; //Eject the cargo
		player sideChat "alle unAssignVehicle";
		//{_x orderGetIn false;}forEach assignedCargo (_veh select 0);
		//waitUntil {sleep 0.1; {!alive _x || !(_x in (_veh select 0))} count (units _infGrp) == count (units _infGrp)};
		{_x assignAsCargo (_veh select 0);} forEach units _group_old;
		player sideChat "neu assignAsCargo";
		units _group_old orderGetIn true;
		player sideChat "neu orderGetIn";
		//{unAssignVehicle _x;} forEach units _infgrp; //Eject the cargo
		//waitUntil {sleep 0.5; !(count assignedCargo == count units _group_old)};
		waitUntil {	sleep 1;
				player sidechat str({!alive _x || (_x in (_veh select 0))} count (units _group_old)) + ":" + str (count (units _group_old));
				{!alive _x || (_x in (_veh select 0))} count (units _group_old) == count (units _group_old)
				};
		player globalChat format ["Helo Cargo Count: %1", {alive _x && (_x in (_veh select 0))} count (units _group_old)];	
		player sideChat str(leader _vehcrew);
		  _vehWp = _vehcrew addWaypoint [getMarkerPos _spawnMrk, 0];
				_vehWp setWaypointType "MOVE";
				_vehWp setWaypointBehaviour "CARELESS";
				_vehWp setWaypointCombatMode "BLUE";
				_vehWp setWaypointSpeed "FULL";
				_vehWp setWaypointStatements ["true", "{deleteVehicle _x;} forEach crew (vehicle this) + [vehicle this];"];
		player sideChat "WP eingeleitet";
		_null = [_infgrp,_LZMrk,_LZRad,_debugMode] spawn gsg_waypoints;
		};
	};
};
[_veh select 0,_veh select 2,_infgrp]
// Function End
