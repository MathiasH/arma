// [[Pos],[Dir]] = [Zentrale Position, bewegliche Position, optional Höhe(default 1000)] call GSG_fnc_startpunkt;
#define maxSz 15000
#define minSz 500
private["_posZ","_posB","_hoehe","_x_achse","_y_achse","_code","_startPos"];
_posZ = [_this,0,[0,0,0],[[]],[3]] call BIS_fnc_param;
_posB = [_this,1,[0,0,0],[[]],[3]] call BIS_fnc_param;
_hoehe = [_this,2,1000,[0]] call BIS_fnc_param;

_x_achse = (_posB select 0) - (_posZ select 0); 
_y_achse = (_posB select 1) - (_posZ select 1);

_code=0;
if (_x_achse<=0) then {_code=_code + 1} else {_code = _code + 2};
if (_y_achse<=0) then {_code=_code + 3} else {_code = _code + 7};

switch (_code) do
{
	case 4: //-x und -y (SW)
	{
	_startPos = [
	[(0 + (random maxSz)),(0 + (random minSz))],
	[(0 + (random minSz)),(0 + (random maxSz))]
	] call BIS_fnc_selectRandom;
	_startPos=_startPos+[_hoehe];
	_startPos=[_startPos,45];
	};
	case 5: //+x und -y (SO)
	{
	_startPos = [
	[(30000 - (random maxSz)),(0 + (random minSz))],
	[(30000 - (random minSz)),(0 + (random maxSz))]
	] call BIS_fnc_selectRandom;
	_startPos=_startPos+[_hoehe];
	_startPos=[_startPos,315];
	};
	case 8: //-x und +y (NW)
	{
	_startPos = [
	[(0 + (random maxSz)),(30000 - (random minSz))],
	[(0 + (random minSz)),(30000 - (random maxSz))]
	] call BIS_fnc_selectRandom;
	_startPos=_startPos+[_hoehe];
	_startPos=[_startPos,135];
	};
	case 9: //+x und +y (NO)
	{
	_startPos = [
	[(30000 - (random maxSz)),(30000 - (random minSz))],
	[(30000 - (random minSz)),(30000 - (random maxSz))]
	] call BIS_fnc_selectRandom;
	_startPos=_startPos+[_hoehe];
	_startPos=[_startPos,225];
	};
};
_startPos	