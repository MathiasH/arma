// ---GSG MP Funktion---------------------------------------------------------------------------------
GSG_AddAction ={
				private["_object","_screenMsg","_scriptToCall"];
				_object = _this select 0;
				_screenMsg = _this select 1;
				_scriptToCall = _this select 2;
				
				if(isNull _object) exitWith {};

				_object addaction [Format["<t color='#ffff00'>capture %1</t>",_screenMsg],_scriptToCall];
				};

GSG_RemoveAction ={
						private["_object","_actID"];
						_object = _this select 0;
						//_actID = _this select 1;
						if(isNull _object) exitWith {};
						//_object removeAction _actID;
						removeAllActions _object;
				};

GSG_setVehicleVarName={
							private["_unit","_unitname"];
							_unit = _this select 0;
							_unitname = _this select 1;
							if (isNull _unit) exitWith {};
							_unit setVehicleVarName _unitname;
						};


// Zeus alle Objekte

if isServer then {
	GSG_AllObj = {
		private ["_obj"];
		_obj = _this select 0;
		{_obj addCuratorEditableObjects [[_x],true];} forEach allunits+vehicles; //{[_x] call GSG_AllObj;} forEach allCurators;
		};
		publicVariable "GSG_AllObj";
	
		if (isServer) then {
		"GSG_test" addPublicVariableEventHandler {[_this select 1]call GSG_AllObj};
		};
};