// 8 x Infanterieeinheiten
private["_randomPos","_spawnGroup","_units","_name","_smarker","_size"];
_units = [_this,0,0,[0]] call BIS_fnc_param;
_smarker = [_this,1,currentAO,["",[]]] call BIS_fnc_param;
_size = [_this,2,500,[0]] call BIS_fnc_param;

if ( typeName _smarker == "STRING") then {_smarker = getMarkerPos _smarker};

_randomPos = [_smarker, _size] call aw_fnc_randomPos;
_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OIA_InfSquad")] call BIS_fnc_spawnGroup;
[(units _spawnGroup)] call aw_setGroupSkill;

if(DEBUG) then
		{
			_name = format ["%1%2",name (leader _spawnGroup),_units];
			createMarker [_name,getPos (leader _spawnGroup)];
			_name setMarkerType "o_unknown";
			_name setMarkerText format ["GSG Squad Patrol %1",_units];
			_name setMarkerColor "ColorRed";
			[_spawnGroup,_name] spawn
			{
				private["_group","_marker"];
				_group = _this select 0;
				_marker = _this select 1;

				while{count (units _group) > 0} do
				{
					_marker setMarkerPos (getPos (leader _group));
					sleep 0.1;
				};
				deleteMarker _marker;
			};
		};
_spawnGroup