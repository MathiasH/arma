private ["_veh","_vehName","_vehVarname","_completeText","_reward","_sidemissiontext"];
_sidemissiontext = [_this, 0, "FEHLER",[""]] call BIS_fnc_param;
_veh = sideBLUEObjRes;
_vehName = _veh select 0;
_vehVarname = _veh select 1;

	_completeText = format[
	"<t align='center'><t size='2.2'>Side Mission</t><br/><t size='1.5' color='#00B2EE'>COMPLETE</t><br/>____________________<br/>Fantastic job, lads! The OPFOR stationed on the island won't last long if you keep that up!<br/><br/>We've given you %1 to help with the fight. You'll find it at base.<br/><br/>Focus on the main objective for now; we'll relay this success to the intel team and see if there's anything else you can do for us. We'll get back to you in 15 - 45 minutes.</t>",_vehName];
	
	
	
	GlobalHint = _completeText; publicVariable "GlobalHint"; hint parseText _completeText;
	showNotification = ["CompletedSideMission", _sidemissiontext]; publicVariable "showNotification";
	showNotification = ["Reward", format["Your team received %1!", _vehName]]; publicVariable "showNotification";