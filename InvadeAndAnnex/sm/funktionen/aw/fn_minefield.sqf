	private["_centralPos","_flatPos","_unitsArray","_mine","_distance","_dir","_sign"];
	_centralPos = _this select 0;
	_unitsArray = [];
	for "_x" from 0 to 45 do
	{
		_mine = createMine ["APERSMine", _centralPos, [], 38];
		_unitsArray = _unitsArray + [_mine];
		_mine setPosATL [getPosATL _mine select 0, (getPosATL _mine select 1) - 10, 0];
	};

	_distance = 50;
	_dir = 0;
	for "_c" from 0 to 15 do
	{
		_pos = [_centralPos, _distance, _dir] call BIS_fnc_relPos;
		_sign = "Land_Sign_Mines_F" createVehicle _pos;
		//waitUntil {alive _sign};
		_sign setDir _dir;
		_sign enableSimulation false;
		_sign allowDamage false;
		_dir = _dir + 15;

		_unitsArray = _unitsArray + [_sign];
	};

	_unitsArray