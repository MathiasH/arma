private ["_randomPos","_spawnGroup","_pos","_units","_armourGroup","_armour","_airGroup","_air_veh","_airType","_startMarker","_sp","_sd","_tp"];
_pos = getMarkerPos (_this select 0);
	_enemiesArray = [];
	_gopoints = call compile preprocessFileLineNumbers "sm\Var\gopoints.sqf";
	_startMarker = [];
	{if ((getMarkerPos _x distance getMarkerPos currentAO) < 5000 and _x != currentAO) then {_startMarker = _startMarker + [_x];};} forEach _gopoints;
	
	_sp = [];
	for "_units" from 1 to PARAMS_SquadsPatrol do {
		sleep 0.5;
		_spawnGroup = [[1,2,3] call BIS_fnc_selectRandom, "SP",[_startMarker call BIS_fnc_selectRandom, currentAO, PARAMS_AOSize], false] call GSG_fnc_reinforcements;

		_enemiesArray = _enemiesArray + _spawnGroup;
		_sp = _sp + [_spawnGroup select 2];
	};

	_sd=[];
	for "_units" from 1 to PARAMS_SquadsDefend do {
		sleep 0.5;
		_spawnGroup = [[1,2,3] call BIS_fnc_selectRandom, "SD",[_startMarker call BIS_fnc_selectRandom, currentAO, PARAMS_AOSize], false] call GSG_fnc_reinforcements;

		_enemiesArray = _enemiesArray + _spawnGroup;
		_sd = _sd + [_spawnGroup select 2];
	};

	_tp=[];
	for "_units" from 1 to PARAMS_TeamsPatrol do {
		sleep 0.5;
		_spawnGroup = [0, "TP",[_startMarker call BIS_fnc_selectRandom,currentAO, PARAMS_AOSize], false] call GSG_fnc_reinforcements;

		_enemiesArray = _enemiesArray + _spawnGroup;
		_tp = _tp +_spawnGroup;
	};
	
	//_units = 0;
	for "_units" from 1 to PARAMS_CarsPatrol do {
		_randomPos = [getMarkerPos currentAO, PARAMS_AOSize,7] call aw_fnc_randomPos;
		_carPatrol =["O_MRAP_02_gmg_F"];// select floor random 4 createVehicle _randomPos;
		_car_Array = [_randomPos, (random 360), _carPatrol call BIS_fnc_selectRandom, east] call BIS_fnc_spawnVehicle;
		_car_veh = _car_Array select 0;
		_car_crew = _car_Array select 1;
		_car_group = _car_Array select 2;
		"O_Soldier_AT_F" createUnit [getpos _car_veh, _car_group];
		"O_Soldier_AR_F" createUnit [getpos _car_veh, _car_group];
		[_car_group, getMarkerPos currentAO, PARAMS_AOSize] call aw_fnc_spawn2_randomPatrol;
		_car_veh spawn aw_fnc_fuelMonitor;
		_enemiesArray = _enemiesArray + [_car_group,_car_veh];
		[(units _car_group)] call aw_setGroupSkill;

		_car_veh lock true;
		if(DEBUG) then
		{
			_name = format ["%1%2",name (leader _car_veh),_units];
			createMarker [_name,getPos (leader _car_veh)];
			_name setMarkerType "o_unknown";
			_name setMarkerText format ["APC %1",_units];;
			_name setMarkerColor "ColorRed";
			[_car_veh,_name] spawn
			{
				private["_group","_marker"];
				_group = _this select 0;
				_marker = _this select 1;

				while{count (units _group) > 0} do
				{
					_marker setMarkerPos (getPos (leader _group));
					sleep 0.1;
				};
				deleteMarker _marker;
			};
		};
	};

	for "_units" from 1 to ( 4 + floor random PARAMS_ArmourPatrol) do 
	{
		//armour_veh = createGroup east;
		_randomPos = [getMarkerPos currentAO, PARAMS_AOSize,7] call aw_fnc_randomPos;
		_armour =["O_APC_Tracked_02_AA_F", "O_MBT_02_cannon_F", "O_APC_Tracked_02_cannon_F","O_MRAP_02_hmg_F","O_APC_Wheeled_02_rcws_F"];// select floor random 4 createVehicle _randomPos;
		_armour_Array = [_randomPos, (random 360), _armour call BIS_fnc_selectRandom, east] call BIS_fnc_spawnVehicle;
		_armour_veh = _armour_Array select 0;
		_armour_crew = _armour_Array select 1;
		_armour_group = _armour_Array select 2;
		[_armour_group,getMarkerPos currentAO,(PARAMS_AOSize / 2)] call aw_fnc_spawn2_perimeterPatrol;
		_armour_veh spawn aw_fnc_fuelMonitor;
		_enemiesArray = _enemiesArray + [_armour_group,_armour_veh];
		[(units _armour_group)] call aw_setGroupSkill;

		_armour_veh lock true;
		if(DEBUG) then
		{
			_name = format ["%1%2",name (leader _armour_veh),_units];
			createMarker [_name,getPos (leader _armour_veh)];
			_name setMarkerType "o_unknown";
			_name setMarkerText format ["APC %1",_units];;
			_name setMarkerColor "ColorRed";
			[_armour_veh,_name] spawn
			{
				private["_group","_marker"];
				_group = _this select 0;
				_marker = _this select 1;

				while{count (units _group) > 0} do
				{
					_marker setMarkerPos (getPos (leader _group));
					sleep 0.1;
				};
				deleteMarker _marker;
			};
		};
	};

	if((random 10 <= PARAMS_AirPatrol))
		then {
		//_air_group = createGroup east;
		_randomPos = [getMarkerPos currentAO, PARAMS_AOSize] call aw_fnc_randomPos;
		_randomPos deleteRange [2,2];
		_randomPos = _randomPos + [550];
		_airType = ["O_Heli_Attack_02_F","O_Heli_Light_02_F"];
		_air_Array = [_randomPos, 90, _airType call BIS_fnc_selectRandom, east] call BIS_fnc_spawnVehicle;
		_air_veh = _air_Array select 0; //Fahrzeug
		_air_crew = _air_Array select 1; //Crew
		_air_group = _air_Array select 2; //Gruppe
		[_air_group, getMarkerPos currentAO, (2 * (PARAMS_AOSize / 3))] call BIS_fnc_taskPatrol;
		_air_veh spawn aw_fnc_fuelMonitor;
		_enemiesArray = _enemiesArray + [_air_group,_air_veh];
		[(units _air_group)] call aw_setGroupSkillSpecial;
		_air_veh lock true;
		
		if(DEBUG) then
		{
			_name = format ["%1",name (leader _air_group)];
			createMarker [_name,getPos (leader _air_group)];
			_name setMarkerType "o_unknown";
			_name setMarkerText "Air";
			_name setMarkerColor "ColorRed";
			[_air_group,_name] spawn
			{
				private["_group","_marker"];
				_group = _this select 0;
				_marker = _this select 1;

				while{count (units _group) > 0} do
				{
					_marker setMarkerPos (getPos (leader _group));
					sleep 0.1;
				};
				deleteMarker _marker;
			};
		};
	};

	{
		_newGrp = [_x] call AW_fnc_garrisonBuildings;
		if (!isNull _newGrp) then { _enemiesArray = _enemiesArray + [_newGrp]; };
	} forEach (getMarkerPos currentAO nearObjects ["House", 600]);
[_enemiesArray,_sp,_sd,_tp]