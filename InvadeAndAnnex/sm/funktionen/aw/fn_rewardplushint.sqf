private ["_veh","_vehName","_vehVarname","_completeText","_reward","_sidemissiontext"];
_sidemissiontext = [_this, 0, "FEHLER",[""]] call BIS_fnc_param;
_veh = smRewards call BIS_fnc_selectRandom;
	_vehName = _veh select 0;
	_vehVarname = _veh select 1;

	_completeText = format[
	"<t align='center'><t size='2.2'>Side Mission</t><br/><t size='1.5' color='#00B2EE'>COMPLETE</t><br/>____________________<br/>Fantastic job, lads! The OPFOR stationed on the island won't last long if you keep that up!<br/><br/>We've given you %1 to help with the fight. You'll find it at base.<br/><br/>Focus on the main objective for now; we'll relay this success to the intel team and see if there's anything else you can do for us. We'll get back to you in 15 - 45 minutes.</t>",_vehName];

if (_vehVarname isKindOf "Air") then {
	if (_vehVarname isKindOf "Plane") then {
		_reward = createVehicle [_vehVarname, getMarkerPos "smReward22",smMarkerListPlane,0,"NONE"];
		waitUntil {!isNull _reward};
		{if (((getPos _reward)distance(markerPos _x))<5) then {
		_reward setdir (markerDir _x);
		};} foreach smMarkerListPlane;
		};
	if (_vehVarname isKindOf "Helicopter") then {
		_reward = createVehicle [_vehVarname, getMarkerPos "smReward28",smMarkerListHeli,0,"NONE"];
		waitUntil {!isNull _reward};
		{if (((getPos _reward)distance(markerPos _x))<5) then {
		_reward setdir (markerDir _x);
		};} foreach smMarkerListHeli;
		};
	}
	else
	{
	_reward = createVehicle [_vehVarname, getMarkerPos "smReward1",smMarkerList,0,"NONE"];
	//if (_reward isKindOf "UAV") then {createVehicleCrew _reward;} else {sleep 1;};
	waitUntil {!isNull _reward};
	{if (((getPos _reward)distance(markerPos _x))<5) then {
		_reward setdir (markerDir _x);
		};} foreach smMarkerList;
	};

	[_reward] execVM "scripts\aw_unitSetup.sqf";

	GlobalHint = _completeText; publicVariable "GlobalHint"; hint parseText _completeText;
	showNotification = ["CompletedSideMission", _sidemissiontext]; publicVariable "showNotification";
	showNotification = ["Reward", format["Your team received %1!", _vehName]]; publicVariable "showNotification";