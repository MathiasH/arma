private ["_unitsArray", "_obj", "_isGroup","_delay"];
	_delay=[_this,1,30,[0]] call BIS_fnc_param;
	sleep _delay;
	_unitsArray = _this select 0;

	for "_c" from 0 to (count _unitsArray - 1) do
	{
		_obj = _unitsArray select _c;
		_isGroup = false; if (_obj in allGroups) then { _isGroup = true; };
		if (_isGroup) then
		{
			{
				if (!isNull _x) then { deleteVehicle _x; };
			} forEach (units _obj);
		} else {
			if (!isNull _obj) then { deleteVehicle _obj; };
		};
	};