//by Rarek [AW] edit by Figuri001 [GSG]
#define waitMIN 600
#define waitMAX 1800
#define minPlayer 8

private ["_priorityVeh1","_priorityVeh2","_priorityTargets","_priorityTargets1","_priorityTargets2","_firstRun","_isGroup","_obj","_position","_flatPos","_nearUnits","_accepted","_debugCounter","_pos","_barrier","_dir","_unitsArray","_randomPos","_spawnGroup","_unit","_targetPos","_debugCount","_radius","_randomWait","_briefing","_flatPosAlt","_flatPosClose","_priorityGroup","_distance","_firingMessages","_completeText","_fuzzyPos"];
_firstRun = true;
_unitsArray = [];
_completeText =
"<t align='center' size='2.2'>Priority Target</t><br/><t size='1.5' color='#08b000'>NEUTRALISED</t><br/>____________________<br/>Incredible job, boys! Make sure you jump on those priority targets quickly; they can really cause havoc if they're left to their own devices.<br/><br/>Keep on with the main objective; we'll tell you if anything comes up.";

	_randomWait = (random waitMAX);
	sleep (waitMIN + _randomWait);

	diag_log format["PT: Waiting %1 before next PT.",(_randomWait + waitMIN)];
	
	/* ================================ */
	/* ====== CREATE MORTAR TEAM ====== */
	/* ================================ */

	diag_log "Priority Target started.";
	
	//Define hint
	_briefing = 
	"<t align='center' size='2.2'>Priority Target</t><br/><t size='1.5' color='#b60000'>Enemy Artillery</t><br/>____________________<br/>OPFOR forces are setting up an artillery battery to hit you guys damned hard! We've picked up their positions with thermal imaging scans and have marked it on your map.<br/><br/>This is a priority target, boys! They're just setting up now; they'll be firing in about five minutes!";

	/*
		Find flat position that's not near spawn or within (PARAMS_AOSize + 200) of AO
		Possibly change this to include mortar teams spawning on a minimum elevation?
	*/

	_flatPos = [0];
	_accepted = false;
	_debugCounter = 1;
	while {!_accepted} do
	{
		_debugCounter = _debugCounter + 1;

		while {(count _flatPos) < 3} do
		{
			_position = [[[getMarkerPos currentAO,2500]],["water","out"]] call BIS_fnc_randomPos;
			_flatPos = _position isFlatEmpty [5, 0, 0.2, 5, 0, false];
		};
		
		if
		((_flatPos distance (getMarkerPos "respawn_west")) > 1300 && (_flatPos distance (getMarkerPos currentAO)) > 800) then {
			_nearUnits = 0;
			if (isMultiplayer) then {
				{
					if ((_flatPos distance (getPos _x)) < 500) then
					{
						_nearUnits = _nearUnits + 1;
					};
				} forEach playableUnits;
			}
			else
			{
				{
					if ((_flatPos distance (getPos _x)) < 500) then
					{
						_nearUnits = _nearUnits + 1;
					};
				} forEach switchableUnits;
			};
			if (_nearUnits == 0) then
			{
				_accepted = true;
			};
		} else {
			_flatPos = [0];
		};
	};
	diag_log format["PT: Finding flat position.<br/>Attempt #%1",_debugCounter];
	diag_log "PT: Spawning mortars, units and fire teams.";

	alivearti = true;
	
	//Spawn units
	_flatPosAlt = [(_flatPos select 0) - 2, (_flatPos select 1), (_flatPos select 2)];
	_flatPosClose = [(_flatPos select 0) + 2, (_flatPos select 1), (_flatPos select 2)];
	_priorityGroup = createGroup EAST;
	_priorityVeh1 = "O_MBT_02_arty_F" createVehicle _flatPosAlt;
	waitUntil {!isNull _priorityVeh1};
	_priorityVeh2 = "O_MBT_02_arty_F" createVehicle _flatPosClose;
	waitUntil {!isNull _priorityVeh2};
	_priorityVeh1 lock 3;
	_priorityVeh2 lock 3;
	
	_priorityVeh1 setVehicleVarName "arti_1"; 
	arti_1 = _priorityVeh1; 
	_index = arti_1 addMPEventHandler ["mpkilled", {
														Null = _this execVM "gsg\gsg_addhalfscore.sqf";
													}];
	_index = arti_1 addEventHandler ["hit", {
												if (!(canMove arti_1) && !(canMove arti_2)) then
												{
													if (alivearti) then
													{
														_completeText =	"<t align='center' size='2.2'>Priority Target</t><br/><t size='1.5' color='#08b000'>NEUTRALISED</t><br/>____________________<br/>Incredible job, boys! Make sure you jump on those priority targets quickly; they can really cause havoc if they're left to their own devices.<br/><br/>Keep on with the main objective; we'll tell you if anything comes up.";
														GlobalHint = _completeText; publicVariable "GlobalHint"; hint parseText _completeText;
														showNotification = ["CompletedPriorityTarget", "Enemy Artillery Neutralised"]; publicVariable "showNotification";
														{[3,_x] call GSG_fnc_createMarker;} forEach ["priorityMarker","priorityCircle"];
														[] spawn aw_cleanGroups;
														alivearti = false;
														arti_1 removeEventHandler ["hit", 0];
														arti_2 removeEventHandler ["hit", 0];
													};
												};
											}
									];
	
	_priorityVeh2 setVehicleVarName "arti_2"; 
	arti_2 = _priorityVeh2; 
	_index = arti_2 addMPEventHandler ["mpkilled", {
														Null = _this execVM "gsg\gsg_addhalfscore.sqf";
													}];
	_index = arti_2 addEventHandler ["hit", {
												if (!(canMove arti_1) && !(canMove arti_2)) then
												{
													if (alivearti) then
													{
														_completeText =	"<t align='center' size='2.2'>Priority Target</t><br/><t size='1.5' color='#08b000'>NEUTRALISED</t><br/>____________________<br/>Incredible job, boys! Make sure you jump on those priority targets quickly; they can really cause havoc if they're left to their own devices.<br/><br/>Keep on with the main objective; we'll tell you if anything comes up.";
														GlobalHint = _completeText; publicVariable "GlobalHint"; hint parseText _completeText;
														showNotification = ["CompletedPriorityTarget", "Enemy Artillery Neutralised"]; publicVariable "showNotification";
														{[3,_x] call GSG_fnc_createMarker;} forEach ["priorityMarker","priorityCircle"];
														[] spawn aw_cleanGroups;
														alivearti = false;
														arti_1 removeEventHandler ["hit", 0];
														arti_2 removeEventHandler ["hit", 0];
													};
												};
											}
									];
	
	_priorityVeh1 addEventHandler["Fired",{if (!isPlayer (gunner _priorityVeh1)) then { _priorityVeh1 setVehicleAmmo 1; };}];
	_priorityVeh2 addEventHandler["Fired",{if (!isPlayer (gunner _priorityVeh2)) then { _priorityVeh2 setVehicleAmmo 1; };}];
	_priorityVeh1 addEventHandler["GetIn",{if (isPlayer (gunner _priorityVeh1)) then { _priorityVeh1 setVehicleAmmo 0; };}];
	_priorityVeh2 addEventHandler["GetIn",{if (isPlayer (gunner _priorityVeh2)) then { _priorityVeh2 setVehicleAmmo 0; };}];	
	_priorityTarget1 = _priorityGroup createUnit ["O_crew_F", _flatPosAlt, [], 5, "NONE"];
	_priorityTarget1 moveInGunner _priorityVeh1;
	_priorityTarget2 = _priorityGroup createUnit ["O_crew_F", _flatPosAlt, [], 5, "NONE"];
	_priorityTarget2 moveInGunner _priorityVeh2;
	//"O_Soldier_F" createUnit [_flatPosAlt, _priorityGroup, "_priorityTarget1 = this; this moveInGunner _priorityVeh1;"];
	//"O_Soldier_F" createUnit [_flatPosClose, _priorityGroup, "_priorityTarget2 = this; this moveInGunner _priorityVeh2;"];
	waitUntil {alive _priorityTarget1 && alive _priorityTarget2};
	_priorityTargets = [_priorityTarget1, _priorityTarget2];
	//{ publicVariable _x; } forEach ["priorityTarget1", "priorityTarget2", "priorityTargets", "priorityVeh1", "priorityVeh2"];
	
	//Small sleep to let units settle in
	sleep 10;

	//Define unitsArray for deletion after completion
	_unitsArray = _unitsArray + [_priorityGroup , _priorityVeh1, _priorityVeh2];

	//Spawn H-Barrier cover "Land_HBarrierBig_F"
	_distance = 20;
	_dir = 0;
	for "_c" from 0 to 15 do
	{
		_pos = [_flatPos, _distance, _dir] call BIS_fnc_relPos;
		_barrier = "Land_HBarrier_3_F" createVehicle _pos;
		waitUntil {alive _barrier};
		_barrier setDir _dir;
		_dir = _dir + 22.5;
		
		_unitsArray = _unitsArray + [_barrier];
	};

	//Spawn some enemies protecting the units
	for "_c" from 0 to 2 do
	{
		_randomPos = [_flatPos, 100] call aw_fnc_randomPos;
		_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OIA_InfTeam")] call BIS_fnc_spawnGroup;
		[_spawnGroup, _flatPos, 200] call aw_fnc_spawn2_perimeterPatrol;
		[(units _spawnGroup)] call aw_setGroupSkill;
		
		_unitsArray = _unitsArray + [_spawnGroup];
	};
	
	for "_c" from 0 to 1 do
	{
		_randomPos = [_flatPos, 100] call aw_fnc_randomPos;
		_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "UInfantry" >> "OIA_GuardSquad")] call BIS_fnc_spawnGroup;
		[_spawnGroup, _flatPos, 100] call aw_fnc_spawn2_randomPatrol;
		[(units _spawnGroup)] call aw_setGroupSkill;
		
		_unitsArray = _unitsArray + [_spawnGroup];
	};

	for "_c" from 0 to 1 do
	{
		_randomPos = [_flatPos, 150] call aw_fnc_randomPos;
		_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OI_SniperTeam")] call BIS_fnc_spawnGroup;
		[_spawnGroup, _flatPos, 300] call aw_fnc_spawn2_perimeterPatrol;
		[(units _spawnGroup)] call aw_setGroupSkillSniper;
				
		_unitsArray = _unitsArray + [_spawnGroup];
	};
	
	for "_c" from 0 to 1 do
	{
		_randomPos = [_flatPos, 100] call aw_fnc_randomPos;
		_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OIA_InfTeam_AA")] call BIS_fnc_spawnGroup;
		[_spawnGroup, _flatPos, 200] call aw_fnc_spawn2_randomPatrol;
		[(units _spawnGroup)] call aw_setGroupSkill;
		
		_unitsArray = _unitsArray + [_spawnGroup];
	};
	
	_spawnGroup = createGroup east;
			_randomPos = [_flatPos, 100,10] call aw_fnc_randomPos;
			_armour = "O_MRAP_02_hmg_F" createVehicle _randomPos;
			waitUntil{!isNull _armour};

			"O_soldier_repair_F" createUnit [_randomPos,_spawnGroup];
			"O_crew_F" createUnit [_randomPos,_spawnGroup];

			((units _spawnGroup) select 0) assignAsDriver _armour;
			((units _spawnGroup) select 1) assignAsGunner _armour;
			((units _spawnGroup) select 0) moveInDriver _armour;
			((units _spawnGroup) select 1) moveInGunner _armour;
			[_spawnGroup, _flatPos, 300] call aw_fnc_spawn2_randomPatrol;
			_armour spawn aw_fnc_fuelMonitor;
			_unitsArray = _unitsArray + [_spawnGroup,_armour];
			//[(units _spawnGroup)] call aw_setGroupSkill;
			_armour lock true;

			
	_spawnGroup2 = createGroup east;
			_randomPos2 = [_flatPos, 300,10] call aw_fnc_randomPos;
			_armour2 = "O_APC_Tracked_02_AA_F" createVehicle _randomPos2;
			waitUntil{!isNull _armour2};
			_armour2 addEventHandler ["Fired",{(_this select 0) setVehicleAmmo 1}]; 
			"O_soldier_repair_F" createUnit [_randomPos2,_spawnGroup2];
			"O_crew_F" createUnit [_randomPos2,_spawnGroup2];
			"O_crew_F" createUnit [_randomPos2,_spawnGroup2];

			((units _spawnGroup2) select 0) assignAsDriver _armour2;
			((units _spawnGroup2) select 1) assignAsGunner _armour2;
			((units _spawnGroup2) select 2) assignAsCommander _armour2;
			((units _spawnGroup2) select 0) moveInDriver _armour2;
			((units _spawnGroup2) select 1) moveInGunner _armour2;
			((units _spawnGroup2) select 2) moveInCommander  _armour2;
			[_spawnGroup2, _flatPos, 300] call aw_fnc_spawn2_perimeterPatrol;
			_armour2 spawn aw_fnc_fuelMonitor;
			_unitsArray = _unitsArray + [_spawnGroup2,_armour2];
			//[(units _spawnGroup2)] call aw_setGroupSkill;
			_armour2 lock true;

	
	//Set marker up
	_fuzzyPos = 
	[
		((_flatPos select 0) - 300) + (random 600),
		((_flatPos select 1) - 300) + (random 600),
		0
	];
	{_x call GSG_fnc_createMarker;} forEach [
	[2,"priorityCircle", _fuzzyPos, "Border","ColorOPFOR",[300,300]],
	[0,"priorityMarker", _fuzzyPos, "o_mortar","ColorOPFOR","Priority Target: Artillery"]
	];

	//Send Global Hint
	GlobalHint = _briefing; publicVariable "GlobalHint"; hint parseText _briefing;
	showNotification = ["NewPriorityTarget", "Destroy Enemy Artillery"]; publicVariable "showNotification";

	debugMessage = "Letting mortars 'set up'.";
	publicVariable "debugMessage";

	//Wait for 1-2 minutes while the mortars "set up"
	sleep (random 60);
	
	//Set mortars attacking while still alive
	_firingMessages = 
	[
		"Thermal scans are picking up those enemy Artillery firing! Heads down!",
		"Enemy Artillery rounds incoming! Advise you seek cover immediately.",
		"OPFOR Artillery rounds incoming! Seek cover immediately!",
		"The Artillery team's firing, boys! Down on the ground!",
		"Get that damned Artillery team down; they're firing right now! Seek cover!",
		"They're zeroing in! Incoming Artillery fire; heads down!"
	];
	_radius = 80; //Declared here so we can "zero in" gradually

	while { canMove arti_1 || canMove arti_2 } do
	{
		_accepted = false;
		_unit = objNull;
		_targetPos = [0,0,0];
		_debugCount = 1;
		_anz = call GSG_fnc_AnzPlayer;
		while {!_accepted} do
		{
			if (isMultiplayer) then 
			{
				_unit = (playableUnits select (floor (random (count playableUnits))));
			}
			else
			{
				_unit = (switchableUnits select (floor (random (count switchableUnits))));
			};
			if !(isNil "_unit") then 
			{
				_targetPos = getPos _unit;
				if (((_targetPos distance (getMarkerPos "respawn_west")) > 1500) && (vehicle _unit == _unit) && (side _unit == WEST) && ((_targetPos distance (getMarkerPos "uss_freedom")) > 500)) then 
				{ 
					_accepted = true; 
				}
				else 	
				{
					sleep 20;
				};
			}
			else 	
			{
				sleep 20;
			};
			_debugCount = _debugCount + 1;
		};
		
		//diag_log "PT: Valid target found; warning players (deactivated) and beginning fire sequence.";
		diag_log format ["PT: Valid target found; warning players (deactivated) and beginning fire sequence. Target: %1      Target Position: %2",_unit,_targetPos];
		/*
		if (PARAMS_PriorityTargetTickWarning == 1) then
		{
			hqSideChat = _firingMessages call BIS_fnc_selectRandom; publicVariable "hqSideChat"; [WEST,"HQ"] sideChat hqSideChat;
		};
		*/
		_dir = [_flatPos, _targetPos] call BIS_fnc_dirTo;
		{ _x setDir _dir; } forEach [_priorityVeh1, _priorityVeh2];
		sleep 5;
		{
			if (alive _x) then
			{
				if (_anz >= minPlayer) then 
				{
					for "_c" from 0 to 8 do
					{
						_pos = 
						[
							(_targetPos select 0) - _radius + (2 * random _radius),
							(_targetPos select 1) - _radius + (2 * random _radius),
							0
						];
						_x doArtilleryFire [_pos, "32Rnd_155mm_Mo_shells", 1]; //update so parameter customises mortar rounds?
						_x setVehicleAmmo 1;
						sleep 8;
					};
				};
			};
		} forEach _priorityTargets;
		
		if (_radius > 10) then { _radius = _radius - 10; }; /* zeroing in */
		if (PARAMS_PriorityTargetTickTimeMax <= PARAMS_PriorityTargetTickTimeMin) then
		{
			sleep PARAMS_PriorityTargetTickTimeMin;
		} else {
			sleep (PARAMS_PriorityTargetTickTimeMin + (random (PARAMS_PriorityTargetTickTimeMax - PARAMS_PriorityTargetTickTimeMin)));
		};
	};

	//Send completion hint
/* 	GlobalHint = _completeText; publicVariable "GlobalHint"; hint parseText _completeText;
	showNotification = ["CompletedPriorityTarget", "Enemy Artillery Neutralised"]; publicVariable "showNotification";
	{[3,_x] call GSG_fnc_createMarker;} forEach ["priorityMarker","priorityCircle"]; */
	//[_unitsArray] spawn AW_fnc_deleteOldUnitsAndVehicles;
	diag_log "Deleting old Priority Target and creating new one";
	{[3,_x] call GSG_fnc_createMarker;} forEach ["priorityMarker","priorityCircle"];
	[_unitsArray] spawn AW_fnc_deleteOldSMUnits;
	[] spawn aw_cleanGroups;
	[] spawn GSG_fnc_priorityTargets;