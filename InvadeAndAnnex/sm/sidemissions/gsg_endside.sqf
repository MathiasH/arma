	diag_log format["Side Mission ended"];
	//Throw out objective completion hint
	[_SM_bez] call AW_fnc_rewardPlusHintBlueOld;

	//[_unitsArray] spawn AW_fnc_deleteOldUnitsAndVehicles;
	[_unitsArray] spawn AW_fnc_deleteOldSMUnits;
	_null = [sideBLUEObj,120] spawn GSG_fnc_deleteRuins;
	["gsg\gsg_deletegroups.sqf","BIS_fnc_execVM",true,true] call BIS_fnc_MP;
	
	//Hide marker
	{[3,_x] call GSG_fnc_createMarker;} forEach ["sideBlueMarker","sideBlueCircle"];
	[] spawn GSG_fnc_sideBlueMissions; 
	if (isserver) then {diag_log format["Side Mission  wird erneut geladen"];};
	//provide players with reward. Place an MH-9 in the hangar, maybe?
		