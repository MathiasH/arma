//Spawn some enemies around the objective
			_unitsArray = [];
			_x = 0;
			for "_x" from 0 to 2 do
			{
				_randomPos = [_flatPos, 50] call aw_fnc_randomPos;
				_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OIA_InfTeam_AA")] call BIS_fnc_spawnGroup;
				[_spawnGroup, _flatPos,50] call aw_fnc_spawn2_perimeterPatrol;
				[(units _spawnGroup)] call aw_setGroupSkill;

						if(DEBUG) then
		{
			_name = format ["%1%2",name (leader _spawnGroup),_x];
			createMarker [_name,getPos (leader _spawnGroup)];
			_name setMarkerType "o_unknown";
			_name setMarkerText format ["Squad Patrol %1",_x];
			_name setMarkerColor "ColorRed";
			[_spawnGroup,_name] spawn
			{
				private["_group","_marker"];
				_group = _this select 0;
				_marker = _this select 1;

				while{count (units _group) > 0} do
				{
					_marker setMarkerPos (getPos (leader _group));
					sleep 0.1;
				};
				deleteMarker _marker;
			};
		};

				_unitsArray = _unitsArray + [_spawnGroup];
			};

			_x = 0;
			for "_x" from 0 to 2 do
			{
				_randomPos = [_flatPos, 250] call aw_fnc_randomPos;
				_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OI_reconSentry")] call BIS_fnc_spawnGroup;
				[_spawnGroup, _flatPos, 300] call aw_fnc_spawn2_perimeterPatrol;
				[(units _spawnGroup)] call aw_setGroupSkill;

						if(DEBUG) then
		{
			_name = format ["%1%2",name (leader _spawnGroup),_x];
			createMarker [_name,getPos (leader _spawnGroup)];
			_name setMarkerType "o_unknown";
			_name setMarkerText format ["Squad Patrol %1",_x];
			_name setMarkerColor "ColorRed";
			[_spawnGroup,_name] spawn
			{
				private["_group","_marker"];
				_group = _this select 0;
				_marker = _this select 1;

				while{count (units _group) > 0} do
				{
					_marker setMarkerPos (getPos (leader _group));
					sleep 0.1;
				};
				deleteMarker _marker;
			};
		};

				_unitsArray = _unitsArray + [_spawnGroup];
			};

			_x = 0;
			for "_x" from 0 to 1 do
			{
				_randomPos = [_flatPos, 200] call aw_fnc_randomPos;
				_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OI_reconTeam")] call BIS_fnc_spawnGroup;
				[_spawnGroup, _flatPos, 150] call aw_fnc_spawn2_perimeterPatrol;
				[(units _spawnGroup)] call aw_setGroupSkillSpecial;

				_unitsArray = _unitsArray + [_spawnGroup];
			};

			_spawnGroup = createGroup east;
			_randomPos = [_flatPos, 100,10] call aw_fnc_randomPos;
			_armour = "O_APC_Tracked_02_AA_F" createVehicle _randomPos;
			waitUntil{!isNull _armour};

			"O_soldier_repair_F" createUnit [_randomPos,_spawnGroup];
			"O_crew_F" createUnit [_randomPos,_spawnGroup];
			"O_crew_F" createUnit [_randomPos,_spawnGroup];

			((units _spawnGroup) select 0) assignAsDriver _armour;
			((units _spawnGroup) select 1) assignAsGunner _armour;
			((units _spawnGroup) select 2) assignAsCommander _armour;
			((units _spawnGroup) select 0) moveInDriver _armour;
			((units _spawnGroup) select 1) moveInGunner _armour;
			((units _spawnGroup) select 2) moveInCommander _armour;
			[_spawnGroup, _flatPos, 300] call aw_fnc_spawn2_randomPatrol;
			_armour spawn aw_fnc_fuelMonitor;
			_unitsArray = _unitsArray + [_spawnGroup,_armour];
			//[(units _spawnGroup)] call aw_setGroupSkill;
			_armour lock true;

			_spawnGroup = createGroup east;
			_randomPos = [_flatPos, 100,10] call aw_fnc_randomPos;
			_armour = "O_MRAP_02_hmg_F" createVehicle _randomPos;
			waitUntil{!isNull _armour};

			"O_soldier_repair_F" createUnit [_randomPos,_spawnGroup];
			"O_crew_F" createUnit [_randomPos,_spawnGroup];

			((units _spawnGroup) select 0) assignAsDriver _armour;
			((units _spawnGroup) select 1) assignAsGunner _armour;
			((units _spawnGroup) select 0) moveInDriver _armour;
			((units _spawnGroup) select 1) moveInGunner _armour;
			[_spawnGroup, _flatPos, 300] call aw_fnc_spawn2_randomPatrol;
			_armour spawn aw_fnc_fuelMonitor;
			_unitsArray = _unitsArray + [_spawnGroup,_armour];
			//[(units _spawnGroup)] call aw_setGroupSkill;
			_armour lock true;

			_spawnGroup = createGroup east;
			_randomPos = [_flatPos, 100,10] call aw_fnc_randomPos;
			_armour = "O_MRAP_02_hmg_F" createVehicle _randomPos;
			waitUntil{!isNull _armour};

			"O_soldier_repair_F" createUnit [_randomPos,_spawnGroup];
			"O_crew_F" createUnit [_randomPos,_spawnGroup];

			((units _spawnGroup) select 0) assignAsDriver _armour;
			((units _spawnGroup) select 1) assignAsGunner _armour;
			((units _spawnGroup) select 0) moveInDriver _armour;
			((units _spawnGroup) select 1) moveInGunner _armour;
			[_spawnGroup, _flatPos, 300] call aw_fnc_spawn2_randomPatrol;
			_armour spawn aw_fnc_fuelMonitor;
			_unitsArray = _unitsArray + [_spawnGroup,_armour];
			//[(units _spawnGroup)] call aw_setGroupSkill;
			_armour lock true;

			_spawnGroup = createGroup east;
			_randomPos = [_flatPos, 20,10] call aw_fnc_randomPos;
			_armour = "C_SUV_01_F" createVehicle _randomPos;
			waitUntil{!isNull _armour};

			"O_officer_F" createUnit [_randomPos,_spawnGroup];
			"O_soldierU_medic_F" createUnit [_randomPos,_spawnGroup];
			"O_soldier_repair_F" createUnit [_randomPos,_spawnGroup];

			((units _spawnGroup) select 0) assignAsCargo _armour;
			((units _spawnGroup) select 1) assignAsCargo _armour;
			((units _spawnGroup) select 2) assignAsCargo _armour;
			((units _spawnGroup) select 0) moveInCargo _armour;
			((units _spawnGroup) select 1) moveInCargo _armour;
			((units _spawnGroup) select 2) moveInCargo _armour;

			((units _spawnGroup) select 0) moveInDriver _armour;
			[_spawnGroup, _flatPos, 300] call aw_fnc_spawn2_randomPatrol;
			_armour spawn aw_fnc_fuelMonitor;
			_unitsArray = _unitsArray + [_spawnGroup,_armour];
			//[(units _spawnGroup)] call aw_setGroupSkill;
			_armour lock true;

		if(DEBUG) then
		{
			_name = format ["%1%2",name (leader _spawnGroup),_x];
			createMarker [_name,getPos (leader _spawnGroup)];
			_name setMarkerType "o_unknown";
			_name setMarkerText format ["Squad Patrol %1",_x];
			_name setMarkerColor "ColorRed";
			[_spawnGroup,_name] spawn
			{
				private["_group","_marker"];
				_group = _this select 0;
				_marker = _this select 1;

				while{count (units _group) > 0} do
				{
					_marker setMarkerPos (getPos (leader _group));
					sleep 0.1;
				};
				deleteMarker _marker;
			};
		};
_unitsarray