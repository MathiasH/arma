			//Spawn some enemies around the objective
			_unitsArray = [];
			_x = 0;
			for "_x" from 0 to 2 do
			{
				_randomPos = [_flatPos, 50] call aw_fnc_randomPos;
				_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "EAST" >> "OPF_F" >> "Infantry" >> "OIA_InfTeam")] call BIS_fnc_spawnGroup;
				[_spawnGroup, _flatPos,50] call aw_fnc_spawn2_perimeterPatrol;
				[(units _spawnGroup)] call aw_setGroupSkill;

				_unitsArray = _unitsArray + [_spawnGroup];
			};

			_x = 0;
			for "_x" from 0 to 2 do
			{
				_randomPos = [_flatPos, 50] call aw_fnc_randomPos;
				_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "EAST" >> "OPF_F" >> "Infantry" >> "OIA_InfTeam")] call BIS_fnc_spawnGroup;
				[_spawnGroup, _flatPos, 300] call aw_fnc_spawn2_randomPatrol;
				[(units _spawnGroup)] call aw_setGroupSkill;

				_unitsArray = _unitsArray + [_spawnGroup];
			};

			_x = 0;
			for "_x" from 0 to floor random 2 do
			{
			_spawnGroup = createGroup EAST;
			_randomPos = [_flatPos, 50] call aw_fnc_randomPos;
			_armour = ["O_APC_Tracked_02_AA_F", "O_MBT_02_cannon_F", "O_APC_Tracked_02_cannon_F","O_MRAP_02_hmg_F","O_APC_Wheeled_02_rcws_F"] select floor random 4 createVehicle _randomPos;
			waitUntil{!isNull _armour};
			_unitsarray = _unitsarray + [_armour];
			"O_crew_F" createUnit [_randomPos,_spawnGroup];
			"O_crew_F" createUnit [_randomPos,_spawnGroup];
			"O_crew_F" createUnit [_randomPos,_spawnGroup];

			((units _spawnGroup) select 0) assignAsDriver _armour;
			((units _spawnGroup) select 1) assignAsGunner _armour;
			((units _spawnGroup) select 2) assignAsCommander _armour;
			((units _spawnGroup) select 0) moveInDriver _armour;
			((units _spawnGroup) select 1) moveInGunner _armour;
			((units _spawnGroup) select 2) moveInCommander _armour;
			[_spawnGroup, _flatPos, 300] call aw_fnc_spawn2_randomPatrol;
			_armour spawn aw_fnc_fuelMonitor;
			_unitsArray = _unitsArray + [_spawnGroup,_armour];
			[(units _spawnGroup)] call aw_setGroupSkill;
			_armour lock true;
			};

			_randomPos = [_flatPos, 50,6] call aw_fnc_randomPos;
			_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "EAST" >> "OPF_F" >> "Armored" >> "OIA_MechInf_AA")] call BIS_fnc_spawnGroup;
			[_spawnGroup, _flatPos, 200] call aw_fnc_spawn2_randomPatrol;
			[(units _spawnGroup)] call aw_setGroupSkill;

			_unitsArray = _unitsArray + [_spawnGroup];
_unitsArray
