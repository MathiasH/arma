private ["_fuzzyPos","_veh","_vehName","_vehVarname","_obj","_road","_position","_briefing","_truck","_flatPos","_accepted","_randomPos","_spawnGroup","_unitsArray","_randomDir","_hangar","_object","_object2","_place"];

			_flatPos = [];
			_accepted = false;
			while {!_accepted} do
			{
				_position = [] call BIS_fnc_randomPos;
				_flatPos = _position isFlatEmpty
				[
					5,
					0,
					0.3,
					10,
					0,
					false
				];

				while {(count _flatPos) < 3} do
				{
					_position = [] call BIS_fnc_randomPos;
					_flatPos = _position isFlatEmpty
					[
						5,
						0,
						0.3,
						10,
						0,
						false
					];
				};

				if (
				(_flatPos distance (getMarkerPos "respawn_west")) > 1300 &&
				(_flatPos distance (getMarkerPos "sideMarker")) > 1500 &&
				(_flatPos distance (getMarkerPos currentAO)) > 1500
				) then //DEBUG - set >500 from AO to (PARAMS_AOSize * 2)
				{
					_accepted = true;
				};
			};
			
			_veh = smRewards call BIS_fnc_selectRandom;
			_vehName = _veh select 0;
			_vehVarname = _veh select 1;
			_hangar = objNull;
			//Spawn hangar and chopper
			_randomDir = (random 360);
			if (_vehVarname isKindOf "Helicopter") then {
			_hangar = "Land_HelipadCircle_F" createVehicle _flatPos;
			}
			else
			{
			_hangar = "Land_TentHangar_V1_F" createVehicle _flatPos;
			};
			waitUntil {!isNull _hangar};
			_hangar setPos [(getPos _hangar select 0), (getPos _hangar select 1), ((getPos _hangar select 2) - 1)];
			
			//Set up briefing message
			_briefing =
			Format["<t align='center'><t size='2.2'>New Side Mission</t><br/><t size='1.5' color='#00B2EE'> regain control over the stolen %1</t><br/>____________________<br/>Opfor have stolen %1 and brought it to a hangar somewhere on the island.<br/>We marked the position on the Map.<br/>Go to that hangar and capture the vehicle.</t>",_vehName];

			sideBLUEObjRes = _veh;
			//publicVariable "sideBLUEObjRes";
			sideBLUEObj = _vehVarname createVehicle _flatPos;
			waitUntil {!isNull sideBLUEObj};

			sideBLUEObj lock 3;
			{_x setDir _randomDir} forEach [sideBLUEObj,_hangar];
			sideBLUEObj setVehicleLock "LOCKED";
			sideBLUEObj allowdammage false;
			sideBLUEObj setVehicleAmmo 0;
			//----MP Addactionbefehl über Funktion GSG_AddAction die in der Init.sqf global gemacht wurde
			//--- so bekommt jeder der Join den AddActionbefehl des Fahrzeuges
			[[sideBLUEObj,_vehName,"GSG_fnc_openSM","capture"],"GSG_AddAction",west,true] spawn BIS_fnc_MP; 
			// [[Objekt, Bezeichnung des Objektes,Funktion nach Aufruf des Befehls AddAction],"Funktion mit dem enthaltenem Code aus der Init.sqf",Wer es nutzen kann(ALL,East,Group..,),True=sofort aufrufen und bei jeden JIP/[False]=nur bei JIP,(optional: true=call/[false]=spawn)] spawn BIS_fnc_MP
			//---------------------------------------------------------------------------------------
			_fuzzyPos =
			[
				((_flatPos select 0) - 300) + (random 600),
				((_flatPos select 1) - 300) + (random 600),
				0
			];

			//{ _x setMarkerPos _fuzzyPos; } forEach ["sideBlueMarker", "sideBlueCircle"];
			//"sideBlueMarker" setMarkerText Format["Side Mission: Capture %1", _vehName];
				{_x call GSG_fnc_createMarker;} forEach [
				[2,"sideBlueCircle",_fuzzyPos, "Border","ColorOPFOR",[300,300]],
				[0,"sideBlueMarker",_fuzzyPos, "hd_dot","ColorOPFOR",Format["Side Mission: Capture %1", _vehName]]
				];
			
			//publicVariable "sideBlueMarker";
			//publicVariable "sideBLUEObj";
			//Spawn some enemies around the objective
			_unitsArray = [];
			_x = 0;
			for "_x" from 0 to 2 do
			{
				_randomPos = [_flatPos, 50] call aw_fnc_randomPos;
				_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "EAST" >> "OPF_F" >> "Infantry" >> "OIA_InfTeam")] call BIS_fnc_spawnGroup;
				[_spawnGroup, _flatPos,50] call aw_fnc_spawn2_perimeterPatrol;
				[(units _spawnGroup)] call aw_setGroupSkill;

				_unitsArray = _unitsArray + [_spawnGroup];
			};

			_x = 0;
			for "_x" from 0 to 2 do
			{
				_randomPos = [_flatPos, 50] call aw_fnc_randomPos;
				_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "EAST" >> "OPF_F" >> "Infantry" >> "OIA_InfTeam")] call BIS_fnc_spawnGroup;
				[_spawnGroup, _flatPos, 100] call aw_fnc_spawn2_randomPatrol;
				[(units _spawnGroup)] call aw_setGroupSkill;

				_unitsArray = _unitsArray + [_spawnGroup];
			};

			_x = 0;
			for "_x" from 0 to floor random 2 do
			{
			_spawnGroup = createGroup EAST;
			_randomPos = [_flatPos, 50] call aw_fnc_randomPos;
			_armour = ["O_APC_Tracked_02_AA_F", "O_MBT_02_cannon_F", "O_APC_Tracked_02_cannon_F","O_MRAP_02_hmg_F","O_APC_Wheeled_02_rcws_F"] select floor random 4 createVehicle _randomPos;
			waitUntil{!isNull _armour};
			_unitsarray = _unitsarray + [_armour];
			"O_crew_F" createUnit [_randomPos,_spawnGroup];
			"O_crew_F" createUnit [_randomPos,_spawnGroup];
			"O_crew_F" createUnit [_randomPos,_spawnGroup];

			((units _spawnGroup) select 0) assignAsDriver _armour;
			((units _spawnGroup) select 1) assignAsGunner _armour;
			((units _spawnGroup) select 2) assignAsCommander _armour;
			((units _spawnGroup) select 0) moveInDriver _armour;
			((units _spawnGroup) select 1) moveInGunner _armour;
			((units _spawnGroup) select 2) moveInCommander _armour;
			[_spawnGroup, _flatPos, 300] call aw_fnc_spawn2_randomPatrol;
			_armour spawn aw_fnc_fuelMonitor;
			_unitsArray = _unitsArray + [_spawnGroup];
			[(units _spawnGroup)] call aw_setGroupSkill;
			_armour lock true;
			};
			
			_randomPos = [_flatPos, 50,6] call aw_fnc_randomPos;
			_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "EAST" >> "OPF_F" >> "Armored" >> "OIA_MechInf_AA")] call BIS_fnc_spawnGroup;
			[_spawnGroup, _flatPos, 100] call aw_fnc_spawn2_randomPatrol;
			[(units _spawnGroup)] call aw_setGroupSkill;

			_unitsArray = _unitsArray + [_spawnGroup];

			//Send new side mission hint
			GlobalHint = _briefing; publicVariable "GlobalHint"; hint parseText _briefing;
			showNotification = ["NewSideMission", format["Capture %1",_vehName]]; publicVariable "showNotification";

/*			sideBLUEMissionUp = true;
			publicVariable "sideBLUEMissionUp";
			SideBlueMarkerText = Format["Capture %1", _vehName];
			publicVariable "sideBlueMarkerText";*/
			
			GSG_SM_Open = false;//publicVariable "GSG_SM_Open";
			waitUntil {sleep 1; GSG_SM_Open}; //Mission erfüllt
			
			_object = SideBlueObj;
			_GSG_PosObj = getpos _object;
			if (_object isKindOf "Plane") then {
				//_units=[];
				_place= smMarkerListPlane call BIS_fnc_selectRandom;
				waituntil {!isNil "_place"};
				call{
					_object setPos (getMarkerPos _place);
					_object setdir (markerDir _place);
					/*GSG_Unlock = _object;	
					publicVariableServer "GSG_Unlock";
					_object allowDamage true;*/
					_object lock 0;
					_object setVehicleLock "UNLOCKED";
					_object allowdamage true;
					_null = [_object] execVM "scripts\aw_unitSetup.sqf";
					},
				//_spawnGroup = createGroup WEST;
				_object2 = "B_Truck_01_box_F" createVehicle _GSG_PosObj;
				waitUntil{!isNil "_object2"};
				createvehiclecrew _object2;
				_units=[_object2] + crew _object2;
				{_x allowDamage false;_x setCaptive true;} forEach crew _object2;
				_object2 allowDamage false;
				_object2 lock 3;
				//sleep 5;
				//_unitsarray=_unitsarray + [_spawnGroup];
				[_units, 180] spawn AW_fnc_deleteOldUnitsAndVehicles;
				[_units, 240] spawn AW_fnc_deleteOldSMUnits;
				//_object2 doMove (getMarkerPos "BLUFOR HQ");
				_null=[_object2,getMarkerPos "BLUFOR HQ"] execVM "GSG\gsg_vehmovetonow.sqf";
}
else
{
			//GSG_Unlock = _object;
			//publicVariableServer "GSG_Unlock";
				_object lock 0;
				_object setVehicleLock "UNLOCKED";
				_object allowdamage true;
				_null = [_object] execVM "scripts\aw_unitSetup.sqf";
};
//-------------------------------------------------------------------------
//SMunlock_COMPLETE=true;publicVariable "SMunlock_COMPLETE";
			/*SideBlueObj setVehicleLock "UNLOCKED";
			SideBlueObj allowDamage true;
			[SideBLUEObj] execVM "scripts\aw_unitSetup.sqf";*/
			
			//SMunlock_COMPLETE=false;publicvariable "SMunlock_COMPLETE";
			//Wait until objective is destroyed
			//waitUntil {sleep 0.5; SMunlock_COMPLETE};
			
			//sideBLUEMissionUp = false;
			//publicVariable "sideBLUEMissionUp";
								
			//Send completion hint
			[Format["Capture %1", _vehName]] call AW_fnc_rewardPlusHintBlueOld; //Fahrzeuge wurden vor Ort erobert

			[_unitsArray] spawn AW_fnc_deleteOldUnitsAndVehicles;
			[_unitsArray] spawn AW_fnc_deleteOldSMUnits;
			_null = [_hangar,15] spawn GSG_fnc_deleteRuins;
			//deleteVehicle sideBLUEObj;

			//Hide SM marker
			/*"sideBlueMarker" setMarkerPos [0,0,0];
			"sideBlueCircle" setMarkerPos [0,0,0];
			publicVariable "sideBlueMarker";*/
			{[3,_x] call GSG_fnc_createMarker;} forEach ["sideBlueMarker","sideBlueCircle"];

			//PROCESS REWARD HERE
 /* case "destroyChopper": */