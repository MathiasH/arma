//Set up briefing message
			private ["_SM_bez","_objbez","_fuzzyPos","_veh","_vehName","_vehVarname","_firstRun","_mission","_isGroup","_obj","_skipTimer","_awayFromBase","_road","_position","_deadHint","_civArray","_briefing","_altPosition","_truck","_chosenCiv","_contactPos","_civ","_flatPos","_accepted","_randomPos","_spawnGroup","_unitsArray","_randomDir","_hangar","_x","_sideMissions","_completeText","_roadList","_object","_object2","_place"];			
			
			_briefing = [_this,0,"FEHLER",[""]] call BIS_fnc_param;
			_objbez = [_this,1,"Land_Airport_Tower_F",[""]] call BIS_fnc_param;
			_SM_bez = [_this,2,"FEHLER",[""]] call BIS_fnc_param;

			_flatPos = [];
			_accepted = false;
			while {!_accepted} do
			{
				//Get random safe position somewhere on the island
				_position = [] call BIS_fnc_randomPos;
				_flatPos = _position isFlatEmpty
				[
					5, //minimal distance from another object
					1, //try and find nearby positions if original is incorrect
					0.5, //30% max gradient
					sizeOf _objbez, //gradient must be consistent for entirety of object
					0, //no water
					false //don't find positions near the shoreline
				];

				while {(count _flatPos) < 1} do
				{
					_position = [] call BIS_fnc_randomPos;
					_flatPos = _position isFlatEmpty
					[
						10, //minimal distance from another object
						1, //try and find nearby positions if original is incorrect
						0.5, //30% max gradient
						sizeOf _objbez, //gradient must be consistent for entirety of object
						0, //no water
						false //don't find positions near the shoreline
					];
				};

				if (
				(_flatPos distance (getMarkerPos "respawn_west")) > 1300 &&
				(_flatPos distance (getMarkerPos "sideMarker")) > 1500 &&
				(_flatPos distance (getMarkerPos currentAO)) > 500
				) then //DEBUG - set >500 from AO to (PARAMS_AOSize * 2)
				{
					_accepted = true;
				};
			};

			//Zufallsbelohnung erstellen
			_veh = smRewards call BIS_fnc_selectRandom;
			_vehName = _veh select 0;
			_vehVarname = _veh select 1;
			sideBLUEObjRes = _veh;
			//publicVariable "sideBLUEObjRes";
			
			//Spawn Transmitter Tower, set vector and add marker
			sideBLUEObj = _objbez createVehicle _flatPos;
			waitUntil {!isNull sideBLUEObj}; //Destroy Tower
			sideBLUEObj setPos [(getPos sideBLUEObj select 0), (getPos sideBLUEObj select 1), ((getPos sideBLUEObj select 2) - 2)];
			sideBLUEObj setVectorUp [0,0,1];
			sideBLUEObj setPosATL [getPosATL sideBLUEObj select 0, (getPosATL sideBLUEObj select 1) - 10, 0.7];
			_fuzzyPos =
			[
				((_flatPos select 0) - 300) + (random 600),
				((_flatPos select 1) - 300) + (random 600),
				0
			];

			
				{_x call GSG_fnc_createMarker;} forEach [
				[2,"sideBlueCircle",_fuzzyPos, "Border","ColorOPFOR",[300,300]],
				[0,"sideBlueMarker",_fuzzyPos, "hd_dot","ColorOPFOR",format["Side Mission: %1",_SM_bez]]
				];

			_unitsArray = call compile preprocessFileLineNumbers "sm\Sidemissions\spawnunits\Standart_Mission.sqf";
			_index = sideBLUEObj addMPEventHandler ["mpkilled", {Null = _this execVM "gsg\gsg_addscore.sqf";}];
			//Throw out objective hint
			GlobalHint = _briefing; publicVariable "GlobalHint"; hint parseText GlobalHint;
			showNotification = ["NewSideMission", format["%1",_SM_bez]]; publicVariable "showNotification";

			diag_log format["Side Mission gestartet"];
			
			waitUntil {sleep 1; !alive sideBLUEObj}; //wait until the objective is destroyed
			diag_log format["Side Mission ended"];
			
			[_SM_bez] call AW_fnc_rewardPlusHintBlueOld;
			_null =  [_unitsArray,120] spawn AW_fnc_deleteOldSMUnits;
			_null = [sideBLUEObj,120] spawn GSG_fnc_deleteRuins;
			["gsg\gsg_deletegroups.sqf","BIS_fnc_execVM",true,true] call BIS_fnc_MP;
			{[3,_x] call GSG_fnc_createMarker;} forEach ["sideBlueMarker","sideBlueCircle"];	//Hide marker

			[] spawn GSG_fnc_sideBlueMissions; 
			if (isserver) then {diag_log format["Side Mission  wird erneut geladen"];};
