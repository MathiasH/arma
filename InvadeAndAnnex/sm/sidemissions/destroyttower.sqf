//Set up briefing message
			private ["_SM_bez","_objbez","_fuzzyPos","_veh","_vehName","_vehVarname","_firstRun","_mission","_isGroup","_obj","_skipTimer","_awayFromBase","_road","_position","_deadHint","_civArray","_briefing","_altPosition","_truck","_chosenCiv","_contactPos","_civ","_flatPos","_accepted","_randomPos","_spawnGroup","_unitsArray","_randomDir","_hangar","_x","_sideMissions","_completeText","_roadList","_object","_object2","_place"];			
			
			_briefing = [_this,0,"FEHLER",[""]] call BIS_fnc_param;
			_objbez = [_this,1,"Sign_Sphere100cm_F",[""]] call BIS_fnc_param;
			_SM_bez = [_this,2,"FEHLER",[""]] call BIS_fnc_param;

			_flatPos = [];
			_accepted = false;
			while {!_accepted} do
			{
				//Get random safe position somewhere on the island
				_position = [] call BIS_fnc_randomPos;
				_flatPos = _position isFlatEmpty
				[
					5, //minimal distance from another object
					1, //try and find nearby positions if original is incorrect
					0.5, //30% max gradient
					sizeOf "Land_TTowerBig_1_F", //gradient must be consistent for entirety of object
					0, //no water
					false //don't find positions near the shoreline
				];

				while {(count _flatPos) < 1} do
				{
					_position = [] call BIS_fnc_randomPos;
					_flatPos = _position isFlatEmpty
					[
						10, //minimal distance from another object
						1, //try and find nearby positions if original is incorrect
						0.5, //30% max gradient
						sizeOf "Land_TTowerBig_1_F", //gradient must be consistent for entirety of object
						0, //no water
						false //don't find positions near the shoreline
					];
				};

				if (
				(_flatPos distance (getMarkerPos "respawn_west")) > 1300 &&
				(_flatPos distance (getMarkerPos "sideMarker")) > 1500 &&
				(_flatPos distance (getMarkerPos currentAO)) > 500
				) then //DEBUG - set >500 from AO to (PARAMS_AOSize * 2)
				{
					_accepted = true;
				};
			};

			//Zufallsbelohnung erstellen
			_veh = smRewards call BIS_fnc_selectRandom;
			_vehName = _veh select 0;
			_vehVarname = _veh select 1;
			sideBLUEObjRes = _veh;
			//publicVariable "sideBLUEObjRes";
			
			//Spawn Transmitter Tower, set vector and add marker
			sideBLUEObj = _objbez createVehicle _flatPos;
			waitUntil {!isNull sideBLUEObj}; //Destroy Tower
			sideBLUEObj setPos [(getPos sideBLUEObj select 0), (getPos sideBLUEObj select 1), ((getPos sideBLUEObj select 2) - 2)];
			sideBLUEObj setVectorUp [0,0,1];
			_fuzzyPos =
			[
				((_flatPos select 0) - 300) + (random 600),
				((_flatPos select 1) - 300) + (random 600),
				0
			];

			/*{ _x setMarkerPos _fuzzyPos; } forEach ["SideBlueMarker", "sideBlueCircle"];
			"SideBlueMarker" setMarkerText "Side Mission: Destroy Transmitter Tower";
			publicVariable "SideBlueMarker";
			publicVariable "sideBLUEObj";*/
			
				{_x call GSG_fnc_createMarker;} forEach [
				[2,"sideBlueCircle",_fuzzyPos, "Border","ColorOPFOR",[300,300]],
				[0,"sideBlueMarker",_fuzzyPos, "hd_dot","ColorOPFOR",format["Side Mission: %1",_SM_bez]]
				];

			//Spawn some enemies around the objective
			_unitsArray = [];
			_x = 0;
			for "_x" from 0 to 2 do
			{
				_randomPos = [_flatPos, 50] call aw_fnc_randomPos;
				_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "EAST" >> "OPF_F" >> "Infantry" >> "OIA_InfTeam")] call BIS_fnc_spawnGroup;
				[_spawnGroup, _flatPos,50] call aw_fnc_spawn2_perimeterPatrol;
				[(units _spawnGroup)] call aw_setGroupSkill;

				_unitsArray = _unitsArray + [_spawnGroup];
			};

			_x = 0;
			for "_x" from 0 to 2 do
			{
				_randomPos = [_flatPos, 50] call aw_fnc_randomPos;
				_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "EAST" >> "OPF_F" >> "Infantry" >> "OIA_InfTeam")] call BIS_fnc_spawnGroup;
				[_spawnGroup, _flatPos, 300] call aw_fnc_spawn2_randomPatrol;
				[(units _spawnGroup)] call aw_setGroupSkill;

				_unitsArray = _unitsArray + [_spawnGroup];
			};

			_x = 0;
			for "_x" from 0 to floor random 2 do
			{
			_spawnGroup = createGroup EAST;
			_randomPos = [_flatPos, 50] call aw_fnc_randomPos;
			_armour = ["O_APC_Tracked_02_AA_F", "O_MBT_02_cannon_F", "O_APC_Tracked_02_cannon_F","O_MRAP_02_hmg_F","O_APC_Wheeled_02_rcws_F"] select floor random 4 createVehicle _randomPos;
			waitUntil{!isNull _armour};
			_unitsarray = _unitsarray + [_armour];
			"O_crew_F" createUnit [_randomPos,_spawnGroup];
			"O_crew_F" createUnit [_randomPos,_spawnGroup];
			"O_crew_F" createUnit [_randomPos,_spawnGroup];

			((units _spawnGroup) select 0) assignAsDriver _armour;
			((units _spawnGroup) select 1) assignAsGunner _armour;
			((units _spawnGroup) select 2) assignAsCommander _armour;
			((units _spawnGroup) select 0) moveInDriver _armour;
			((units _spawnGroup) select 1) moveInGunner _armour;
			((units _spawnGroup) select 2) moveInCommander _armour;
			[_spawnGroup, _flatPos, 300] call aw_fnc_spawn2_randomPatrol;
			_armour spawn aw_fnc_fuelMonitor;
			_unitsArray = _unitsArray + [_spawnGroup];
			[(units _spawnGroup)] call aw_setGroupSkill;
			_armour lock true;
			};

			_randomPos = [_flatPos, 50,6] call aw_fnc_randomPos;
			_spawnGroup = [_randomPos, EAST, (configfile >> "CfgGroups" >> "EAST" >> "OPF_F" >> "Armored" >> "OIA_MechInf_AA")] call BIS_fnc_spawnGroup;
			[_spawnGroup, _flatPos, 200] call aw_fnc_spawn2_randomPatrol;
			[(units _spawnGroup)] call aw_setGroupSkill;			
			_unitsArray = _unitsArray + [_spawnGroup];
			
			_index = sideBLUEObj addMPEventHandler ["mpkilled", {Null = _this execVM "gsg\gsg_addscore.sqf";}];
			//Throw out objective hint
			GlobalHint = _briefing; publicVariable "GlobalHint"; hint parseText GlobalHint;
			showNotification = ["NewSideMission", "Destroy Enemy Transmitter Tower"]; publicVariable "showNotification";

			diag_log format["Side Mission gestartet"];
			
			waitUntil {sleep 1; !alive sideBLUEObj}; //wait until the objective is destroyed		
			diag_log format["Side Mission ended"];
			
			[_SM_bez] call AW_fnc_rewardPlusHintBlueOld;
			_null =  [_unitsArray,120] spawn AW_fnc_deleteOldSMUnits;
			_null = [sideBLUEObj,120] spawn GSG_fnc_deleteRuins;
			["gsg\gsg_deletegroups.sqf","BIS_fnc_execVM",true,true] call BIS_fnc_MP;
			{[3,_x] call GSG_fnc_createMarker;} forEach ["sideBlueMarker","sideBlueCircle"];	//Hide marker
			[] spawn GSG_fnc_sideBlueMissions; 
			if (isserver) then {diag_log format["Side Mission  wird erneut geladen"];};

		