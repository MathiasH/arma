/* =============================================== */
/* =============== GLOBAL VARIABLES ============== */

/*
	These targets are simply markers on the map with
	the same name.

	Each AO will be a randomly-picked "target" from
	this list which will be removed upon completion.

	To ensure the mission works, make sure that any
	new targets you add have a relevant marker on
	the mission map.

	You can NOT have an AO called "Nothing".
*/

debugMode = true; publicVariable "debugMode";
eastSide = createCenter EAST;
radioTowerAlive = false;
radarTowerAlive = false;
aapostAlive = false;
sideMissionUp = false;
sideCSATMissionUp = false;
sideBLUEMissionUp = false;
priorityTargetUp = false;
currentAOUp = false;
refreshMarkers = true;
sideObj = objNull;
sideCSATObj = objNull;
sideBLUEObj = objNull;
priorityTargets = ["None"];
smRewards =
[
 	["an Offroad (Armed)", "B_G_Offroad_01_armed_F"],
	["an Offroad (Armed)", "B_G_Offroad_01_armed_F"],
	["an Strider", "I_MRAP_03_F"],
	["an Strider", "I_MRAP_03_F"],
	["an Strider", "I_MRAP_03_F"],
	["an Strider", "I_MRAP_03_F"],
	["an Strider HMG", "I_MRAP_03_hmg_F"],
	["an Strider HMG", "I_MRAP_03_hmg_F"],
	["an Strider HMG", "I_MRAP_03_hmg_F"],
	["an Strider HMG", "I_MRAP_03_hmg_F"],
	["an Strider HMG", "I_MRAP_03_hmg_F"],
	["an Strider GMG", "I_MRAP_03_gmg_F"],
	["an Strider GMG", "I_MRAP_03_gmg_F"],
	["an Strider GMG", "I_MRAP_03_gmg_F"],
	["an Hunter GMG", "B_MRAP_01_gmg_F"],
	["an Hunter GMG", "B_MRAP_01_gmg_F"],
	["an Hunter GMG", "B_MRAP_01_gmg_F"],
	["an Prowler Armed", "B_LSV_01_armed_F"],
	["an Prowler Armed", "B_LSV_01_armed_F"],
	["an Prowler Armed", "B_LSV_01_armed_F"],
	["an Prowler Armed", "B_LSV_01_armed_F"],
	["an AMV-7 Marshall", "B_APC_Wheeled_01_cannon_F"],
	["an AMV-7 Marshall", "B_APC_Wheeled_01_cannon_F"],
	["an FV-720 Mora", "I_APC_tracked_03_cannon_F"],
	["an FV-720 Mora", "I_APC_tracked_03_cannon_F"],
	["an AH-9 Pawnee", "B_Heli_Light_01_armed_F"],
	["an WY-55 Hellcat", "I_Heli_light_03_F"],
	["an V-44 X Blackfish", "B_T_VTOL_01_infantry_F"],
	["an V-44 X Blackfish", "B_T_VTOL_01_infantry_F"],
	["an A-149 Gryphon", "I_Plane_Fighter_04_F"],
	["an To-201 Shrika (Tarn)", "O_Plane_Fighter_02_Stealth_F"],
	["an To-201 Shrika", "O_Plane_Fighter_02_F"]
];
smRewardsCSAT =
[
	["an T-100 Varsuk", "O_MBT_02_cannon_F"],
	["an MSE-3 Marid", "O_APC_Wheeled_02_rcws_F"],
	["an Mi_48 Kajman Black", "O_Heli_Attack_02_black_F"],
	["an AFV-4 Gorgon", "I_APC_Wheeled_03_cannon_F"],
	["an FV-720 Mora", "I_APC_tracked_03_cannon_F"],
	["an PO-30 Orca", "O_Heli_Light_02_F"],
	["an Ifrit HMG", "O_MRAP_02_hmg_F"],
	["an Strider HMG", "I_MRAP_03_hmg_F"]
];
smMarkerList =
["smReward1","smReward2","smReward3","smReward4","smReward5","smReward6","smReward7","smReward8","smReward9","smReward10","smReward11","smReward12","smReward13","smReward14","smReward15","smReward16","smReward17","smReward18","smReward19","smReward20","smReward21"];
smMarkerListPlane =
["smReward22","smReward23","smReward24","smReward25","smReward26","smReward27"];
publicVariable "smMarkerListPlane";
smMarkerListHeli =
["smReward28","smReward29","smReward30","smReward31","smReward32","smReward33","smReward34","smReward35","smReward36","smReward37","smReward38","smReward39"];
smMarkerListCSAT =
["smRewardCSAT_1","smRewardCSAT_2","smRewardCSAT_3","smRewardCSAT_4","smRewardCSAT_5","smRewardCSAT_6","smRewardCSAT_7","smRewardCSAT_8","smRewardCSAT_9","smRewardCSAT_10"];