private ["_start"];
if (isServer) then {_start = diag_tickTime};

private ["_enemiesArray","_pos","_uavAction","_isAdmin","_i","_isPerpetual","_accepted","_position","_randomWreck","_firstTarget","_validTarget","_targetsLeft","_flatPos","_targetStartText","_lastTarget","_targets","_radioTowerDownText","_targetCompleteText","_null","_unitSpawnPlus","_unitSpawnMinus","_missionCompleteText"];

//Orte fuer die Mainmission
_initialTargets = call compile preprocessFileLineNumbers "sm\Var\maintargets.sqf";

_targets = call compile preprocessFileLineNumbers "sm\Var\maintargets.sqf";

_isPerpetual = false;

if (PARAMS_Perpetual == 1) then
{
	_isPerpetual = true;
};

currentAO = "Nothing";
publicVariable "currentAO";
_lastTarget = "Nothing";
_targetsLeft = count _targets;

if (isserver) then {diag_log format["AO Variabel '%1' definiert in %2",currentAO,(diag_tickTime - _start)];};


while {count _targets > 0} do
{
	sleep 10;

	//Set new current target and calculate targets left
	if (_isPerpetual) then
			{
				_validTarget = false;
				while {!_validTarget} do
				{
					currentAO = _targets call BIS_fnc_selectRandom;
					if (currentAO != _lastTarget) then
						{
							_validTarget = true;
						};
					debugMessage = format["_validTarget = %1; %2 was our last target.",_validTarget,currentAO];
					publicVariable "debugMessage";
				};
			} else {
				currentAO = _targets call BIS_fnc_selectRandom;
				_targetsLeft = count _targets;
			};

	//Set currentAO for UAVs and JIP updates
	publicVariable "currentAO";
if (isserver) then {diag_log format["AO Variabel '%1' definiert in %2",currentAO,(diag_tickTime - _start)];};
	GSG_enemiesArray=[];
if (isserver) then {diag_log format["AO gestartet in %1",(diag_tickTime - _start)];};
	AO_Status = true;

	//Create AO detection trigger
	GSG_dt = createTrigger ["EmptyDetector", getMarkerPos currentAO];
	GSG_dt setTriggerArea [PARAMS_AOSize, PARAMS_AOSize, 0, false];
	GSG_dt setTriggerActivation ["EAST", "PRESENT", false];
	GSG_dt setTriggerStatements ["this","",""];
	//dt=GSG_dt;
	 //_gt=[]; {if !(group _x in _gt) then {_gt=_gt+[group _x];};} foreach list GSG_dt; 
	 
	//Spawn enemies
	_enemiesArray = [currentAO] call AW_fnc_spawnUnits; 
	GSG_enemiesArray = GSG_enemiesArray + (_enemiesArray deleteAt 0);
	GSG_AO_Groups = _enemiesArray;
	
	//Spawn radiotower
	[] spawn GSG_fnc_radioTower;

	//Spawn radartower
	[] spawn GSG_fnc_radarTower;
	
	//Spawn aapost
	[] spawn GSG_fnc_aapost;
	waitUntil{sleep 1; !(isNil "radioTower") AND !(isNil "radarTower") AND !(isNil "aapost")};

	//Set target start text
	_targetStartText = format
	[
		"<t align='center' size='2.2'>New Target</t><br/><t size='1.5' align='center' color='#FFCF11'>%1</t><br/>____________________<br/>We did a good job with the last target, lads. Get yourselves over to %1 and take 'em all down!<br/><br/><t size='1.5' align='center' color='#FFCF11'>Take down that radar, the radio tower and the AA post so the enemy cannot call in reinforcements!</t>",
		currentAO
	];

	if (!_isPerpetual) then
	{
		_targetStartText = format
		[
			"%1 Only %2 more targets to go!",
			_targetStartText,
			_targetsLeft
		];
	};

	/* =============================================== */
	/* ========= WAIT FOR TARGET COMPLETION ========== */
	waitUntil {sleep 2; count list GSG_dt > PARAMS_EnemyLeftThreshhold};
	if (isserver) then {diag_log format["AO gestartet mit %2 Einheiten in %1",(diag_tickTime - _start),count list GSG_dt];};
		//Show global target start hint
		{_x call GSG_fnc_createMarker;} forEach [
	[2,"aoCircle",getMarkerPos currentAO, "FDiagonal","ColorOPFOR",[PARAMS_AOSize,PARAMS_AOSize]],
	[0,"aoMarker",getMarkerPos currentAO, "flag_CSAT","Default",format["Take %1",currentAO]]
	];
	
	GlobalHint = _targetStartText; publicVariable "GlobalHint"; hint parseText GlobalHint;
	showNotification = ["NewMain", currentAO]; publicVariable "showNotification";
	gsg_playsound = "newAO_1"; publicVariable "gsg_playsound";
	showNotification = ["NewSub", "Destroy the enemy radio tower."]; publicVariable "showNotification";
	showNotification = ["NewSub", "Destroy the enemy radar tower."]; publicVariable "showNotification";
	showNotification = ["NewSub", "Destroy the enemy anti air post."]; publicVariable "showNotification";
	
	waitUntil {sleep 5;[GSG_enemiesArray] call GSG_fnc_delete_null; (count list GSG_dt < PARAMS_EnemyLeftThreshhold) AND (!alive radioTower) AND (!alive radarTower) AND (!alive aapost)};
	if (isserver) then {diag_log format["AO erledigt in %1",(diag_tickTime - _start)];};
	
	if (_isPerpetual) then
	{
		_lastTarget = currentAO;
		if ((count (_targets)) == 1) then
		{
			_targets = _initialTargets;
		} else {
			_targets = _targets - [currentAO];
		};
	} else {
		_targets = _targets - [currentAO];
	};

	/*publicVariable "refreshMarkers";
	currentAOUp = false;
	publicVariable "currentAOUp";*/



	//Small sleep to let deletions process
	sleep 5;

	//Set target completion text
	_targetCompleteText = format
	[
		"<t align='center' size='2.2'>Target Taken</t><br/><t size='1.5' align='center' color='#FFCF11'>%1</t><br/>____________________<br/><t align='left'>Fantastic job taking %1, boys! Give us a moment here at HQ and we'll line up your next target for you.</t>",
		currentAO
	];

	//{_x setMarkerPos [0,0,0];} forEach ["aoCircle","aoMarker","radioMineCircle"];
	//lösche MinenfeldMarker
	{[3,_x] call GSG_fnc_createMarker;} forEach ["aoCircle","aoMarker","radioMineCircle"];
	
	//Show global target completion hint
	GlobalHint = _targetCompleteText; publicVariable "GlobalHint"; hint parseText GlobalHint;
	gsg_playsound = "completedAO_1"; publicVariable "gsg_playsound";
	showNotification = ["CompletedMain", currentAO]; publicVariable "showNotification";

	//Set enemy kill timer
	[GSG_enemiesArray] spawn AW_fnc_deleteOldAOUnits;
	AO_defend = true;

// AI Retaliation by Jester [AW]
	_handle = [_start] spawn 
	{
		private ["_start","_aoUnderAttack"];
		_start = _this select 0;
		if (PARAMS_DefendAO == 1) then
		{	
			if ([true,false] call BIS_fnc_selectRandom) exitWith{};
			if (isserver) then {diag_log format["AO verteidigen in %1",(diag_tickTime - _start)];};
			AO_defend = false;
			_aoUnderAttack = [] spawn GSG_fnc_aoRetake;
			waitUntil {sleep 5; scriptDone _aoUnderAttack};
			if (isserver) then {diag_log format["AO verteidigt in %1",(diag_tickTime - _start)];};
		};
	};
	waitUntil {sleep 1;AO_defend};
	if (isserver) then {diag_log format["AO ist durch in %1",(diag_tickTime - _start)];};
	AO_Status = false;
	{_null = [_x] spawn GSG_fnc_deleteRuins;}forEach [Radiotower,Radartower,aapost];
	
	//Small sleep to let deletions process
	sleep 1;

	//löscht alle Marker
	{[3,_x] call GSG_fnc_createMarker;} forEach ["aoCircle_2","aoCircle_3","aoMarker_2"];
	//Delete detection trigger and markers
	deleteVehicle GSG_dt;
	
	[] spawn aw_cleanGroups;
	[] remoteExecCall ["aw_cleanGroups", 0, false];
	if (isserver) then {diag_log format["AO beendet und wird neugestart bei %1",(diag_tickTime - _start)];};
	if (isServer) then {_start = diag_tickTime};
};

//Set completion text
_missionCompleteText = "<t align='center' size='2.0'>Congratulations!</t><br/>
<t size='1.2' align='center'>You've successfully completed Ahoy World Invade &amp; Annex!</t><br/>
____________________<br/>
<br/>
Thank you so much for playing and we hope to see you in the future. For more and to aid in the development of this mission, please visit www.AhoyWorld.co.uk.<br/>
<br/>
The game will return to the mission screen in 30 seconds. Consider turning Perpetual Mode on in the parameters to make the game play infinitely.";

//Show global mission completion hint
GlobalHint = _missionCompleteText;
publicVariable "GlobalHint";
hint parseText GlobalHint;

//Wait 30 seconds
sleep 30;

//End mission
endMission "END1";