/*	www.grenzschutzgruppe.de
	sideBLueMission - by Figuri001
	(parts of this Script is from Rarek (AW))
	
	Insert public variable event handler into player init:

	"SideBlueMarker" addPublicVariableEventHandler
	{
		"SideBlueMarker" setMarkerPos (markerPos "SideBlueMarker");
	};

	Also, we need an event handler for playing custom sounds via SAY.
	Pass the EH a variable that it can use to play the correct sound.

	For the addAction issue, we need to run the addAction command LOCALLY. Using forEach allUnits won't work as the action is still being run on the server.
	So, set up an EH that can add actions to units.

	For deletion of contact (and this applies to AO units, too!)...
		Using []spawn { }; will run code that then ignores the rest of the script!
		AWESOME!
*/
// ZEIT ÄNDERN (900 + 1800)
#define waitMIN 900
#define waitMAX 1800
//Create base array of differing side missions

"GSG_SM_Open_Reserve" addPublicVariableEventHandler
{
	private ["_timeleft"];
	_timeleft = _this select 1;
	_timeleft spawn {
	sleep _this;
	GSG_SM_Open = true;
	};
};


private ["_fuzzyPos","_veh","_vehName","_vehVarname","_firstRun","_mission","_isGroup","_obj","_skipTimer","_awayFromBase","_road","_position","_deadHint","_civArray","_briefing","_altPosition","_truck","_chosenCiv","_contactPos","_civ","_flatPos","_accepted","_randomPos","_spawnGroup","_unitsArray","_randomDir","_hangar","_x","_sideMissions","_completeText","_roadList","_object","_object2","_place"];
			
_sideMissions =
[
	//"captcherVehicle",
	"destroySmallRadar",
	"destroyTTower",
	"destroyExplosivesCoast",
	"destroyLighthouse",
	"destroyWeaponsSupply",
	"destroyResearchOutpost",
	"destroyInsurgentHQ"
	//"destroyRogueHQ"
];

_mission = "";

_completeText =
"<t align='center'><t size='2.2'>Side Mission</t><br/><t size='1.5' color='#00B2EE'>COMPLETE</t><br/>____________________<br/>Fantastic job, lads! The Opfor stationed on the island won't last long if you keep that up!<br/><br/>Focus on the main objective for defending now; We'll get back to you in 15 - 45 minutes.</t>";

//Set up some vars
//_firstRun = true; //debug
_firstRun = missionNamespace getVariable "firstrun";
//_skipTimer = false;
_roadList = island nearRoads 7000; //change this to the BIS function that creates a trigger covering the map
_contactPos = [0,0,0];
_unitsArray = [];

	sleep (waitMIN  + (random waitMAX));
	_mission = _sideMissions call BIS_fnc_selectRandom;

	//Grab the code for the selected mission
	switch (_mission) do
	{
	
		case "captcherVehicle":
		{
			_aktivmission = [] spawn compile preprocessFileLineNumbers "sm\Sidemissions\captcherVeh.sqf";
			waitUntil{scriptdone _aktivmission};
		};
	
		case "destroyTTower":
		{
			_briefing =	"<t align='center'><t size='2.2'>New Side Mission</t><br/><t size='1.5' color='#00B2EE'>Destroy Transmitter Tower</t><br/>____________________<br/>Opfor forces have erected a Transmitter Tower on the island as part of a project to spy friendly Information.<br/><br/>We've marked the position on your map; head over there and take down that Transmitter Tower.</t>";
			_obj = "Land_TTowerBig_1_F";
			_SM_txt = "Destroy Transmitter Tower";
			_aktivmission = [_briefing,_obj,_SM_txt] spawn compile preprocessFileLineNumbers"sm\Sidemissions\destroyObj.sqf";
			waitUntil{scriptdone _aktivmission};
		}; 

		case "destroySmallRadar":
		{
			//Set up briefing message
			_briefing =	"<t align='center'><t size='2.2'>New Side Mission</t><br/><t size='1.5' color='#00B2EE'>Destroy Radar</t><br/>____________________<br/>OPFOR forces have erected a small radar on the island as part of a project to keep friendly air support at bay.<br/><br/>We've marked the position on your map; head over there and take down that radar.</t>";
			_obj = "Land_Radar_Small_F";
			_SM_txt = "Destroy Enemy Radar";
			_aktivmission = [_briefing,_obj,_SM_txt] spawn compile preprocessFileLineNumbers"sm\Sidemissions\destroyObj.sqf";
			waitUntil{scriptdone _aktivmission};
		};	
		
		case "destroyExplosivesCoast":
		{
			//Set up briefing message
			_briefing = "<t align='center'><t size='2.2'>New Side Mission</t><br/><t size='1.5' color='#00B2EE'>Destroy Smuggled Explosives</t><br/>____________________<br/>The OPFOR have been smuggling explosives onto the island and hiding them in a Mobile HQ on the coastline.<br/><br/>We've marked the building on your map; head over there and destroy their stock. Keep well back when you blow it; there's a lot of stuff in that building.</t>";
			_obj = "Land_Cargo_HQ_V1_F";
			_SM_txt = "Destroy Smuggled Explosives";
			_aktivmission = [_briefing,_obj,_SM_txt] spawn compile preprocessFileLineNumbers"sm\Sidemissions\destroyObj.sqf";
			waitUntil{scriptdone _aktivmission};
		};
		
		case "destroyLighthouse":
		{
			//Set up briefing message
			_briefing =
			"<t align='center'><t size='2.2'>New Side Mission</t><br/><t size='1.5' color='#00B2EE'>Destroy Lighthouse</t><br/>____________________<br/>The OPFOR need Lighthouse to the Shipnavigation in the near the island.<br/><br/>We've marked the building on your map; head over there and destroy the Lighthouse.</t>";
			_obj = "Land_LightHouse_F";
			_SM_txt = "Destroy Lighthouse";
			_aktivmission = [_briefing,_obj,_SM_txt] spawn compile preprocessFileLineNumbers"sm\Sidemissions\destroyObj.sqf";
			waitUntil{scriptdone _aktivmission};
		};
		
		case "destroyResearchOutpost":
		{
			//Set up briefing message
			_briefing =
			"<t align='center'><t size='2.2'>New Side Mission</t><br/><t size='1.5' color='#00B2EE'>Destroy Research Outpost</t><br/>____________________<br/>The OPFOR are conducting biological weapons testing in a small building somewhere on Altis.<br/><br/>We've marked the approximate location on your map; head over there and destroy their work before it's too late!</t>";
			_obj = "Land_Medevac_house_V1_F";
			_SM_txt = "Destroy Research Outpost";
			_aktivmission = [_briefing,_obj,_SM_txt] spawn compile preprocessFileLineNumbers"sm\Sidemissions\xtrMission.sqf";
			waitUntil{scriptdone _aktivmission};
		};

		case "destroyWeaponsSupply":
		{
			//Set up briefing message
			_briefing ="<t align='center'><t size='2.2'>New Side Mission</t><br/><t size='1.5' color='#00B2EE'>Intercept Weapons Transfer</t><br/>____________________<br/>Rogue Independents are supplying OPFOR with advanced weapons and intel.<br/><br/>We've marked the transfer building on your map. They are using an old cargo house. Head over there and destroy it before the transfer is complete!</t>";
			_obj = "Land_Research_HQ_F";
			_SM_txt = "Destroy Weapons Depot";
			_aktivmission = [_briefing,_obj,_SM_txt] spawn compile preprocessFileLineNumbers"sm\Sidemissions\xtrMission.sqf";
			waitUntil{scriptdone _aktivmission};
		};

		case "destroyInsurgentHQ":
		{
			//Set up briefing message
			_briefing ="<t align='center'><t size='2.2'>New Side Mission</t><br/><t size='1.5' color='#00B2EE'>Destroy Insurgency HQ</t><br/>____________________<br/>OPFOR are training an insurgency on Altis.<br/><br/>We've marked the position on your map; head over there, sanitize the area and destroy their HQ.</t>";
			_obj = "Land_Cargo_HQ_V2_F";
			_SM_txt = "Destroy Insurgency HQ";
			_aktivmission = [_briefing,_obj,_SM_txt] spawn compile preprocessFileLineNumbers"sm\Sidemissions\xtrMission.sqf";
			waitUntil{scriptdone _aktivmission};
		};

		case "destroyRogueHQ":
		{
			//Set up briefing message
			_briefing ="<t align='center'><t size='2.2'>New Side Mission</t><br/><t size='1.5' color='#00B2EE'>Eliminate Rogue BLUFOR Unit</t><br/>____________________<br/>Treason! A section of BLUFOR soldiers have gone rogue and are working with OPFOR!  <br/><br/>Take them out and destroy their HQ!</t>";
			_obj = "Land_Cargo_HQ_V1_F";
			_SM_txt = "Eliminate Rogue HQ";
			_aktivmission = [_briefing,_obj,_SM_txt] spawn compile preprocessFileLineNumbers"sm\Sidemissions\xtrMission.sqf";
			waitUntil{scriptdone _aktivmission};
		};
	};
