//by Figuri001
private["_position","_flatPos","_start"];

if (isServer) then {_start = diag_tickTime};
if (isserver) then {diag_log format["Errichtung Radartower gestartet in %1",(diag_tickTime - _start)];};
_position = [[[getMarkerPos currentAO, PARAMS_AOSize],GSG_dt],["water","out"]] call BIS_fnc_randomPos;
_flatPos = _position isFlatEmpty[3, 1, 0.7, 20, 0, false];

while {(count _flatPos) < 1} do
	{
		_position = [[[getMarkerPos currentAO, PARAMS_AOSize],GSG_dt],["water","out"]] call BIS_fnc_randomPos;
		_flatPos = _position isFlatEmpty[3, 1, 0.7, 20, 0, false];
	};

radarTower = "Land_Radar_Small_F" createVehicle _flatPos;
waitUntil {sleep 1; alive radarTower};
radarTower setVectorUp [0,0,1];
radarTowerAlive = true;
publicVariable "radarTowerAlive";

//"radarMarker" setMarkerPos (getPos radarTower);
[0,"radarMarker",getPos radarTower, "respawn_para","ColorOPFOR","Radartower"] call GSG_fnc_createMarker;
if (isserver) then {diag_log format["radartower fertig gestellt in %1",(diag_tickTime - _start)];};

	if (PARAMS_AOReinforcementJet == 1) then {[] spawn GSG_fnc_airDefense;};
	if (PARAMS_AOReinforcementHeavyHelo == 1) then {[] spawn GSG_fnc_heloDefense;};
_index = radarTower addMPEventHandler ["mpkilled", {Null = _this execVM "gsg\gsg_addscore.sqf";}];
_index1 = radarTower addEventHandler ["killed", {Null = _this execVM "sm\scripts\fn_radartower_dead.sqf";}];
