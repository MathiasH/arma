//by Figuri001
private["_position","_flatPos","_start"];

if (isServer) then {_start = diag_tickTime};
if (isserver) then {diag_log format["Errichtung aapost gestartet in %1",(diag_tickTime - _start)];};
_position = [[[getMarkerPos currentAO, PARAMS_AOSize],GSG_dt],["water","out"]] call BIS_fnc_randomPos;
_flatPos = _position isFlatEmpty[5, 1, 0.1, 20, 0, false];

while {(count _flatPos) < 1} do
	{
		_position = [[[getMarkerPos currentAO, PARAMS_AOSize],GSG_dt],["water","out"]] call BIS_fnc_randomPos;
		_flatPos = _position isFlatEmpty[5, 1, 0.1, 20, 0, false];
	};

aapost = "Land_Cargo_HQ_V2_F" createVehicle _flatPos;
aapost setVectorUp [0,0,1];
[3,"aaMarker"] call GSG_fnc_createMarker;
[0,"aaMarker",getPos aapost, "o_unknown","ColorOPFOR","Anti Air Post"] call GSG_fnc_createMarker;
aa1 = "O_static_AA_F" createVehicle _flatPos; aa1 allowdamage false; aa1 setVehicleLock "LOCKEDPLAYER"; aa1 attachTo [aapost, [-3.07, -4.3, 0.24], "Pelvis"]; aa1 setVectorDirAndUp [[0,-1,0],[0,0,1]];
aa2 = "O_static_AA_F" createVehicle _flatPos; aa2 allowdamage false; aa2 setVehicleLock "LOCKEDPLAYER"; aa2 attachTo [aapost, [-3.37, 2.86, 0.24], "Pelvis"];  aa2 setVectorDir [0,-1,0];
aa3 = "O_static_AA_F" createVehicle _flatPos; aa3 allowdamage false; aa3 setVehicleLock "LOCKEDPLAYER"; aa3 attachTo [aapost, [6, 2.86, 0.24], "Pelvis"]; aa3 setVectorDirAndUp [[1,0,0],[0,0,1]];
waitUntil {sleep 1; (alive aapost) && (alive aa1) && (alive aa2) && (alive aa3)};

_spawnGroup = createGroup east;
"O_crew_F" createUnit [aa1, _spawnGroup,"loon1 = this assignAsGunner aa1; this moveInGunner aa1;", 0.7, "corporal"];
"O_crew_F" createUnit [aa2, _spawnGroup,"loon1 = this assignAsGunner aa2; this moveInGunner aa2;", 0.7, "corporal"];
"O_crew_F" createUnit [aa3, _spawnGroup,"loon1 = this assignAsGunner aa3; this moveInGunner aa3;", 0.7, "corporal"];
 
aa1 allowdamage true;
aa2 allowdamage true;
aa3 allowdamage true;
aapostAlive = true;
publicVariable "aapostAlive";
if (isserver) then {diag_log format["aapost fertig gestellt in %1",(diag_tickTime - _start)];};
_index = aapost addMPEventHandler ["mpkilled", {Null = _this execVM "gsg\gsg_addscore.sqf";}];
_index1 = aapost addEventHandler ["killed", {Null = _this execVM "sm\scripts\fn_aapost_dead.sqf";}];