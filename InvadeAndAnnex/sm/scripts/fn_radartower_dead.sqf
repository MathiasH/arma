	if (isServer) then {_start = diag_tickTime};
	if (isserver) then {diag_log format["Radartower zerstört bei %1",(diag_tickTime - _start)];};
	radarTowerAlive = false;
	publicVariable "radarTowerAlive";
	[3,"radarMarker"] call GSG_fnc_createMarker;
	_radarTowerDownText =
		"<t align='center' size='2.2'>radar Tower</t><br/><t size='1.5' color='#08b000' align='center'>DESTROYED</t><br/>____________________<br/>The enemy radar tower has been destroyed! Fantastic job, lads!<br/><br/><t size='1.2' color='#08b000' align='center'> The enemy cannot call in anymore air support now!</t><br/>";
	GlobalHint = _radarTowerDownText; publicVariable "GlobalHint"; hint parseText GlobalHint;
	showNotification = ["CompletedSub", "Enemy radar tower destroyed."]; publicVariable "showNotification";
