//Wracks auf der Insel
{		_accepted = false;
		_position = [0,0,0];
		while {!_accepted} do
		{
			_position = [] call BIS_fnc_randomPos;
			if (_position distance (getMarkerPos "respawn_west") > 1200) then
			{
				_accepted = true;
			};
		};
		_randomWreck = _x createVehicle _position;
		_randomWreck setDir (random 360);
} forEach [
"Land_Wreck_Heli_Attack_01_F",
"Land_UWreck_Mv22_F",
"Land_UWreck_Heli_Attack_02_F",
"Land_Wreck_Offroad_F",
"Land_Wreck_Offroad_F",
"Land_Wreck_Offroad_F",
"Land_Wreck_Offroad_F",
"Land_Wreck_Offroad_F",
"Land_Wreck_Truck_dropside_F",
"Land_Wreck_Truck_F",
"Land_Wreck_Car_F",
"Land_Wreck_Car2_F",
"Land_Wreck_Car3_F"
];
