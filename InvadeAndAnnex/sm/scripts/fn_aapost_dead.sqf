	if (isServer) then {_start = diag_tickTime};
	if (isserver) then {diag_log format["aapost zerstört bei %1",(diag_tickTime - _start)];};
	aapostAlive = false;
	publicVariable "aapostAlive";
	[3,"aaMarker"] call GSG_fnc_createMarker;
	_aapostDownText =
		"<t align='center' size='2.2'>anti air post</t><br/><t size='1.5' color='#08b000' align='center'>DESTROYED</t><br/>____________________<br/>The enemy anti air post has been destroyed! Fantastic job, lads!<br/><br/><t size='1.2' color='#08b000' align='center'> The enemy is getting weaker!</t><br/>";
	GlobalHint = _aapostDownText; publicVariable "GlobalHint"; hint parseText GlobalHint;
	showNotification = ["CompletedSub", "Enemy anti air post destroyed."]; publicVariable "showNotification";
	
	sleep 5;
	aa1 setdamage 1;
	aa2 setdamage 1;
	aa3 setdamage 1;