	if (isServer) then {_start = diag_tickTime};
	if (isserver) then {diag_log format["Radiotower zerstört bei %1",(diag_tickTime - _start)];};
	radioTowerAlive = false;
	publicVariable "radioTowerAlive";
	[3,"radioMarker"] call GSG_fnc_createMarker;
	_radioTowerDownText =
		"<t align='center' size='2.2'>Radio Tower</t><br/><t size='1.5' color='#08b000' align='center'>DESTROYED</t><br/>____________________<br/>The enemy radio tower has been destroyed! Fantastic job, lads!<br/><br/><t size='1.2' color='#08b000' align='center'> The enemy cannot call in anymore infantry support now!</t><br/><br/> You're now all free to use your Personal UAVs!";
	GlobalHint = _radioTowerDownText; publicVariable "GlobalHint"; hint parseText GlobalHint;
	showNotification = ["CompletedSub", "Enemy radio tower destroyed."]; publicVariable "showNotification";
	showNotification = ["Reward", "Personal UAVs now available."]; publicVariable "showNotification";
