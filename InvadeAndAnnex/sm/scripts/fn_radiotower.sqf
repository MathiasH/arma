//by Figuri001
private["_position","_flatPos","_start"];

if (isServer) then {_start = diag_tickTime};
if (isserver) then {diag_log format["Errichtung Radiotower gestartet in %1",(diag_tickTime - _start)];};
_position = [[[getMarkerPos currentAO, PARAMS_AOSize],GSG_dt],["water","out"]] call BIS_fnc_randomPos;
_flatPos = _position isFlatEmpty[3, 1, 0.1, 20, 0, false];

while {(count _flatPos) < 1} do
	{
		_position = [[[getMarkerPos currentAO, PARAMS_AOSize],GSG_dt],["water","out"]] call BIS_fnc_randomPos;
		_flatPos = _position isFlatEmpty[3, 1, 0.1, 20, 0, false];
	};

radioTower = "Land_TTowerBig_2_F" createVehicle _flatPos;
waitUntil {sleep 1; alive radioTower};
radioTower setVectorUp [0,0,1];
radioTowerAlive = true;
publicVariable "radioTowerAlive";

	_chance = random 10;
	
if (_chance != PARAMS_RadioTowerMineFieldChance) then
	{
	_mines = [getPos radioTower] call AW_fnc_minefield;
	GSG_enemiesArray = GSG_enemiesArray + _mines;
	[2,"radioMineCircle",getPos radioTower, "BDiagonal","ColorRed",[50,50]] call GSG_fnc_createMarker;
	[0,"radioMarker",getPos radioTower,"loc_Transmitter","ColorOPFOR","Radiotower (Minenfeld)"] call GSG_fnc_createMarker;
	}else{
	[0,"radioMarker",getPos radioTower,"loc_Transmitter","ColorOPFOR","Radiotower"] call GSG_fnc_createMarker;
	};
if (isserver) then {diag_log format["Radiotower fertig gestellt in %1",(diag_tickTime - _start)];};
_index = radioTower addMPEventHandler ["mpkilled", {Null = _this execVM "gsg\gsg_addscore.sqf";}];
_index1 = radioTower addEventHandler ["killed", {Null = _this execVM "sm\scripts\fn_radiotower_dead.sqf";}];
