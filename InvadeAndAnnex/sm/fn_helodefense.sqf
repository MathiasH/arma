private ["_priorityMessageHelo","_time","_startpos","_helo_Array","_helo_Patrol","_helo_Array","_patrolPos","_start"];
if (isServer) then {_start = diag_tickTime};
if !(isServer) exitWith {};
if (isserver) then {diag_log format["Helo Defense Script gestartet in %1",(diag_tickTime - _start)];};
#define AIR_TYPE "O_Heli_Attack_02_F"
#define SPAWN_LIMIT 2
#define EASY false
#define FIXED_TIME 300
#define RANDOM_TIME 900
#define minPlayer 15

while {alive radarTower} do {
	
	sleep (FIXED_TIME + (random RANDOM_TIME));
	if !(alive radarTower) exitWith {};
	_anz = call GSG_fnc_AnzPlayer;
    if  ((({(typeOf _x == AIR_TYPE) && (side _x == east)} count vehicles) < SPAWN_LIMIT) AND (_anz >= minPlayer)) then {
		_null = [] spawn{
		private ["_priorityMessageHelo","_time","_startpos","_helo_Array","_helo_Patrol","_helo_Array","_patrolPos","_start","_unitsArray"];
		_unitsArray=[];		
		if (isServer) then {_start = diag_tickTime};
		_patrolPos=getMarkerPos currentAO;
		_blue_hq = getMarkerPos "BLUFOR HQ";
		_startpos =[_blue_hq, _patrolPos, 550] call GSG_fnc_startpunkt;
		if (isserver) then {diag_log format["Helo Defense - Hubschrauber StartPosition %2 in %1",(diag_tickTime - _start),str(_startpos select 0)];};
		_helo_Array = [_startpos select 0, _startpos select 1, [AIR_TYPE] call BIS_fnc_selectRandom, east] call BIS_fnc_spawnVehicle;
		if (isserver) then {diag_log format["Helo Defense - spawnVehicle %2 in %1",(diag_tickTime - _start),str(_helo_Array)];};
		_helo_Patrol = _helo_Array select 0; //Fahrzeug
		_helo_crew = _helo_Array select 1; // Crew
		_helo_group = _helo_Array select 2; // Gruppe
		_unitsArray = _unitsArray +[_helo_group, _helo_Patrol];
		[_helo_group, _patrolPos, 500] call BIS_fnc_taskPatrol;
		_helo_Patrol setVehicleLock "LOCKEDPLAYER";
		//showNotification = ["EnemyHeavyHelo", "Enemy Mi-48 Kajman approaching to AO."]; publicVariable "showNotification";
		//_priorityMessageHelo =		"<t align='center' size='2.2'>Priority Target (AO)</t><br/><t size='1.5' color='#b60000'>Enemy Mi-48 Kajman Inbound</t><br/>____________________<br/>OPFOR forces are inbound with a Kajman to support their infantry forces!<br/><br/>This is a priority target, boys!<br/><br/>HQ suggests to all Transport Helicopters to return to base until the danger is neutralised!";
		//GlobalHint = _priorityMessageHelo; publicVariable "GlobalHint"; hint parseText _priorityMessageHelo;
		
		_time = time;
		waitUntil {
			sleep 5;
			if ((time-_time) > 300) then {
				_helo_Patrol setVehicleAmmo 1;
				_time=time;
				sleep 1;
				if (EASY) then {
					_helo_Patrol removeMagazineTurret ["8Rnd_LG_scalpel",[0]];
					_helo_Patrol removeMagazines "8Rnd_LG_scalpel";
					_helo_Patrol removeMagazineTurret ["38Rnd_80mm_rockets",[0]];
					_helo_Patrol removeMagazines "38Rnd_80mm_rockets";
				};
			};
		if !(AO_Status) exitWith {
									[_unitsArray,5] spawn AW_fnc_deleteOldUnitsAndVehicles;
									[_unitsArray,5] spawn AW_fnc_deleteOldSMUnits;
									sleep 5;
									if (isserver) then {diag_log format["Helo Defense - Helo wird gelöscht in %1",(diag_tickTime - _start)];};
									};
		!alive _helo_Patrol || {!canMove _helo_Patrol}
		};
		if (isserver) then {diag_log format["Helo Defense - Hubschrauber zerstört in %1",(diag_tickTime - _start)];};
		[] call AW_fnc_rewardPlusHintMI;
		sleep 5;
		{_x setDamage 1;} foreach crew _helo_Patrol;
		_helo_Patrol setDamage 1;
		[_unitsArray] spawn AW_fnc_deleteOldUnitsAndVehicles;
		[_unitsArray] spawn AW_fnc_deleteOldSMUnits;
		};
	};
};
