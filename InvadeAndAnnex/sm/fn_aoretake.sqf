private["_zufall ","_start","_defendTimer","_targetStartText","_playersOnline","_targetStartText2","_defendMessages","_enemiesArray","_defTimer"];
if (isServer) then {_start = diag_tickTime};
if (isServer) then {diag_log format["aoRetake geladen in %1",(diag_tickTime - _start)];};
		
		if (isServer) then {diag_log format["aoRetake gestartet in %1",(diag_tickTime - _start)];};
		_defendMessages =
			[
				"OPFOR Forces incoming! Seek cover immediately and defend the target!"
			];
		_targetStartText = format
			[
				"<t align='center' size='2.2'>Defend Target</t><br/><t size='1.5' align='center' color='#0d4e8f'>%1</t><br/>____________________<br/>We got a problem. The enemy managed to call in land reinforcements. They are on the way to take back the last target. You need to defend it at all costs!<br/><br/>If the last man of BluFor dies in the target area the enemy have won.<br/><br/>Forces are expected to be there in a couple minutes, hurry up and dig in!",
				currentAO
			];

		GlobalHint = _targetStartText; publicVariable "GlobalHint"; hint parseText GlobalHint;
		showNotification = ["NewMainDefend", currentAO]; publicVariable "showNotification";
		
		{_x call GSG_fnc_createMarker;} forEach [
			[2,"aoCircle_2",getMarkerPos currentAO, "FDiagonal","ColorBLUFOR",[PARAMS_AOSize,PARAMS_AOSize]],
			[2,"aoCircle_3",getMarkerPos currentAO, "HDiagonal","ColorOPFOR",[PARAMS_AOSize,PARAMS_AOSize]],
			[0,"aoMarker_2",getMarkerPos currentAO, "flag_UN","Default",format["Defend %1",currentAO]]
		];

		_playersOnline = format
			[
				"Target: %1! Get ready boys - They are almost there! - UAVs available.", currentAO
			];

		_playersOnlineHint = format
			[
				"<t size='1.5' align='left' color='#C92626'>Target:%1!</t><br/><br/>____________________<br/>Get ready boys they are almost there! - UAVs available.", currentAO
			];
		
		_defendTimer = 900 + ((floor random 10)*60);
		if (isServer) then {diag_log format["aoRetake Defendtimer mit %2 Sek gestartet in %1",(diag_tickTime - _start), _defendTimer];};
		hqSideChat = _playersOnline; publicVariable "hqSideChat"; [WEST,"HQ"] sideChat hqSideChat;
		GlobalHint = _playersOnlineHint; publicVariable "GlobalHint"; hint parseText GlobalHint;
		sleep (5 + (random 25)); // time before they spawn

		hqSideChat = _defendMessages call BIS_fnc_selectRandom; publicVariable "hqSideChat"; [WEST,"HQ"] sideChat hqSideChat;

		_enemiesArray = [currentAO] call GSG_fnc_ao_retake_spawn_Units;
		hint "Thermal images show enemy are at the perimeter of the AO!";

				// countdown timer

				[[hint "Enemy Spotted. Standby..."],"BIS_fnc_spawn",nil,true] spawn BIS_fnc_MP;
				sleep 0.5;
				waitUntil {sleep 1; count list gsg_dt > 15};
				_defTimer = 0;
				while {true} do {
				//hintsilent format ["Assualt will end in :%1", [((_defendTimer)/60)+.01,"HH:MM"] call bis_fnc_timetostring];
				_targetStartText2 = format ["Assualt will end in: %1", [((_defendTimer)/60)+.01,"HH:MM"] call bis_fnc_timetostring];
					if(DEBUG) then
						{GlobalHint = _targetStartText2; publicVariable "GlobalHint"; hint parseText GlobalHint;};
				if (_defTimer >= _defendTimer or (count list gsg_dt) <= 10) exitWith{
					_targetCompleteText = format
					[
					"<t align='center' size='2.2'>Target Defended</t><br/><t size='1.5' align='center' color='#00FF80'>%1</t><br/>____________________<br/><t align='left'>Fantastic job defending %1, boys! Give us a moment here at HQ and we'll line up your next target for you.</t>",
					currentAO
					];
					//Show global target completion hint
					GlobalHint = _targetCompleteText; publicVariable "GlobalHint"; hint parseText GlobalHint;
					gsg_playsound = "completedAO_2"; publicVariable "gsg_playsound";
					showNotification = ["CompletedMainDefended", currentAO]; publicVariable "showNotification"; if (isServer) then {diag_log format["aoRetake fertig in %1",(diag_tickTime - _start)];};
					[_enemiesArray] spawn AW_fnc_deleteOldAOUnits;
					AO_defend = true;
					};
				_defTimer = _defTimer + 1;
				{{_x allowFleeing (_defTimer / _defendTimer)} forEach (units _x);}forEach _enemiesArray;
				player commandChat format["%1 / %2 = %3",_defTimer,_defendTimer,(_defTimer / _defendTimer)];
				sleep 1;
				};