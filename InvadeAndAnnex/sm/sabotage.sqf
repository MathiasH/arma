_actUsed = _this select 1;  // Unit that used the Action (also _this in the addAction command)
_actID = _this select 2;  // ID of the Action

_object = _this select 0;

sleep 1;
//ZEIT ÄNDERN AUF 120
_timeleft = 120;

[[hint "Charge placed on Objective. Standby..."],"BIS_fnc_spawn",nil,true] spawn BIS_fnc_MP;
sleep 0.5;
while {true} do {
hintsilent format ["Charge Explode in :%1", [((_timeleft)/60)+.01,"HH:MM"] call bis_fnc_timetostring];
if (_timeleft < 1) exitWith{};
  _timeleft = _timeleft -1;
sleep 1;
};
"Bo_GBU12_LGB" createvehicle getpos _object;
{_x setdamage 0} foreach crew _object + [_object];


////////////////////////////// START SCORE //////////////////////////////
	_type = typeOf _object;
	_Objectname = 
	{
		if ( isClass( configFile >> _x >> _type ) ) exitWith 
		{
			getText( configFile >> _x >> _type >> "displayName" )
		};
	}forEach [ "CfgVehicles", "CfgNonAIVehicles" ];

diag_log format["%1 destroyed %2",name _actUsed,_Objectname];
_actUsed sidechat format ["Congratulations, %2 destroyed %1 and got 5 Points",_Objectname, name _actUsed];

[_actUsed, 5] remoteExec ["BIS_fnc_addScore", 2];

////////////////////////////// END SCORE //////////////////////////////

SM_COMPLETE=true;publicvariable "SM_COMPLETE";

