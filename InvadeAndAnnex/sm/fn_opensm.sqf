private ["_object","_object2","_GSG_PosObj","_timeleft","_actUsed","_actID","_spawngroup","_unitsarray","_place"];

_actUsed = _this select 1;  // Unit that used the Action (also _this in the addAction command)
_actID = _this select 2;  // ID of the Action
// if (side _actUsed == West) exitWith {};

_object = _this select 0;
_GSG_PosObj = getpos _object;

[[_object],"GSG_RemoveAction",west,true] spawn BIS_fnc_MP;

sleep 1;
_timeleft = 30 + (floor random 60);

[[hint "Unlocker placed on Objective. Standby..."],"BIS_fnc_spawn",nil,true] spawn BIS_fnc_MP;
sleep 1;
//"Sub_F_Signal_Red" createVehicle getPos _object;
GSG_SM_Open_Reserve = _timeleft + 5;
publicVariableServer "GSG_SM_Open_Reserve";
while {true} do {
hintSilent format ["unlocked in %1", [((_timeleft)/60)+.01,"HH:MM"] call bis_fnc_timetostring];
if (_timeleft < 1) exitWith{};
  _timeleft = _timeleft -1;
sleep 1;
};
GSG_SM_Open=true; publicVariableServer "GSG_SM_Open";

// Wird alles Local beim Spieler ausgelöst und nicht auf dem Server
