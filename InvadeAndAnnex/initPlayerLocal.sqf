waitUntil {!isNull player && isPlayer player};
if (hasInterface) then 
{
//Groups
["InitializePlayer", [player]] call BIS_fnc_dynamicGroups;

// Allow to Jump set the key in the .sqf defualt is f - 2x's
execVM "misc\jump.sqf";

// restrictions
_null = [] execVM "misc\restrictions.sqf";
_null = [] execVM "misc\grenadeStop.sqf";

//Earplugs
execVM "scripts\NRE_earplugs.sqf";

//Minendetektor
null = [] execVM "misc\minedetector.sqf";

//Thermal
[] execVM "gsg\gsg_disable_thermal.sqf";

//Disable bobcat manual Fire
[] execVM "gsg\gsg_disable_manual_fire.sqf";

//holster
execVM "misc\holster.sqf";

//Voicecontrol
execVM "misc\voiceControl.sqf";

//UAV control
_uavcontrolers = ["B_soldier_UAV_F","B_recon_JTAC_F","B_Protagonist_VR_F"];
if (typeOf player in _uavcontrolers) then 
{
	[] execVM "gsg\gsg_uav.sqf";
};

[] spawn (compile preprocessFileLineNumbers "GSG\gsg_check_Lizenz.sqf");

[] spawn compile PreProcessFile "serverRules.sqf";

player addEventHandler ["SeatSwitchedMan",	{[_this select 0,_this select 1,_this select 2] execVM"gsg\vehcheck\gsg_seatswitchPlayer.sqf"}];
player addEventHandler ["GetInMan", 		{[_this select 0,_this select 1,_this select 2] execVM"gsg\vehcheck\gsg_vehcheckPlayer.sqf"}];

player addEventHandler ["Fired", {
	params ["_unit", "_weapon", "", "", "", "", "_projectile"];
	if (_unit distance2D (getMarkerPos "Base_Protect_1") < 200) then {
		deleteVehicle _projectile;
		titleText ["Placing / throwing items and firing at base is STRICTLY PROHIBITED!", "BLACK FADED"];
		titleCut ["Placing / throwing items and firing at base is STRICTLY PROHIBITED!", "BLACK IN", 3];
		hintC "Placing / throwing items and firing at base is STRICTLY PROHIBITED!";
	}}];
player addEventHandler ["Fired", {
	params ["_unit", "_weapon", "", "", "", "", "_projectile"];
	if (_unit distance2D (getMarkerPos "Base_Protect_2") < 250) then {
		deleteVehicle _projectile;
		titleText ["Placing / throwing items and firing at base is STRICTLY PROHIBITED!", "BLACK FADED"];
		titleCut ["Placing / throwing items and firing at base is STRICTLY PROHIBITED!", "BLACK IN", 3];
		hintC "Placing / throwing items and firing at base is STRICTLY PROHIBITED!";
	}}];
player addEventHandler ["Fired", {
	params ["_unit", "_weapon", "", "", "", "", "_projectile"];
	if (_unit distance2D (getMarkerPos "Base_Protect_3") < 150) then {
		deleteVehicle _projectile;
		titleText ["Placing / throwing items and firing at base is STRICTLY PROHIBITED!", "BLACK FADED"];
		titleCut ["Placing / throwing items and firing at base is STRICTLY PROHIBITED!", "BLACK IN", 3];
		hintC "Placing / throwing items and firing at base is STRICTLY PROHIBITED!";
	}}];
};