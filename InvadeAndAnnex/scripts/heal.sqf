private ["_timeleft","_player"];

_player = _this select 0;
sleep 1;
disableUserInput true;
_timeleft = floor((damage _player) * 60);

[[hint "Player is medically treated. Standby..."],"BIS_fnc_spawn",nil,true] spawn BIS_fnc_MP;
sleep 1;
_player switchMove "acts_InjuredCoughRifle02";
sleep 1;
//"Sub_F_Signal_Red" createVehicle getPos _object;
while {true} do {
hintSilent format ["healing in %1", [((_timeleft)/60)+.01,"HH:MM"] call bis_fnc_timetostring];
if (_timeleft < 1) exitWith{};
	_timeleft = _timeleft -1;
	sleep 1;
};
_player switchMove "AidlPercMstpSnonWnonDnon_AI";
disableUserInput false;
_player setDamage 0;

// Wird alles Local beim Spieler ausgelöst und nicht auf dem Server
