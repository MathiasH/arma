private ["_unit","_clanve","_SanHeli","_clantank"];
_unit = _this select 0;
//einheit = _unit;
//einheitenname = vehicleVarName _unit;
if(isNull _unit) exitWith {};

//clearitemcargo _unit; 
clearWeaponCargoGlobal _unit; 

//Datalink
_unit setVehicleReportRemoteTargets true;
_unit setVehicleReceiveRemoteTargets true;
_unit setVehicleReportOwnPosition true;

if (_unit isKindOf "Air") then 
{
	clearMagazineCargoGlobal _unit;
	_unit addEventHandler ["SeatSwitched",	{[_this select 0,_this select 1,_this select 2] execVM"gsg\vehcheck\gsg_seatswitchServer.sqf"}];
	_unit addEventHandler ["GetIn", 		{[_this select 0,_this select 1,_this select 2] execVM"gsg\vehcheck\gsg_vehcheckServer.sqf"}];
};

if (_unit isKindOf "B_APC_Tracked_01_CRV_F") then 
{
	[_unit] execVM "GSG\gsg_bobcat_landscape_repair.sqf";
	_unit animate ["HideTurret",1];
	_unit lockturret [[0],true];
	_unit addEventHandler ["GetIn", 		{[_this select 0,_this select 1,_this select 2] execVM"gsg\vehcheck\gsg_vehcheckServer.sqf"}];
};

if ((_unit isKindOf "B_Plane_CAS_01_F") OR (_unit isKindOf "B_Plane_CAS_01_dynamicLoadout_F")) then 
{ 
	_unit setVehicleVarName "Wipeout"; 
	Wipeout = _unit;
	_unit setVehicleLock "LOCKED";
	if (isServer) then { [[_unit, [localize "STR_GSG_UNLOCK","gsg\helilock\gsg_aufschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;}; 
	if (isServer) then { [[_unit, [localize "STR_GSG_LOCK","gsg\helilock\gsg_abschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;};
	_unit setPylonLoadOut [1, "PylonRack_7Rnd_Rocket_04_AP_F", true, []]; 
	_unit setPylonLoadOut [2, "PylonRack_1Rnd_Missile_AGM_02_F", true, []]; 
	_unit setPylonLoadOut [3, "PylonRack_1Rnd_Missile_AGM_02_F", true, []]; 
	_unit setPylonLoadOut [4, "PylonMissile_1Rnd_Bomb_04_F", true, []]; 
	_unit setPylonLoadOut [5, "PylonMissile_1Rnd_Bomb_04_F", true, []]; 
	_unit setPylonLoadOut [6, "PylonMissile_1Rnd_Bomb_04_F", true, []]; 
	_unit setPylonLoadOut [7, "PylonMissile_1Rnd_Bomb_04_F", true, []]; 
	_unit setPylonLoadOut [8, "PylonRack_1Rnd_Missile_AGM_02_F", true, []]; 
	_unit setPylonLoadOut [9, "PylonRack_1Rnd_Missile_AGM_02_F", true, []]; 
	_unit setPylonLoadOut [10, "PylonRack_7Rnd_Rocket_04_HE_F", true, []]; 
};

if (typeOf _unit == "B_Plane_Fighter_01_F") then 
{ 
	_unit setVehicleVarName "BlackWasp"; 
	BlackWasp = _unit;
	_unit setVehicleLock "LOCKED";
	if (isServer) then { [[_unit, [localize "STR_GSG_UNLOCK","gsg\helilock\gsg_aufschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;}; 
	if (isServer) then { [[_unit, [localize "STR_GSG_LOCK","gsg\helilock\gsg_abschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;};
 	_unit setPylonLoadOut [1, "PylonMissile_Bomb_GBU12_x1", true, []]; 
	_unit setPylonLoadOut [2, "PylonMissile_Bomb_GBU12_x1", true, []]; 
	_unit setPylonLoadOut [3, "PylonRack_Missile_AGM_02_x2", true, []]; 
	_unit setPylonLoadOut [4, "PylonRack_Missile_AGM_02_x2", true, []]; 
 	_unit setPylonLoadOut [5, "", true, []]; //PylonMissile_Missile_BIM9X_x1
	_unit setPylonLoadOut [6, "", true, []]; //PylonMissile_Missile_BIM9X_x1
	_unit setPylonLoadOut [7, "", true, []]; //PylonMissile_Missile_AMRAAM_D_INT_x1
	_unit setPylonLoadOut [8, "", true, []]; //PylonMissile_Missile_AMRAAM_D_INT_x1
	_unit setPylonLoadOut [9, "", true, []]; //PylonMissile_Missile_AMRAAM_D_INT_x1
	_unit setPylonLoadOut [10, "", true, []]; //PylonMissile_Missile_AMRAAM_D_INT_x1
	_unit setPylonLoadOut [11, "PylonMissile_Bomb_GBU12_x1", true, []]; 
	_unit setPylonLoadOut [12, "PylonMissile_Bomb_GBU12_x1", true, []];
};

if (_unit isKindOf "B_Plane_Fighter_01_Stealth_F") then 
{ 
	_unit setVehicleVarName "BlackWaspAA"; 
	BlackWaspAA = _unit;
  	_unit setPylonLoadOut [1, "", true, []]; 
	_unit setPylonLoadOut [2, "", true, []]; 
	_unit setPylonLoadOut [3, "", true, []]; 
	_unit setPylonLoadOut [4, "", true, []];
	_unit setPylonLoadOut [5, "PylonMissile_Missile_BIM9X_x1", true, []]; 
	_unit setPylonLoadOut [6, "PylonMissile_Missile_BIM9X_x1", true, []]; 
	_unit setPylonLoadOut [7, "PylonMissile_Missile_AMRAAM_D_INT_x1", true, []]; 
	_unit setPylonLoadOut [8, "PylonMissile_Missile_AMRAAM_D_INT_x1", true, []]; 
	_unit setPylonLoadOut [9, "PylonMissile_Missile_AMRAAM_D_INT_x1", true, []]; 
	_unit setPylonLoadOut [10, "PylonMissile_Missile_AMRAAM_D_INT_x1", true, []]; 
	_unit setPylonLoadOut [11, "PylonMissile_Missile_AMRAAM_D_INT_x1", true, []]; 
	_unit setPylonLoadOut [12, "PylonMissile_Missile_AMRAAM_D_INT_x1", true, []];
};

if (_unit isKindOf "I_Plane_Fighter_04_F") then 
{ 
	_unit setPylonLoadOut [1, "PylonMissile_Missile_BIM9X_x1", true, []]; 
	_unit setPylonLoadOut [2, "PylonRack_Missile_AMRAAM_C_x1", true, []]; 
	_unit setPylonLoadOut [3, "PylonRack_Missile_AGM_02_x1", true, []]; 
	_unit setPylonLoadOut [4, "PylonRack_Missile_AGM_02_x1", true, []]; 
	_unit setPylonLoadOut [5, "PylonMissile_Bomb_GBU12_x1", true, []]; 
	_unit setPylonLoadOut [6, "PylonMissile_Bomb_GBU12_x1", true, []]; 
	_unit setVehicleLock "LOCKED";
	[_unit,["CamoGrey",1], true] call BIS_fnc_initVehicle; 
	if (isServer) then { [[_unit, [localize "STR_GSG_UNLOCK","gsg\helilock\gsg_aufschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;}; 
	if (isServer) then { [[_unit, [localize "STR_GSG_LOCK","gsg\helilock\gsg_abschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;};
};
if (_unit isKindOf "Plane_Fighter_02_Base_F") then { [_unit,["CamoBlue",1], true] call BIS_fnc_initVehicle; };

if (_unit isKindOf "O_Plane_Fighter_02_F") then 
{ 
 	_unit setPylonLoadOut [1, "", true, []]; 
	_unit setPylonLoadOut [2, "", true, []]; 
	_unit setPylonLoadOut [3, "PylonMissile_Missile_AGM_KH25_x1", true, []]; 
	_unit setPylonLoadOut [4, "PylonMissile_Missile_AGM_KH25_x1", true, []]; 
	_unit setPylonLoadOut [5, "PylonMissile_Bomb_KAB250_x1", true, []]; 
	_unit setPylonLoadOut [6, "PylonMissile_Bomb_KAB250_x1", true, []]; 
 	_unit setPylonLoadOut [7, "", true, []]; 
	_unit setPylonLoadOut [8, "", true, []]; 
	_unit setPylonLoadOut [9, "", true, []]; 
	_unit setPylonLoadOut [10, "", true, []]; 
	_unit setPylonLoadOut [11, "", true, []]; 
	_unit setPylonLoadOut [12, "", true, []]; 
	_unit setPylonLoadOut [13, "PylonMissile_Missile_AGM_KH25_INT_x1", true, []];
	_unit setVehicleLock "LOCKED";
	if (isServer) then { [[_unit, [localize "STR_GSG_UNLOCK","gsg\helilock\gsg_aufschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;}; 
	if (isServer) then { [[_unit, [localize "STR_GSG_LOCK","gsg\helilock\gsg_abschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;};
};

if (_unit isKindOf "O_Plane_Fighter_02_Stealth_F") then 
{ 
	_unit setPylonLoadOut [7, "PylonMissile_Missile_AA_R73_x1", true, []]; 
	_unit setPylonLoadOut [8, "PylonMissile_Missile_AA_R73_x1", true, []]; 
	_unit setPylonLoadOut [9, "PylonMissile_Missile_AA_R77_x1", true, []]; 
	_unit setPylonLoadOut [10, "PylonMissile_Missile_AA_R77_x1", true, []]; 
	_unit setPylonLoadOut [11, "PylonMissile_Missile_AA_R77_INT_x1", true, []]; 
	_unit setPylonLoadOut [12, "PylonMissile_Missile_AA_R77_INT_x1", true, []];
};

if (_unit isKindOf "I_Plane_Fighter_03_dynamicLoadout_F") then 
{ 
	_unit setVehicleVarName "Buzzard_1"; 
	Buzzard_1 = _unit;
 	_unit setPylonLoadOut [1, "PylonRack_1Rnd_Missile_AA_04_F", true, []]; 
	_unit setPylonLoadOut [2, "PylonRack_1Rnd_GAA_missiles", true, []]; 
	_unit setPylonLoadOut [3, "PylonRack_1Rnd_GAA_missiles", true, []]; 
	_unit setPylonLoadOut [4, "PylonWeapon_300Rnd_20mm_shells", true, []]; 
	_unit setPylonLoadOut [5, "PylonRack_1Rnd_GAA_missiles", true, []]; 
	_unit setPylonLoadOut [6, "PylonRack_1Rnd_GAA_missiles", true, []]; 
 	_unit setPylonLoadOut [7, "PylonRack_1Rnd_Missile_AA_04_F", true, []]; 
};

if (_unit isKindOf "B_UAV_05_F") then 
{ 
 	_unit setPylonLoadOut [1, "PylonMissile_Bomb_GBU12_x1", true, [0]]; 
	_unit setPylonLoadOut [2, "PylonMissile_Missile_AGM_02_x2", true, [0]]; 
};

if (_unit isKindOf "B_UAV_02_dynamicLoadout_F") then 
{ 
 	_unit setPylonLoadOut [1, "PylonMissile_Bomb_GBU12_x1", true, [0]]; 
	_unit setPylonLoadOut [2, "PylonMissile_Missile_AGM_02_x2", true, [0]]; 
};

if((_unit isKindOf "B_Heli_Light_01_armed_F") OR (_unit isKindOf "Heli_Light_01_base_F")) then {_unit setObjectTexture [0,"a3\air_f\Heli_Light_01\Data\heli_light_01_ext_ion_co.paa"]};

if(_unit isKindOf "B_Heli_Transport_01_camo_F") then {[_unit] execVM "misc\aw_heliDoor.sqf";};

//Whitelist
if (_unit isKindOf "I_Heli_light_03_F") then 
{ 
	_unit setVehicleVarName "MH_2"; 
	MH_2 = _unit;
	_unit setVehicleLock "LOCKED";
	if (isServer) then { [[_unit, [localize "STR_GSG_UNLOCK","gsg\helilock\gsg_aufschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;}; 
	if (isServer) then { [[_unit, [localize "STR_GSG_LOCK","gsg\helilock\gsg_abschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;};
};

if (_unit isKindOf "B_Heli_Attack_01_F") then 
{ 
	_unit setVehicleVarName "LB_1"; 
	LB_1 = _unit;
	_unit setVehicleLock "LOCKED";
	if (isServer) then { [[_unit, [localize "STR_GSG_UNLOCK","gsg\helilock\gsg_aufschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;}; 
	if (isServer) then { [[_unit, [localize "STR_GSG_LOCK","gsg\helilock\gsg_abschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;};
};

if (_unit isKindOf "B_Heli_light_01_armed_F") then 
{
	if (!alive MH_6) then 
	{
		_unit setVehicleVarName "MH_6"; MH_6 = _unit;
	};
	if (!alive MH_5) then 
	{
		_unit setVehicleVarName "MH_5"; MH_5 = _unit;
	};
	if (!alive MH_4) then 
	{
		_unit setVehicleVarName "MH_4"; MH_4 = _unit;
	};
	if (!alive MH_3) then 
	{
		_unit setVehicleVarName "MH_3"; MH_3 = _unit;
	};
	_unit setVehicleLock "LOCKED";
	if (isServer) then { [[_unit, [localize "STR_GSG_UNLOCK","gsg\helilock\gsg_aufschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;}; 
	if (isServer) then { [[_unit, [localize "STR_GSG_LOCK","gsg\helilock\gsg_abschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;};
};

if (_unit isKindOf "I_MBT_03_cannon_F") then { 
	_unit setVehicleVarName "Kuma_1"; 
	Kuma_1 = _unit; 
	_unit setVehicleLock "LOCKED";
	if (isServer) then { [[_unit, [localize "STR_GSG_UNLOCK","gsg\tanklock\gsg_aufschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;}; 
	if (isServer) then { [[_unit, [localize "STR_GSG_LOCK","gsg\tanklock\gsg_abschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;};
	};

if (_unit isKindOf "B_MBT_01_TUSK_F") then { 
	_unit setVehicleVarName "Slammer_1"; 
	Slammer_1 = _unit; 
	_unit setVehicleLock "LOCKED";
	if (isServer) then { [[_unit, [localize "STR_GSG_UNLOCK","gsg\tanklock\gsg_aufschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;}; 
	if (isServer) then { [[_unit, [localize "STR_GSG_LOCK","gsg\tanklock\gsg_abschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;};
	};

if (_unit isKindOf "B_APC_Tracked_01_AA_F") then { 
	_unit setVehicleVarName "Cheetah_1"; 
	Cheetah_1 = _unit; 
	_unit setVehicleLock "LOCKED";
	if (isServer) then { [[_unit, [localize "STR_GSG_UNLOCK","gsg\tanklock\gsg_aufschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;}; 
	if (isServer) then { [[_unit, [localize "STR_GSG_LOCK","gsg\tanklock\gsg_abschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;};
	};

if (_unit isKindOf "B_APC_Tracked_01_rcws_F") then { 
	_unit setVehicleVarName "Panther_1"; 
	Panther_1 = _unit; 
	_unit setVehicleLock "LOCKED";
	if (isServer) then { [[_unit, [localize "STR_GSG_UNLOCK","gsg\tanklock\gsg_aufschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;}; 
	if (isServer) then { [[_unit, [localize "STR_GSG_LOCK","gsg\tanklock\gsg_abschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;};
	};

if (_unit isKindOf "I_APC_Wheeled_03_cannon_F") then 
{
	_unit setVehicleVarName "Mora_1_1"; 
	Mora_1_1 = _unit; 
	_unit setVehicleLock "LOCKED";
	_unit setObjectTextureGlobal [0, "\A3\armor_f_gamma\APC_Wheeled_03\data\APC_Wheeled_03_Ext_CO.paa"];
	_unit setObjectTextureGlobal [1, "\A3\armor_f_gamma\APC_Wheeled_03\data\APC_Wheeled_03_Ext2_CO.paa"];
	_unit setObjectTextureGlobal [2, "\A3\armor_f_gamma\APC_Wheeled_03\data\RCWS30_CO.paa"];
	_unit setObjectTextureGlobal [3, "\A3\armor_f_gamma\APC_Wheeled_03\data\APC_Wheeled_03_Ext_alpha_CO.paa"];
	if (isServer) then { [[_unit, [localize "STR_GSG_UNLOCK","gsg\tanklock\gsg_aufschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;}; 
	if (isServer) then { [[_unit, [localize "STR_GSG_LOCK","gsg\tanklock\gsg_abschliessen.sqf"]],"addAction",true,true] call BIS_fnc_MP;};
};

if (_unit isKindOf "MRAP_03_base_F") then {[_unit,["Blufor",1], true] call BIS_fnc_initVehicle;};

_clanve=["MH_2","MH_3","LB_1","Wipeout","MH_4","MH_5","MH_6","BlackWasp"];
if (vehicleVarName _unit in _clanve) then {_unit addEventHandler ["getin", {_this execVM "ClanJet.sqf"}]}; // Clanmitglieder only 

if ((_unit iskindof "B_Heli_Light_01_F") OR (_unit iskindof "I_Heli_light_03_unarmed_F") OR (_unit iskindof "I_Heli_Transport_02_F") OR (_unit iskindof "B_Heli_Transport_01_camo_F") OR (_unit iskindof "O_Heli_Transport_04_F") OR (_unit iskindof "B_Heli_Transport_03_unarmed_F") OR (_unit iskindof "I_Heli_light_03_F")) then {_unit addAction["<t color='#09810b'>Put backpack on chest and take parachute</t>","gsg\gsg_backpackonchest.sqf",nil,1.5,true,true,"","(player getVariable ['GSG_BackpackonChest','0'] == '0') && !(vehicle player != player) && !('B_Parachute' == backpack player)",6,false,""]; _unit addEventHandler ["killed", "removeAllActions _this select 0"];};

/*_clantank=["Kuma_1","Slammer_1","Cheetah_1","Panther_1","Mora_1_1"];
if (vehicleVarName _unit in _clantank) then {_unit addEventHandler ["getin", {_this execVM "Clantank.sqf"}]}; // Clanmitglieder only 
*/
if (vehicleVarName _unit in ["Taru_1"]) then { _unit execVM "\a3\Air_F_Heli\Heli_Transport_04\Scripts\Heli_Transport_04_basic_black.sqf";};
if (_unit isKindOf  "Pod_Heli_Transport_04_base_F") then { _unit execVM "\a3\Air_F_Heli\Heli_Transport_04\Scripts\Heli_Transport_04_pods_black.sqf";};
if ((_unit isKindOf "Land_Pod_Heli_Transport_04_bench_F") OR (_unit isKindOf "Land_Pod_Heli_Transport_04_covered_F") OR (_unit isKindOf "Land_Pod_Heli_Transport_04_medevac_F")) then { _unit execVM "\a3\Air_F_Heli\Heli_Transport_04\Scripts\Heli_Transport_04_pods_black.sqf";};
if (vehicleVarName _unit in ["MH_2"]) then { _unit setObjectTexture [0, "\A3\Air_F_EPB\Heli_Light_03\data\Heli_Light_03_base_CO.paa"];};

if (_unit isKindOf "Land_Cargo20_military_green_F") then {null = [_unit] execVM "scripts\arsenalneu.sqf"; _unit allowDamage false; };

if (_unit isKindOf "B_Slingload_01_Ammo_F") then {null = [_unit] execVM "scripts\arsenalneu.sqf";};

if (_unit isKindOf "B_CargoNet_01_ammo_F") then {null = [_unit] execVM "loadout\nachschubkiste.sqf";};

if(_unit isKindOf "B_Heli_Transport_01_camo_F") then {_unit addAction ["<t color='#0000f6'>Ammo Drop</t>", "scripts\aw_drop.sqf",[1],0,false,true,""," driver  _target == _this"];};

//if( _unit isKindOf "Helicopter" ) then {_unit execVM "scripts\a3rc_PilotsPoints.sqf";};

if((_unit isKindOf "Helicopter") AND !((_unit isKindOf "B_Heli_Attack_01_F"))) then {[_unit] execVM "scripts\aw_sling\aw_sling_setupUnit.sqf";};

if((_unit isKindOf "Ship") OR (_unit isKindOf "Wheeled_APC_F")) then {[_unit] execVM "scripts\aw_boatPush\aw_boatPush_setupUnit.sqf";};

if((_unit isKindOf "B_UAV_02_F") OR (_unit isKindOf "B_UAV_02_CAS_F") OR (_unit isKindOf "B_UAV_02_dynamicLoadout_F") OR (_unit isKindOf "B_UGV_01_rcws_F") OR (_unit isKindOf "B_UAV_05_F")) then {_unit setBehaviour "CARELESS"; _unit setAutonomous false; _unit setVariable ["asr_ai_exclude", true];};
if((_unit isKindOf "B_UAV_02_F") OR (_unit isKindOf "B_UAV_02_CAS_F") OR (_unit isKindOf "B_UAV_02_dynamicLoadout_F")) then {_unit setfuel 0;};
//[_unit] execVM "misc\uavnoexplosion.sqf";